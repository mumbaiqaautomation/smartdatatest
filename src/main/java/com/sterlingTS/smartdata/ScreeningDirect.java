package com.sterlingTS.smartdata;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;

import com.sterlingTS.seleniumUI.seleniumCommands;
import com.sterlingTS.utils.commonUtils.APP_LOGGER;
import com.sterlingTS.utils.commonUtils.BaseCommon;
import com.sterlingTS.utils.commonVariables.Enum.State;
import com.sterlingTS.utils.commonVariables.Globals;

public class ScreeningDirect extends BaseCommon{
	
	public static Logger log = Logger.getLogger(ScreeningDirect.class);
	
	public static seleniumCommands sc = new seleniumCommands(driver);
	
	public static String login(String browser,String url,String username,String password) throws Exception{
		APP_LOGGER.startFunction("login");
		String retval=Globals.KEYWORD_FAIL;
		String tempRetval=Globals.KEYWORD_FAIL;
		int timeOutinSeconds =  60;
		try {
			String urlLaunched= sc.launchURL(browser,url);
			if (urlLaunched.equalsIgnoreCase(Globals.KEYWORD_PASS) && sc.waitforElementToDisplay("sdLogin_username_txt",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_PASS)){

				sc.STAF_ReportEvent("Pass", "SE-Login", " Login page has loaded" , 1);

				
				tempRetval=sc.setValue("sdLogin_username_txt", username);
				tempRetval=sc.setValue("sdLogin_password_txt",password);

				log.debug(tempRetval + " Setting value for username and password for SE login");

				timeOutinSeconds =5;
				sc.clickWhenElementIsClickable("sdLogin_login_btn",timeOutinSeconds  );

				timeOutinSeconds =  60;
				if (sc.waitforElementToDisplay("sdHomepage_ssn_txt",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_PASS)){
					log.info("SE User Logged In | Username - "+ username + " | Password - "+password);
					sc.STAF_ReportEvent("Pass", "SE-Login", "User has logged in - " + username , 1);

					retval=Globals.KEYWORD_PASS;					
				}else{
					sc.STAF_ReportEvent("Fail", "SE-Login", "User unable log in - " + username , 1);
					log.error("SE User Unable to Log In | Username - "+ username + " | Password - "+password);
					throw new Exception("SE User Unable to Log In | Username - "+ username);
				}

			}else{
				sc.STAF_ReportEvent("Fail", "SE-Login", " Login page has NOT loaded" , 1);
				log.error("Unable to Log to SE In As Login Page has not Launched");
				throw new Exception("Unable to Log to SE In As Login Page has not Launched");
			}


		} catch (Exception e) {

			log.error("Exception occurred in SE Login | "+e.toString());
			e.printStackTrace();
			throw e;
		}

		return retval;
	}
	
	public static String step1ManualOrder() throws Exception{
		APP_LOGGER.startFunction("step1ManualOrder");
		String retval=Globals.KEYWORD_FAIL;
		

		String billTo="";
		String jobPosition="";
		String packageName="";
		String packageToBeSelected="";
		String billCode="";
		String stepDesc="";
		int timeOutinSeconds=10;
		
		try {
			
			if(sc.waitforElementToDisplay("sdStep1_ScreeningPackage_dd",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_FAIL)){
				stepDesc= "Unable to create manual order as Step1 page has not loaded";
				sc.STAF_ReportEvent("Fail", "SE-Step1", stepDesc , 1);
				log.error(stepDesc);
				throw new Exception(stepDesc);
			
			}
			billTo=Globals.testSuiteXLS.getCellData_fromTestData("BillTo");
			jobPosition=Globals.testSuiteXLS.getCellData_fromTestData("PostionName");
			packageName=Globals.testSuiteXLS.getCellData_fromTestData("PackageName");
			billCode=Globals.testSuiteXLS.getCellData_fromTestData("BillCode");
			
			if(!billTo.isEmpty()){
				sc.selectValue_byVisibleText("sdStep1_billTo_dd", billTo);
				
			}
			
			if(!jobPosition.equalsIgnoreCase("Create")){
				//Fill Screeing Direct Invite Details 
				sc.clickWhenElementIsClickable("sdJobPosition_dd", 10);
				
				sc.clickWhenElementIsClickable("sdStep1_JobPosition_Option", 5);
			}
			
			if(packageName.equalsIgnoreCase("alacarte")){
				packageToBeSelected = "<Order A'la Carte>";
			}else{
				packageToBeSelected = packageName;
			}
			sc.selectValue_byVisibleText("sdStep1_ScreeningPackage_dd", packageToBeSelected);
			
			if(!billCode.isEmpty()){
				sc.selectValue_byVisibleText("sdStep1_billCode_txt", billCode);
				
			}
			
			sc.clickWhenElementIsClickable("sdStep1_next_btn", (int) timeOutinSeconds  );
			timeOutinSeconds =  60;
			
			if (sc.waitforElementToDisplay("sdStep2_lastName_txt",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_PASS)){
				
				sc.STAF_ReportEvent("Pass", "SE-Step 1", "Order creation format selected - "+packageName , 0);

				retval=Globals.KEYWORD_PASS;					
			}else{
				stepDesc= "Unable to create manual order as Step2 page has not loaded";
				sc.STAF_ReportEvent("Fail", "SE-Step2", stepDesc , 1);
				log.error(stepDesc);
				throw new Exception(stepDesc);
			}
			
			
		}catch (Exception e) {
			sc.STAF_ReportEvent("Fail", "SE-Step1", "Exception occurred in order creation- Step 1 | "+e.toString() , 1);
			log.error("Exception occurred in order creation- Step 1 | "+e.toString());
			e.printStackTrace();
			throw e;
		}

		return retval;
		
		
	}
	
	public static String step2ManualOrder() throws Exception{
		APP_LOGGER.startFunction("step2ManualOrder");
		String retval=Globals.KEYWORD_FAIL;
		

		String lastName="";
		String firstName="";
		String middleName="";
		String dob="";
		String stepDesc="";
		String suffix="";
		String gender="";
		String dayPhone="";
		String emailAddress="";
		String driversLicense="";
		String dlState="";
		String dlLname="";
		String dlFname="";
		String dlMidname="";
		String salary="";
		String country="";
		String addressStreet="";
		String addressState="";
		String addressZipCode="";
		String addressYears="";
		String addressCity="";
		
		int timeOutinSeconds=10;
		
		try {
			
			if(sc.waitforElementToDisplay("sdStep2_lastName_txt",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_FAIL)){
				stepDesc= "Unable to create manual order as Step2 page has not loaded";
				sc.STAF_ReportEvent("Fail", "SE-Step2", stepDesc , 1);
				log.error(stepDesc);
				throw new Exception(stepDesc);
			
			}
			
			
			lastName		=Globals.testSuiteXLS.getCellData_fromTestData("LastName");
			firstName		=Globals.testSuiteXLS.getCellData_fromTestData("FirstName");
			middleName		=Globals.testSuiteXLS.getCellData_fromTestData("MidName");
			dob				=Globals.testSuiteXLS.getCellData_fromTestData("DOB");
			suffix			=Globals.testSuiteXLS.getCellData_fromTestData("Suffix");
			gender			=Globals.testSuiteXLS.getCellData_fromTestData("Gender");
			dayPhone		=Globals.testSuiteXLS.getCellData_fromTestData("DayPhone");
			emailAddress	=Globals.fromEmailID;
			driversLicense	=Globals.testSuiteXLS.getCellData_fromTestData("DL");
			dlState			=Globals.testSuiteXLS.getCellData_fromTestData("DLState");
			dlLname			=Globals.testSuiteXLS.getCellData_fromTestData("DLLastName");
			dlFname			=Globals.testSuiteXLS.getCellData_fromTestData("DLFName");
			dlMidname		=Globals.testSuiteXLS.getCellData_fromTestData("DLMidName");
			salary			=Globals.testSuiteXLS.getCellData_fromTestData("SalaryRange");
			country			=Globals.testSuiteXLS.getCellData_fromTestData("Country");
			addressStreet	=Globals.testSuiteXLS.getCellData_fromTestData("AddressLine1");
			addressCity		=Globals.testSuiteXLS.getCellData_fromTestData("City");
			addressState	=Globals.testSuiteXLS.getCellData_fromTestData("State");
			addressZipCode	=Globals.testSuiteXLS.getCellData_fromTestData("ZIP");
			addressYears	=Globals.testSuiteXLS.getCellData_fromTestData("YearsStayed");
			
			sc.setValue("sdStep2_lastName_txt", lastName);
			sc.setValue("sdStep2_firstName_txt", firstName);
			
			sc.addParamVal_InEmail("FirstName", firstName);
			sc.addParamVal_InEmail("LastName", lastName);
			
			
			if(middleName.isEmpty()){
				sc.checkCheckBox("sdStep2_midName_chk");
			}else{
				sc.setValue("sdStep2_midName_txt", middleName);
			}
			
			
			
			if(!suffix.equalsIgnoreCase("") && !suffix.isEmpty()){
				sc.setValue("sdStep2_suffix_txt", suffix);
			}
			if(!gender.equals("") && !gender.isEmpty()){
				if(gender.equalsIgnoreCase("male")){
					sc.clickWhenElementIsClickable("sdStep2_genderMale_rdb", timeOutinSeconds  );
				}else if(gender.equalsIgnoreCase("female")){
					sc.clickWhenElementIsClickable("sdStep2_genderFemale_rdb", timeOutinSeconds  );
				}else{
					sc.clickWhenElementIsClickable("sdStep2_genderUnknown_rdb", timeOutinSeconds  );
				}
			}
			
			sc.setValue("sdStep2_dob_txt", dob);
			
			if(!dayPhone.equalsIgnoreCase("") && !dayPhone.isEmpty()){
				sc.setValue("sdStep2_dayPhone_txt", dayPhone);
			}
			
			if(!dayPhone.equalsIgnoreCase("") && !dayPhone.isEmpty()){
				sc.setValue("sdStep2_dayPhone_txt", dayPhone);
			}
			
			if(!emailAddress.equalsIgnoreCase("") && !emailAddress.isEmpty()){
				sc.setValue("sdStep2_emailAddress_txt", emailAddress);
			}
			
			if(!driversLicense.equalsIgnoreCase("") && !driversLicense.isEmpty()){
				sc.setValue("sdStep2_driversLicense_txt", driversLicense);
			}
			
			if(!dlLname.equalsIgnoreCase("") && !dlLname.isEmpty()){
				sc.setValue("sdStep2_dlLastName_txt", dlLname);
			}
			
			if(!dlFname.equalsIgnoreCase("") && !dlFname.isEmpty()){
				sc.setValue("sdStep2_dlFirstName_txt", dlFname);
			}
			
			if(!dlMidname.equalsIgnoreCase("") && !dlMidname.isEmpty()){
				sc.setValue("sdStep2_dlMidName_txt", dlMidname);
			}
			
			if(!dlState.equalsIgnoreCase("") && !dlState.isEmpty()){
				sc.selectValue_byVisibleText("sdStep2_dlState_dd", dlState);
			}
			
			if(!salary.equals("") && !salary.isEmpty()){
				String value2beSelected="";
			
				if(salary.equalsIgnoreCase("less than 20000")){
					value2beSelected="Less than $20,000";
				}else if(salary.equalsIgnoreCase("between 20000 and 24999")){
					value2beSelected="Between $20,000-$24,999";
				}else if(salary.equalsIgnoreCase("between 25000 and 74999")){
					value2beSelected="Between $25,000-$74,999";
				}else if(salary.equalsIgnoreCase("over 75000")){
					value2beSelected="Over $75,000";
				}else{
					value2beSelected="Unable to provide salary";
				}
				sc.selectValue_byVisibleText("sdStep2_salaray_dd", value2beSelected);
			}
			
			if(!country.equalsIgnoreCase("") && !country.isEmpty()){
				sc.selectValue_byVisibleText("sdStep2_country_dd", country);
			}
			
			sc.setValue("sdStep2_address_txt", addressStreet);
			sc.setValue("sdStep2_city_txt", addressCity);
			String stateCode = State.valueOf(addressState.toUpperCase()).abbreviation.toUpperCase();
			sc.selectValue_byVisibleText("sdStep2_state_dd", stateCode);
			sc.setValue("sdStep2_zipcode_txt", addressZipCode);
			
			if(!addressYears.equalsIgnoreCase("") && !addressYears.isEmpty()){
				sc.selectValue_byVisibleText("sdStep2_years_dd", addressYears);
			}
			
			sc.STAF_ReportEvent("Done", "SE-Step 2", "Data entered in Step 2" , 1);			
			sc.clickWhenElementIsClickable("sdStep2_next_btn", timeOutinSeconds  );
			timeOutinSeconds =  60;
			
			if(middleName.isEmpty()){
				sc.clickWhenElementIsClickable("sdStep2_middleNameYesPopUp_btn", timeOutinSeconds  );
			}
			
			
			if (sc.waitforElementToDisplay("sdStep3_addressHistory_tbl",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_PASS)){
				
				sc.STAF_ReportEvent("Pass", "SE-Step 2", "Data entered in Step 2" , 0);

				retval=Globals.KEYWORD_PASS;					
			}else{
				stepDesc= "Unable to create manual order as Step3 page has not loaded";
				sc.STAF_ReportEvent("Fail", "SE-Step2", stepDesc , 1);
				log.error(stepDesc);
				throw new Exception(stepDesc);
			}
			
			
		}catch (Exception e) {
			sc.STAF_ReportEvent("Fail", "SE-Step2", "Exception occurred in order creation- Step 2 | "+e.toString() , 1);
			log.error("Exception occurred in order creation- Step 2 | "+e.toString());
			e.printStackTrace();
			throw e;
		}

		return retval;

	
	}
	
	public static String step3ManualOrder() throws Exception{
		APP_LOGGER.startFunction("step3ManualOrder");
		String retval=Globals.KEYWORD_FAIL;
		String stepDesc="";
		
		int timeOutinSeconds=10;
		
		try {
			
			if(sc.waitforElementToDisplay("sdStep3_addressHistory_tbl",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_FAIL)){
				stepDesc= "Unable to create manual order as Step3 page has not loaded";
				sc.STAF_ReportEvent("Fail", "SE-Step3", stepDesc , 1);
				log.error(stepDesc);
				throw new Exception(stepDesc);
			
			}
			
			
			sc.clickWhenElementIsClickable("sdStep3_draftOrder_btn",timeOutinSeconds  );
			timeOutinSeconds =  60;
			
			if (sc.waitforElementToDisplay("sdOrderEditor_orderInfo_tbl",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_PASS)){
				
				sc.STAF_ReportEvent("Pass", "SE-Step 3", "Step 3 data entered" , 1);

				retval=Globals.KEYWORD_PASS;					
			}else{
				stepDesc= "Unable to create manual order as Draft Order Editor page has not loaded";
				sc.STAF_ReportEvent("Fail", "SE-Draft Order", stepDesc , 1);
				log.error(stepDesc);
				throw new Exception(stepDesc);
			}
			
			
		}catch (Exception e) {
			sc.STAF_ReportEvent("Fail", "SE-Step3", "Exception occurred in order creation- Step 3 | "+e.toString() , 0);
			log.error("Exception occurred in order creation- Step 3 | "+e.toString());
			e.printStackTrace();
			throw e;
		}

		return retval;
		
		
	}
	
	
	public static String draftOrderEditor() throws Exception{
		APP_LOGGER.startFunction("draftOrderEditor");
		String retval=Globals.KEYWORD_FAIL;
		String stepDesc="";
		String orderID="";
		String orderedDate="";
		String packageName="";
		String alacarteProductsList="";
		String actionType="";
		int timeOutinSeconds=10;
		
		try {
			
			if(sc.waitforElementToDisplay("sdOrderEditor_orderInfo_tbl",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_FAIL)){
				stepDesc= "Unable to create manual order as draftOrderEditor page has not loaded";
				sc.STAF_ReportEvent("Fail", "SE-draftOrderEditor", stepDesc , 1);
				log.error(stepDesc);
				throw new Exception(stepDesc);
			
			}
			
			WebElement orderDetailsTbl = sc.createWebElement("sdOrderEditor_orderInfo_tbl");
			orderID=sc.getCellData_tbl(orderDetailsTbl, 2, 2);
			orderedDate = sc.getTodaysDate();
			
			sc.addParamVal_InEmail("OrderID", orderID);
			sc.addParamVal_InEmail("OrderedDate", orderedDate);
			Globals.testSuiteXLS.setCellData_inTestData("SWestOrderID", orderID);
			Globals.testSuiteXLS.setCellData_inTestData("OrderedDate", orderedDate);
			
			
			
			packageName =  Globals.testSuiteXLS.getCellData_fromTestData("PackageName");
			actionType  =  Globals.testSuiteXLS.getCellData_fromTestData("ActionToPerform");
			if(packageName.equalsIgnoreCase("alacarte")){
			
				alacarteProductsList=  Globals.testSuiteXLS.getCellData_fromTestData("AlacarteProducts");
				
				String[] alacarteProducts  = alacarteProductsList.split(";");
				String productName="";
				
				String tempretval = Globals.KEYWORD_FAIL;
				
				for (int i = 0; i < alacarteProducts.length; i++) {
					if(alacarteProducts[i].equalsIgnoreCase("CRFM")){
						productName = "Criminal - County";
					}else if(alacarteProducts[i].equalsIgnoreCase("CRST")){
						productName = "Criminal - State";
					}
					
					sc.selectValue_byVisibleText("sdOrderEditor_alacarte_dd", productName);
					if (sc.waitforElementToDisplay("sdProduct_lastName_txt",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_FAIL)){
						stepDesc= "Unable to add "+alacarteProducts[i] + " through alacarte as page has not loaded";
						sc.STAF_ReportEvent("Fail", "SE-Draft Order", stepDesc , 1);
						log.error(stepDesc);
						throw new Exception(stepDesc);
					}
					
					tempretval = populateDataForProduct(alacarteProducts[i]);
					if(tempretval.equalsIgnoreCase(Globals.KEYWORD_FAIL)){
						stepDesc= "Unable to alcarte product -"+alacarteProducts[i];
						sc.STAF_ReportEvent("Fail", "SE-OrderEditor", stepDesc , 1);
						log.error(stepDesc);
						throw new Exception(stepDesc);
					}
					
								
				}
				
			
				
			}else if(actionType.equalsIgnoreCase("Edit")){
				
				alacarteProductsList=  Globals.testSuiteXLS.getCellData_fromTestData("AlacarteProducts");
				
				String[] alacarteProducts  = alacarteProductsList.split(";");
				String productName="";
				
				String tempretval = Globals.KEYWORD_FAIL;
				
				if(sc.waitforElementToDisplay("sdOrderEditor_draftOrder_tbl",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_PASS))
				{
					sc.clickWhenElementIsClickable("sdOrder_EditLink", timeOutinSeconds);
				}
				
				for (int i = 0; i < alacarteProducts.length; i++) {
					
					if (sc.waitforElementToDisplay("sdProduct_lastName_txt",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_FAIL)){
						stepDesc= "Unable to add "+alacarteProducts[i] + " through alacarte as page has not loaded";
						sc.STAF_ReportEvent("Fail", "SE-Draft Order", stepDesc , 1);
						log.error(stepDesc);
						throw new Exception(stepDesc);
					}
					
					tempretval = populateDataForProduct(alacarteProducts[i]);
					if(tempretval.equalsIgnoreCase(Globals.KEYWORD_FAIL)){
						stepDesc= "Unable to alcarte product -"+alacarteProducts[i];
						sc.STAF_ReportEvent("Fail", "SE-OrderEditor", stepDesc , 1);
						log.error(stepDesc);
						throw new Exception(stepDesc);
					}
					
								
				}
			}else if(actionType.equalsIgnoreCase("Delete"))
			{
					alacarteProductsList=  Globals.testSuiteXLS.getCellData_fromTestData("AlacarteProducts");
					
					String[] alacarteProducts  = alacarteProductsList.split(";");
					String productName="";
					
					String tempretval = Globals.KEYWORD_FAIL;
					
					if(sc.waitforElementToDisplay("sdOrderEditor_draftOrder_tbl",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_PASS))
					{
						sc.clickWhenElementIsClickable("sdOrder_DeleteLink", timeOutinSeconds);
					}
					
					for (int i = 0; i < alacarteProducts.length; i++) {
						
						if(alacarteProducts[i].equalsIgnoreCase("CRFM")){
							productName = "Criminal - County";
						}else if(alacarteProducts[i].equalsIgnoreCase("CRST")){
							productName = "Criminal - State";
						}
						
						sc.selectValue_byVisibleText("sdOrderEditor_alacarte_dd", productName);
						if (sc.waitforElementToDisplay("sdProduct_lastName_txt",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_FAIL)){
							stepDesc= "Unable to add "+alacarteProducts[i] + " through alacarte as page has not loaded";
							sc.STAF_ReportEvent("Fail", "SE-Draft Order", stepDesc , 1);
							log.error(stepDesc);
							throw new Exception(stepDesc);
						}
						
						tempretval = populateDataForProduct(alacarteProducts[i]);
						if(tempretval.equalsIgnoreCase(Globals.KEYWORD_FAIL)){
							stepDesc= "Unable to alcarte product -"+alacarteProducts[i];
							sc.STAF_ReportEvent("Fail", "SE-OrderEditor", stepDesc , 1);
							log.error(stepDesc);
							throw new Exception(stepDesc);
						}
						
									
					}
			}
			
			
			sc.clickWhenElementIsClickable("sdOrderEditor_submit_btn",timeOutinSeconds  );
			timeOutinSeconds =  60;
			
			if (sc.waitforElementToDisplay("sdOrderEditor_orderInfo_tbl",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_PASS)){
				
				sc.STAF_ReportEvent("Pass", "SE-OrderEditor", "Order has been succesfully submitted-"+orderID , 1);

				retval=Globals.KEYWORD_PASS;					
			}else{
				stepDesc= "Unable to create manual order as  products could not be added.Order id-"+orderID;
				sc.STAF_ReportEvent("Fail", "SE-OrderEditor", stepDesc , 1);
				log.error(stepDesc);
				throw new Exception(stepDesc);
			}
			
			
		}catch (Exception e) {
			sc.STAF_ReportEvent("Fail", "SE-OrderEditor", "Exception occurred in order creation- OrderEditor | "+e.toString() , 0);
			log.error("Exception occurred in order creation- OrderEditor | "+e.toString());
			e.printStackTrace();
			throw e;
		}

		return retval;
		
		
	}
	
	public static String populateDataForProduct(String productCode) throws Exception{
		APP_LOGGER.startFunction("draftOrderEditor");
		String retval=Globals.KEYWORD_FAIL;
		String stepDesc="";
		int timeOutinSeconds = 10;
		try{
			
			
			
			switch (productCode) {
			case "CRFM":
				String jurisdiction =  Globals.testSuiteXLS.getCellData_fromTestData("JurisState");
				String jurisCriteria = "<counties>";
				String jurisValue = Globals.testSuiteXLS.getCellData_fromTestData("JurisValue");
				
				String stateName = State.valueOf(jurisdiction.toUpperCase()).name;
				
				sc.selectValue_byVisibleText("sdProduct_jurisState_dd", stateName);
				Thread.sleep(1000);
				sc.selectValue_byVisibleText("sdProduct_jurisCriteria_dd", jurisCriteria);
				Thread.sleep(1000);
				sc.selectValue_byVisibleText("sdProduct_jurisValue_dd", jurisValue);
				
				
				break;
				
			case "CRST":
				String jurisState =  Globals.testSuiteXLS.getCellData_fromTestData("JurisState");
				String stateCode = State.valueOf(jurisState.toUpperCase()).abbreviation.toUpperCase();
				sc.selectValue_byVisibleText("sdProduct_Juris_dd", stateCode);
				
				break;
				
			default:
				break;
			}
			
			sc.clickWhenElementIsClickable("sdProduct_save_btn",timeOutinSeconds  );
			timeOutinSeconds =  60;
			
			if (sc.waitforElementToDisplay("sdOrderEditor_orderInfo_tbl",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_PASS)){
				sc.scrollIntoView("sdOrderEditor_orderInfo_tbl");
				sc.STAF_ReportEvent("Pass", "Alacarte Product", productCode + " added successfully" , 1);

				retval=Globals.KEYWORD_PASS;					
			}else{
				stepDesc= "Unable to add alacarte product "+productCode;
				sc.STAF_ReportEvent("Fail", "Alacarte Product", stepDesc , 1);
				log.error(stepDesc);
				throw new Exception(stepDesc);
			}
			
			
		}catch (Exception e) {

			log.error("Exception occurred in order creation- ala carte product being added | "+e.toString());
			e.printStackTrace();
			throw e;
		}

		return retval;
		
	}
	
	public static String logout() throws Exception {
		APP_LOGGER.startFunction("logout");
		String tempRetval=Globals.KEYWORD_FAIL;
		long timeOutInSeconds = 60;
		String retval=Globals.KEYWORD_FAIL;

		try{
			Globals.driver.switchTo().defaultContent();// this is done as in some cases we are switching to frames for report validations.

			tempRetval = sc.waitforElementToDisplay("sdHomepage_logout_link", timeOutInSeconds);
			if(tempRetval.equalsIgnoreCase(Globals.KEYWORD_PASS)){


				sc.clickWhenElementIsClickable("sdHomepage_logout_link",(int) timeOutInSeconds  );

				tempRetval = Globals.KEYWORD_FAIL;
				tempRetval = sc.waitforElementToDisplay("sdLogin_username_txt", timeOutInSeconds);
				if(tempRetval.equalsIgnoreCase(Globals.KEYWORD_PASS)){

					sc.STAF_ReportEvent("Pass", "SD-Logout", "User log off successful", 1);
					
					retval=Globals.KEYWORD_PASS;

				}else{
					sc.STAF_ReportEvent("Fail", "SD-Logout", "Unable to log off from SD", 1);

				}
			}
		}catch(Exception e){
			retval=Globals.KEYWORD_FAIL;
			log.error("Method-logout | Exception - "+ e.toString());
			e.printStackTrace();
			throw e;
		}

		return retval;

	}
	
	public static String createManualOrder() throws Exception{
		Globals.testSuiteXLS.setCellData_inTestData("SWestOrderID", "");
		
		String ssn=Globals.testSuiteXLS.getCellData_fromTestData("SSN");
		sc.setValue("sdHomepage_ssn_txt", ssn);
		sc.clickWhenElementIsClickable("sdHomepage_go_btn",10  );
		
		step1ManualOrder();
		step2ManualOrder();
		step3ManualOrder();
		return ScreeningDirect.draftOrderEditor();
	}

}

package com.sterlingTS.smartdata;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import com.sterlingTS.seleniumUI.seleniumCommands;
import com.sterlingTS.utils.commonUtils.APP_LOGGER;
import com.sterlingTS.utils.commonUtils.BaseCommon;
import com.sterlingTS.utils.commonUtils.ReportUtil;
import com.sterlingTS.utils.commonVariables.Globals;

public class SmartDataUI extends BaseCommon{
	
	public static Logger log = Logger.getLogger(Argentum.class);
	public static seleniumCommands sc = new seleniumCommands(driver);
	
	public static String LoginWithChecker() throws Exception
	{
		APP_LOGGER.startFunction("login With Checker in SmardData UI");
		String retval=Globals.KEYWORD_FAIL;
		String tempRetval=Globals.KEYWORD_FAIL;
		int timeOutinSeconds =  60;
		
		try{
			
			String urlLaunched = null;
			String username = null;
			String password = null;
			
			urlLaunched= sc.launchURL(Globals.BrowserType,Globals.getEnvPropertyValue("SmartDataUI_URL"));
			
			username = Globals.getEnvPropertyValue("Checker_ID");
			password = Globals.getEnvPropertyValue("Checker_pwd");
			
			if(sc.waitTillElementDisplayed("smartData_username_id", timeOutinSeconds) == Globals.KEYWORD_PASS)
			{
				sc.setValue("smartData_username_id", username);
				sc.setValue("smartData_Password_id", password);
				
				sc.clickWhenElementIsClickable("smartData_Login_Btn_id", timeOutinSeconds);
			}
			
			
//		}else{
//			urlLaunched= sc.launchURL(Globals.BrowserType,Globals.getEnvPropertyValue("AG_SuperUserURL"));
//		}
		
		if (urlLaunched.equalsIgnoreCase(Globals.KEYWORD_PASS) && sc.waitforElementToDisplay("smartData_UserName_path",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_PASS)){
			timeOutinSeconds =  60;
			if (sc.waitforElementToDisplay("smartData_UserName_path",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_PASS)){
				log.info("Smart Data Checker Logged In | Username - "+ username);
				ReportUtil.STAF_ReportEvent("Pass", "Smart Data Checker User", "User has logged in - " + username , 1);

				retval=Globals.KEYWORD_PASS;					
			}else{
				ReportUtil.STAF_ReportEvent("Fail", "Smart Data Checker", "User unable log in - " + username , 1);
				log.error("AG Super User Unable to Log In | Username - "+ username);
				throw new Exception("Smart Data Checker Unable to Log In | Username - "+ username);
			}

		}else{
			ReportUtil.STAF_ReportEvent("Fail", "Smart Data Checker User", " Login page has NOT loaded" , 1);
			log.error("Unable to Log to Smart Data Checker In As Login Page has not Launched");
			throw new Exception("Unable to Log to Smart Data Checker In As Login Page has not Launched");
		}

		} catch (Exception e) {

			log.error("Exception occurred in Smart Data Checker User | "+e.toString());
			e.printStackTrace();
			throw e;
		}
		
		return retval;
	}
	
	public static String LoginWithMaker() throws Exception
	{
		APP_LOGGER.startFunction("login With Checker in SmardData UI");
		String retval=Globals.KEYWORD_FAIL;
		String tempRetval=Globals.KEYWORD_FAIL;
		int timeOutinSeconds =  60;
		
		try{
			
			String urlLaunched = null;
			String username = null;
			String password = null;
			
			urlLaunched= sc.launchURL(Globals.BrowserType,Globals.getEnvPropertyValue("SmartDataUI_URL"));
			
			username = Globals.getEnvPropertyValue("Maker_ID");
			password = Globals.getEnvPropertyValue("Maker_pwd");
			
			if(sc.waitTillElementDisplayed("smartData_username_id", timeOutinSeconds) == Globals.KEYWORD_PASS)
			{
				sc.setValue("smartData_username_id", username);
				sc.setValue("smartData_Password_id", password);
				
				sc.clickWhenElementIsClickable("smartData_Login_Btn_id", timeOutinSeconds);
			}
			
			
//		}else{
//			urlLaunched= sc.launchURL(Globals.BrowserType,Globals.getEnvPropertyValue("AG_SuperUserURL"));
//		}
		
		if (urlLaunched.equalsIgnoreCase(Globals.KEYWORD_PASS) && sc.waitforElementToDisplay("smartData_UserName_path",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_PASS)){
			timeOutinSeconds =  60;
			if (sc.waitforElementToDisplay("smartData_UserName_path",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_PASS)){
				log.info("Smart Data Checker Logged In | Username - "+ username);
				ReportUtil.STAF_ReportEvent("Pass", "Smart Data Checker User", "User has logged in - " + username , 1);

				retval=Globals.KEYWORD_PASS;					
			}else{
				ReportUtil.STAF_ReportEvent("Fail", "Smart Data Checker", "User unable log in - " + username , 1);
				log.error("AG Super User Unable to Log In | Username - "+ username);
				throw new Exception("Smart Data Checker Unable to Log In | Username - "+ username);
			}

		}else{
			ReportUtil.STAF_ReportEvent("Fail", "Smart Data Checker User", " Login page has NOT loaded" , 1);
			log.error("Unable to Log to Smart Data Checker In As Login Page has not Launched");
			throw new Exception("Unable to Log to Smart Data Checker In As Login Page has not Launched");
		}

		} catch (Exception e) {

			log.error("Exception occurred in Smart Data Checker User | "+e.toString());
			e.printStackTrace();
			throw e;
		}
		
		return retval;
	}
	
	public static String ruleSetCreation() throws Exception
	{
		APP_LOGGER.startFunction("Create RuleSet SmardData UI");
		String retval=Globals.KEYWORD_FAIL;
		String tempRetval=Globals.KEYWORD_FAIL;
		int timeOutinSeconds =  60;
		String RulesetName = "qatest"+RandomStringUtils.randomNumeric(6);
		
		try{
			sc.clickWhenElementIsClickable("smartData_Rulset_path", timeOutinSeconds);
			sc.clickWhenElementIsClickable("smartData_RuleSearch_link", timeOutinSeconds);
			
			if(sc.waitTillElementDisplayed("smartData_AddNew_btn", timeOutinSeconds) == Globals.KEYWORD_PASS)
			{
				sc.clickWhenElementIsClickable("smartData_AddNew_btn", timeOutinSeconds);
			}
			
			if(sc.waitTillElementDisplayed("smartData_ruleSet_Name", timeOutinSeconds) == Globals.KEYWORD_PASS)
			{
				sc.setValue("smartData_ruleSet_Name",RulesetName);
				sc.setValue("smartData_ruleSet_Desc", "testing");
				sc.clickWhenElementIsClickable("smartData_ruleSet_Save_btn", timeOutinSeconds);
				Thread.sleep(5000);
				driver.switchTo().alert().accept();
				if(sc.waitTillElementDisplayed("smartData_ruleSet_AgeOfRecordChargeExclusion_path", timeOutinSeconds) == Globals.KEYWORD_PASS)
				{
					ReportUtil.STAF_ReportEvent("Pass", "rule Set name Save", "rule Set name Save successfully", 1);
				}else{
					ReportUtil.STAF_ReportEvent("Fail", "rule Set name Save", "Fail To save rule set", 1);
				}
			}
			
			if(sc.waitTillElementDisplayed("smartData_ruleSet_AgeOfRecordChargeExclusion_path", timeOutinSeconds) == Globals.KEYWORD_PASS)
			{
				sc.clickWhenElementIsClickable("smartData_ruleSet_AgeOfRecordChargeExclusion_path", timeOutinSeconds);
				sc.clickWhenElementIsClickable("smartData_ruleSet_Save_btn", timeOutinSeconds);
				
				
				driver.switchTo().alert().accept();
				Thread.sleep(10000);
				
				if(sc.waitTillElementDisplayed("smartData_ruleSet_table_notConfigured_path", timeOutinSeconds) == Globals.KEYWORD_PASS)
				{
					ReportUtil.STAF_ReportEvent("Pass", "rule Select", "rule selected successfully", 1);
				}else{
					ReportUtil.STAF_ReportEvent("Pass", "rule Select", "rule selected successfully", 1);
				}
				
				sc.clickWhenElementIsClickable("smartData_ruleSet_table_notConfigured_path", timeOutinSeconds);
				Thread.sleep(10000);
				if(sc.waitTillElementDisplayed("smartData_ruleSet_AgeOfRecord_txt", timeOutinSeconds) == Globals.KEYWORD_PASS)
				{
					sc.setValue("smartData_ruleSet_AgeOfRecord_txt", "7");
				}
				sc.clickWhenElementIsClickable("smartData_ruleSet_AgeOfRecord_txt", timeOutinSeconds);
				sc.setValue("smartData_ruleSet_AgeOfRecord_txt", "7");
				driver.findElement(By.xpath(".//*[contains(@id,'txtAgeOfRecord')]")).sendKeys("7");
				sc.clickWhenElementIsClickable("smartData_clientRule_save", timeOutinSeconds);
				Thread.sleep(5000);
				
				if(sc.waitTillElementDisplayed("smartData_clientRule_back", timeOutinSeconds) == Globals.KEYWORD_PASS)
				{
					ReportUtil.STAF_ReportEvent("Pass", "rule configuration", "rule data configured successfully", 1);
				}else{
					ReportUtil.STAF_ReportEvent("Pass", "rule configuration", "rule data configured successfully", 1);
				}
				sc.clickWhenElementIsClickable("smartData_clientRule_back", timeOutinSeconds);
				retval= Globals.KEYWORD_PASS;
			}
		} catch (Exception e) {

			log.error("Exception occurred in RuleSet Creation | "+e.toString());
			e.printStackTrace();
			throw e;
		}
		
		return retval;
	}
	
	public static String logout() throws Exception
	{
		APP_LOGGER.startFunction("Logout SmardData UI");
		String retval=Globals.KEYWORD_FAIL;
		String tempRetval=Globals.KEYWORD_FAIL;
		int timeOutinSeconds =  60;
		
		try{
			sc.clickWhenElementIsClickable("smartData_dropDown_path", timeOutinSeconds);
			sc.clickWhenElementIsClickable(driver.findElement(By.xpath(".//*[contains(@id,'popover')]/div[2]/div[1]/div[1]/div[2]/div[1]")), timeOutinSeconds);
			//sc.clickElementUsingAction(driver.findElement(By.xpath(".//*[@id='popover288877']/div[2]/div[1]/div[1]/div[2]/div[1]")));
			Thread.sleep(20000);
			//driver.findElement(By.xpath(".//*[@class=\"notification-messages info\"]//*[@class=\"message-wrapper\" and @xpath='1']")).click();
			//driver.findElement(By.xpath(".//*[@id='popover288877']/div[2]/div[1]/div[1]/div[2]/div[1]")).click();
			if(sc.waitTillElementDisplayed("smartData_username_id", timeOutinSeconds) == Globals.KEYWORD_PASS)
			{
				retval=Globals.KEYWORD_PASS;
			}else{
				retval=Globals.KEYWORD_FAIL;
			}
			
		} catch (Exception e) {

			log.error("Exception occurred in Logout | "+e.toString());
			e.printStackTrace();
			throw e;
		}
		
		return retval;
	}

}

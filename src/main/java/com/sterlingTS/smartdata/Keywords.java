package com.sterlingTS.smartdata;

import com.sterlingTS.seleniumUI.seleniumCommands;
import com.sterlingTS.utils.commonUtils.BaseCommon;
import com.sterlingTS.utils.commonVariables.Globals;



public class Keywords extends BaseCommon{
	
	public static seleniumCommands sc = new seleniumCommands(driver);
	
	public String SMARTDATA_OrderPlace() throws Exception {
		String retval = Globals.KEYWORD_FAIL;
		retval = Argentum.placeOrder("AG");
		if(retval.equalsIgnoreCase(Globals.KEYWORD_FAIL)){
			Globals.testSuiteXLS.setCellData_inTestData("CurrrentOrderCreationStatus","Fail");
		}else{
			Globals.testSuiteXLS.setCellData_inTestData("CurrrentOrderCreationStatus","PASS");
			
		}
		return retval;
    }
	
	public static String SDLogin() throws Exception{
		
//		String username=Globals.testSuiteXLS.getCellData_fromTestData("SE_Username");
//		String password=Globals.testSuiteXLS.getCellData_fromTestData("SE_UserPWD");
		String username=Globals.getEnvPropertyValue("ScreenDirect_Username");
		String password=Globals.getEnvPropertyValue("ScreenDirect_Password");
		return ScreeningDirect.login(Globals.BrowserType,Globals.getEnvPropertyValue("ScreeningEditionURL"),username,password);
	}
	
	public static String SDLogout() throws Exception{

		return ScreeningDirect.logout();
	}
	
	public String CreateSDOrder() throws Exception{
		
		Globals.testSuiteXLS.setCellData_inTestData("OrderedDate", "");
		Globals.testSuiteXLS.setCellData_inTestData("CurrrentOrderCreationStatus", "FAIL");
		String retval = Globals.KEYWORD_FAIL;
		retval = ScreeningDirect.createManualOrder();
		if(retval.equalsIgnoreCase(Globals.KEYWORD_FAIL)){
			Globals.testSuiteXLS.setCellData_inTestData("CurrrentOrderCreationStatus","Fail");
		}else{
			Globals.testSuiteXLS.setCellData_inTestData("CurrrentOrderCreationStatus","PASS");
			
		}
		
		String orderTxtFile = "";
		
		orderTxtFile = Globals.TestDir + "\\src\\test\\Resources\\refFile\\generatedOrderList.txt";
		String textToBeAppended = Globals.TestCaseID +"___" 
		        + Globals.tcDescription+"___"
		        + Globals.testSuiteXLS.getCellData_fromTestData("SWestOrderID")+"___"
		        + Globals.testSuiteXLS.getCellData_fromTestData("OrderedDate");
		
		sc.appendToTextFile(orderTxtFile, textToBeAppended);
		
		return retval;
	}
	
	public static String AGLoginWithSuperUser() throws Exception
	{
		return Argentum.loginWithSuperUser();
	}
	
	public static String AGLoginWithBasicUser() throws Exception
	{
		return Argentum.loginWithBasicUser();
	}
	
	public static String AGLogout() throws Exception
	{
		return Argentum.logout();
	}
	
	public static String AGAssignTask() throws Exception
	{
		return Argentum.assignTask();
	}
	
	public static String AGEnterResult() throws Exception
	{
		return Argentum.EnterResult();
	}
	
	public String VerifyStateRule() throws Exception{
		return Argentum.verifyStateRule();
	}
	
	public String ChargeDispositionSanitization()
	{
		if(!Globals.EXECUTION_MACHINE.equalsIgnoreCase("jenkins"))
		{
			return Argentum.chargeDispositionSanitization();
		}else{
			return Globals.KEYWORD_PASS;
		}
	}
	
	public String PersonaIdentifierCheck()
	{
		if(!Globals.EXECUTION_MACHINE.equalsIgnoreCase("jenkins"))
		{
			return Argentum.piiCheck();
		}else{
			return Globals.KEYWORD_PASS;
		}
	}
	
	public String ChargeDispositionSanitizationWithDB()
	{
		return Argentum.chargeDispositionDBVerification();
	}
	
	public String AgeOfRecordVerification()
	{
		return Argentum.ageOfRecordVerification();
	}
	
	public String ProbationStatusVerification()
	{
		return Argentum.checkProbationStatus();
	}
	
	public String WarrantStatusVerification()
	{
		return Argentum.checkWarrantStatus();
	}
	
	public String SmartDataUILoginwithChecker() throws Exception
	{
		return SmartDataUI.LoginWithChecker();
	}
	
	public String SmartDataUILoginwithMaker() throws Exception
	{
		return SmartDataUI.LoginWithMaker();
	}
	
	public String RuleSetCreationWithChecker() throws Exception
	{
		return SmartDataUI.ruleSetCreation();
	}
	
	public String SmartDataUILogout() throws Exception
	{
		return SmartDataUI.logout();
	}

}

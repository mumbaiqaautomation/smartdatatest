package com.sterlingTS.smartdata;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.w3c.dom.Document;

import com.sterlingTS.seleniumUI.seleniumCommands;
import com.sterlingTS.smartdataComm.DeserializeJsonData;
import com.sterlingTS.smartdataComm.smartDataDAO;
import com.sterlingTS.utils.commonUtils.APP_LOGGER;
import com.sterlingTS.utils.commonUtils.BaseCommon;
import com.sterlingTS.utils.commonUtils.CommonHelpMethods;
import com.sterlingTS.utils.commonUtils.LocatorAccess;
import com.sterlingTS.utils.commonUtils.ReportUtil;
import com.sterlingTS.utils.commonUtils.XMLUtils;
import com.sterlingTS.utils.commonVariables.Globals;

public class Argentum extends BaseCommon{
	public static Logger log = Logger.getLogger(Argentum.class);
	public static seleniumCommands sc = new seleniumCommands(driver);
	
	public static String placeOrder(String OrderPlacedThrough) throws Exception
	{
		APP_LOGGER.startFunction("OrderCreation");
		Globals.testSuiteXLS.setCellData_inTestData("SWestOrderID", "");

        //Create XML_Results Folder if not present 
        Globals.currentPlatform_XMLResults = Globals.currentPlatform_Path+"\\"+"XML_Results";
        CommonHelpMethods.mkDirs(Globals.currentPlatform_XMLResults);
        
        //Initialize variables
        String InputReferenceXML    = "";
        String vendor               = "";                       
        String region               = "";
        String county               = "";
        String lastName             = "";
        String firstName            = "";
        String middleName           = "";
        String dob                  = "";
        String courtName            = "";
        String ssn                  = "";
        String randomNumber         = "";
        String refID                = "";
        String screeningType        = "";
        String soapServer           = "";
        XMLUtils  orderRequestXML   = null; 
        
        try{
        
        InputReferenceXML    	= Globals.TestDir +File.separator+"src"+File.separator+"test"+File.separator+"Resources"+File.separator+"refFile"+File.separator+"XML_SD"+File.separator+"OrderPlace.xml";
        orderRequestXML       = new XMLUtils(InputReferenceXML);
        
        String orderId = "SMD"+RandomStringUtils.randomNumeric(6);
        
        String dataPayload = orderRequestXML.getXMLNodeValByTagName("web:screeningOrder");
        
        String payload = makeRequestForCrim(dataPayload,orderId);
        
        orderRequestXML.updatedXMLNodeValueByTagName("web:screeningOrder",payload);
        
        orderRequestXML.updatedXMLNodeValueByTagName("web:agent", OrderPlacedThrough);
        
        String updatedXMlFileName = orderId+"_Request.xml";
        orderRequestXML.saveXMLDOM2File(updatedXMlFileName);
        String updatedXMLFilePath = Globals.currentPlatform_XMLResults +File.separator+ updatedXMlFileName;
        
        soapServer = Globals.getEnvPropertyValue("SMD_SoupURL");
        
        //Post Updated XML 
        String response ="";
        
        XMLUtils updatedXMLFile   = null;
        updatedXMLFile         = new XMLUtils(updatedXMLFilePath, soapServer);
        
        Thread.sleep(5000);                 
        
        response = updatedXMLFile.postXML();
        
        Globals.Result_Link_FilePath = updatedXMLFilePath;     //For Linking in Results 
        ReportUtil.STAF_ReportEvent("Pass", "Post Order Request", "Modified Input XML is at XML_results folder. FileName-"+updatedXMlFileName, 3);
        
        String responseFileName      = orderId+"_Response.xml";
        String responseFilePath      = Globals.currentPlatform_XMLResults +File.separator+ responseFileName;
        updatedXMLFile.saveXMLString(responseFilePath);
        Globals.Result_Link_FilePath = responseFilePath;
        ReportUtil.STAF_ReportEvent("Pass", "Order Request Response", "Response XML stored at XML_results folder.FileName--"+responseFileName, 3);
        
        //Get the Order Number from Response File 
        XMLUtils responceXMLFile = new XMLUtils(responseFilePath);
        
        String Message  = "";
       // XMLUtils responceXMLFile = new XMLUtils("C:\\STAF_Selenium\\smartData\\executionResults\\SanityTest_Cycle1_ENV_QA1\\Run_04_08_2017_04_42_09_PM\\SMARTDATA\\CHROME\\XML_Results\\SMD580168_Response.xml");
        
        if (responceXMLFile.getNumberOfNodesPresentByTagName("ns1:return") != 0) {
            Message = responceXMLFile.getXMLNodeValByTagName("ns1:return");
            if(Message.contains("Acknowledged"))
            {
            	Globals.testSuiteXLS.setCellData_inTestData("SWestOrderID", orderId);
    			
            	ReportUtil.STAF_ReportEvent("Pass", "Order ID Generation Check", "Order ID is : "+Message, 0); 
            }else{
            	ReportUtil.STAF_ReportEvent("Fail", "Order ID Generation Check", "Order ID is not generated : "+Message, 0);
            	throw new Exception("Failed to Place the order");
            }
       }else{
    	   ReportUtil.STAF_ReportEvent("Fail", "Order ID Generation Check", "Order ID is not generated : "+Message, 0);
    	   throw new Exception("Failed to Place the order");
       }
        System.out.println(Message);
        
        
		
		
		
        }catch(Exception ie)
        {
        	throw ie;
        }
        
        return Globals.KEYWORD_PASS;
	}
	
	public static String makeRequestForCrim(String payload,String OrderID)
	{
		String requestPayload = null;
		String personalAlias_Array = null;
		
		String orderId = OrderID;
		String profileInfo = "<ProfileID>%s</ProfileID>";
		String personalCurrent_Info = "<ScreeningSubjectDetails ID=\"Current\"><PersonLegalID xmlns=\"http://www.sterlingtesting.com/hrxml/1.0\" schemeName=\"SSN\">%s</PersonLegalID><PersonName xmlns=\"http://www.sterlingtesting.com/hrxml/1.0\"><GivenName>%s</GivenName><MiddleName>%s</MiddleName><FamilyName>%s</FamilyName></PersonName><PostalAddress><AddressLine>%s</AddressLine><CityName>%s</CityName><CountrySubDivisionCode>%s</CountrySubDivisionCode><PostalCode>%s</PostalCode></PostalAddress><BirthDateDetails xmlns=\"http://www.sterlingtesting.com/hrxml/1.0\"><YearMonthDate>%s</YearMonthDate></BirthDateDetails><BirthPlaceDetails xmlns=\"http://www.sterlingtesting.com/hrxml/1.0\"><BirthPlace/></BirthPlaceDetails><GenderCode xmlns=\"http://www.sterlingtesting.com/hrxml/1.0\">9</GenderCode><Salary xmlns=\"http://www.sterlingtesting.com/hrxml/1.0\"><Currency/><SalaryRange>%s</SalaryRange></Salary></ScreeningSubjectDetails>";
		String personalAlias_Info = "<ScreeningSubjectDetails ID=\"Alias\"><PersonLegalID schemeName=\"SSN\">%s</PersonLegalID><PersonName><GivenName>%s</GivenName><MiddleName>%s</MiddleName><FamilyName>%s</FamilyName></PersonName><PostalAddress><AddressLine>%s</AddressLine><CityName>%s</CityName><CountrySubDivisionCode>%s</CountrySubDivisionCode><PostalCode>%s</PostalCode></PostalAddress><GenderCode>9</GenderCode><BirthDateDetails><YearMonthDate>%s</YearMonthDate></BirthDateDetails><BirthPlaceDetails><BirthPlace/></BirthPlaceDetails><Salary><Currency/><SalaryRange>%s</SalaryRange></Salary></ScreeningSubjectDetails>";
		String ScreeningProduct = "<ID schemeAgencyID=\"Admin Client\" schemeID=\"ScreeningID\">%s</ID><ID schemeAgencyID=\"Admin Client\" schemeID=\"Product\">%s</ID>";
		String CountryInfo = "<SearchCriminalTypeCode>County</SearchCriminalTypeCode><CountryCode>US</CountryCode><CountrySubdivisionCode>%s</CountrySubdivisionCode><CountyName>%s</CountyName><CityName/><CourtName/>";
		
		String customerName = Globals.testSuiteXLS.getCellData_fromTestData("Customer_Name");
		String accountName = Globals.testSuiteXLS.getCellData_fromTestData("Account_Name");
		String profileName = Globals.testSuiteXLS.getCellData_fromTestData("Profile_Name");
		
		String currentSSN = Globals.testSuiteXLS.getCellData_fromTestData("SSN");
		String currentFirstName = Globals.testSuiteXLS.getCellData_fromTestData("FirstName");
		String currentMiddleName = Globals.testSuiteXLS.getCellData_fromTestData("MidName");
		String currentLastName = Globals.testSuiteXLS.getCellData_fromTestData("LastName");
		String currentAddressLine = Globals.testSuiteXLS.getCellData_fromTestData("AddressLine1");
		String currentCityName = Globals.testSuiteXLS.getCellData_fromTestData("City");
		String currentCountrySubdivision = Globals.testSuiteXLS.getCellData_fromTestData("SubDivisionCountry");
		String currentPinCode = Globals.testSuiteXLS.getCellData_fromTestData("ZIP");
		String currentDob = Globals.testSuiteXLS.getCellData_fromTestData("DOB");
		String currentSalaryRange = Globals.testSuiteXLS.getCellData_fromTestData("SalaryRange");
		
		int aliasCnt = Integer.parseInt(Globals.testSuiteXLS.getCellData_fromTestData("NoOf_Alias"));
		
		String aliasSSN[] = Globals.testSuiteXLS.getCellData_fromTestData("Alias_SSN").split(":");
		String aliasFirstName[] = Globals.testSuiteXLS.getCellData_fromTestData("Alias_FirstName").split(":");
		String aliasMiddleName[] = Globals.testSuiteXLS.getCellData_fromTestData("Alias_MiddleName").split(":");
		String aliasLastName[] = Globals.testSuiteXLS.getCellData_fromTestData("Alias_LastName").split(":");
		String aliasAddressLine[] = Globals.testSuiteXLS.getCellData_fromTestData("Alias_AddressLine").split(":");
		String aliasCityName[] = Globals.testSuiteXLS.getCellData_fromTestData("Alias_City").split(":");
		String aliasCountrySubdivision[] = Globals.testSuiteXLS.getCellData_fromTestData("Alias_SubDivisionCountry").split(":");
		String aliasPinCode[] = Globals.testSuiteXLS.getCellData_fromTestData("Alias_PostalCode").split(":");
		String aliasDob[] = Globals.testSuiteXLS.getCellData_fromTestData("Alias_DOB").split(":");
		String aliasSalaryRange[] = Globals.testSuiteXLS.getCellData_fromTestData("Alias_SalaryRange").split(":");
		
		String ScreeningID = orderId;
		String ProductName = Globals.testSuiteXLS.getCellData_fromTestData("AlacarteProducts");
		
		String profileUpdated = null;
		if(profileName != null && !profileName.isEmpty())
		{
			profileUpdated = String.format(profileInfo, profileName);
		}
	
		StringBuilder ssno = new StringBuilder(currentSSN);
		ssno.insert(3, "-");
		ssno.insert(6, "-");
		currentSSN = ssno.toString();
		String currentDOBirth[] = currentDob.split("/");
		currentDob = currentDOBirth[2]+"-"+currentDOBirth[0]+"-"+currentDOBirth[1];
		
		
		if(!currentSalaryRange.equals("") && !currentSalaryRange.isEmpty()){
		
			if(currentSalaryRange.equalsIgnoreCase("less than 20000")){
				currentSalaryRange="Less than $20,000";
			}else if(currentSalaryRange.equalsIgnoreCase("between 20000 and 24999")){
				currentSalaryRange="Between $20,000-$24,999";
			}else if(currentSalaryRange.equalsIgnoreCase("between 25000 and 74999")){
				currentSalaryRange="Between $25,000-$74,999";
			}else if(currentSalaryRange.equalsIgnoreCase("over 75000")){
				currentSalaryRange="Over $75,000";
			}else{
				currentSalaryRange="Unable to provide salary";
			}
			
		}
		
		personalCurrent_Info = String.format(personalCurrent_Info, currentSSN,currentFirstName,currentMiddleName,currentLastName,currentAddressLine,currentCityName,currentCountrySubdivision,currentPinCode,currentDob,currentSalaryRange);
		
		for(int i=0;i<aliasCnt;i++)
		{
			StringBuilder aliasSsno = new StringBuilder(aliasSSN[i]);
			aliasSsno.insert(3, "-");
			aliasSsno.insert(6, "-");
			aliasSSN[i] = aliasSsno.toString();
			
			String alisDOBirth[] = aliasDob[i].split("/");
			aliasDob[i] = alisDOBirth[2]+"-"+alisDOBirth[0]+"-"+alisDOBirth[1];
			if(personalAlias_Array == null)
			{
				personalAlias_Array = String.format(personalAlias_Info, aliasSSN[i],aliasFirstName[i],aliasMiddleName[i],aliasLastName[i],aliasAddressLine[i],aliasCityName[i],aliasCountrySubdivision[i],aliasPinCode[i],aliasDob[i],aliasSalaryRange[i]);
			}else{
				personalAlias_Array = personalAlias_Array+String.format(personalAlias_Info, aliasSSN[i],aliasFirstName[i],aliasMiddleName[i],aliasLastName[i],aliasAddressLine[i],aliasCityName[i],aliasCountrySubdivision[i],aliasPinCode[i],aliasDob[i],aliasSalaryRange[i]);
			}
		}
		
		ScreeningProduct = String.format(ScreeningProduct, ScreeningID,ProductName);
		
		CountryInfo = String.format(CountryInfo, currentCountrySubdivision,currentCityName);
		
		if(aliasCnt==0)
		{
			if(profileUpdated != null && !profileUpdated.isEmpty())
			{
				requestPayload = String.format(payload, orderId,customerName,currentCountrySubdivision,personalCurrent_Info,"",ScreeningProduct,accountName,profileUpdated,CountryInfo);
			}else{
				requestPayload = String.format(payload, orderId,customerName,currentCountrySubdivision,personalCurrent_Info,"",ScreeningProduct,accountName,"",CountryInfo);
			}
		}else{
			if(profileUpdated != null && !profileUpdated.isEmpty())
			{
				requestPayload = String.format(payload, orderId,customerName,currentCountrySubdivision,personalCurrent_Info,personalAlias_Array,ScreeningProduct,accountName,profileUpdated,CountryInfo);
			}else{
				requestPayload = String.format(payload, orderId,customerName,currentCountrySubdivision,personalCurrent_Info,personalAlias_Array,ScreeningProduct,accountName,"",CountryInfo);
			}
		}
		
		
		return requestPayload;
	}
	

	public static String loginWithSuperUser() throws Exception
	{
		APP_LOGGER.startFunction("login With Super User");
		String retval=Globals.KEYWORD_FAIL;
		String tempRetval=Globals.KEYWORD_FAIL;
		int timeOutinSeconds =  60;
		try {
			
//			String username = "Sfirke";
			String urlLaunched = null;
			String username = null;
			String password = null;
//			urlLaunched= sc.launchURL(Globals.BrowserType,Globals.getEnvPropertyValue("AG_SuperUserURL"));
//			if(Globals.Env_To_Execute_ON.equalsIgnoreCase("ENV_PROD"))
//			{
				urlLaunched= sc.launchURL(Globals.BrowserType,Globals.getEnvPropertyValue("AG_URL"));
				
				username = Globals.getEnvPropertyValue("AG_SuperUserName");
				password = Globals.getEnvPropertyValue("AG_SuperUserPassword");
				
				if(sc.waitTillElementDisplayed("agLogin_userID_txt", timeOutinSeconds) == Globals.KEYWORD_PASS)
				{
					sc.setValue("agLogin_userID_txt", username);
					sc.setValue("agLogin_password_txt", password);
					
					sc.clickWhenElementIsClickable("agLogin_login_btn", timeOutinSeconds);
				}
				
				sc.waitTillElementDisplayed("agLast_LoginUser_btn", 15);
				if(sc.waitTillElementDisplayed("agLast_LoginUser_btn", 15) == Globals.KEYWORD_PASS)
				{
					sc.clickWhenElementIsClickable("agLast_LoginUser_btn", 15);
				}
//			}else{
//				urlLaunched= sc.launchURL(Globals.BrowserType,Globals.getEnvPropertyValue("AG_SuperUserURL"));
//			}
			
			if (urlLaunched.equalsIgnoreCase(Globals.KEYWORD_PASS) && sc.waitforElementToDisplay("agHomepage_tasks_link",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_PASS)){
				timeOutinSeconds =  60;
				if (sc.waitforElementToDisplay("agHomepage_tasks_link",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_PASS)){
					log.info("AG Super User Logged In | Username - "+ username);
					ReportUtil.STAF_ReportEvent("Pass", "AG Super User", "User has logged in - " + username , 1);

					retval=Globals.KEYWORD_PASS;					
				}else{
					ReportUtil.STAF_ReportEvent("Fail", "AG Super User", "User unable log in - " + username , 1);
					log.error("AG Super User Unable to Log In | Username - "+ username);
					throw new Exception("AG Super User Unable to Log In | Username - "+ username);
				}

			}else{
				ReportUtil.STAF_ReportEvent("Fail", "AG Super User", " Login page has NOT loaded" , 1);
				log.error("Unable to Log to AG Super User In As Login Page has not Launched");
				throw new Exception("Unable to Log to AG Super User In As Login Page has not Launched");
			}


		} catch (Exception e) {

			log.error("Exception occurred in AG Login With Super User | "+e.toString());
			e.printStackTrace();
			throw e;
		}

		return retval;
	}
	
	public static String loginWithBasicUser() throws Exception
	{
		APP_LOGGER.startFunction("login With Basic User");
		String retval=Globals.KEYWORD_FAIL;
		String tempRetval=Globals.KEYWORD_FAIL;
		int timeOutinSeconds =  60;
		try {
			String urlLaunched = null;
			String username = null;
			String password = null;
			
//			if(Globals.Env_To_Execute_ON.equalsIgnoreCase("ENV_PROD"))
//			{
				urlLaunched= sc.launchURL(Globals.BrowserType,Globals.getEnvPropertyValue("AG_URL"));
				
				username = Globals.getEnvPropertyValue("AG_BasicUserName");
				password = Globals.getEnvPropertyValue("AG_BasicUserPassword");
				
				if(sc.waitTillElementDisplayed("agLogin_userID_txt", timeOutinSeconds) == Globals.KEYWORD_PASS)
				{
					sc.setValue("agLogin_userID_txt", username);
					sc.setValue("agLogin_password_txt", password);
					
					sc.clickWhenElementIsClickable("agLogin_login_btn", timeOutinSeconds);
				}
				
				sc.waitTillElementDisplayed("agLast_LoginUser_btn", 15);
				if(sc.waitTillElementDisplayed("agLast_LoginUser_btn", 15) == Globals.KEYWORD_PASS)
				{
					sc.clickWhenElementIsClickable("agLast_LoginUser_btn", 15);
				}
//			}else{
//				urlLaunched= sc.launchURL(Globals.BrowserType,Globals.getEnvPropertyValue("AG_BasicUserURL"));
//			}
			
			if (urlLaunched.equalsIgnoreCase(Globals.KEYWORD_PASS) && sc.waitforElementToDisplay("agHomepage_tasks_link",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_PASS)){
				timeOutinSeconds =  60;
				if (sc.waitforElementToDisplay("agHomepage_tasks_link",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_PASS)){
					log.info("AG Super User Logged In | Username - "+ "sfirke");
					ReportUtil.STAF_ReportEvent("Pass", "AG Super User", "User has logged in - " + "Argentum5" , 1);

					retval=Globals.KEYWORD_PASS;					
				}else{
					ReportUtil.STAF_ReportEvent("Fail", "AG Super User", "User unable log in - " + "Argentum5" , 1);
					log.error("AG Super User Unable to Log In | Username - "+ "sfirke");
					throw new Exception("AG Super User Unable to Log In | Username - "+ "Argentum5");
				}

			}else{
				ReportUtil.STAF_ReportEvent("Fail", "AG Super User", " Login page has NOT loaded" , 1);
				log.error("Unable to Log to AG Super User In As Login Page has not Launched");
				throw new Exception("Unable to Log to AG Super User In As Login Page has not Launched");
			}


		} catch (Exception e) {

			log.error("Exception occurred in AG Login With Super User | "+e.toString());
			e.printStackTrace();
			throw e;
		}

		return retval;
	}
	
	public static String EnterResult() throws Exception
	{
		APP_LOGGER.startFunction("Enter Result");
		String retval=Globals.KEYWORD_FAIL;
		int timeOutinSeconds =  60;
		String tempRetval=Globals.KEYWORD_FAIL;
		
		try{
			String orderNo 			= Globals.testSuiteXLS.getCellData_fromTestData("SWestOrderID");
			String taskType 			= Globals.testSuiteXLS.getCellData_fromTestData("TaskType");
			sc.clickWhenElementIsClickable("agHomepage_tasks_link", 5);
			if(sc.waitTillElementDisplayed("tasks_criminalTasks_link", 5) == Globals.KEYWORD_PASS)
			{
				if(taskType.equalsIgnoreCase("Criminal"))
				{
					sc.clickWhenElementIsClickable("tasks_criminalTasks_link", 20);
				}else if(taskType.equalsIgnoreCase("Abuse"))
				{
					sc.clickWhenElementIsClickable("tasks_abuseTasks_link", 20);
				}else if(taskType.equalsIgnoreCase("Sanction"))
				{
					sc.clickWhenElementIsClickable("tasks_sanctionTasks_link", 20);
				}else if(taskType.equalsIgnoreCase("Civil"))
				{
					sc.clickWhenElementIsClickable("tasks_civilTasks_link", 20);
				}
			}
			sc.clickWhenElementIsClickable("View_My_WorkList_link", 5);
			sc.clickWhenElementIsClickable("Actions_Enter_Results_link", 5);
			
			sc.clickWhenElementIsClickable("Actions_Search_btn_path", 5);
			if(sc.waitTillElementDisplayed("OrderID_id", 5) == Globals.KEYWORD_PASS)
			{
				sc.setValue("OrderID_id", orderNo);
			}
			
			sc.clickWhenElementIsClickable("Order_Search_path", timeOutinSeconds);
			
			if(sc.waitforElementToDisplay("Result_Table_Required_Action_path", timeOutinSeconds) == Globals.KEYWORD_PASS)
			{
				sc.clickWhenElementIsClickable("Result_Table_Required_Action_path", timeOutinSeconds);
				Thread.sleep(2000);
				ArrayList<String> tabs = new ArrayList<String>(Globals.driver.getWindowHandles());
				if(tabs.size()>1)
				{
					driver.switchTo().window(tabs.get(1));
					driver.close();
					Thread.sleep(1000);
					driver.switchTo().window(tabs.get(0));
				}
				
				
			}
			
			if(EnterCourtRecordDetails() == Globals.KEYWORD_PASS)
			{
				log.info("Court and Record Details are entered successfully");
				ReportUtil.STAF_ReportEvent("Pass", "OrderID-"+orderNo, "Enter Data sucessfully in Court and Record Details", 1);
			}else{
				log.info("Failure while Entering data in Court and Record Details");
				ReportUtil.STAF_ReportEvent("Fail", "OrderID-"+orderNo, "Enter Data Failed in Court and Record Details", 1);
			}
			
			sc.clickWhenElementIsClickable("SD_Show_path", timeOutinSeconds);
			sc.waitforElementToDisplay("SD_FirstName_id", timeOutinSeconds);
			if(EnterSubjectDetails() == Globals.KEYWORD_PASS)
			{
				log.info("Court and Record Details are entered successfully");
				ReportUtil.STAF_ReportEvent("Pass", "OrderID-"+orderNo, "Enter Data sucessfully in Subject Details", 1);
			}else{
				log.info("Court and Record Details are entered successfully");
				ReportUtil.STAF_ReportEvent("Fail", "OrderID-"+orderNo, "Enter Data Failed in Subject Details", 1);
			}
			
			sc.clickWhenElementIsClickable("CS_Show_path", timeOutinSeconds);
			sc.waitTillElementDisplayed("CS_ChargeDescription_path", timeOutinSeconds);
			
			if(EnterChargesAndSentences() == Globals.KEYWORD_PASS)
			{
				log.info("Charges and Sentences are entered successfully");
				ReportUtil.STAF_ReportEvent("Pass", "OrderID-"+orderNo, "Enter Data sucessfully in Charges and Sentences", 1);
			}else{
				log.info("Court and Record Details are entered successfully");
				ReportUtil.STAF_ReportEvent("Fail", "OrderID-"+orderNo, "Enter Data Failed in Charges and Sentences", 1);
			}
			
			sc.clickWhenElementIsClickable("ER_ValidAndSave_Record_id", timeOutinSeconds);
			
			sc.clickWhenElementIsClickable("ER_CompleteTask_id", timeOutinSeconds);
			sc.clickWhenElementIsClickable("ER_CompleteTask_Confirmation_Yes_id", timeOutinSeconds);
			
			sc.waitTillElementDisplayed("ER_NewReportableInformation_id", timeOutinSeconds);
			if(sc.waitTillElementDisplayed("ER_NewReportableInformation_id", timeOutinSeconds) == Globals.KEYWORD_PASS)
			{
				sc.clickWhenElementIsClickable("ER_NewReportableInformation_id", timeOutinSeconds);
				sc.checkCheckBox("ER_CheckBoxCheck_id");
				sc.clickWhenElementIsClickable("ER_KnowHit_Yes_id", timeOutinSeconds);
			}
			retval = Globals.KEYWORD_PASS;
			
			
		}catch(Exception ie)
		{
			
		}
		
		return retval;
	}
	
	public static String EnterCourtRecordDetails()
	{
		APP_LOGGER.startFunction("Court and Record Details");
		String retval=Globals.KEYWORD_FAIL;
		int timeOutinSeconds =  60;
		String tempRetval=Globals.KEYWORD_FAIL;
		try{
			
			String arrestDate       =            Globals.testSuiteXLS.getCellData_fromTestData("Arrest_Date");
			String dispositionDate  =            Globals.testSuiteXLS.getCellData_fromTestData("Disposition_Date");
			String warrentDate      =            Globals.testSuiteXLS.getCellData_fromTestData("Warrant_Date");
			String nextCourtDate    =            Globals.testSuiteXLS.getCellData_fromTestData("Next_Court_Date");
			String CaseDocketNumber =            Globals.testSuiteXLS.getCellData_fromTestData("Case_Docket");
			
			if(arrestDate != null)
			{
				sc.setValue("CRD_ArrestDate_id", arrestDate);
			}
			if(dispositionDate != null)
			{
				sc.setValue("CRD_DispositionDate_id", dispositionDate);
			}
			if(warrentDate != null)
			{
				sc.setValue("CRD_WarrentDate_id", warrentDate);
			}
			if(nextCourtDate != null)
			{
				sc.setValue("CRD_NextCourtDate_id", nextCourtDate);
			}
			if(CaseDocketNumber != null)
			{
				sc.setValue("CRD_CaseNoDocket_id", CaseDocketNumber);
			}
			
			retval = Globals.KEYWORD_PASS;
			
		}catch(Exception ie)
		{
			log.error("Exception occurred While Entering data in Court and Details | "+ie.toString());
			ie.printStackTrace();
			throw ie;
		}
		
		return retval;
	}
	
	public static String EnterSubjectDetails()
	{
		APP_LOGGER.startFunction("Court and Record Details");
		String retval=Globals.KEYWORD_FAIL;
		int timeOutinSeconds =  60;
		String tempRetval=Globals.KEYWORD_FAIL;
		
		try {
			
			String firstName   =  Globals.testSuiteXLS.getCellData_fromTestData("FirstName");
			String lastName   =  Globals.testSuiteXLS.getCellData_fromTestData("LastName");
			String middleName   =  Globals.testSuiteXLS.getCellData_fromTestData("MidName");
			
			String address   =  Globals.testSuiteXLS.getCellData_fromTestData("SubjectDetails_Address");
			String cityName   =  Globals.testSuiteXLS.getCellData_fromTestData("City");
			String state   =  Globals.testSuiteXLS.getCellData_fromTestData("State");
			String pincode   =  Globals.testSuiteXLS.getCellData_fromTestData("ZIP");
			
			String ssn   =  Globals.testSuiteXLS.getCellData_fromTestData("SSN");
			String dob   =  Globals.testSuiteXLS.getCellData_fromTestData("DOB");
			
			String possibleRecords = Globals.testSuiteXLS.getCellData_fromTestData("SubjectDetails_PossibleRecord");
			
			int aliasCnt = Integer.parseInt(Globals.testSuiteXLS.getCellData_fromTestData("NoOf_Alias"));
			
			if(firstName != null)
			{
				sc.setValue("SD_FirstName_id", firstName);
			}
			if(lastName != null)
			{
				sc.setValue("SD_LastName_id", lastName);
			}
			if(middleName != null)
			{
				sc.setValue("SD_MiddleName_id", middleName);
			}
			
			if(address != null)
			{
				sc.setValue("SD_Address_id", address);
			}
			if(cityName != null)
			{
				sc.setValue("SD_CityName_id", cityName);
			}
			if(state != null)
			{
				sc.setValue("SD_State_id", state);
			}
			if(pincode != null)
			{
				sc.setValue("SD_PostalCode_id", pincode);
			}
			
			if(ssn != null)
			{
				sc.setValue("SD_SSN_id", ssn);
			}
			
			if(dob != null)
			{
				sc.uncheckCheckBox("SD_PartialDOB_Checkbox_id");
				sc.setValue("SD_DOB_id", dob);
			}
			
			if(possibleRecords != null)
			{
				if(possibleRecords.equalsIgnoreCase("Yes"))
				{
					sc.clickWhenElementIsClickable("SD_PossibleRecord_Yes_id", timeOutinSeconds);
				}else if(possibleRecords.equalsIgnoreCase("No"))
				{
					sc.clickWhenElementIsClickable("SD_PossibleRecord_No_id", timeOutinSeconds);
				}
			}
			
			if(aliasCnt>0)
			{
				String aliasSSN[] = Globals.testSuiteXLS.getCellData_fromTestData("Alias_SSN").split(":");
				String aliasFirstName[] = Globals.testSuiteXLS.getCellData_fromTestData("Alias_FirstName").split(":");
				String aliasMiddleName[] = Globals.testSuiteXLS.getCellData_fromTestData("Alias_MiddleName").split(":");
				String aliasLastName[] = Globals.testSuiteXLS.getCellData_fromTestData("Alias_LastName").split(":");
				String aliasDob[] = Globals.testSuiteXLS.getCellData_fromTestData("Alias_DOB").split(":");
				for(int i=1;i<=aliasCnt;i++)
				{
					int row = i+1;
					sc.clickWhenElementIsClickable("SD_ADDAliasName_id", timeOutinSeconds);
					sc.waitTillElementDisplayed("SD_ADDAliasName_Tbl_path", timeOutinSeconds);
					sc.dblClickElementUsingAction(driver.findElement(By.xpath(".//*[@id='aliasNameGrid']/div[2]/table/tbody/tr["+row+"]/td[2]")));
					sc.setValue(driver.findElement(By.xpath(".//*[@id='aliasNameGrid']/div[2]/table/tbody/tr["+row+"]/td[2]/input")), aliasFirstName[i-1]);
					
					sc.dblClickElementUsingAction(driver.findElement(By.xpath(".//*[@id='aliasNameGrid']/div[2]/table/tbody/tr["+row+"]/td[3]")));
					sc.setValue(driver.findElement(By.xpath(".//*[@id='aliasNameGrid']/div[2]/table/tbody/tr["+row+"]/td[3]/input")), aliasMiddleName[i-1]);
					
					sc.dblClickElementUsingAction(driver.findElement(By.xpath(".//*[@id='aliasNameGrid']/div[2]/table/tbody/tr["+row+"]/td[4]")));
					sc.setValue(driver.findElement(By.xpath(".//*[@id='aliasNameGrid']/div[2]/table/tbody/tr["+row+"]/td[4]/input")), aliasLastName[i-1]);
					
					sc.clickWhenElementIsClickable("SD_ADDAliasDOB_id", timeOutinSeconds);
					sc.waitTillElementDisplayed("SD_ADDAliasDOB_Tbl_path", timeOutinSeconds);
					sc.dblClickElementUsingAction(driver.findElement(By.xpath(".//*[@id='aliasDobGrid']/div[2]/table/tbody/tr["+row+"]/td[2]")));
					sc.setValue(driver.findElement(By.xpath(".//*[@id='aliasDobGrid']/div[2]/table/tbody/tr["+row+"]/td[2]/input")), aliasDob[i-1]);
					driver.findElement(By.xpath(".//*[@id='aliasDobGrid']/div[2]/table/tbody/tr["+row+"]/td[2]/input")).sendKeys(Keys.TAB);
					
					sc.clickWhenElementIsClickable("SD_ADDAliasSSN_id", timeOutinSeconds);
					sc.waitTillElementDisplayed("SD_ADDAliasSSN_Tbl_path", timeOutinSeconds);
					sc.dblClickElementUsingAction(driver.findElement(By.xpath(".//*[@id='aliasSsnGrid']/div[2]/table/tbody/tr["+row+"]/td[2]")));
					sc.setValue(driver.findElement(By.xpath(".//*[@id='aliasSsnGrid']/div[2]/table/tbody/tr["+row+"]/td[2]/input")), aliasSSN[i-1]);
				}
			}
			
			retval = Globals.KEYWORD_PASS;
			
		}catch(Exception ie)
		{
			log.error("Exception occurred While Entering data in Subject Details | "+ie.toString());
			ie.printStackTrace();
			throw ie;
		}
		
		return retval;
	}
	
	public static String EnterChargesAndSentences() throws Exception
	{
		APP_LOGGER.startFunction("Changes and Sentences");
		String retval=Globals.KEYWORD_FAIL;
		int timeOutinSeconds =  60;
		String tempRetval=Globals.KEYWORD_FAIL;
		
		try{
			String addCharges   =  Globals.testSuiteXLS.getCellData_fromTestData("AddCharge");
			String addSentence   =  Globals.testSuiteXLS.getCellData_fromTestData("AddSentence");
			
			if(addCharges.equalsIgnoreCase("Yes"))
			{
				if(EnterCharges() == Globals.KEYWORD_PASS)
				{
					log.info("Enter Charge Details successfully");
					ReportUtil.STAF_ReportEvent("Pass", "Enter Charges", "All Charges Entered Successfully", 1);
				}else{
					log.info("Enter Charge Details Failed");
					ReportUtil.STAF_ReportEvent("Pass", "Enter Charges", "Failed to Enter Charges", 1);
				}
			}
			
			if(addSentence.equalsIgnoreCase("Yes"))
			{
				if(EnterSentence() == Globals.KEYWORD_PASS)
				{
					log.info("Enter Sentence Details successfully");
					ReportUtil.STAF_ReportEvent("Pass", "Enter Sentences", "All Sentences Entered Successfully", 1);
				}else{
					log.info("Enter Sentence Details Failed");
					ReportUtil.STAF_ReportEvent("Pass", "Enter Sentences", "Failed to Enter Sentences", 1);
				}
			}
			
			retval = Globals.KEYWORD_PASS;
		}catch(Exception ie)
		{
			log.error("Exception occurred While Entering data in Charges and Sentences | "+ie.toString());
			ie.printStackTrace();
			throw ie;
		}
		
		return retval;
	}
	
	public static String EnterCharges() throws Exception
	{
		APP_LOGGER.startFunction("Enter Charges");
		String retval=Globals.KEYWORD_FAIL;
		int timeOutinSeconds =  60;
		String tempRetval=Globals.KEYWORD_FAIL;
		
		try{
			String NumberOfCharges = Globals.testSuiteXLS.getCellData_fromTestData("NoOfCharges");
			String [] chargeLevel = Globals.testSuiteXLS.getCellData_fromTestData("Charge_Level").split(",");
			String [] charges   =  Globals.testSuiteXLS.getCellData_fromTestData("Charge").split("#AddCharges#");
			String [] disposition   =  Globals.testSuiteXLS.getCellData_fromTestData("Disposition").split("#AddDisposition#");
			String [] dispositionDate  =  Globals.testSuiteXLS.getCellData_fromTestData("Add_Disposition_Date").split(",");
			String [] convictionType = Globals.testSuiteXLS.getCellData_fromTestData("Charge_Conviction_Type").split(",");
			String [] ReportToClient = Globals.testSuiteXLS.getCellData_fromTestData("Charge_Report_To_Client").split(",");
			
			int rowCount = Integer.parseInt(NumberOfCharges);
			
			for(int i=1;i<=rowCount;i++)
			{
				int row = i+1;
				sc.clickWhenElementIsClickable("CS_ADDChargeLink_id", timeOutinSeconds);
				sc.waitTillElementDisplayed("CS_Charges_Table_path", timeOutinSeconds);
				if(chargeLevel[i-1] != null && !chargeLevel[i-1].isEmpty())
				{
					sc.selectValue_byVisibleText(driver.findElement(By.xpath(".//*[@id='chargesGrid']/div[2]/table/tbody/tr["+row+"]/td[3]/select")), chargeLevel[i-1]);
				}
				
				if(convictionType.length<=rowCount && i<=convictionType.length)
				{
					if(convictionType[i-1] != null && !convictionType[i-1].isEmpty())
					{
						sc.selectValue_byVisibleText(driver.findElement(By.xpath(".//*[@id='chargesGrid']/div[2]/table/tbody/tr["+row+"]/td[9]/select")), convictionType[i-1]);
					}
				}
				
//				if(ReportToClient[i-1] != null && !ReportToClient[i-1].isEmpty())
//				{
//					sc.selectValue_byVisibleText("CS_Charge_ReportToClient_path", ReportToClient[i-1]);
//				}
				
				sc.dblClickElementUsingAction(driver.findElement(By.xpath(".//*[@id='chargesGrid']/div[2]/table/tbody/tr["+row+"]/td[4]")));
				sc.setValue(driver.findElement(By.xpath(".//*[@id='chargesGrid']/div[2]/table/tbody/tr["+row+"]/td[4]/textarea")), charges[i-1]);
				
				sc.dblClickElementUsingAction(driver.findElement(By.xpath(".//*[@id='chargesGrid']/div[2]/table/tbody/tr["+row+"]/td[7]")));
				sc.setValue(driver.findElement(By.xpath(".//*[@id='chargesGrid']/div[2]/table/tbody/tr["+row+"]/td[7]/textarea")), disposition[i-1]);
				
				sc.dblClickElementUsingAction(driver.findElement(By.xpath(".//*[@id='chargesGrid']/div[2]/table/tbody/tr["+row+"]/td[8]")));
				sc.setValue(driver.findElement(By.xpath(".//*[@id='chargesGrid']/div[2]/table/tbody/tr["+row+"]/td[8]/textarea")), dispositionDate[i-1]);
				
			}
			
			retval = Globals.KEYWORD_PASS;
		}catch(Exception ie)
		{
			log.error("Exception occurred While Entering data in Charges | "+ie.toString());
			ie.printStackTrace();
			throw ie;
		}
		
		return retval;
	}
	
	public static String EnterSentence() throws Exception
	{
		APP_LOGGER.startFunction("Enter Sentence");
		String retval=Globals.KEYWORD_FAIL;
		int timeOutinSeconds =  60;
		String tempRetval=Globals.KEYWORD_FAIL;
		
		try{
			String NumberOfSentence = Globals.testSuiteXLS.getCellData_fromTestData("NoOfSentences");
			if(Integer.parseInt(NumberOfSentence)>0)
			{
				String [] sentenceWhichType = Globals.testSuiteXLS.getCellData_fromTestData("Sentence_ForWhichCharge").split(",");
				String [] senteceWhichCharge = Globals.testSuiteXLS.getCellData_fromTestData("Sentence_SpecificCharge").split(",");
				
				String [] senteceType = Globals.testSuiteXLS.getCellData_fromTestData("Sentence_SentenceType").split(",");
				String [] senteceCategory = Globals.testSuiteXLS.getCellData_fromTestData("Sentence_SentenceCategory").split(",");
				
				String [] senteceAmountAndDuration = Globals.testSuiteXLS.getCellData_fromTestData("Sentence_AmountAndDuration").split(",");
				String [] suspendedAmountAndDuration = Globals.testSuiteXLS.getCellData_fromTestData("Sentence_SuspendedAmountAndDuration").split(",");
				
				String [] Sentence_ConsucutiveORConcurrent = Globals.testSuiteXLS.getCellData_fromTestData("Sentence_ConsucutiveORConcurrent").split(",");
				
				for(int i=1;i<=sentenceWhichType.length;i++)
				{
					int row = i+1;
					sc.clickWhenElementIsClickable("CS_ADDSentences_id", timeOutinSeconds);
					if(sc.waitTillElementDisplayed("CS_Sentences_Tbl_path", timeOutinSeconds) == Globals.KEYWORD_PASS)
					{
						if(sentenceWhichType[i-1].equalsIgnoreCase("Specific Charges"))
						{
							sc.clickWhenElementIsClickable(driver.findElement(By.xpath("//*[@id='sentencesGrid']/div[2]/table/tbody/tr["+row+"]/td[2]/input[2]")), timeOutinSeconds);
						}else{
							sc.clickWhenElementIsClickable(driver.findElement(By.xpath("//*[@id='sentencesGrid']/div[2]/table/tbody/tr["+row+"]/td[2]/input[1]")), timeOutinSeconds);
						}
						
						if(!senteceWhichCharge[i-1].equalsIgnoreCase("All"))
						{
							sc.selectValue_byVisibleText(driver.findElement(By.xpath("//*[@id='sentencesGrid']/div[2]/table/tbody/tr["+row+"]/td[3]/select")), senteceWhichCharge[i-1]);
						}
						
						if(senteceType[i-1].equalsIgnoreCase("Monetary Value"))
						{
							sc.clickWhenElementIsClickable(driver.findElement(By.xpath("//*[@id='sentencesGrid']/div[2]/table/tbody/tr["+row+"]/td[4]/input[1]")), timeOutinSeconds);
							sc.selectValue_byVisibleText(driver.findElement(By.xpath("//*[@id='sentencesGrid']/div[2]/table/tbody/tr["+row+"]/td[5]/select")), senteceCategory[i-1]);
							
							sc.clickWhenElementIsClickable(driver.findElement(By.xpath("//*[@id='sentencesGrid']/div[2]/table/tbody/tr["+row+"]/td[6]/a")), timeOutinSeconds);
							if(sc.waitTillElementDisplayed("CS_Sentence_SentenceAmount_id", timeOutinSeconds) == Globals.KEYWORD_PASS)
							{
								sc.setValue("CS_Sentence_SentenceAmount_id", senteceAmountAndDuration[i-1]);
								if(suspendedAmountAndDuration[i-1].equalsIgnoreCase("Complete"))
								{
									sc.clickWhenElementIsClickable("CS_Sentence_SuspendedAmount_Complete_id", timeOutinSeconds);
								}else if(suspendedAmountAndDuration[i-1].equalsIgnoreCase("Not Suspended"))
								{
									sc.clickWhenElementIsClickable("CS_Sentence_SuspendedAmount_NotSuspended_id", timeOutinSeconds);
								}else if(suspendedAmountAndDuration[i-1].equalsIgnoreCase("Not Known"))
								{
									sc.clickWhenElementIsClickable("CS_Sentence_SuspendedAmount_NotKnow_id", timeOutinSeconds);
								}else{
									sc.clickWhenElementIsClickable("CS_Sentence_SuspendedAmount_Partial_id", timeOutinSeconds);
									if(sc.waitTillElementDisplayed("CS_Sentence_SuspendedAmount_id", timeOutinSeconds) == Globals.KEYWORD_PASS)
									{
										sc.setValue("CS_Sentence_SuspendedAmount_id", suspendedAmountAndDuration[i-1]);
									}
								}
								sc.clickWhenElementIsClickable("CS_Sentence_SuspendedAmount_Close_id", timeOutinSeconds);
							}
						}else if(senteceType[i-1].equalsIgnoreCase("Duration Value"))
						{
							sc.clickWhenElementIsClickable(driver.findElement(By.xpath("//*[@id='sentencesGrid']/div[2]/table/tbody/tr["+row+"]/td[4]/input[2]")), timeOutinSeconds);
							sc.selectValue_byVisibleText(driver.findElement(By.xpath("//*[@id='sentencesGrid']/div[2]/table/tbody/tr["+row+"]/td[5]/select")), senteceCategory[i-1]);
							
							sc.clickWhenElementIsClickable(driver.findElement(By.xpath("//*[@id='sentencesGrid']/div[2]/table/tbody/tr["+row+"]/td[6]/a")), timeOutinSeconds);
							if(sc.waitTillElementDisplayed("CS_Sentence_SentenceDuration_Years_id", timeOutinSeconds) == Globals.KEYWORD_PASS)
							{
								String [] senteceAmountAndDuration_split = senteceAmountAndDuration[i-1].split(":");
								String sentence_years = null,suspended_years=null;
								String sentence_months = null,suspended_months = null;
								String sentence_weeks = null,suspended_weeks = null;
								String sentence_days = null,suspended_days = null;
								String sentence_hours = null,suspended_hours = null;
								for(int j=0;j<senteceAmountAndDuration_split.length;j++)
								{
									if(senteceAmountAndDuration_split[j].contains("Y"))
									{
										sentence_years = senteceAmountAndDuration_split[j].split("Y")[0];
										sc.setValue("CS_Sentence_SentenceDuration_Years_id", sentence_years);
									}else if(senteceAmountAndDuration_split[j].contains("M"))
									{
										sentence_months = senteceAmountAndDuration_split[j].split("M")[0];
										sc.setValue("CS_Sentence_SentenceDuration_Months_id", sentence_months);
									}else if(senteceAmountAndDuration_split[j].contains("W"))
									{
										sentence_weeks = senteceAmountAndDuration_split[j].split("W")[0];
										sc.setValue("CS_Sentence_SentenceDuration_Weeks_id", sentence_weeks);
									}else if(senteceAmountAndDuration_split[j].contains("D"))
									{
										sentence_days = senteceAmountAndDuration_split[j].split("D")[0];
										sc.setValue("CS_Sentence_SentenceDuration_Days_id", sentence_days);
									}else if(senteceAmountAndDuration_split[j].contains("H"))
									{
										sentence_hours = senteceAmountAndDuration_split[j].split("H")[0];
										sc.setValue("CS_Sentence_SentenceDuration_Hours_id", sentence_hours);
									}
								}
								
								if(suspendedAmountAndDuration[i-1].equalsIgnoreCase("Complete"))
								{
									sc.clickWhenElementIsClickable("CS_Sentence_SuspenededDuration_Complete_id", timeOutinSeconds);
								}else if(suspendedAmountAndDuration[i-1].equalsIgnoreCase("Not Suspended"))
								{
									sc.clickWhenElementIsClickable("CS_Sentence_SuspenededDuration_NotSuspeneded_id", timeOutinSeconds);
								}else if(suspendedAmountAndDuration[i-1].equalsIgnoreCase("Not Known"))
								{
									sc.clickWhenElementIsClickable("CS_Sentence_SuspenededDuration_NotKnow_id", timeOutinSeconds);
								}else{
									sc.clickWhenElementIsClickable("CS_Sentence_SuspenededDuration_Partial_id", timeOutinSeconds);
									String [] suspendedAmountAndDuration_split = suspendedAmountAndDuration[i-1].split(":");
									
									if(sc.waitTillElementDisplayed("CS_Sentence_SuspendedDuration_Years_id", timeOutinSeconds) == Globals.KEYWORD_PASS)
									{
										for(int j=0;j<suspendedAmountAndDuration_split.length;j++)
										{
											if(suspendedAmountAndDuration_split[j].contains("Y"))
											{
												suspended_years = suspendedAmountAndDuration_split[j].split("Y")[0];
												sc.setValue("CS_Sentence_SuspendedDuration_Years_id", suspended_years);
											}else if(suspendedAmountAndDuration_split[j].contains("M"))
											{
												suspended_months = suspendedAmountAndDuration_split[j].split("M")[0];
												sc.setValue("CS_Sentence_SuspendedDuration_Months_id", suspended_months);
											}else if(suspendedAmountAndDuration_split[j].contains("W"))
											{
												suspended_weeks = suspendedAmountAndDuration_split[j].split("W")[0];
												sc.setValue("CS_Sentence_SuspendedDuration_Weeks_id", suspended_weeks);
											}else if(suspendedAmountAndDuration_split[j].contains("D"))
											{
												suspended_days = suspendedAmountAndDuration_split[j].split("D")[0];
												sc.setValue("CS_Sentence_SuspendedDuration_Days_id", suspended_days);
											}else if(suspendedAmountAndDuration_split[j].contains("H"))
											{
												suspended_hours = suspendedAmountAndDuration_split[j].split("H")[0];
												sc.setValue("CS_Sentence_SuspendedDuration_Hours_id", suspended_hours);
											}
										}
								}
									
								
								}
								
								sc.clickWhenElementIsClickable("CS_Sentence_suspendedDution_Close_id", timeOutinSeconds);
								
								if(Sentence_ConsucutiveORConcurrent[i-1].equalsIgnoreCase("Consecutive"))
								{
									sc.clickWhenElementIsClickable(driver.findElement(By.xpath("//*[@id='sentencesGrid']/div[2]/table/tbody/tr["+row+"]/td[7]/span/input[1]")), timeOutinSeconds);
								}else if(Sentence_ConsucutiveORConcurrent[i-1].equalsIgnoreCase("Concurrent"))
								{
									sc.clickWhenElementIsClickable(driver.findElement(By.xpath("//*[@id='sentencesGrid']/div[2]/table/tbody/tr["+row+"]/td[7]/span/input[2]")), timeOutinSeconds);
								}
								
								
							}
						}
					}
				}
			}
			retval = Globals.KEYWORD_PASS;
		}catch(Exception ie)
		{
			log.error("Exception occurred While Entering data in Sentence | "+ie.toString());
			ie.printStackTrace();
			throw ie;
		}
		
		return retval;
	}
	
	public static String doSDInfoNeeded() throws Exception
	{
		APP_LOGGER.startFunction("SD InfoNeeded");
		String retval=Globals.KEYWORD_FAIL;
		int timeOutinSeconds =  60;
		String tempRetval=Globals.KEYWORD_FAIL;
		
		try{
			String orderNo 			= Globals.testSuiteXLS.getCellData_fromTestData("SWestOrderID");
			String taskType 			= Globals.testSuiteXLS.getCellData_fromTestData("TaskType");
			sc.clickWhenElementIsClickable("agHomepage_tasks_link", 5);
			if(sc.waitTillElementDisplayed("tasks_criminalTasks_link", 5) == Globals.KEYWORD_PASS)
			{
				if(taskType.equalsIgnoreCase("Criminal"))
				{
					sc.clickWhenElementIsClickable("tasks_criminalTasks_link", 20);
				}else if(taskType.equalsIgnoreCase("Abuse"))
				{
					sc.clickWhenElementIsClickable("tasks_abuseTasks_link", 20);
				}else if(taskType.equalsIgnoreCase("Sanction"))
				{
					sc.clickWhenElementIsClickable("tasks_sanctionTasks_link", 20);
				}else if(taskType.equalsIgnoreCase("Civil"))
				{
					sc.clickWhenElementIsClickable("tasks_civilTasks_link", 20);
				}
			}
			sc.clickWhenElementIsClickable("View_Role_Worklist_link", 5);
			sc.clickWhenElementIsClickable("Actions_SD_InfoNeeded_link", 5);
			
			sc.clickWhenElementIsClickable("Actions_Search_btn_path", 5);
			if(sc.waitTillElementDisplayed("OrderID_id", 5) == Globals.KEYWORD_PASS)
			{
				sc.setValue("OrderID_id", orderNo);
			}
			
			sc.clickWhenElementIsClickable("Order_Search_path", timeOutinSeconds);
			if(sc.waitTillElementDisplayed("Result_Table_Result_path", 5) == Globals.KEYWORD_PASS)
			{
				if(sc.waitforElementToDisplay("Result_Table_Required_Action_path", timeOutinSeconds) == Globals.KEYWORD_PASS)
				{
					sc.clickWhenElementIsClickable("Result_Table_Required_Action_path", timeOutinSeconds);
					Thread.sleep(2000);
					ArrayList<String> tabs = new ArrayList<String>(Globals.driver.getWindowHandles());
					if(tabs.size()>1)
					{
						driver.switchTo().window(tabs.get(1));
						driver.close();
						Thread.sleep(1000);
						driver.switchTo().window(tabs.get(0));
					}
				}
				
				if(sc.waitforElementToDisplay("SDIN_CaseTable_path", timeOutinSeconds) == Globals.KEYWORD_PASS)
				{
					sc.clickWhenElementIsClickable("SDIN_CaseTable_View_path", timeOutinSeconds);
					if(sc.waitforElementToDisplay("SDIN_SmartDataJson_link", timeOutinSeconds) == Globals.KEYWORD_PASS)
					{
						Argentum ag = new Argentum();
						ag.SanitizationCheck();
					}
					
					if(sc.waitforElementToDisplay("CS_Show_path", timeOutinSeconds) == Globals.KEYWORD_PASS)
					{
						sc.clickWhenElementIsClickable("CS_Show_path", timeOutinSeconds);
						
						if(sc.waitforElementToDisplay("CS_Rule_Audit_path", timeOutinSeconds) == Globals.KEYWORD_PASS)
						{
							sc.clickWhenElementIsClickable("CS_Rule_Audit_path", timeOutinSeconds);
							String expRulesApplied = Globals.testSuiteXLS.getCellData_fromTestData("Exp_ruleName");
							int rulesRowCount = 0;
							if(sc.waitforElementToDisplay("CS_Rule_Audit_path", timeOutinSeconds) == Globals.KEYWORD_PASS)
							{
								WebElement appliedRulesTbl = Globals.driver.findElement(LocatorAccess.getLocator("rules_appliedRules_tbl"));
								rulesRowCount = sc.getRowCount_tbl(appliedRulesTbl, "./tbody/tr");
								if(rulesRowCount == 1){
									ReportUtil.STAF_ReportEvent("Fail", "State Rule", "State Rules not present.Exp-"+ expRulesApplied, 1);
								}else{
									String actRuleApplied="";
									boolean ruleFound = false;
									
									for(int k=2;k<=rulesRowCount;k++){
										actRuleApplied = appliedRulesTbl.findElement(By.xpath("./tbody/tr["+k+"]/td[1]")).getText();
										log.info("Actual Rule "+actRuleApplied+" Expcted Rule "+expRulesApplied);
										if(expRulesApplied.equalsIgnoreCase(actRuleApplied)){
											ruleFound = true;
											break;
										}
									}
									
									if(ruleFound){
										ReportUtil.STAF_ReportEvent("Pass", "State Rule", "State Rules has been applied.Exp-"+ expRulesApplied, 1);
										retval = Globals.KEYWORD_PASS;
									}else{
										ReportUtil.STAF_ReportEvent("Fail", "State Rule", "State Rules has NOT been applied.Exp-"+ expRulesApplied, 1);
									}
								}
								
								if (sc.waitforElementToDisplay("rules_close_btn",2 ).equalsIgnoreCase(Globals.KEYWORD_PASS)){
									Globals.driver.findElement(LocatorAccess.getLocator("rules_close_btn")).click();
								}
							}
						}
					}
				}
			}
		}catch(Exception ie)
		{
			throw ie;
		}
		
		return retval;
	}
	
	public static String doManualReview() throws Exception
	{
		APP_LOGGER.startFunction("Manual Review");
		String retval=Globals.KEYWORD_FAIL;
		int timeOutinSeconds =  60;
		String tempRetval=Globals.KEYWORD_FAIL;
		
		try{
			String orderNo 			= Globals.testSuiteXLS.getCellData_fromTestData("SWestOrderID");
			String taskType 			= Globals.testSuiteXLS.getCellData_fromTestData("TaskType");
			sc.clickWhenElementIsClickable("agHomepage_tasks_link", 5);
			if(sc.waitTillElementDisplayed("tasks_criminalTasks_link", 5) == Globals.KEYWORD_PASS)
			{
				if(taskType.equalsIgnoreCase("Criminal"))
				{
					sc.clickWhenElementIsClickable("tasks_criminalTasks_link", 20);
				}else if(taskType.equalsIgnoreCase("Abuse"))
				{
					sc.clickWhenElementIsClickable("tasks_abuseTasks_link", 20);
				}else if(taskType.equalsIgnoreCase("Sanction"))
				{
					sc.clickWhenElementIsClickable("tasks_sanctionTasks_link", 20);
				}else if(taskType.equalsIgnoreCase("Civil"))
				{
					sc.clickWhenElementIsClickable("tasks_civilTasks_link", 20);
				}
			}
			sc.clickWhenElementIsClickable("View_Role_Worklist_link", 5);
			sc.clickWhenElementIsClickable("Actions_Manual_Review_link", 5);
			
			sc.clickWhenElementIsClickable("Actions_Search_btn_path", 5);
			if(sc.waitTillElementDisplayed("OrderID_id", 5) == Globals.KEYWORD_PASS)
			{
				sc.setValue("OrderID_id", orderNo);
			}
			
			sc.clickWhenElementIsClickable("Order_Search_path", timeOutinSeconds);
			
			if(sc.waitTillElementDisplayed("Result_Table_Result_path", 5) == Globals.KEYWORD_PASS)
			{
				sc.clickWhenElementIsClickable("Select_ALL_path", 5);
				sc.clickWhenElementIsClickable("Claim_Selected_path", timeOutinSeconds);
				
				sc.clickWhenElementIsClickable("MR_ClaimTask_Yes_path", timeOutinSeconds);
				Thread.sleep(5000);
				if(sc.waitTillElementDisplayed("View_My_WorkList_link", 20) == Globals.KEYWORD_PASS)
				{
					sc.clickWhenElementIsClickable("View_My_WorkList_link", timeOutinSeconds);
				}
				
				sc.clickWhenElementIsClickable("Actions_Manual_Review_link", 5);
				
				sc.clickWhenElementIsClickable("Actions_Search_btn_path", 5);
				if(sc.waitTillElementDisplayed("OrderID_id", 5) == Globals.KEYWORD_PASS)
				{
					sc.setValue("OrderID_id", orderNo);
				}
				
				sc.clickWhenElementIsClickable("Order_Search_path", timeOutinSeconds);
				
				if(sc.waitforElementToDisplay("Result_Table_Required_Action_path", timeOutinSeconds) == Globals.KEYWORD_PASS)
				{
					sc.clickWhenElementIsClickable("Result_Table_Required_Action_path", timeOutinSeconds);
					Thread.sleep(2000);
					ArrayList<String> tabs = new ArrayList<String>(Globals.driver.getWindowHandles());
					if(tabs.size()>1)
					{
						driver.switchTo().window(tabs.get(1));
						driver.close();
						Thread.sleep(1000);
						driver.switchTo().window(tabs.get(0));
					}
				}
				
				sc.clickWhenElementIsClickable("ER_ValidAndSave_Record_id", timeOutinSeconds);
				
				if(sc.waitforElementToDisplay("ER_ContinueAndConfirmation_path", 30) == Globals.KEYWORD_PASS)
				{
					sc.clickWhenElementIsClickable("ER_ContinueAndConfirmation_path", 20);
				}
				
				sc.clickWhenElementIsClickable("ER_CompleteTask_id", timeOutinSeconds);
				sc.clickWhenElementIsClickable("ER_CompleteTask_Confirmation_Yes_id", timeOutinSeconds);
				
				sc.waitTillElementDisplayed("ER_NewReportableInformation_id", timeOutinSeconds);
				if(sc.waitTillElementDisplayed("ER_NewReportableInformation_id", timeOutinSeconds) == Globals.KEYWORD_PASS)
				{
					sc.clickWhenElementIsClickable("ER_NewReportableInformation_id", timeOutinSeconds);
					sc.checkCheckBox("ER_CheckBoxCheck_id");
					sc.clickWhenElementIsClickable("ER_KnowHit_Yes_id", timeOutinSeconds);
				}
				if (sc.waitforElementToDisplay("agHomepage_tasks_link",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_FAIL)){
					throw new Exception("Tasks link in Side menu is not visible");
				}
				
				sc.clickWhenElementIsClickable("agHomepage_tasks_link",(int) timeOutinSeconds  );
				if (sc.waitforElementToDisplay("tasks_orderSearch_link",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_FAIL)){
					throw new Exception("Order Search link in Side menu is not visible.Please check user rights");
				}
				
				
				sc.clickWhenElementIsClickable("tasks_orderSearch_link",(int) timeOutinSeconds  );
				if (sc.waitforElementToDisplay("orderSearch_orderNumber_txt",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_FAIL)){
					throw new Exception("Order Search field in Searh Page is not visible.");
				}
				
								
				tempRetval = sc.setValue("orderSearch_orderNumber_txt", orderNo);
				if (tempRetval.equalsIgnoreCase(Globals.KEYWORD_FAIL)){
					throw new Exception("Unable to set the Order Number in Order Search field");
				}
				
				sc.clickWhenElementIsClickable("orderSearch_search_btn",(int) timeOutinSeconds  );
				if (sc.waitforElementToDisplay("orderSearch_resultsHdr_tbl",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_FAIL)){
					throw new Exception("Order results grid is not displayed");
				}
				
				retval = Globals.KEYWORD_PASS;
				
			}
		}catch(Exception ie)
		{
			throw ie;
		}
		
		return retval;
	}
	
	
	public static String searchOrder(String orderNo,int maxTimeoutInHrs) throws Exception{
		
		String retval			=	Globals.KEYWORD_FAIL;
		String tempRetval		=	Globals.KEYWORD_FAIL;
		long timeOutinSeconds 	=	60;

		
					
			try{
				Globals.driver.switchTo().defaultContent();
				
				if (sc.waitforElementToDisplay("agHomepage_tasks_link",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_FAIL)){
					throw new Exception("Tasks link in Side menu is not visible");
				}
				
				sc.clickWhenElementIsClickable("agHomepage_tasks_link",(int) timeOutinSeconds  );
				if (sc.waitforElementToDisplay("tasks_orderSearch_link",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_FAIL)){
					throw new Exception("Order Search link in Side menu is not visible.Please check user rights");
				}
				
				
				sc.clickWhenElementIsClickable("tasks_orderSearch_link",(int) timeOutinSeconds  );
				if (sc.waitforElementToDisplay("orderSearch_orderNumber_txt",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_FAIL)){
					throw new Exception("Order Search field in Searh Page is not visible.");
				}
				
								
				tempRetval = sc.setValue("orderSearch_orderNumber_txt", orderNo);
				if (tempRetval.equalsIgnoreCase(Globals.KEYWORD_FAIL)){
					throw new Exception("Unable to set the Order Number in Order Search field");
				}
				
				sc.clickWhenElementIsClickable("orderSearch_search_btn",(int) timeOutinSeconds  );
				if (sc.waitforElementToDisplay("orderSearch_resultsHdr_tbl",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_FAIL)){
					throw new Exception("Order results grid is not displayed");
				}
				
				
				WebElement resultTbl 	=	null;
				int rowCount 			= 	-1;
				
				boolean resultFound		=	false;
				int counter				=   0;
				int manualReviewCount   =   0;
				int maxTimeoutInMin		=	maxTimeoutInHrs * 60;
				int pollingInterval		= 	1;
				String ordertempStatus	=	"";
				String SD_IN_Flow = Globals.testSuiteXLS.getCellData_fromTestData("SD_IN_Flow");
				
				tempRetval = sc.waitforElementToDisplay("orderSearch_loadingOrder_img",10);
				
				while(tempRetval.equalsIgnoreCase(Globals.KEYWORD_PASS)){
					Thread.sleep(1000);
				}
				
				resultTbl				=	sc.createWebElement("orderSearch_resultsData_tbl");
				rowCount				= 	sc.getRowCount_tbl(resultTbl, "./tbody/tr");
				while (!resultFound && counter <= maxTimeoutInMin) {
					if(rowCount == 1){
						resultFound = false;						
					}else{
						if(resultTbl.findElements(By.xpath("./tbody/tr[2]/td[15]/a")).size()==1){
							resultFound = true;
						}else {
							ordertempStatus = resultTbl.findElement(By.xpath("./tbody/tr[2]/td[10]")).getText();
							log.info("Order status is :"+ordertempStatus);
							if(ordertempStatus.equalsIgnoreCase("SD InfoNeeded") || ordertempStatus.equalsIgnoreCase("In Progress")){
								if(SD_IN_Flow.equalsIgnoreCase("Yes"))
								{
									String sdInfoStatus = Argentum.doSDInfoNeeded();
									if(sdInfoStatus == Globals.KEYWORD_PASS)
									{
										resultFound	= true;
										retval = Globals.KEYWORD_PASS;
										ReportUtil.STAF_ReportEvent("Pass", "OrderID-"+orderNo, "Order is "+ordertempStatus +".SD INFO verification Process is done.", 1);
									}else{
										resultFound	= true;
										retval = Globals.KEYWORD_PASS;
										ReportUtil.STAF_ReportEvent("Fail", "OrderID-"+orderNo, "Order is "+ordertempStatus +".SD INFO verification Process Failed.", 1);
					
									}
								}else{
									resultFound	= true;
									ReportUtil.STAF_ReportEvent("Fail", "OrderID-"+orderNo, "Order is present in "+ordertempStatus +".Unable to proceed further.", 1);
									throw new Exception("OrderID-"+orderNo+" is present in "+ordertempStatus +".Unable to proceed further.");
								}
								
							}else if(ordertempStatus.equalsIgnoreCase("Manual Review") && manualReviewCount == 0){
								manualReviewCount++;
								String manualReviewStatus = doManualReview();
								resultFound = false;
								ReportUtil.STAF_ReportEvent("Pass", "OrderID-"+orderNo, "Order is present in "+ordertempStatus +".Manual Review Process is done.", 1);
							}
							else{
							
						
								resultFound = false;
							}
						
						}
					}
					
					if(!resultFound){
						Globals.log.info("No order found yet for order id-"+orderNo+" . Waiting for 60 seconds before searching again.........." );
						Thread.sleep(1000*60*pollingInterval);
						sc.clickWhenElementIsClickable("orderSearch_search_btn",(int) timeOutinSeconds  );
						Thread.sleep(60000);
						resultTbl				=	sc.createWebElement("orderSearch_resultsData_tbl");
						rowCount				= 	sc.getRowCount_tbl(resultTbl, "./tbody/tr");
						counter 				=	counter + pollingInterval;
					}
					else{
						break;
					}
				}
				
				
				
				if(resultFound && !SD_IN_Flow.equalsIgnoreCase("Yes")){
					ReportUtil.STAF_ReportEvent("Pass", "OrderID-"+orderNo, rowCount-1 +"-Results found for order search", 1);
					
					resultTbl.findElement(By.xpath("./tbody/tr[2]/td[15]/a")).click();
					
					retval = Globals.KEYWORD_PASS;			
				}else if(SD_IN_Flow.equalsIgnoreCase("Yes"))
				{
					retval = Globals.KEYWORD_PASS;		
				}else if(counter == maxTimeoutInMin && ordertempStatus.equalsIgnoreCase("SD Processing")){
					retval = Globals.KEYWORD_PASS;
				}else {
					
					throw new Exception("OrderID-"+orderNo+"No results found for order search");
					
				}
				
				
			}catch(Exception e){
				
				throw e;
			}
			
		return retval;
	}
	
	/**
	 * @return
	 * @throws Exception
	 */
	public static String verifyStateRule() throws Exception {
		Globals.Component = Thread.currentThread().getStackTrace()[1].getMethodName();
		APP_LOGGER.startFunction(Globals.Component);
		long timeOutinSeconds 	=	60;
	
		
			try{
				
				String tcDesc 			= Globals.testSuiteXLS.getCellData_fromTestData("TestCaseDescr");
				Globals.Component 		= tcDesc;
				String orderNo 			= Globals.testSuiteXLS.getCellData_fromTestData("SWestOrderID");
				int maxTimeoutInHrs		=	(int)Double.parseDouble(Globals.testSuiteXLS.getCellData_fromTestData("Max_Timeout_inHrs"));
				
				String orderCreationStatus =   Globals.testSuiteXLS.getCellData_fromTestData("CurrrentOrderCreationStatus");
				if(orderCreationStatus.equalsIgnoreCase(Globals.KEYWORD_FAIL)){
					ReportUtil.STAF_ReportEvent("Fail", tcDesc, "State Rule verification has been skipped as Order creation had been failed", 0);
					throw new Exception("State Rule verification has been skipped as Order creation had been failed");
				}
				String expCaseNumber 	=  	Globals.testSuiteXLS.getCellData_fromTestData("Exp_CaseNumber");
				String lName =  	Globals.testSuiteXLS.getCellData_fromTestData("LastName");
				String fName =    	Globals.testSuiteXLS.getCellData_fromTestData("FirstName");
				String salRange =Globals.testSuiteXLS.getCellData_fromTestData("SalaryRange");
				String jurisState =Globals.testSuiteXLS.getCellData_fromTestData("JurisState");
				String jurisCounty =Globals.testSuiteXLS.getCellData_fromTestData("JurisValue");
				String checkSentences = Globals.testSuiteXLS.getCellData_fromTestData("AddSentence");
				
				Globals.EmailParam.put("OrderID", orderNo);
				Globals.EmailParam.put("LastName", lName);
				Globals.EmailParam.put("FirstName", fName);
				Globals.EmailParam.put("SalaryRange", salRange);
				Globals.EmailParam.put("Juris", jurisCounty +"-"+jurisState);
				Globals.EmailParam.put("Exp-CaseNumber(s)", expCaseNumber);
				String SD_IN_Flow = Globals.testSuiteXLS.getCellData_fromTestData("SD_IN_Flow");
				
				
				if(searchOrder(orderNo,maxTimeoutInHrs).equalsIgnoreCase(Globals.KEYWORD_FAIL)){
					throw new Exception("State Rule verification has been skipped as Order search in AG  has been failed");
				}else{
					
					WebElement resultTbl				=	sc.createWebElement("orderSearch_resultsData_tbl");
					int rowCount				= 	sc.getRowCount_tbl(resultTbl, "./tbody/tr");
					
					String ordertempStatus = resultTbl.findElement(By.xpath("./tbody/tr[2]/td[10]")).getText();
					log.info("Order status is :"+ordertempStatus);
					
					if(SD_IN_Flow.equalsIgnoreCase("Yes"))
					{
						log.info("SD INFONEEDED Process Executed");
					}else if(ordertempStatus.equalsIgnoreCase("SD Processing")){
						log.info("Too much time took in SD Processing ...");
					}else{
					
						
					
						ArrayList<String> tabs = new ArrayList<String>(Globals.driver.getWindowHandles());
						Globals.driver.switchTo().window(tabs.get(1));
						
						
						if (sc.waitforElementToDisplay("crimResults_header_txt",timeOutinSeconds ).equalsIgnoreCase(Globals.KEYWORD_FAIL)){
							throw new Exception("Criminal Results Page is not visible.");
						}
						
						
						String[] expCaseNumbers 				=	expCaseNumber.split(";");
						String[] expCharges		 				=		Globals.testSuiteXLS.getCellData_fromTestData("Exp_Charges").split("#NewCase#");
						String[] expSentences                   =   Globals.testSuiteXLS.getCellData_fromTestData("Exp_Sentences").split("###");
						String expRulesApplied					=	"";
		//				String actLevel							=	"";
						boolean caseFound						=	false;
						String[] arrExpCharge					= 	null; 
						String expLevel							= 	null;
						String expCharge						= 	null;
						int noOfCases 							=   expCaseNumbers.length;
						String actCaseNumber					=	"";
						List<WebElement> courtRecordDetailsTbls	=	null;
						List<WebElement> chargesTbls			=	null;
						List<WebElement> sentenceTbls           =   null;
						courtRecordDetailsTbls					=	driver.findElements(By.xpath("//strong[contains(text(),'Docket Number')]/ancestor::table[1]"));
						chargesTbls								=	driver.findElements(By.xpath("//a[text()='View Rules']/ancestor::table[1]"));
						
						if(checkSentences.equalsIgnoreCase("Yes"))
						{
							sentenceTbls                            =   driver.findElements(By.xpath("//i[text()='Sentence Type: ']/ancestor::table[1]"));
						}
						WebElement parentTbl        			= 	null;
						
						if(noOfCases != expCharges.length ){
							throw new Exception(" Test Data Error as no of data present in Cases/Charges/RulesApplied/Levels are different");
						}
						
						for(int i = 1; i <= noOfCases ; i++){
							int j=0;
							for ( ; j < courtRecordDetailsTbls.size() ; j++) {
								parentTbl 		=	courtRecordDetailsTbls.get(j); 
								actCaseNumber 	= 	parentTbl.findElement(By.xpath("./tbody/tr[5]/td[2]")).getText();
								log.info("Exp Case No."+expCaseNumbers[i-1]+" and "+"Act Case No."+actCaseNumber);
								
								if(expCaseNumbers[i-1].equals(actCaseNumber)){
									sc.scrollIntoView(parentTbl.findElement(By.xpath("./tbody/tr[5]/td[2]")));
									caseFound 	= true;
									break;
									
								}else{
									sc.scrollIntoView(parentTbl.findElement(By.xpath("./tbody/tr[5]/td[2]")));
									caseFound 	= true;
									break;
								}
							}
							
							if(caseFound){
								ReportUtil.STAF_ReportEvent("Pass", "Case Number", "Case Number found in Results page-"+actCaseNumber, 1);
								
		//						chrge level
		//						actLevel = parentTbl.findElement(By.xpath("./tbody/tr[2]/td[2]")).getText();
		//						if(expLevels[i-1].equals(actLevel)){
		//							GenUtils.STAF_ReportEvent("Pass", "Record Level", "Record Level matches as Expected-"+expLevels[i-1], 1);
		//						}else{
		//							GenUtils.STAF_ReportEvent("Fail", "Record Level", "Record Level does not match with Expected.Exp-"+expLevels[i-1] + " Actual-"+actLevel, 1);
		//						}
								
								//charges
								int chargeRowCount	= sc.getRowCount_tbl(chargesTbls.get(j), "./tbody/tr");
								String actCharge 	= "";
								String actChargeLevel = "";
								String actReportDecision = "";
								
								boolean chargeFound = false;
								WebElement eleCell = null;
								String chargeText ="";
								arrExpCharge			= expCharges[i-1].split("###");
								 	
								
								for(int expChargeIter = 1 ;expChargeIter<=arrExpCharge.length;expChargeIter++){
									int iter = 1;
									//for(; iter<chargeRowCount ; iter++){
										
										expLevel 			=	get_parameterValue_fromCharges(arrExpCharge[expChargeIter-1],"Charge Level:");
										eleCell 			= 	chargesTbls.get(j).findElement(By.xpath("./tbody/tr["+expChargeIter+"]/td[2]"));
										chargeText			=	eleCell.getText();
										log.info("Actual Charge Text"+" "+chargeText);
										actChargeLevel 		=	get_parameterValue_fromCharges(chargeText,"Charge Level:"); 	
									
																		
										expCharge = get_parameterValue_fromCharges(arrExpCharge[expChargeIter-1],"Charge:");
										actCharge = get_parameterValue_fromCharges(chargeText,"Charge:");
										
										if(expCharge.trim().equals(actCharge) && expLevel.trim().equals(actChargeLevel)){
											chargeFound = true;
											//break;
										}else{
											chargeFound = false;
										}
									
									if(chargeFound){
										sc.scrollIntoView(eleCell);
										
										ReportUtil.STAF_ReportEvent("Pass", "Charge Level", "Charge Level matches as Expected-"+actChargeLevel, 1);
										ReportUtil.STAF_ReportEvent("Pass", "Case Charge", "Charge: matches as Expected-"+actCharge, 1);
										
										//report decision
										String formattedReporDecision="";
										String expReportDecision ="";
										
										expReportDecision = 	get_parameterValue_fromCharges(arrExpCharge[expChargeIter-1],"Report Decision:");
										actReportDecision 	=	get_parameterValue_fromCharges(chargeText,"Report Decision:");
										
		
										if(expReportDecision.equals(actReportDecision)){
											
											ReportUtil.STAF_ReportEvent("Pass", "Report Decision", "Report Decision matches as Expected-"+actReportDecision, 1);
										}else{
											ReportUtil.STAF_ReportEvent("Fail", "Report Decision", "Report Decision not match with Expected.Exp-"+ expReportDecision + " Actual-"+actReportDecision, 1);
										}
										
										//view rules validations
										eleCell.findElement(By.xpath("./a")).click();
										
															
										if (sc.waitforElementToDisplay("rules_appliedRules_tbl",timeOutinSeconds ).equalsIgnoreCase(Globals.KEYWORD_FAIL)){
											ReportUtil.STAF_ReportEvent("Fail", "State Rule", "State Rules Pop Up has not been displayed", 1);
											throw new Exception("Applied State Rules Pop Up is not displayed.");
										}
										
										expRulesApplied = get_parameterValue_fromCharges(arrExpCharge[expChargeIter-1],"View Rules");
										
										int rulesRowCount = 0;
										WebElement appliedRulesTbl = Globals.driver.findElement(LocatorAccess.getLocator("rules_appliedRules_tbl"));
										rulesRowCount = sc.getRowCount_tbl(appliedRulesTbl, "./tbody/tr");
										if(rulesRowCount == 1){
											ReportUtil.STAF_ReportEvent("Fail", "State Rule", "State Rules not present.Exp-"+ expRulesApplied, 1);
										}else{
											String actRuleApplied="";
											boolean ruleFound = false;
											
											for(int k=2;k<=rulesRowCount;k++){
												actRuleApplied = appliedRulesTbl.findElement(By.xpath("./tbody/tr["+k+"]/td[1]")).getText();
												log.info("Actual Rule "+actRuleApplied+" Expcted Rule "+expRulesApplied);
												if(expRulesApplied.equalsIgnoreCase(actRuleApplied)){
													ruleFound = true;
													break;
												}
											}
											
											if(ruleFound){
												ReportUtil.STAF_ReportEvent("Pass", "State Rule", "State Rules has been applied.Exp-"+ expRulesApplied, 1);
												
											}else{
												ReportUtil.STAF_ReportEvent("Fail", "State Rule", "State Rules has NOT been applied.Exp-"+ expRulesApplied, 1);
											}
										}
										
										if (sc.waitforElementToDisplay("rules_close_btn",2 ).equalsIgnoreCase(Globals.KEYWORD_PASS)){
											Globals.driver.findElement(LocatorAccess.getLocator("rules_close_btn")).click();
										}
										
									}else{
										ReportUtil.STAF_ReportEvent("Fail", "Charge Level/Case Charge", "Charge Level/Charge does not match with Expected.Exp-"+ expLevel +"/"+expCharge , 1);
									}
								//}
									
								}
							
								if(checkSentences.equalsIgnoreCase("Yes"))
								{
									int sentenceRowCnt = sc.getRowCount_tbl(sentenceTbls.get(j), "./tbody/tr");
									WebElement eleCellsentence = null;
									String sentenceText = null;
									int count =0;
									for(int k=1;k<=expSentences.length;k++)
									{
										eleCellsentence 			= 	sentenceTbls.get(j).findElement(By.xpath("./tbody/tr["+k+"]/td[2]"));
										sc.scrollIntoView(eleCell);
										sentenceText			=	eleCellsentence.getText();
										log.info("Sentence is : "+sentenceText);
										log.info("Sentence is : "+expSentences[k-1]);
										if(sentenceText.equalsIgnoreCase(expSentences[k-1]))
										{
											count++;
										}
									}
									if(count == expSentences.length)
									{
										ReportUtil.STAF_ReportEvent("Pass", "Sentences", "Sentences got matched", 1);
									}else{
										ReportUtil.STAF_ReportEvent("Fail", "Sentences", "Sentences not matched", 1);
									}
								}
								Argentum ag = new Argentum();
								if(!Globals.EXECUTION_MACHINE.equalsIgnoreCase("jenkins"))
								{
									ag.SanitizationCheck();
								}
								
								
								
								
								
							}else{
								ReportUtil.STAF_ReportEvent("Fail", "Case Number", "Case Number Not found in Results page-"+actCaseNumber, 1);
							}
						}
						
						
						Globals.driver.close();
						tabs= null;
						tabs = new ArrayList<String>(Globals.driver.getWindowHandles());
						Globals.driver.switchTo().window(tabs.get(0));
						
					}
				
				}
				
			}catch(Exception e){
				
				throw e;
			}
			
		
		return Globals.KEYWORD_PASS;
	}
	
	public static String get_parameterValue_fromCharges(String inputText,String parameterName){
		
		String[] paramVal =inputText.split(";");
		String retval = "";
		
		for(int i=0; i<paramVal.length;i++){
			if(paramVal[i].contains(parameterName)){
				int count = StringUtils.countMatches(paramVal[i], ":");
				if(count >= 2){
					retval 			= 	paramVal[i].trim().split(":",2)[1].trim();
				}else{
					retval 			= 	paramVal[i].trim().split(":")[1].trim();	
				}
				
				break;
				
			}
		}
		return retval;		
	}
	
	public   String SanitizationCheck() throws Exception
	{
		APP_LOGGER.startFunction("Assign Task");
		String retval=Globals.KEYWORD_FAIL;
		int timeOutinSeconds =  60;
		String tempRetval=Globals.KEYWORD_FAIL;
		
		try{
			 Globals.testSuiteXLS.setCellData_inTestData("SmartData_Json", "");
			 String SD_IN_Check = Globals.testSuiteXLS.getCellData_fromTestData("SD_IN_Flow");
			 if(SD_IN_Check.equalsIgnoreCase("Yes"))
			 {
				 sc.scrollIntoView("SDIN_SmartDataJson_link");
				 sc.clickWhenElementIsClickable("SDIN_SmartDataJson_link", timeOutinSeconds);
			 }else{
				 sc.scrollIntoView("VSR_ViewJsonResult_link");
				 sc.clickWhenElementIsClickable("VSR_ViewJsonResult_link", timeOutinSeconds);
			 }
			 Thread.sleep(10000);
			 
			 ArrayList<String> tabs = new ArrayList<String>(Globals.driver.getWindowHandles());
			 driver.switchTo().window(tabs.get(tabs.size()-1));
				
				 driver.manage().window().maximize();
			        Thread.sleep(5000);
			        Robot robot = new Robot();
			        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			        
			        robot.keyPress(KeyEvent.VK_CONTROL);
			        robot.keyPress(KeyEvent.VK_A);
			        robot.keyRelease(KeyEvent.VK_A);
			        robot.keyRelease(KeyEvent.VK_CONTROL);
			        Thread.sleep(5000);
			        
			        System.out.println("CTRL+A is done");
			        
			        robot.keyPress(KeyEvent.VK_CONTROL);
			        robot.keyPress(KeyEvent.VK_C);
			        robot.keyRelease(KeyEvent.VK_C);
			        robot.keyRelease(KeyEvent.VK_CONTROL);
			        
			        System.out.println("CTRL+C is done");
			        
			        driver.close();
			       
			        Transferable t = clipboard.getContents(this);
			        String jsonRequired = null;
			        
			        if (t == null)
			        	jsonRequired = null;
			    		
			        try {
			        	jsonRequired = (String) t.getTransferData(DataFlavor.stringFlavor);
			        	System.out.println(jsonRequired);
			        } catch (Exception e){
			            e.printStackTrace();
			        }
			        jsonRequired = jsonRequired.replace("\n", "");
			        System.out.println(jsonRequired);
			        jsonRequired = jsonRequired.split("Smart Data JSON\"")[1];
			        jsonRequired = jsonRequired.split("\"Close")[0];
			        
			        if(jsonRequired!=null)
			        {
			        	Globals.testSuiteXLS.setCellData_inTestData("SmartData_Json", jsonRequired);
			        }
			        
			        driver.switchTo().window(tabs.get(tabs.size()-2));
			        Thread.sleep(5000);
			        
			      
			      retval = Globals.KEYWORD_PASS;
			
		}catch(Exception e)
		{
			log.error("Exception occurred in While Verifying Sanitization | "+e.toString());
			e.printStackTrace();
			throw e;
		}
		
		return retval;
	}
	
	public static String checkProbationStatus()
	{
		APP_LOGGER.startFunction("Check Probation Status");
		String retval=Globals.KEYWORD_FAIL;
		int timeOutinSeconds =  60;
		String tempRetval=Globals.KEYWORD_FAIL;
		try{
			String smartDataJson = Globals.testSuiteXLS.getCellData_fromTestData("SmartData_Json");
			Map<String, String> chargeDispositionMap = new HashMap<String,String>();
			chargeDispositionMap = DeserializeJsonData.probationAndWarrantStatusData(smartDataJson);
			String DispositionDate = Globals.testSuiteXLS.getCellData_fromTestData("Disposition_Date");
			String probationYr = Globals.testSuiteXLS.getCellData_fromTestData("Sentence_AmountAndDuration");
			int dispositionYear = Integer.parseInt(DispositionDate.split("/")[2]);
			int probationYear = Integer.parseInt(probationYr.split("Y")[0]);
			
			int today = LocalDate.now().getYear();
			if((dispositionYear+probationYear) >= today)
			{
				if(chargeDispositionMap.get("IsProbationActive_1_1").equalsIgnoreCase("true"))
				{
					ReportUtil.STAF_ReportEvent("Pass", "Probation Status", "Actual = "+chargeDispositionMap.get("IsProbationActive_1_1")+" and Expected = "+"true", 1);
				}else
				{
					ReportUtil.STAF_ReportEvent("Fail", "Probation Status", "Actual = "+chargeDispositionMap.get("IsProbationActive_1_1")+" and Expected = "+"false", 1);
				}
			}else{
				if(chargeDispositionMap.get("IsProbationActive_1_1").equalsIgnoreCase("false"))
				{
					ReportUtil.STAF_ReportEvent("Pass", "Probation Status", "Actual = "+chargeDispositionMap.get("IsProbationActive_1_1")+" and Expected = "+"false", 1);
				}else
				{
					ReportUtil.STAF_ReportEvent("Fail", "Probation Status", "Actual = "+chargeDispositionMap.get("IsProbationActive_1_1")+" and Expected = "+"true", 1);
				}
			}
			retval = Globals.KEYWORD_PASS;
		}catch(Exception ie)
		{
			
		}
		
		return retval;
	}
	
	public static String checkWarrantStatus()
	{
		APP_LOGGER.startFunction("Check Warrant Status");
		String retval=Globals.KEYWORD_FAIL;
		int timeOutinSeconds =  60;
		String tempRetval=Globals.KEYWORD_FAIL;
		
		try{
			String smartDataJson = Globals.testSuiteXLS.getCellData_fromTestData("SmartData_Json");
			Map<String, String> chargeDispositionMap = new HashMap<String,String>();
			chargeDispositionMap = DeserializeJsonData.probationAndWarrantStatusData(smartDataJson);
			String WarrantDate = Globals.testSuiteXLS.getCellData_fromTestData("Warrant_Date");
			
			Period p = null;
			LocalDate today = LocalDate.now();
			
			
			if(!chargeDispositionMap.get("warrantDate_1_1").equalsIgnoreCase(""))
			{
				LocalDate localdateWarrant = LocalDate.of(Integer.parseInt(WarrantDate.split("/")[2]), Integer.parseInt(WarrantDate.split("/")[0]), Integer.parseInt(WarrantDate.split("/")[1]));
				String expWarrantDate = WarrantDate.split("/")[2]+"-"+WarrantDate.split("/")[0]+"-"+WarrantDate.split("/")[1];
				p = Period.between(localdateWarrant, today);
				if(chargeDispositionMap.get("warrantDate_1_1").equalsIgnoreCase(expWarrantDate))
				{
					ReportUtil.STAF_ReportEvent("Pass", "Warrant Date", "Actual = "+chargeDispositionMap.get("warrantDate_1_1")+" and Expected = "+expWarrantDate, 1);
				}else{
					ReportUtil.STAF_ReportEvent("Fail", "Warrant Date", "Actual = "+chargeDispositionMap.get("warrantDate_1_1")+" and Expected = "+expWarrantDate, 1);
				}
			}
			
			if(!chargeDispositionMap.get("warrantDate_1_1").equalsIgnoreCase(""))
			{
				if(chargeDispositionMap.get("ageOfWarrant_1_1").equalsIgnoreCase(String.valueOf(p.getYears())))
				{
					ReportUtil.STAF_ReportEvent("Pass", "Age Of Warrant", "Actual = "+chargeDispositionMap.get("ageOfWarrant_1_1")+" and Expected = "+p.getYears(), 1);
				}else{
					ReportUtil.STAF_ReportEvent("Fail", "Age Of Warrant", "Actual = "+chargeDispositionMap.get("ageOfWarrant_1_1")+" and Expected = "+p.getYears(), 1);
				}
			}else{
				if(chargeDispositionMap.get("ageOfWarrant_1_1").equalsIgnoreCase("-1"))
				{
					ReportUtil.STAF_ReportEvent("Pass", "Age Of Warrant", "Actual = "+chargeDispositionMap.get("ageOfWarrant_1_1")+" and Expected = "+"-1", 1);
				}else{
					ReportUtil.STAF_ReportEvent("Fail", "Age Of Warrant", "Actual = "+chargeDispositionMap.get("ageOfWarrant_1_1")+" and Expected = "+"-1", 1);
				}
			}
			retval = Globals.KEYWORD_PASS;
		}catch(Exception ie)
		{
			
		}
		
		return retval;
		
	}
	
	public static String chargeDispositionSanitization()
	{
		APP_LOGGER.startFunction("Charge and Disposition Sanitization");
		String retval=Globals.KEYWORD_FAIL;
		int timeOutinSeconds =  60;
		String tempRetval=Globals.KEYWORD_FAIL;
		
		try{
			
			String smartDataJson = Globals.testSuiteXLS.getCellData_fromTestData("SmartData_Json");
			Map<String, String> chargeDispositionMap = new HashMap<String,String>();
			chargeDispositionMap = DeserializeJsonData.caseChargeData(smartDataJson);
			String Sanitized_ChargeDescription = Globals.testSuiteXLS.getCellData_fromTestData("Sanitized_ChargeDescription");
			String Sanitized_ChargeLevel = Globals.testSuiteXLS.getCellData_fromTestData("Sanitized_ChargeLevel");
			String Sanitized_chargeCategory = Globals.testSuiteXLS.getCellData_fromTestData("Sanitized_chargeCategory");
			String Sanitized_DispositionType = Globals.testSuiteXLS.getCellData_fromTestData("Sanitized_DispositionType");
			String Sanitized_DispositionDescription = Globals.testSuiteXLS.getCellData_fromTestData("Sanitized_DispositionDescription");
			String dispositionSanitizationLevel = Globals.testSuiteXLS.getCellData_fromTestData("dispositionSanitizationLevel");
			String chargeSanitizationLevel = Globals.testSuiteXLS.getCellData_fromTestData("chargeSanitizationLevel");
			
			
			String noOfRules = Globals.testSuiteXLS.getCellData_fromTestData("Exp_NoOf_Rule");
			int rulesNumber = Integer.parseInt(noOfRules);
			
			String [] Exp_ruleName = Globals.testSuiteXLS.getCellData_fromTestData("Exp_ruleName").split(",");
			String [] Exp_ruleType = Globals.testSuiteXLS.getCellData_fromTestData("Exp_ruleType").split(",");
			String Exp_ruleDecision = Globals.testSuiteXLS.getCellData_fromTestData("Exp_ruleDecision");
			
			String NumberOfCharges = Globals.testSuiteXLS.getCellData_fromTestData("NoOfCharges");
			int row = Integer.parseInt(NumberOfCharges);
			
			for(int i=1;i<=row;i++)
			{
				if(chargeDispositionMap.get("saniztizedChargeDescription_1_"+(i)).equalsIgnoreCase(Sanitized_ChargeDescription))
				{
					ReportUtil.STAF_ReportEvent("Pass", "Sanitization Charge Description", "Actual = "+chargeDispositionMap.get("saniztizedChargeDescription_1_"+(i))+" and Expected = "+Sanitized_ChargeDescription, 1);
				}else
				{
					ReportUtil.STAF_ReportEvent("Fail", "Sanitization Charge Description", "Actual = "+chargeDispositionMap.get("saniztizedChargeDescription_1_"+(i))+" and Expected = "+Sanitized_ChargeDescription, 1);
				}
				
				if(chargeDispositionMap.get("sanitizedChargeLevel_1_"+(i)).equalsIgnoreCase(Sanitized_ChargeLevel))
				{
					ReportUtil.STAF_ReportEvent("Pass", "Sanitization Charge Level", "Actual = "+chargeDispositionMap.get("sanitizedChargeLevel_1_"+(i))+" and Expected = "+Sanitized_ChargeLevel, 1);
				}else
				{
					ReportUtil.STAF_ReportEvent("Fail", "Sanitization Charge Level", "Actual = "+chargeDispositionMap.get("sanitizedChargeLevel_1_"+(i))+" and Expected = "+Sanitized_ChargeLevel, 1);
				}
				
				if(chargeDispositionMap.get("chargeCategory_1_"+(i)).equalsIgnoreCase(Sanitized_chargeCategory))
				{
					ReportUtil.STAF_ReportEvent("Pass", "Sanitization Charge Category", "Actual = "+chargeDispositionMap.get("chargeCategory_1_"+(i))+" and Expected = "+Sanitized_chargeCategory, 1);
				}else
				{
					ReportUtil.STAF_ReportEvent("Fail", "Sanitization Charge Category", "Actual = "+chargeDispositionMap.get("chargeCategory_1_"+(i))+" and Expected = "+Sanitized_chargeCategory, 1);
				}
				
				if(chargeDispositionMap.get("sanitizedDispositionType_1_"+(i)).equalsIgnoreCase(Sanitized_DispositionType))
				{
					ReportUtil.STAF_ReportEvent("Pass", "Sanitization Disposition Type", "Actual = "+chargeDispositionMap.get("sanitizedDispositionType_1_"+(i))+" and Expected = "+Sanitized_DispositionType, 1);
				}else
				{
					ReportUtil.STAF_ReportEvent("Fail", "Sanitization Disposition Type", "Actual = "+chargeDispositionMap.get("sanitizedDispositionType_1_"+(i))+" and Expected = "+Sanitized_DispositionType, 1);
				}
				
				if(chargeDispositionMap.get("sanitizedDispositionDescription_1_"+(i)).equalsIgnoreCase(Sanitized_DispositionDescription))
				{
					ReportUtil.STAF_ReportEvent("Pass", "Sanitization Disposition Description", "Actual = "+chargeDispositionMap.get("sanitizedDispositionDescription_1_"+(i))+" and Expected = "+Sanitized_DispositionDescription, 1);
				}else
				{
					ReportUtil.STAF_ReportEvent("Fail", "Sanitization Disposition Description", "Actual = "+chargeDispositionMap.get("sanitizedDispositionDescription_1_"+(i))+" and Expected = "+Sanitized_DispositionDescription, 1);
				}
				
				if(chargeDispositionMap.get("dispositionSanitizationLevel_1_"+(i)).equalsIgnoreCase(dispositionSanitizationLevel))
				{
					ReportUtil.STAF_ReportEvent("Pass", "Disposition Sanitization Level", "Actual = "+chargeDispositionMap.get("dispositionSanitizationLevel_1_"+(i))+" and Expected = "+dispositionSanitizationLevel, 1);
				}else
				{
					ReportUtil.STAF_ReportEvent("Fail", "Disposition Sanitization Level", "Actual = "+chargeDispositionMap.get("dispositionSanitizationLevel_1_"+(i))+" and Expected = "+dispositionSanitizationLevel, 1);
				}
				
				if(chargeDispositionMap.get("chargeSanitizationLevel_1_"+(i)).equalsIgnoreCase(chargeSanitizationLevel))
				{
					ReportUtil.STAF_ReportEvent("Pass", "Charge Sanitization Level", "Actual = "+chargeDispositionMap.get("chargeSanitizationLevel_1_"+(i))+" and Expected = "+chargeSanitizationLevel, 1);
				}else
				{
					ReportUtil.STAF_ReportEvent("Fail", "Charge Sanitization Level", "Actual = "+chargeDispositionMap.get("chargeSanitizationLevel_1_"+(i))+" and Expected = "+chargeSanitizationLevel, 1);
				}
				
				if(chargeDispositionMap.get("ruleDecision_1_"+(i)).equalsIgnoreCase(Exp_ruleDecision))
				{
					ReportUtil.STAF_ReportEvent("Pass", "Rule Decision", "Actual = "+chargeDispositionMap.get("ruleDecision_1_"+(i))+" and Expected = "+Exp_ruleDecision, 1);
				}else
				{
					ReportUtil.STAF_ReportEvent("Fail", "Rule Decision", "Actual = "+chargeDispositionMap.get("ruleDecision_1_"+(i))+" and Expected = "+Exp_ruleDecision, 1);
				}
				
				
				
				for(int j=1;j<=Integer.parseInt(chargeDispositionMap.get("NoOfRulesApplied"));j++)
				{
					for(int k=1;k<=rulesNumber;k++)
					{
						if(chargeDispositionMap.get("ruleName_1_"+i+"_"+j).equalsIgnoreCase(Exp_ruleName[k-1]) && j<=Integer.parseInt(chargeDispositionMap.get("NoOfRulesApplied")))
						{
							ReportUtil.STAF_ReportEvent("Pass", "Rule Name", "Actual = "+chargeDispositionMap.get("ruleName_1_"+i+"_"+j)+" and Expected = "+Exp_ruleName[k-1], 1);
						}else if(!chargeDispositionMap.get("ruleName_1_"+i+"_"+j).equalsIgnoreCase(Exp_ruleName[k-1]) && j==Integer.parseInt(chargeDispositionMap.get("NoOfRulesApplied")) && j==k)
						{
							ReportUtil.STAF_ReportEvent("Fail", "Rule Name", "Actual = "+chargeDispositionMap.get("ruleName_1_"+i+"_"+j)+" and Expected = "+Exp_ruleName[k-1], 1);
						}
						
						if(chargeDispositionMap.get("ruleType_1_"+i+"_"+j).equalsIgnoreCase(Exp_ruleType[k-1]) && j<=Integer.parseInt(chargeDispositionMap.get("NoOfRulesApplied")))
						{
							ReportUtil.STAF_ReportEvent("Pass", "Rule Type", "Actual = "+chargeDispositionMap.get("ruleType_1_"+i+"_"+j)+" and Expected = "+Exp_ruleType[k-1], 1);
						}else if(!chargeDispositionMap.get("ruleType_1_"+i+"_"+j).equalsIgnoreCase(Exp_ruleType[k-1]) && j==Integer.parseInt(chargeDispositionMap.get("NoOfRulesApplied")) && j==k)
						{
							ReportUtil.STAF_ReportEvent("Fail", "Rule Type", "Actual = "+chargeDispositionMap.get("ruleType_1_"+i+"_"+j)+" and Expected = "+Exp_ruleType[k-1], 1);
						}
					}
					
				}
				
				if(!chargeDispositionMap.isEmpty())
				{
					chargeDispositionMap.clear();
				}
				
				retval = Globals.KEYWORD_PASS;
			}
			
		}catch(Exception ie)
		{
			
		}
		
		return retval;
	}
	
	public static String chargeDispositionDBVerification()
	{
		APP_LOGGER.startFunction("Charge disposition Sanitization with DB");
		String retval=Globals.KEYWORD_FAIL;
		int timeOutinSeconds =  60;
		String tempRetval=Globals.KEYWORD_FAIL;
		try{
			String dbConnection = null;
			dbConnection = smartDataDAO.getSMARTDATADBConnection();
			
			if(dbConnection.equalsIgnoreCase(Globals.KEYWORD_FAIL))
			{
				for(int j=1;j<=12;j++)
				{
					dbConnection = smartDataDAO.getSMARTDATADBConnection();
					if(dbConnection.equalsIgnoreCase(Globals.KEYWORD_PASS))
					{
						break;
					}
				}
			}
				Map<String, String> dbSanitizedChargeDispostionData = smartDataDAO.getSmartDataChargeData();
				String Sanitized_ChargeDescription = Globals.testSuiteXLS.getCellData_fromTestData("Sanitized_ChargeDescription");
				String Sanitized_ChargeLevel = Globals.testSuiteXLS.getCellData_fromTestData("Sanitized_ChargeLevel");
				String Sanitized_chargeCategory = Globals.testSuiteXLS.getCellData_fromTestData("Sanitized_chargeCategory");
				String Sanitized_DispositionType = Globals.testSuiteXLS.getCellData_fromTestData("Sanitized_DispositionType");
				String Sanitized_DispositionDescription = Globals.testSuiteXLS.getCellData_fromTestData("Sanitized_DispositionDescription");
				
				String NumberOfCharges = Globals.testSuiteXLS.getCellData_fromTestData("NoOfCharges");
				int row = Integer.parseInt(NumberOfCharges);
				
				for(int i=1;i<=row;i++)
				{
					if(dbSanitizedChargeDispostionData.get("sanitizedChargeLevel_1_"+(i)).equalsIgnoreCase(Sanitized_ChargeLevel))
					{
						ReportUtil.STAF_ReportEvent("Pass", "Sanitization Charge Level", "Actual = "+dbSanitizedChargeDispostionData.get("sanitizedChargeLevel_1_"+(i))+" and Expected = "+Sanitized_ChargeLevel, 1);
					}else
					{
						ReportUtil.STAF_ReportEvent("Fail", "Sanitization Charge Level", "Actual = "+dbSanitizedChargeDispostionData.get("sanitizedChargeLevel_1_"+(i))+" and Expected = "+Sanitized_ChargeLevel, 1);
					}
					
					if(dbSanitizedChargeDispostionData.get("chargeCategory_1_"+(i)).equalsIgnoreCase(Sanitized_chargeCategory))
					{
						ReportUtil.STAF_ReportEvent("Pass", "Sanitization Charge Category", "Actual = "+dbSanitizedChargeDispostionData.get("chargeCategory_1_"+(i))+" and Expected = "+Sanitized_chargeCategory, 1);
					}else
					{
						ReportUtil.STAF_ReportEvent("Fail", "Sanitization Charge Category", "Actual = "+dbSanitizedChargeDispostionData.get("chargeCategory_1_"+(i))+" and Expected = "+Sanitized_chargeCategory, 1);
					}
					
					if(dbSanitizedChargeDispostionData.get("sanitizedDispositionType_1_"+(i)).equalsIgnoreCase(Sanitized_DispositionType))
					{
						ReportUtil.STAF_ReportEvent("Pass", "Sanitization Disposition Type", "Actual = "+dbSanitizedChargeDispostionData.get("sanitizedDispositionType_1_"+(i))+" and Expected = "+Sanitized_DispositionType, 1);
					}else
					{
						ReportUtil.STAF_ReportEvent("Fail", "Sanitization Disposition Type", "Actual = "+dbSanitizedChargeDispostionData.get("sanitizedDispositionType_1_"+(i))+" and Expected = "+Sanitized_DispositionType, 1);
					}
					
					if(!dbSanitizedChargeDispostionData.isEmpty())
					{
						dbSanitizedChargeDispostionData.clear();
					}
					
					retval = Globals.KEYWORD_PASS;
				}
			
		}catch(Exception ie)
		{
			log.error("Exception occurred While Database Verification | "+ie.toString());
			ie.printStackTrace();
			throw ie;
		}
		return retval;
	}
	
	public static String piiCheck()
	{
		APP_LOGGER.startFunction("PII Check");
		String retval=Globals.KEYWORD_FAIL;
		int timeOutinSeconds =  60;
		String tempRetval=Globals.KEYWORD_FAIL;
		
		try{
			String smartDataJson = Globals.testSuiteXLS.getCellData_fromTestData("SmartData_Json");
			Map<String, String> piiMap = new HashMap<String,String>();
			piiMap = DeserializeJsonData.piiData(smartDataJson);
			String piiAddress = Globals.testSuiteXLS.getCellData_fromTestData("Exp_PII_Address");
			String piiFirstNameAndLastName = Globals.testSuiteXLS.getCellData_fromTestData("Exp_PII_FirstNameAndLastName");
			String piiDOB = Globals.testSuiteXLS.getCellData_fromTestData("Exp_PII_DOB");
			String piiMiddleName = Globals.testSuiteXLS.getCellData_fromTestData("Exp_PII_MiddleName");
			String piiDriverLicense = Globals.testSuiteXLS.getCellData_fromTestData("Exp_PII_DriverLicense");
			String piiSSN = Globals.testSuiteXLS.getCellData_fromTestData("Exp_PII_SSN");
			String piiScore = Globals.testSuiteXLS.getCellData_fromTestData("Exp_PII_Score");
			String piiThresoldScore = Globals.testSuiteXLS.getCellData_fromTestData("Exp_PII_ThresoldScore");
			String piiIsCommonName = Globals.testSuiteXLS.getCellData_fromTestData("Exp_PII_CommonName");
			
			if(piiMap.get("Address_Weightage_1") != null)
			{
				if(String.valueOf(Double.parseDouble(piiAddress)).equalsIgnoreCase("0.0"))
				{
					if(piiMap.get("Address_Weightage_1").equalsIgnoreCase(piiAddress) && piiMap.get("Address_Identifier_1").equalsIgnoreCase("Address no match.") && piiMap.get("Address_PIIMatch_1").equalsIgnoreCase("NO_MATCH"))
					{
						ReportUtil.STAF_ReportEvent("Pass", "PII Address", "Actual = "+piiMap.get("Address_Weightage_1")+" and Expected = "+piiAddress, 1);
					}else{
						ReportUtil.STAF_ReportEvent("Fail", "PII Address", "Actual = "+piiMap.get("Address_Weightage_1")+" and Expected = "+piiAddress, 1);
					}
				}else{
					if(piiMap.get("Address_Weightage_1").equalsIgnoreCase(piiAddress) && piiMap.get("Address_Identifier_1").equalsIgnoreCase("Address match.") && piiMap.get("Address_PIIMatch_1").equalsIgnoreCase("MATCH"))
					{
						ReportUtil.STAF_ReportEvent("Pass", "PII Address", "Actual = "+piiMap.get("Address_Weightage_1")+" and Expected = "+piiAddress, 1);
					}else{
						ReportUtil.STAF_ReportEvent("Fail", "PII Address", "Actual = "+piiMap.get("Address_Weightage_1")+" and Expected = "+piiAddress, 1);
					}
				}
			}else{
				ReportUtil.STAF_ReportEvent("Fail", "PII Address", "PII Address Not Came", 1);
			}
			
			if(piiMap.get("FirstAndLastName_Weightage_1") != null)
			{
				if(String.valueOf(Double.parseDouble(piiFirstNameAndLastName)).equalsIgnoreCase("0.0"))
				{
					if(piiMap.get("FirstAndLastName_Weightage_1").equalsIgnoreCase(piiFirstNameAndLastName) && piiMap.get("FirstAndLastName_Identifier_1").equalsIgnoreCase("First and last name no match.") && piiMap.get("FirstAndLastName_PIIMatch_1").equalsIgnoreCase("NO_MATCH"))
					{
						ReportUtil.STAF_ReportEvent("Pass", "PII First and Last Name", "Actual = "+piiMap.get("FirstAndLastName_Weightage_1")+" and Expected = "+piiFirstNameAndLastName, 1);
					}else{
						ReportUtil.STAF_ReportEvent("Fail", "PII First and Last Name", "Actual = "+piiMap.get("FirstAndLastName_Weightage_1")+" and Expected = "+piiFirstNameAndLastName, 1);
					}
				}else{
					if(piiMap.get("FirstAndLastName_Weightage_1").equalsIgnoreCase(piiFirstNameAndLastName) && piiMap.get("FirstAndLastName_Identifier_1").equalsIgnoreCase("First and last name match.") && piiMap.get("FirstAndLastName_PIIMatch_1").equalsIgnoreCase("MATCH"))
					{
						ReportUtil.STAF_ReportEvent("Pass", "PII First and Last Name", "Actual = "+piiMap.get("FirstAndLastName_Weightage_1")+" and Expected = "+piiFirstNameAndLastName, 1);
					}else{
						ReportUtil.STAF_ReportEvent("Fail", "PII First and Last Name", "Actual = "+piiMap.get("FirstAndLastName_Weightage_1")+" and Expected = "+piiFirstNameAndLastName, 1);
					}
				}
			}else{
				ReportUtil.STAF_ReportEvent("Fail", "PII First and Last Name", "PII First and last name Not Came", 1);
			}
			
			if(piiMap.get("MiddleName_Weightage_1") != null)
			{
				if(String.valueOf(Double.parseDouble(piiMiddleName)).equalsIgnoreCase("0.0"))
				{
					if(piiMap.get("MiddleName_Weightage_1").equalsIgnoreCase(piiMiddleName) && piiMap.get("MiddleName_Identifier_1").equalsIgnoreCase("Middle name no match.") && piiMap.get("MiddleName_PIIMatch_1").equalsIgnoreCase("NO_MATCH"))
					{
						ReportUtil.STAF_ReportEvent("Pass", "PII Middle name", "Actual = "+piiMap.get("MiddleName_Weightage_1")+" and Expected = "+piiMiddleName, 1);
					}else{
						ReportUtil.STAF_ReportEvent("Fail", "PII Middle name", "Actual = "+piiMap.get("MiddleName_Weightage_1")+" and Expected = "+piiMiddleName, 1);
					}
				}else{
					if(piiMap.get("MiddleName_Weightage_1").equalsIgnoreCase(piiMiddleName) && piiMap.get("MiddleName_Identifier_1").equalsIgnoreCase("Middle name match.") && piiMap.get("MiddleName_PIIMatch_1").equalsIgnoreCase("MATCH"))
					{
						ReportUtil.STAF_ReportEvent("Pass", "PII Middle name", "Actual = "+piiMap.get("MiddleName_Weightage_1")+" and Expected = "+piiMiddleName, 1);
					}else{
						ReportUtil.STAF_ReportEvent("Fail", "PII Middle name", "Actual = "+piiMap.get("MiddleName_Weightage_1")+" and Expected = "+piiMiddleName, 1);
					}
				}
			}else{
				ReportUtil.STAF_ReportEvent("Fail", "PII Middle name", "PII Middle name Not Came", 1);
			}
			
			if(piiMap.get("DOB_Weightage_1") != null)
			{
				if(String.valueOf(Double.parseDouble(piiDOB)).equalsIgnoreCase("0.0"))
				{
					if(piiMap.get("DOB_Weightage_1").equalsIgnoreCase(piiDOB) && piiMap.get("DOB_Identifier_1").equalsIgnoreCase("Date Of Birth no match.") && piiMap.get("DOB_PIIMatch_1").equalsIgnoreCase("NO_MATCH"))
					{
						ReportUtil.STAF_ReportEvent("Pass", "PII DOB", "Actual = "+piiMap.get("DOB_Weightage_1")+" and Expected = "+piiDOB, 1);
					}else{
						ReportUtil.STAF_ReportEvent("Fail", "PII DOB", "Actual = "+piiMap.get("DOB_Weightage_1")+" and Expected = "+piiDOB, 1);
					}
				}else{
					if(piiMap.get("DOB_Weightage_1").equalsIgnoreCase(piiDOB) && piiMap.get("DOB_Identifier_1").equalsIgnoreCase("Date Of Birth match.") && piiMap.get("DOB_PIIMatch_1").equalsIgnoreCase("MATCH"))
					{
						ReportUtil.STAF_ReportEvent("Pass", "PII DOB", "Actual = "+piiMap.get("DOB_Weightage_1")+" and Expected = "+piiDOB, 1);
					}else{
						ReportUtil.STAF_ReportEvent("Fail", "PII DOB", "Actual = "+piiMap.get("DOB_Weightage_1")+" and Expected = "+piiDOB, 1);
					}
				}
			}else{
				ReportUtil.STAF_ReportEvent("Fail", "PII DOB", "PII DOB Not Came", 1);
			}
			
			if(piiMap.get("DriverLicense_Weightage_1") != null)
			{
				if(String.valueOf(Double.parseDouble(piiDriverLicense)).equalsIgnoreCase("0.0"))
				{
					if(piiMap.get("DriverLicense_Weightage_1").equalsIgnoreCase(piiDriverLicense) && piiMap.get("DriverLicense_Identifier_1").equalsIgnoreCase("Driver license no match.") && piiMap.get("DriverLicense_PIIMatch_1").equalsIgnoreCase("NO_MATCH"))
					{
						ReportUtil.STAF_ReportEvent("Pass", "PII Driving License", "Actual = "+piiMap.get("DriverLicense_Weightage_1")+" and Expected = "+piiDriverLicense, 1);
					}else{
						ReportUtil.STAF_ReportEvent("Fail", "PII Driving License", "Actual = "+piiMap.get("DriverLicense_Weightage_1")+" and Expected = "+piiDriverLicense, 1);
					}
				}else{
					if(piiMap.get("DriverLicense_Weightage_1").equalsIgnoreCase(piiDriverLicense) && piiMap.get("DriverLicense_Identifier_1").equalsIgnoreCase("Driver license match.") && piiMap.get("DriverLicense_PIIMatch_1").equalsIgnoreCase("MATCH"))
					{
						ReportUtil.STAF_ReportEvent("Pass", "PII Driving License", "Actual = "+piiMap.get("DriverLicense_Weightage_1")+" and Expected = "+piiDriverLicense, 1);
					}else{
						ReportUtil.STAF_ReportEvent("Fail", "PII Driving License", "Actual = "+piiMap.get("DriverLicense_Weightage_1")+" and Expected = "+piiDriverLicense, 1);
					}
				}
			}else{
				ReportUtil.STAF_ReportEvent("Fail", "PII Driving License", "PII Driving License Not Came", 1);
			}
			
			if(piiMap.get("SSN_Weightage_1") != null)
			{
				if(String.valueOf(Double.parseDouble(piiSSN)).equalsIgnoreCase("0.0"))
				{
					if(piiMap.get("SSN_Weightage_1").equalsIgnoreCase(piiSSN) && piiMap.get("SSN_Identifier_1").equalsIgnoreCase("SSN no match.") && piiMap.get("SSN_PIIMatch_1").equalsIgnoreCase("NO_MATCH"))
					{
						ReportUtil.STAF_ReportEvent("Pass", "PII SSN", "Actual = "+piiMap.get("SSN_Weightage_1")+" and Expected = "+piiSSN, 1);
					}else{
						ReportUtil.STAF_ReportEvent("Fail", "PII SSN", "Actual = "+piiMap.get("SSN_Weightage_1")+" and Expected = "+piiSSN, 1);
					}
				}else{
					if(piiMap.get("SSN_Weightage_1").equalsIgnoreCase(piiSSN) && piiMap.get("SSN_Identifier_1").equalsIgnoreCase("SSN match.") && piiMap.get("SSN_PIIMatch_1").equalsIgnoreCase("MATCH"))
					{
						ReportUtil.STAF_ReportEvent("Pass", "PII SSN", "Actual = "+piiMap.get("SSN_Weightage_1")+" and Expected = "+piiSSN, 1);
					}else{
						ReportUtil.STAF_ReportEvent("Fail", "PII SSN", "Actual = "+piiMap.get("SSN_Weightage_1")+" and Expected = "+piiSSN, 1);
					}
				}
			}else{
				ReportUtil.STAF_ReportEvent("Fail", "PII SSN", "PII SSN Not Came", 1);
			}
			
			if(piiMap.get("piiWeightageScore_1") != null)
			{
				if(piiScore.equalsIgnoreCase(piiMap.get("piiWeightageScore_1")))
				{
					ReportUtil.STAF_ReportEvent("Pass", "PII Weightage Score", "Actual = "+piiMap.get("piiWeightageScore_1")+" and Expected = "+piiScore, 1);
				}else{
					ReportUtil.STAF_ReportEvent("Fail", "PII Weightage Score", "Actual = "+piiMap.get("piiWeightageScore_1")+" and Expected = "+piiScore, 1);
				}
			}else{
				ReportUtil.STAF_ReportEvent("Fail", "PII Weightage Score", "PII Weightage Score Not Came", 1);
			}
			
			if(piiMap.get("piiCaseThresoldScore") != null)
			{
				if(piiThresoldScore.equalsIgnoreCase(piiMap.get("piiCaseThresoldScore")))
				{
					ReportUtil.STAF_ReportEvent("Pass", "PII Thresold Score", "Actual = "+piiMap.get("piiCaseThresoldScore")+" and Expected = "+piiThresoldScore, 1);
				}else{
					ReportUtil.STAF_ReportEvent("Fail", "PII Thresold Score", "Actual = "+piiMap.get("piiCaseThresoldScore")+" and Expected = "+piiThresoldScore, 1);
				}
			}else{
				ReportUtil.STAF_ReportEvent("Fail", "PII Thresold Score", "PII Thresold Score Not Came", 1);
			}
			
			if(piiMap.get("piiCommonName") != null)
			{
				if(piiIsCommonName.equalsIgnoreCase(piiMap.get("piiCommonName")))
				{
					ReportUtil.STAF_ReportEvent("Pass", "PII Common Name Status", "Actual = "+piiMap.get("piiCommonName")+" and Expected = "+piiIsCommonName, 1);
				}else{
					ReportUtil.STAF_ReportEvent("Fail", "PII Common Name Status", "Actual = "+piiMap.get("piiCommonName")+" and Expected = "+piiIsCommonName, 1);
				}
			}else{
				ReportUtil.STAF_ReportEvent("Fail", "PII Common Name Status", "PII Common Name Not Came", 1);
			}
			
			if(!piiMap.isEmpty())
			{
				piiMap.clear();
			}
			
			retval = Globals.KEYWORD_PASS;
			
		}catch(Exception ie)
		{
			
		}
		
		return retval;
	}
	
	public static String ageOfRecordVerification()
	{
		APP_LOGGER.startFunction("Age Of Record Verification");
		String retval=Globals.KEYWORD_FAIL;
		int timeOutinSeconds =  60;
		String tempRetval=Globals.KEYWORD_FAIL;
		try{
			int jailedDuration = 0;
			int probationDuration = 0;
			String smartDataJson = Globals.testSuiteXLS.getCellData_fromTestData("SmartData_Json");
			Map<String, String> ageOdRecordData = DeserializeJsonData.ageOfRecordData(smartDataJson);
			String Sanitized_DispositionType = Globals.testSuiteXLS.getCellData_fromTestData("Sanitized_DispositionType");
			String arrestDate = Globals.testSuiteXLS.getCellData_fromTestData("Arrest_Date");
			String dispositionDate = Globals.testSuiteXLS.getCellData_fromTestData("Disposition_Date");
			
			int noOfSentence = Integer.parseInt(Globals.testSuiteXLS.getCellData_fromTestData("NoOfSentences"));
			String [] sentenceCategoryArray = Globals.testSuiteXLS.getCellData_fromTestData("Sentence_SentenceCategory").split(",");
			String [] sentenceDurationArray = Globals.testSuiteXLS.getCellData_fromTestData("Sentence_AmountAndDuration").split(",");
			
			for(int j=0;j<noOfSentence;j++)
			{
				if(sentenceCategoryArray[j].equalsIgnoreCase("Jail"))
				{
					String [] jailedDurationArray = sentenceDurationArray[j].split(":");
					
					for(int k=0;k<jailedDurationArray.length;k++)
					{
						log.info("Sentence Duration"+jailedDurationArray[k]);
						if(jailedDurationArray[k].contains("Y"))
						{
							if(jailedDuration < Integer.parseInt(jailedDurationArray[k].split("Y")[0]))
							{
								jailedDuration = Integer.parseInt(jailedDurationArray[k].split("Y")[0]);
							}
						}
					}
					
				}
				if(sentenceCategoryArray[j].contains("Probation"))
				{
					String [] probationDurationArray = sentenceDurationArray[j].split(":");
					
					for(int k=0;k<probationDurationArray.length;k++)
					{
						log.info("Sentence Probation Duration"+probationDurationArray[k]);
						if(probationDurationArray[k].contains("Y"))
						{
							if(probationDuration < Integer.parseInt(probationDurationArray[k].split("Y")[0]))
							{
								probationDuration = Integer.parseInt(probationDurationArray[k].split("Y")[0]);
							}
						}
					}
				}
			}
			
			LocalDate today = LocalDate.now();
			LocalDate localdateArrest = LocalDate.of(Integer.parseInt(arrestDate.split("/")[2]), Integer.parseInt(arrestDate.split("/")[0]), Integer.parseInt(arrestDate.split("/")[1]));
			LocalDate localdateDisposition = LocalDate.of(Integer.parseInt(dispositionDate.split("/")[2]), Integer.parseInt(dispositionDate.split("/")[0]), Integer.parseInt(dispositionDate.split("/")[1]));
			log.info("Today time"+today);
			log.info("Arrest Time"+localdateArrest);
			log.info("Disposition Time"+localdateDisposition);
			Period p = null;
			String NumberOfCharges = Globals.testSuiteXLS.getCellData_fromTestData("NoOfCharges");
			log.info("Number of Charges"+"\t"+NumberOfCharges);
			int row = Integer.parseInt(NumberOfCharges);
			
			for(int i=1;i<=row;i++)
			{
			
				
				if(Sanitized_DispositionType.equalsIgnoreCase("Conviction"))
				{
					p = Period.between(localdateDisposition, today);
					if(ageOdRecordData.get("AgeOfRecord_1_"+i).equalsIgnoreCase(String.valueOf(p.getYears())))
					{
						ReportUtil.STAF_ReportEvent("Pass", "Age Of Record", "Actual = "+ageOdRecordData.get("AgeOfRecord_1_"+i)+" and Expected = "+String.valueOf(p.getYears()), 1);
					}else{
						ReportUtil.STAF_ReportEvent("Fail", "Age Of Record", "Actual = "+ageOdRecordData.get("AgeOfRecord_1_"+i)+" and Expected = "+String.valueOf(p.getYears()), 1);
					}
				}else{
					
					p = Period.between(localdateArrest, today);
					if(ageOdRecordData.get("AgeOfRecord_1_"+i).equalsIgnoreCase(String.valueOf(p.getYears())))
					{
						ReportUtil.STAF_ReportEvent("Pass", "Age Of Record", "Actual = "+ageOdRecordData.get("AgeOfRecord_1_"+i)+" and Expected = "+String.valueOf(p.getYears()), 1);
					}else{
						ReportUtil.STAF_ReportEvent("Fail", "Age Of Record", "Actual = "+ageOdRecordData.get("AgeOfRecord_1_"+i)+" and Expected = "+String.valueOf(p.getYears()), 1);
					}
				}
				
				if(jailedDuration > 0)
				{
					String jailedDurationJson = String.valueOf((float)(Integer.parseInt(ageOdRecordData.get("AgeOfRecord_1_"+i)) - jailedDuration));
					if(ageOdRecordData.get("AgeOfSentence_1_"+i).equalsIgnoreCase(jailedDurationJson))
					{
						ReportUtil.STAF_ReportEvent("Pass", "Age Of Sentence", "Actual = "+ageOdRecordData.get("AgeOfSentence_1_"+i)+" and Expected = "+jailedDurationJson, 1);
					}else{
						ReportUtil.STAF_ReportEvent("Fail", "Age Of Sentence", "Actual = "+ageOdRecordData.get("AgeOfSentence_1_"+i)+" and Expected = "+jailedDurationJson, 1);
					}
				}else{
					if(ageOdRecordData.get("AgeOfSentence_1_"+i).equalsIgnoreCase(String.valueOf(Float.parseFloat(ageOdRecordData.get("AgeOfRecord_1_"+i)))))
					{
						ReportUtil.STAF_ReportEvent("Pass", "Age Of Sentence", "Actual = "+ageOdRecordData.get("AgeOfSentence_1_"+i)+" and Expected = "+ageOdRecordData.get("AgeOfRecord_1_"+i), 1);
					}else{
						ReportUtil.STAF_ReportEvent("Fail", "Age Of Sentence", "Actual = "+ageOdRecordData.get("AgeOfSentence_1_"+i)+" and Expected = "+ageOdRecordData.get("AgeOfRecord_1_"+i), 1);
					}
				}
				
				if(probationDuration > 0)
				{
					String probationDurationJson = String.valueOf(Integer.parseInt(ageOdRecordData.get("AgeOfRecord_1_"+i)) - probationDuration);
					if(ageOdRecordData.get("ageofSentenceProbation_1_"+i).equalsIgnoreCase(probationDurationJson))
					{
						ReportUtil.STAF_ReportEvent("Pass", "Age Of Sentence Probation", "Actual = "+ageOdRecordData.get("ageofSentenceProbation_1_"+i)+" and Expected = "+probationDurationJson, 1);
					}else{
						ReportUtil.STAF_ReportEvent("Fail", "Age Of Sentence Probation", "Actual = "+ageOdRecordData.get("ageofSentenceProbation_1_"+i)+" and Expected = "+probationDurationJson, 1);
					}
				}else if(jailedDuration > 0)
				{
					String probationDurationJson = String.valueOf(Integer.parseInt(ageOdRecordData.get("AgeOfRecord_1_"+i)) - jailedDuration);
					if(ageOdRecordData.get("ageofSentenceProbation_1_"+i).equalsIgnoreCase(probationDurationJson))
					{
						ReportUtil.STAF_ReportEvent("Pass", "Age Of Sentence Probation", "Actual = "+ageOdRecordData.get("ageofSentenceProbation_1_"+i)+" and Expected = "+probationDurationJson, 1);
					}else{
						ReportUtil.STAF_ReportEvent("Fail", "Age Of Sentence Probation", "Actual = "+ageOdRecordData.get("ageofSentenceProbation_1_"+i)+" and Expected = "+probationDurationJson, 1);
					}
				}
					else{
					if(ageOdRecordData.get("ageofSentenceProbation_1_"+i).equalsIgnoreCase(ageOdRecordData.get("AgeOfRecord_1_"+i)))
					{
						ReportUtil.STAF_ReportEvent("Pass", "Age Of Sentence Probation", "Actual = "+ageOdRecordData.get("ageofSentenceProbation_1_"+i)+" and Expected = "+ageOdRecordData.get("AgeOfRecord_1_"+i), 1);
					}else{
						ReportUtil.STAF_ReportEvent("Fail", "Age Of Sentence Probation", "Actual = "+ageOdRecordData.get("ageofSentenceProbation_1_"+i)+" and Expected = "+ageOdRecordData.get("AgeOfRecord_1_"+i), 1);
					}
				}
			}
			
			retval = Globals.KEYWORD_PASS;
			
		}catch(Exception ie)
		{
			
		}
		
		return retval;
	}
	
	
	public static String assignTask() throws Exception
	{
		APP_LOGGER.startFunction("Assign Task");
		String retval=Globals.KEYWORD_FAIL;
		int timeOutinSeconds =  60;
		String tempRetval=Globals.KEYWORD_FAIL;
		try {
			String orderNo 			= Globals.testSuiteXLS.getCellData_fromTestData("SWestOrderID");
			System.out.println("OrderID is :"+orderNo);
			if(orderNo!=null)
			{
				Globals.driver.switchTo().defaultContent();
				
				if (sc.waitforElementToDisplay("agHomepage_tasks_link",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_FAIL)){
					throw new Exception("Tasks link in Side menu is not visible");
				}
				
				sc.clickWhenElementIsClickable("agHomepage_tasks_link",(int) timeOutinSeconds  );
				if (sc.waitforElementToDisplay("tasks_orderSearch_link",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_FAIL)){
					throw new Exception("Order Search link in Side menu is not visible.Please check user rights");
				}
				
				
				sc.clickWhenElementIsClickable("tasks_orderSearch_link",(int) timeOutinSeconds  );
				if (sc.waitforElementToDisplay("orderSearch_orderNumber_txt",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_FAIL)){
					throw new Exception("Order Search field in Searh Page is not visible.");
				}
				
								
				tempRetval = sc.setValue("orderSearch_orderNumber_txt", orderNo);
				if (tempRetval.equalsIgnoreCase(Globals.KEYWORD_FAIL)){
					throw new Exception("Unable to set the Order Number in Order Search field");
				}
				
				sc.clickWhenElementIsClickable("orderSearch_search_btn",(int) timeOutinSeconds  );
				if (sc.waitforElementToDisplay("orderSearch_resultsHdr_tbl",timeOutinSeconds).equalsIgnoreCase(Globals.KEYWORD_FAIL)){
					throw new Exception("Order results grid is not displayed");
				}
				
				WebElement resultTbl 	=	null;
				int rowCount 			= 	-1;
				
				boolean resultFound		=	false;
				String ordertempStatus	=	"";
				
				tempRetval = sc.waitforElementToDisplay("orderSearch_loadingOrder_img",10);
				
				while(tempRetval.equalsIgnoreCase(Globals.KEYWORD_PASS)){
					Thread.sleep(1000);
				}
				
				resultTbl				=	sc.createWebElement("orderSearch_resultsData_tbl");
				
				for(int i=1;i<=12;i++)
				{
					rowCount				= 	sc.getRowCount_tbl(resultTbl, "./tbody/tr");
					if(rowCount == 1){
						Thread.sleep(10000);
						sc.clickWhenElementIsClickable("orderSearch_search_btn",(int) timeOutinSeconds);
						Thread.sleep(30000);
					}else if(rowCount > 1){
						
							ordertempStatus = resultTbl.findElement(By.xpath("./tbody/tr[2]/td[10]")).getText();
							log.info("Assign Order OrderStatus : "+ordertempStatus);
							if(ordertempStatus.equalsIgnoreCase("Manual Data Collection")){
								
								ReportUtil.STAF_ReportEvent("Pass", "OrderID-"+orderNo, "Order is present in "+ordertempStatus, 1);
								break;
							}
					}else {
						ReportUtil.STAF_ReportEvent("Pass", "OrderID-"+orderNo, "No results found for order search", 1);
						throw new Exception("OrderID-"+orderNo+"No results found for order search");
					}
				}
			}
			String taskType 			= Globals.testSuiteXLS.getCellData_fromTestData("TaskType");
			String assignTo 			= Globals.testSuiteXLS.getCellData_fromTestData("AssignTo");
			sc.clickWhenElementIsClickable("agHomepage_tasks_link", 5);
			if(sc.waitTillElementDisplayed("tasks_criminalTasks_link", 5) == Globals.KEYWORD_PASS)
			{
				if(taskType.equalsIgnoreCase("Criminal"))
				{
					sc.clickWhenElementIsClickable("tasks_criminalTasks_link", 20);
				}else if(taskType.equalsIgnoreCase("Abuse"))
				{
					sc.clickWhenElementIsClickable("tasks_abuseTasks_link", 20);
				}else if(taskType.equalsIgnoreCase("Sanction"))
				{
					sc.clickWhenElementIsClickable("tasks_sanctionTasks_link", 20);
				}else if(taskType.equalsIgnoreCase("Civil"))
				{
					sc.clickWhenElementIsClickable("tasks_civilTasks_link", 20);
				}
			}
			sc.clickWhenElementIsClickable("View_Role_Worklist_link", 5);
			sc.clickWhenElementIsClickable("Actions_Enter_Results_link", 5);
			
			sc.clickWhenElementIsClickable("Actions_Search_btn_path", 5);
			if(sc.waitTillElementDisplayed("OrderID_id", 5) == Globals.KEYWORD_PASS)
			{
				sc.setValue("OrderID_id", orderNo);
			}
			
			sc.clickWhenElementIsClickable("Order_Search_path", timeOutinSeconds);
			if(sc.waitTillElementDisplayed("Result_Table_Result_path", 5) == Globals.KEYWORD_PASS)
			{
				sc.clickWhenElementIsClickable("Select_ALL_path", 5);
				sc.clickWhenElementIsClickable("ReAssign_Selected_path", 5);
				if(sc.waitTillElementDisplayed("Reassign_Internal_radio_id", 5) == Globals.KEYWORD_PASS)
				{
					sc.selectValue_byVisibleText("Reassign_SearchBy_id", "User ID");
					sc.setValue("Reassign_EnterText_path", assignTo);
					Thread.sleep(2000);
					driver.findElement(By.xpath(".//*[@id='userId']//*[@class='dhx_combo_input']")).sendKeys(Keys.ARROW_DOWN);
					sc.setValueEnter("Reassign_EnterText_path");
					sc.checkCheckBox("Reassign_Distribute_Task_Event_name");
					sc.clickWhenElementIsClickable("Reassign_Add_id", 5);
					
					if(sc.waitTillElementDisplayed("Reassign_UserID_path", 5) == Globals.KEYWORD_PASS)
					{
						if(sc.getWebText("Reassign_UserID_path").equals(assignTo))
						{
							ReportUtil.STAF_ReportEvent("Pass", "Reassign To", "Task Assign To User "+assignTo, 1);
						}else{
							
							ReportUtil.STAF_ReportEvent("Pass", "Reassign To", "Task is not Assigned", 1);
						}
					}
					
					sc.clickWhenElementIsClickable("Reassign_Distribute_Task_btn_path", 5);
				}
			}
			
			if(sc.waitTillElementDisplayed("Result_Table_Assign_To_path", 5) == Globals.KEYWORD_PASS)
			{
				if(sc.getWebText("Result_Table_Assign_To_path").contains(assignTo))
				{
					ReportUtil.STAF_ReportEvent("Pass", "Reassign To", "Task Assign To User contains "+assignTo, 1);
				}else{
					
					ReportUtil.STAF_ReportEvent("Pass", "Reassign To", "Task is not Assigned", 1);
				}
			}
			
			retval = Globals.KEYWORD_PASS;
		
		} catch (Exception e) {

			log.error("Exception occurred in Assign Task by Super User | "+e.toString());
			e.printStackTrace();
			throw e;
		}
		return retval;
	}
	
	public static String logout() throws Exception{
		Globals.Component = Thread.currentThread().getStackTrace()[1].getMethodName();
		APP_LOGGER.startFunction(Globals.Component);
		String retval			=	Globals.KEYWORD_FAIL;
		long timeOutinSeconds 	=	60;
		String tempRetval 		=	Globals.KEYWORD_FAIL;
		
		
		try{
			Globals.driver.switchTo().defaultContent();// this is done as in some cases we are switching to frames for report validations.

			tempRetval = sc.waitforElementToDisplay("agHomepage_logout_link", timeOutinSeconds);
			if(tempRetval.equalsIgnoreCase(Globals.KEYWORD_PASS)){


				sc.clickWhenElementIsClickable("agHomepage_logout_link",(int) timeOutinSeconds  );

//				tempRetval = sc.waitforElementToDisplay("agLogin_userID_txt", timeOutinSeconds);
				tempRetval = Globals.KEYWORD_PASS;
				if(tempRetval.equalsIgnoreCase(Globals.KEYWORD_PASS)){

					ReportUtil.STAF_ReportEvent("Pass", "User LogOff", "User logged off successfully", 1);
					retval=Globals.KEYWORD_PASS;

				}else{
					ReportUtil.STAF_ReportEvent("Fail", "User LogOff", "Unable to log off from AG Aplication", 1);

				}
			}
			

		}catch(Exception e){
			
			throw new Exception("Unable to logout from AG application due to Exception-"+e.toString());
		}


		return retval;
	}
	
	
	public static void main(String[] args) throws Exception {
		
		 String InputReferenceXML    = "";
	        String vendor               = "";                       
	        String region               = "";
	        String county               = "";
	        String lastName             = "";
	        String firstName            = "";
	        String middleName           = "";
	        String dob                  = "";
	        String courtName            = "";
	        String ssn                  = "";
	        String randomNumber         = "";
	        String refID                = "";
	        String screeningType        = "";
	        String soapServer           = "";
	        XMLUtils  orderRequestXML   = null; 
	        
	        InputReferenceXML = System.getProperty("user.dir")+"/src/test/Resources/refFile/XML_SD/OrderPlace.xml";
	        File fXmlFile;
			fXmlFile = new File(InputReferenceXML);
			Document doc;
			DocumentBuilder dBuilder = null;
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
	        
	        orderRequestXML       = new XMLUtils(InputReferenceXML);
	        
//	        String s2 = doc.getElementsByTagName("web:screeningOrder").item(0).getTextContent();
	        String s2 = orderRequestXML.getXMLNodeValByTagName("web:screeningOrder");
	        System.out.println(s2);
	        String s3 = String.format(s2, "SMD788","SMD788");
	        System.out.println(s3);
	        orderRequestXML.updatedXMLNodeValueByTagName("web:screeningOrder",s3);
	        
	        orderRequestXML.updatedXMLNodeValueByTagName("web:agent", "AISS");
	        
	        System.out.println(orderRequestXML);
	        
	        String updatedXMlFileName = "XML_Results_Row_Request.xml";
	        
	        StreamResult result = new StreamResult(new StringWriter());
			DOMSource source = new DOMSource(doc);
			
			//generate the modified input xml and save in Results folder
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			
			//initialize StreamResult with File object to save to file
			transformer.transform(source, result);
			String xmlString = result.getWriter().toString();

			File file=new File(System.getProperty("user.dir")+"/src/test/Resources/refFile/XML_SD"+File.separator+updatedXMlFileName);
			String encode="UTF-8";
			FileUtils.writeStringToFile(file, xmlString,encode);
			
	        //orderRequestXML.saveXMLDOM2File(updatedXMlFileName);
	        
	}
}

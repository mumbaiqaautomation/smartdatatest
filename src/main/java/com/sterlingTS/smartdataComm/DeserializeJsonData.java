package com.sterlingTS.smartdataComm;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.google.gson.Gson;
import com.sterlingTS.smartdataComm.model.ChargeDescription;
import com.sterlingTS.smartdataComm.model.ChargeRuleHistory;
import com.sterlingTS.smartdataComm.model.CrimCase;
import com.sterlingTS.smartdataComm.model.CrimCharge;
import com.sterlingTS.smartdataComm.model.PersonalIdentifier;
import com.sterlingTS.smartdataComm.model.ScreeningResult;

public class DeserializeJsonData {
	
	static Gson gson = new Gson();
	
	public static ScreeningResult deserializeJson(String jsondata)
	{
		ScreeningResult resultjson = gson.fromJson(jsondata, ScreeningResult.class);
		
		return resultjson;
	}
	
//	Actual charge description
//	Sanitized charge description
//	Actual disposition description
//	Sanitized disposition description
//	Actual charge level description
//	Sanitized charge level description
//	Rule Decision
//	Change rule history
//	SLG chargetype
//	SLG disposition type
	
	public static Map<String,String> caseChargeData(String responseData)
	{
		Map<String, String> crimcaseData = new HashMap<String, String>();
		
		ScreeningResult sr = deserializeJson(responseData);
		Set<CrimCase> cc = sr.getCrimCases();
		Iterator crimCaseItr = cc.iterator();
		for(int i=0;i<cc.size();i++)
		{
			CrimCase ccm = (CrimCase) crimCaseItr.next();
			Set<CrimCharge> cchr = ccm.getCrimCharges();
			Iterator itrCharges = cchr.iterator();
			for(int j=0;j<cchr.size();j++)
			{
				CrimCharge cchrSingle = (CrimCharge) itrCharges.next();
				crimcaseData.put("actualChargeDescription"+"_"+(i+1)+"_"+(j+1), cchrSingle.getActualChargeDescription());
				
				if(cchrSingle.getSaniztizedChargeDescription() == null)
				{
					crimcaseData.put("saniztizedChargeDescription"+"_"+(i+1)+"_"+(j+1), "");
				}else{
					crimcaseData.put("saniztizedChargeDescription"+"_"+(i+1)+"_"+(j+1), cchrSingle.getSaniztizedChargeDescription().getDescription());
				}
				crimcaseData.put("actualDispositionDescription"+"_"+(i+1)+"_"+(j+1), cchrSingle.getActualDispositionDescription());
				crimcaseData.put("sanitizedDispositionDescription"+"_"+(i+1)+"_"+(j+1), cchrSingle.getSanitizedDispositionDescription().getDescription());
				crimcaseData.put("actualChargeLevel"+"_"+(i+1)+"_"+(j+1), cchrSingle.getActualChargeLevel().getDescription());
				crimcaseData.put("sanitizedChargeLevel"+"_"+(i+1)+"_"+(j+1), cchrSingle.getSanitizedChargeLevel().getDescription());
				crimcaseData.put("ruleDecision"+"_"+(i+1)+"_"+(j+1), cchrSingle.getRuleDecision().toString());
				Set<ChargeRuleHistory> chrgRlHist = cchrSingle.getChargeRuleHistory();
				Iterator chrgRlHistItr = chrgRlHist.iterator();
				crimcaseData.put("NoOfRulesApplied", String.valueOf(chrgRlHist.size()));
				for(int k=0;k<chrgRlHist.size();k++)
				{
					ChargeRuleHistory chrgRlHst = (ChargeRuleHistory) chrgRlHistItr.next();
					crimcaseData.put("ruleName"+"_"+(i+1)+"_"+(j+1)+"_"+(k+1), chrgRlHst.getRuleName());
					crimcaseData.put("ruleType"+"_"+(i+1)+"_"+(j+1)+"_"+(k+1), chrgRlHst.getRuleType());
				}
				crimcaseData.put("chargeSanitizationLevel"+"_"+(i+1)+"_"+(j+1), cchrSingle.getChargeSanitizationLevel().toString());
				crimcaseData.put("dispositionSanitizationLevel"+"_"+(i+1)+"_"+(j+1), cchrSingle.getDispositionSanitizationLevel().toString());
				if(cchrSingle.getChargeCategory() == null)
				{
					crimcaseData.put("chargeCategory"+"_"+(i+1)+"_"+(j+1), "");
				}else{
					crimcaseData.put("chargeCategory"+"_"+(i+1)+"_"+(j+1), cchrSingle.getChargeCategory().getCategory());
				}
				crimcaseData.put("actualDispositionType"+"_"+(i+1)+"_"+(j+1), cchrSingle.getActualDispositionType().getDescription());
				crimcaseData.put("sanitizedDispositionType"+"_"+(i+1)+"_"+(j+1), cchrSingle.getSanitizedDispositionType().getDescription());			
			}
			
		}
		
		return crimcaseData;
	}
	
	public static Map<String,String> piiData(String responseData)
	{
		Map<String, String> crimcaseData = new HashMap<String, String>();
		
		ScreeningResult sr = deserializeJson(responseData);
		Set<CrimCase> cc = sr.getCrimCases();
		Iterator crimCaseItr = cc.iterator();
		for(int i=0;i<cc.size();i++)
		{
			CrimCase ccm = (CrimCase) crimCaseItr.next();
			crimcaseData.put("piiWeightageScore_"+(i+1), ccm.getPiiWeightageScore().toString());
			Set<PersonalIdentifier> piiIdentifier = ccm.getPersonalIdentifiers();
			Iterator personalID = piiIdentifier.iterator();
			for(int j=0;j<piiIdentifier.size();j++)
			{
				PersonalIdentifier pd = (PersonalIdentifier) personalID.next();
				if(pd.getIdentifier().toString().contains("Address"))
				{
					crimcaseData.put("Address_Identifier_"+(i+1), pd.getIdentifier().toString());
					crimcaseData.put("Address_PIIMatch_"+(i+1), pd.getPiiMatch().toString());
					crimcaseData.put("Address_Weightage_"+(i+1), pd.getWeightage().toString());
				}else if(pd.getIdentifier().toString().contains("SSN"))
				{
					crimcaseData.put("SSN_Identifier_"+(i+1), pd.getIdentifier().toString());
					crimcaseData.put("SSN_PIIMatch_"+(i+1), pd.getPiiMatch().toString());
					crimcaseData.put("SSN_Weightage_"+(i+1), pd.getWeightage().toString());
				}else if(pd.getIdentifier().toString().contains("First and last name"))
				{
					crimcaseData.put("FirstAndLastName_Identifier_"+(i+1), pd.getIdentifier().toString());
					crimcaseData.put("FirstAndLastName_PIIMatch_"+(i+1), pd.getPiiMatch().toString());
					crimcaseData.put("FirstAndLastName_Weightage_"+(i+1), pd.getWeightage().toString());
				}else if(pd.getIdentifier().toString().contains("Driver license"))
				{
					crimcaseData.put("DriverLicense_Identifier_"+(i+1), pd.getIdentifier().toString());
					crimcaseData.put("DriverLicense_PIIMatch_"+(i+1), pd.getPiiMatch().toString());
					crimcaseData.put("DriverLicense_Weightage_"+(i+1), pd.getWeightage().toString());
				}else if(pd.getIdentifier().toString().contains("Middle name"))
				{
					crimcaseData.put("MiddleName_Identifier_"+(i+1), pd.getIdentifier().toString());
					crimcaseData.put("MiddleName_PIIMatch_"+(i+1), pd.getPiiMatch().toString());
					crimcaseData.put("MiddleName_Weightage_"+(i+1), pd.getWeightage().toString());
				}else if(pd.getIdentifier().toString().contains("Date Of Birth"))
				{
					crimcaseData.put("DOB_Identifier_"+(i+1), pd.getIdentifier().toString());
					crimcaseData.put("DOB_PIIMatch_"+(i+1), pd.getPiiMatch().toString());
					crimcaseData.put("DOB_Weightage_"+(i+1), pd.getWeightage().toString());
				}
				
			}
		}
		crimcaseData.put("piiCaseThresoldScore", sr.getPiiCaseThresoldScore());
		crimcaseData.put("piiCommonName", String.valueOf(sr.isCommonName()));
		
		
		
		return crimcaseData;
		
	}
	
	public static Map<String, String> ageOfRecordData(String responseData)
	{
		Map<String, String> crimcaseData = new HashMap<String, String>();
		
		ScreeningResult sr = deserializeJson(responseData);
		Set<CrimCase> cc = sr.getCrimCases();
		Iterator crimCaseItr = cc.iterator();
		for(int i=0;i<cc.size();i++)
		{
			CrimCase ccm = (CrimCase) crimCaseItr.next();
			Set<CrimCharge> cchr = ccm.getCrimCharges();
			Iterator itrCharges = cchr.iterator();
			for(int j=0;j<cchr.size();j++)
			{
				CrimCharge cchrSingle = (CrimCharge) itrCharges.next();
				
				crimcaseData.put("AgeOfRecord_"+(i+1)+"_"+(j+1), String.valueOf(cchrSingle.getAgeOfRecord()));
				crimcaseData.put("AgeOfSentence_"+(i+1)+"_"+(j+1), String.valueOf(cchrSingle.getAgeOfSentence()));
				crimcaseData.put("ageofSentenceProbation_"+(i+1)+"_"+(j+1), String.valueOf(cchrSingle.getAgeofSentenceProbation()));
				
			}
		}
		
		return crimcaseData;
	}
	
	public static Map<String, String> probationAndWarrantStatusData(String responseData)
	{
		Map<String, String> crimcaseData = new HashMap<String, String>();
		
		ScreeningResult sr = deserializeJson(responseData);
		Set<CrimCase> cc = sr.getCrimCases();
		Iterator crimCaseItr = cc.iterator();
		for(int i=0;i<cc.size();i++)
		{
			CrimCase ccm = (CrimCase) crimCaseItr.next();
			Set<CrimCharge> cchr = ccm.getCrimCharges();
			Iterator itrCharges = cchr.iterator();
			for(int j=0;j<cchr.size();j++)
			{
				CrimCharge cchrSingle = (CrimCharge) itrCharges.next();
				
				crimcaseData.put("IsProbationActive_"+(i+1)+"_"+(j+1), String.valueOf(cchrSingle.getIsProbationActive()));
				if(cchrSingle.getWarrantDate() == null)
				{
					crimcaseData.put("warrantDate"+"_"+(i+1)+"_"+(j+1), "");
				}else{
					crimcaseData.put("warrantDate"+"_"+(i+1)+"_"+(j+1), cchrSingle.getWarrantDate().toString());
				}
				crimcaseData.put("ageOfWarrant_"+(i+1)+"_"+(j+1), String.valueOf(cchrSingle.getAgeOfWarrant()));
			}
		}
		
		return crimcaseData;
	}
	
	public static void main(String[] args) {
		String s = "{\"screeningResultId\": 17890883,\"crimCases\": [ {\"crimCaseId\": 41207215,\"systemIdentifier\": \"889533\",\"isScoreComplete\": true,\"needsPIIReview\": false,\"lastOnRecordName\": \"adams\",\"firstOnRecordName\": \"aaron\",\"cityOnRecord\": \"Chambers\",\"stateOnRecord\": \"ALABAMA\",\"postCodeOnRecord\": \"36862\",\"middleInitialOnRecord\": \"Chris\",\"dobOnFile\": \"1963-03-15\",\"governmentId\": \"427060653\",\"dispositionDate\": \"2009-10-10\",\"arrestDate\": \"2008-10-10\",\"jurisdiction\": {\"countyName\": \"CHAMBERS\",\"stateA2Cd\": \"AL\",\"countryA2Cd\": \"US\",\"qualifiedName\": \"CHAMBERS,AL,US\", \"jurisdictionType\": {\"jurisdictionTypeName\": \"County\"}},\"caseNBR\": \"6B00330063\",\"knownResult\": \"KNOWN_HIT\",\"isReportedEarlier\": false,\"crimCharges\": [ {\"crimChargeId\": 56606759,\"systemIdentifier\": \"1540031\",\"isScoreComplete\": true,\"clientDNR\": false,\"actualChargeDescription\": \"Theft Or Deception:Third Degree\", \"actualChargeLevel\": {\"level\": \"MISDEMEANOR\",\"description\": \"Misdemeanor\"},\"actualDispositionType\": {\"dispositionType\": \"UNKNOWN\",\"description\": \"Unknown\",\"weightage\": 0},\"statuteCode\": \"\",\"chargeCategory\": {\"category\": \"Theft\",\"description\": \"Theft\"},\"ageOfRecord\": 8,\"ageOfSentence\": 8,\"ageofSentenceProbation\": 7,\"ageOnOffense\": 46,\"ageOfArrest\": 8,\"sanitizedChargeLevel\": {\"level\": \"MISDEMEANOR\",\"description\": \"Misdemeanor\"},\"sanitizedDispositionType\": {\"dispositionType\": \"ALTERNATE_DISPOSITION\",\"description\": \"ALTERNATE DISPOSITION\",\"weightage\": 0},\"mappedDispositionType\": {\"dispositionType\": \"NON_CONVICTION\",\"description\": \"Non Conviction\",\"weightage\": 1},\"saniztizedChargeDescription\": {\"description\": \"Theft Or Deception:Third Degree\"},\"missingGlobalReportingParams\": [],\"ruleDecision\": \"REPORT\",\"actualDispositionDescription\": \"Pretrial Diversion\",\"sanitizedDispositionDescription\": {\"description\": \"Pretrial Diversion\"},\"isReportedEarlier\": false,\"crimSentences\": [ {\"crimSentenceId\": 24036001,\"isScoreComplete\": true,\"actualSentenceType\": {\"sentenceType\": \"PROBATION\",\"description\": \"Probation\",\"sentenceCategory\": \"DURATION\",\"considerDuration\": true,\"durationServed\": false},\"sentenceLengthMonths\": 6,\"consecutive\": false,\"suspended\": false,\"sanitizedSentenceType\": {\"sentenceType\": \"PROBATION\",\"description\": \"Probation\",\"sentenceCategory\": \"DURATION\",\"considerDuration\": true,\"durationServed\": false},\"systemIdentifier\": \"1050482\",\"missingGlobalReportingParams\": [],\"sentenceSanitizationLevel\": \"NONE\",\"slgMissing\": false}], \"dispositionDate\": \"2009-10-10\",\"arrestDate\": \"2008-10-10\",\"chargeDescriptionLevel\": {\"chargeLevel\": {\"level\": \"MISDEMEANOR\",\"description\": \"Misdemeanor\"},\"chargeDescription\": {\"description\": \"Theft Or Deception:Third Degree\"}},\"chargeLevelDisposition\": {\"chargeLevel\": {\"level\": \"MISDEMEANOR\",\"description\": \"Misdemeanor\"},\"dispositionType\": {\"dispositionType\": \"ALTERNATE_DISPOSITION\",\"description\": \"ALTERNATE DISPOSITION\",\"weightage\": 0}},\"chargeCategoryDescription\": {\"chargeCategory\": {\"category\": \"Theft\",\"description\": \"Theft\"},\"chargeDescription\": {\"description\": \"Theft Or Deception:Third Degree\"}},\"ageOfApplicant\": 54,\"additionalItems\": [],\"warrantDate\": \"2010-07-10\",\"ageOfWarrant\": 7,\"chargeSanitizationLevel\": \"SPECIFIC_JURISDICTION_DESCRIPTION\",\"dispositionSanitizationLevel\": \"NATIONAL_JURISDICTION_DESCRIPTION\",\"chargeRuleHistory\": [ {\"ruleName\": \"Open_Warrant\",\"ruleType\": \"FCRA\",\"ruleDescription\": \"Any record type with an Active Warrant regardless of age,BECOMES REPORTABLE.\", \"timestamp\": \"2017-09-18\",\"timestampStr\": \"2017-09-18 11:34:42\"},{\"ruleName\": \"Deferred_Disposition_All_Probation_Report\",\"ruleType\": \"FCRA\",\"ruleDescription\": \"Deferred Disposition with Age of Record GREATER THAN 0 Years & Age of Sentence LESS THAN 99 Years & Probation Active = YES will be Reported\",\"timestamp\": \"2017-09-18\",\"timestampStr\": \"2017-09-18 11:34:42\"}], \"isProbationActive\": true,\"isChargeInClientDNR\": false,\"durationType\": \"PROBATION\",\"isChargeInGlobalDNR\": false,\"slgMissing\": false,\"ageOfDisposition\": 7,\"isEqualProbationStatus\": false,\"sentenceEmptyForConvictionCharge\": false,\"probationDisposition\": {\"dispositionType\": {\"dispositionType\": \"ALTERNATE_DISPOSITION\",\"description\": \"ALTERNATE DISPOSITION\",\"weightage\": 0},\"probationStatus\": {\"probationStatusId\": 1,\"probationStatusName\": \"Active\",\"probationStatusValue\": \"Active\"}},\"aorDays\": 3265,\"fine\": 0,\"isInScope\": false}], \"missingGlobalReportingParams\": [],\"ruleDecision\": \"REPORT\",\"chargeLevel\": {\"level\": \"MISDEMEANOR\",\"description\": \"Misdemeanor\"},\"dispositionType\": {\"dispositionType\": \"ALTERNATE_DISPOSITION\",\"description\": \"ALTERNATE DISPOSITION\",\"weightage\": 0},\"jurisdictionDetail\": {\"countyName\": \"CHAMBERS\",\"stateA2Cd\": \"AL\",\"countryA2Cd\": \"US\",\"type\": {\"jurisdictionTypeName\": \"County\"}},\"caseStatus\": \"ACTIVE\",\"piiWeightageScore\": 4.50,\"personalIdentifiers\": [ {\"identifier\": \"Middle name match.\",\"weightage\": 0.50,\"piiMatch\": \"MATCH\"},{\"identifier\": \"Driver license no match.\",\"weightage\": 0,\"piiMatch\": \"NO_MATCH\"},{\"identifier\": \"Date Of Birth match.\",\"weightage\": 1.00,\"piiMatch\": \"MATCH\"},{\"identifier\": \"First and last name match.\",\"weightage\": 1.00,\"piiMatch\": \"MATCH\"},{\"identifier\": \"SSN match.\",\"weightage\": 2.00,\"piiMatch\": \"MATCH\"},{\"identifier\": \"Address no match.\",\"weightage\": 0.00,\"piiMatch\": \"NO_MATCH\"}], \"caseScore\": 1420091010,\"additionalItems\": [],\"requestAdditionalInfo\": \" Address:Chambers,chambers,36862,alabama, Us, Chambers, 36862, ALABAMA, Warrant Date : 07/10/2010\",\"slgMissing\": false,\"isPIIMisMatch\": false,\"isProhibitedState\": false,\"validatorCounties\": [],\"Offenses\": []}], \"requestURI\": \"CRIM Request URI\",\"resultScoreComplete\": true,\"containsActiveSentence\": false,\"sentenceMissing\": false,\"smartDataStatus\": {\"smartDataStatusId\": 4,\"statusName\": \"INFO_NEEDED\",\"statusDescription\": \"Info needed due to insufficient scoring\",\"type\": \"Record\"},\"isHit\": true,\"screeningId\": \"1647922\",\"orderId\": \"SMD329214\",\"orderDate\": \"2015-04-14\",\"parentSystem\": {\"parentSystem\": \"Admin Client\",\"isSampleSystem\": false},\"product\": {\"code\": \"CRFM\",\"description\": \"Criminal County\",\"parentSystem\": {\"parentSystem\": \"Admin Client\",\"isSampleSystem\": false},\"productCategory\": [ {\"productCategoryId\": 1,\"categoryName\": \"County Criminal\",\"description\": \"Criminal\",\"creationDate\": \"2016-01-13\",\"modificationDate\": \"2016-01-13\"}]},\"salaryRange\": {\"salaryRangeHigh\": 19999,\"salaryRangeLow\": 0},\"subjectResidentState\": {\"stateA2Cd\": \"AL \",\"countryA2Cd\": \"US \",\"stateName\": \"Alabama\",\"qualifiedName\": \"Alabama,US \", \"jurisdictionType\": {\"jurisdictionTypeName\": \"State\"}},\"subjectRequestState\": {\"stateA2Cd\": \"AL \",\"countryA2Cd\": \"US \",\"stateName\": \"Alabama\",\"qualifiedName\": \"Alabama,US \", \"jurisdictionType\": {\"jurisdictionTypeName\": \"State\"}},\"screeningReferenceId\": \"SMD329214\",\"account\": {\"accountName\": \"Friday Staffing-81983\"},\"knownResult\": \"NEW_RECORD\",\"productCategory\": {\"productCategoryId\": 1,\"categoryName\": \"County Criminal\",\"description\": \"Criminal\",\"creationDate\": \"2016-01-13\",\"modificationDate\": \"2016-01-13\"},\"jurisdictionType\": {\"jurisdictionTypeName\": \"County\"},\"jurisdiction\": {\"countyName\": \"CHAMBERS\",\"stateA2Cd\": \"AL\",\"countryA2Cd\": \"US\",\"qualifiedName\": \"CHAMBERS,AL,US\", \"jurisdictionType\": {\"jurisdictionTypeName\": \"County\"}},\"jurisdictionDetail\": {\"countyName\": \"CHAMBERS\",\"stateA2Cd\": \"AL\",\"countryA2Cd\": \"US\",\"type\": {\"jurisdictionTypeName\": \"County\"}},\"isNewRequest\": true,\"missingGlobalReportingParams\": [],\"agent\": {\"agentId\": 1,\"agentName\": \"AG\",\"protocol\": {\"protocolId\": 1,\"protocolType\": \"Azure \"}},\"logHistoryNotes\": [ {\"logHistoryNotesID\": 37141951,\"smartDataStatus\": {\"smartDataStatusId\": 20,\"statusName\": \"REQ_PROFILING\",\"statusDescription\": \"request entered\",\"type\": \"Record\"},\"historyNotesDescription\": \"request entered\",\"timestampLogHistory\": \"2017-09-18\",\"createdAt\": \"2017-09-18 11:34:40\"},{\"logHistoryNotesID\": 37141952,\"smartDataStatus\": {\"smartDataStatusId\": 5,\"statusName\": \"INFO_PROVIDED\",\"statusDescription\": \"Info Provided by Parent System\",\"type\": \"Record\"},\"historyNotesDescription\": \"Info Provided by Parent System\",\"timestampLogHistory\": \"2017-09-18\",\"createdAt\": \"2017-09-18 11:34:41\"},{\"logHistoryNotesID\": 0,\"smartDataStatus\": {\"smartDataStatusId\": 22,\"statusName\": \"LOAD_GLOBAL_CLIENT_SETTING\",\"statusDescription\": \"Load global client setting\",\"type\": \"Record\"},\"historyNotesDescription\": \"Load global client setting\",\"timestampLogHistory\": \"2017-09-18\",\"createdAt\": \"2017-09-18 11:34:41\"},{\"logHistoryNotesID\": 0,\"smartDataStatus\": {\"smartDataStatusId\": 6,\"statusName\": \"DATA_SANITIZIED\",\"statusDescription\": \"Case/Charge sanitization done\",\"type\": \"Record\"},\"historyNotesDescription\": \"Case/Charge sanitization done\",\"timestampLogHistory\": \"2017-09-18\",\"createdAt\": \"2017-09-18 11:34:42\"},{\"logHistoryNotesID\": 0,\"smartDataStatus\": {\"smartDataStatusId\": 7,\"statusName\": \"RULES_APPLIED\",\"statusDescription\": \"Rules Applied\",\"type\": \"Record\"},\"historyNotesDescription\": \"Rules Applied\",\"timestampLogHistory\": \"2017-09-18\",\"createdAt\": \"2017-09-18 11:34:42\"},{\"logHistoryNotesID\": 0,\"smartDataStatus\": {\"smartDataStatusId\": 3,\"statusName\": \"SCORING_SUCCESS\",\"statusDescription\": \"Scoring and PII success\",\"type\": \"Record\"},\"historyNotesDescription\": \"Scoring and PII success\",\"timestampLogHistory\": \"2017-09-18\",\"createdAt\": \"2017-09-18 11:34:42\"},{\"logHistoryNotesID\": 0,\"smartDataStatus\": {\"smartDataStatusId\": 21,\"statusName\": \"RES_PROFILING\",\"statusDescription\": \"response preprocesing\",\"type\": \"Record\"},\"historyNotesDescription\": \"response preprocesing\",\"timestampLogHistory\": \"2017-09-18\",\"createdAt\": \"2017-09-18 11:34:42\"},{\"logHistoryNotesID\": 0,\"smartDataStatus\": {\"smartDataStatusId\": 4,\"statusName\": \"INFO_NEEDED\",\"statusDescription\": \"Info needed due to insufficient scoring\",\"type\": \"Record\"},\"historyNotesDescription\": \"Info needed due to insufficient scoring\",\"timestampLogHistory\": \"2017-09-18\",\"createdAt\": \"2017-09-18 11:34:42\"}], \"globalClientSetting\": {\"ruleType\": \"FCRA\",\"numberOfReportableCases\": 100,\"manualReviewRequired\": false,\"convictionState\": {\"countryName\": \"United States\",\"countryA2Cd\": \"US \",\"qualifiedName\": \"US \",\"jurisdictionType\": {\"jurisdictionTypeName\": \"National\"}},\"nonConvictionState\": {\"countryName\": \"United States\",\"countryA2Cd\": \"US \",\"qualifiedName\": \"US \",\"jurisdictionType\": {\"jurisdictionTypeName\": \"National\"}},\"isJuvenilleState\": false,\"juvenilleAgeLimit\": 16,\"infoNeededRequired\": false},\"containsNotReviewedCases\": false,\"isKeywordSearchReviewed\": true,\"piiCaseThresoldScore\": 2.0,\"additionalItems\": [ \"Needs additional Info due to keyword:KEYWORD : (WARRANT) FOUND AT CASE:6B00330063\", \"Processed from SmartData AWS server\" ], \"needsPIIReview\": false,\"keywordAsString\": [ \"Keyword:(Warrant) found at Case: 6B00330063\" ],\"slgMissing\": false,\"isCommonName\": true,\"verifyJuvenilleInfo\": false,\"overrideManualReview\": false,\"escapeKeywordSearch\": false,\"machineName\": \"2933bf16579f\",\"smartDataOrderCreatedAt\": \"2017-09-18\",\"requestingSystem\": \"ag\",\"brokerName\": \"SQS\",\"dynamicDatasource\": {\"dataSourceId\": 213,\"datasourceName\": \"MANUAL\",\"dataFrequency\": 0,\"dispositionTypes\": [],\"chargeLevels\": [],\"datasourceStatus\": {\"DatasourceStatusId\": 4,\"statusName\": \"NOTACTIVE\",\"statusDescription\": \"Datasource NotActive\",\"type\": \"Record\"},\"ignoreSample\": true,\"monitoringActive\": false,\"generateSample\": false,\"generateAlert\": false},\"isSampleRequest\": false,\"raiseDatasourceWarning\": false,\"MRForQualityCheck\": false,\"salaryOverideFromSetting\": false,\"residentStateOverideFromSetting\": false,\"requestProcessTimeMillis\": 0}";
		//ScreeningResult sr = deserializeJson(s);
		String s1 = "{\"screeningResultId\":19316400,\"crimCases\":[{\"crimCaseId\":42816392,\"systemIdentifier\":\"882145\",\"isScoreComplete\":true,\"needsPIIReview\":false,\"lastOnRecordName\":\"HERRERA\",\"firstOnRecordName\":\"JEFFREY\",\"cityOnRecord\":\"MATTHEWS\",\"stateOnRecord\":\"NC\",\"postCodeOnRecord\":\"28105 - 0000\",\"middleInitialOnRecord\":\"JOSUE\",\"dobOnFile\":\"1990-01-03\",\"dispositionDate\":\"2010-08-23\",\"arrestDate\":\"2010-06-26\",\"jurisdiction\":{\"countyName\":\"MONTGOMERY\",\"stateA2Cd\":\"MD\",\"countryA2Cd\":\"US\",\"qualifiedName\":\"MONTGOMERY,MD,US\",\"jurisdictionType\":{\"jurisdictionTypeName\":\"County\"}},\"caseType\":\"CRIMINAL\",\"caseNBR\":\"3D00228469\",\"knownResult\":\"KNOWN_HIT\",\"isReportedEarlier\":false,\"crimCharges\":[{\"crimChargeId\":58949796,\"systemIdentifier\":\"1529568\",\"isScoreComplete\":true,\"clientDNR\":false,\"actualChargeDescription\":\"ALC BEV/OPEN CONT/RETL EST\",\"actualChargeLevel\":{\"level\":\"UNKNOWN\",\"description\":\"Unknown\"},\"actualDispositionType\":{\"dispositionType\":\"UNKNOWN\",\"description\":\"Unknown\",\"weightage\":0},\"statuteCode\":\"2B.19.301\",\"chargeCategory\":{\"category\":\"Open Container\",\"description\":\"Open Container\"},\"ageOfRecord\":6,\"ageOfSentence\":6,\"ageofSentenceProbation\":6,\"ageOnOffense\":20,\"ageOfArrest\":7,\"sanitizedChargeLevel\":{\"level\":\"MISDEMEANOR\",\"description\":\"Misdemeanor\"},\"sanitizedDispositionType\":{\"dispositionType\":\"CONVICTION\",\"description\":\"Conviction\",\"weightage\":0},\"mappedDispositionType\":{\"dispositionType\":\"CONVICTION\",\"description\":\"Conviction\",\"weightage\":8},\"saniztizedChargeDescription\":{\"description\":\"Alcohol Beverage/Open Container/Retail Establishment\"},\"missingGlobalReportingParams\":[],\"ruleDecision\":\"REPORT\",\"actualDispositionDescription\":\"Guilty\",\"sanitizedDispositionDescription\":{\"description\":\"Guilty\"},\"isReportedEarlier\":false,\"crimSentences\":[{\"crimSentenceId\":24657312,\"isScoreComplete\":true,\"actualSentenceType\":{\"sentenceType\":\"COURT_COSTS\",\"description\":\"Court Costs\",\"sentenceCategory\":\"MONETARY_AMOUNT\",\"considerDuration\":false,\"durationServed\":false},\"fine\":57.5,\"consecutive\":false,\"suspended\":false,\"sanitizedSentenceType\":{\"sentenceType\":\"COURT_COSTS\",\"description\":\"Court Costs\",\"sentenceCategory\":\"MONETARY_AMOUNT\",\"considerDuration\":false,\"durationServed\":false},\"systemIdentifier\":\"1046207\",\"missingGlobalReportingParams\":[],\"sentenceSanitizationLevel\":\"NONE\",\"slgMissing\":false},{\"crimSentenceId\":24657313,\"isScoreComplete\":true,\"actualSentenceType\":{\"sentenceType\":\"FINE\",\"description\":\"Fine\",\"sentenceCategory\":\"MONETARY_AMOUNT\",\"considerDuration\":false,\"durationServed\":false},\"fine\":25,\"consecutive\":false,\"suspended\":false,\"sanitizedSentenceType\":{\"sentenceType\":\"FINE\",\"description\":\"Fine\",\"sentenceCategory\":\"MONETARY_AMOUNT\",\"considerDuration\":false,\"durationServed\":false},\"systemIdentifier\":\"1046208\",\"missingGlobalReportingParams\":[],\"sentenceSanitizationLevel\":\"NONE\",\"slgMissing\":false}],\"dispositionDate\":\"2010-08-23\",\"arrestDate\":\"2010-06-26\",\"fileDate\":\"2010-06-26\",\"offenseDate\":\"2010-06-26\",\"chargeDescriptionLevel\":{\"chargeLevel\":{\"level\":\"MISDEMEANOR\",\"description\":\"Misdemeanor\"},\"chargeDescription\":{\"description\":\"Alcohol Beverage/Open Container/Retail Establishment\"}},\"chargeLevelDisposition\":{\"chargeLevel\":{\"level\":\"MISDEMEANOR\",\"description\":\"Misdemeanor\"},\"dispositionType\":{\"dispositionType\":\"CONVICTION\",\"description\":\"Conviction\",\"weightage\":0}},\"chargeCategoryDescription\":{\"chargeCategory\":{\"category\":\"Open Container\",\"description\":\"Open Container\"},\"chargeDescription\":{\"description\":\"Alcohol Beverage/Open Container/Retail Establishment\"}},\"ageOfApplicant\":27,\"additionalItems\":[],\"ageOfWarrant\":-1,\"chargeSanitizationLevel\":\"NATIONAL_JURISDICTION_DESCRIPTION\",\"dispositionSanitizationLevel\":\"SPECIFIC_JURISDICTION_DESCRIPTION\",\"chargeRuleHistory\":[{\"ruleName\":\"Conviction_Sentence\",\"ruleType\":\"FCRA\",\"ruleDescription\":\"If a conviction is disposed more than 0 years and the person incurs a JAIL sentence that extends to any date within the past 7 years,IT BECOMES REPORTABLE.\",\"timestamp\":\"2017-07-11\",\"timestampStr\":\"2017-07-11 03 : 50:26\"}],\"isProbationActive\":false,\"isChargeInClientDNR\":false,\"isChargeInGlobalDNR\":false,\"slgMissing\":false,\"ageOfDisposition\":6,\"isEqualProbationStatus\":false,\"sentenceEmptyForConvictionCharge\":false,\"probationDisposition\":{\"dispositionType\":{\"dispositionType\":\"CONVICTION\",\"description\":\"Conviction\",\"weightage\":0},\"probationStatus\":{\"probationStatusId\":1,\"probationStatusName\":\"Inactive\",\"probationStatusValue\":\"Inactive\"}},\"aorDays\":2514,\"fine\":82.5}],\"missingGlobalReportingParams\":[],\"ruleDecision\":\"REPORT\",\"chargeLevel\":{\"level\":\"MISDEMEANOR\",\"description\":\"Misdemeanor\"},\"dispositionType\":{\"dispositionType\":\"CONVICTION\",\"description\":\"Conviction\",\"weightage\":0},\"jurisdictionDetail\":{\"countyName\":\"MONTGOMERY\",\"stateA2Cd\":\"MD\",\"countryA2Cd\":\"US\",\"type\":{\"jurisdictionTypeName\":\"County\"}},\"caseStatus\":\"CLOSED\",\"piiWeightageScore\":2,\"personalIdentifiers\":[{\"identifier\":\"SSN no match.\",\"weightage\":0,\"piiMatch\":\"NO_MATCH\"},{\"identifier\":\"Date Of Birth match.\",\"weightage\":1,\"piiMatch\":\"MATCH\"},{\"identifier\":\"Middle name no match.\",\"weightage\":0,\"piiMatch\":\"NO_MATCH\"},{\"identifier\":\"Address no match.\",\"weightage\":0,\"piiMatch\":\"NO_MATCH\"},{\"identifier\":\"First and last name match.\",\"weightage\":1,\"piiMatch\":\"MATCH\"},{\"identifier\":\"Driver license no match.\",\"weightage\":0,\"piiMatch\":\"NO_MATCH\"}],\"caseScore\":1820100823,\"additionalItems\":[],\"requestAdditionalInfo\":\" Address : 1127 Somersby Ln, Matthews, 28105 - 0000, NC, US\",\"slgMissing\":false,\"isPIIMisMatch\":false,\"isProhibitedState\":false,\"validatorCounties\":[],\"Offenses\":[]}],\"requestURI\":\"CRIM Request URI\",\"resultScoreComplete\":true,\"containsActiveSentence\":false,\"sentenceMissing\":false,\"smartDataStatus\":{\"smartDataStatusId\":10,\"statusName\":\"REVIEWED\",\"statusDescription\":\"Reviewed by Smart Data\",\"type\":\"Record\"},\"isHit\":true,\"screeningId\":\"1643747\",\"orderId\":\"35174823\",\"orderDate\":\"2017-07-11\",\"parentSystem\":{\"parentSystem\":\"Admin Client\",\"isSampleSystem\":false},\"product\":{\"code\":\"CRFM\",\"description\":\"Criminal County\",\"parentSystem\":{\"parentSystem\":\"Admin Client\",\"isSampleSystem\":false},\"productCategory\":[{\"productCategoryId\":1,\"categoryName\":\"County Criminal\",\"description\":\"Criminal\",\"creationDate\":\"2016-01-13\",\"modificationDate\":\"2016-01-13\"}]},\"salaryRange\":{\"salaryRangeHigh\":19999,\"salaryRangeLow\":0},\"subjectResidentState\":{\"stateA2Cd\":\"MD \",\"countryA2Cd\":\"US \",\"stateName\":\"Maryland\",\"qualifiedName\":\"Maryland, US \",\"jurisdictionType\":{\"jurisdictionTypeName\":\"State\"}},\"subjectRequestState\":{\"stateA2Cd\":\"MD \",\"countryA2Cd\":\"US \",\"stateName\":\"Maryland\",\"qualifiedName\":\"Maryland, US \",\"jurisdictionType\":{\"jurisdictionTypeName\":\"State\"}},\"screeningReferenceId\":\"12809750\",\"account\":{\"accountName\":\"Abso Assessments-9955\"},\"profile\":{\"profileId\":142170,\"profileName\":\"0\"},\"knownResult\":\"KNOWN_HIT\",\"productCategory\":{\"productCategoryId\":1,\"categoryName\":\"County Criminal\",\"description\":\"Criminal\",\"creationDate\":\"2016-01-13\",\"modificationDate\":\"2016-01-13\"},\"jurisdictionType\":{\"jurisdictionTypeName\":\"County\"},\"jurisdiction\":{\"countyName\":\"MONTGOMERY\",\"stateA2Cd\":\"MD\",\"countryA2Cd\":\"US\",\"qualifiedName\":\"MONTGOMERY, MD,US\",\"jurisdictionType\":{\"jurisdictionTypeName\":\"County\"}},\"jurisdictionDetail\":{\"countyName\":\"MONTGOMERY\",\"stateA2Cd\":\"MD\",\"countryA2Cd\":\"US\",\"type\":{\"jurisdictionTypeName\":\"COUNTY\"}},\"isNewRequest\":true,\"missingGlobalReportingParams\":[],\"agent\":{\"agentId\":1,\"agentName\":\"AG\",\"protocol\":{\"protocolId\":1,\"protocolType\":\"Azure \"}},\"logHistoryNotes\":[{\"logHistoryNotesID\":38595407,\"smartDataStatus\":{\"smartDataStatusId\":20,\"statusName\":\"REQ_PROFILING\",\"statusDescription\":\"request entered\",\"type\":\"Record\"},\"historyNotesDescription\":\"request entered\",\"timestampLogHistory\":\"2017-07-11\",\"createdAt\":\"2017-07-11 03 : 50:25\"},{\"logHistoryNotesID\":38595408,\"smartDataStatus\":{\"smartDataStatusId\":5,\"statusName\":\"INFO_PROVIDED\",\"statusDescription\":\"Info Provided by Parent System\",\"type\":\"Record\"},\"historyNotesDescription\":\"Info Provided by Parent System\",\"timestampLogHistory\":\"2017-07-11\",\"createdAt\":\"2017-07-11 03 : 50:25\"},{\"logHistoryNotesID\":0,\"smartDataStatus\":{\"smartDataStatusId\":22,\"statusName\":\"LOAD_GLOBAL_CLIENT_SETTING\",\"statusDescription\":\"Load global client setting\",\"type\":\"Record\"},\"historyNotesDescription\":\"Load global client setting\",\"timestampLogHistory\":\"2017-07-11\",\"createdAt\":\"2017-07-11 03 : 50:25\"},{\"logHistoryNotesID\":0,\"smartDataStatus\":{\"smartDataStatusId\":6,\"statusName\":\"DATA_SANITIZIED\",\"statusDescription\":\"Case/Charge sanitization done\",\"type\":\"Record\"},\"historyNotesDescription\":\"Case/Charge sanitization done\",\"timestampLogHistory\":\"2017-07-11\",\"createdAt\":\"2017-07-11 03 : 50:25\"},{\"logHistoryNotesID\":0,\"smartDataStatus\":{\"smartDataStatusId\":7,\"statusName\":\"RULES_APPLIED\",\"statusDescription\":\"Rules Applied\",\"type\":\"Record\"},\"historyNotesDescription\":\"Rules Applied\",\"timestampLogHistory\":\"2017-07-11\",\"createdAt\":\"2017-07-11 03 : 50:26\"},{\"logHistoryNotesID\":0,\"smartDataStatus\":{\"smartDataStatusId\":3,\"statusName\":\"SCORING_SUCCESS\",\"statusDescription\":\"Scoring and PII success\",\"type\":\"Record\"},\"historyNotesDescription\":\"Scoring and PII success\",\"timestampLogHistory\":\"2017-07-11\",\"createdAt\":\"2017-07-11 03 : 50:26\"},{\"logHistoryNotesID\":0,\"smartDataStatus\":{\"smartDataStatusId\":21,\"statusName\":\"RES_PROFILING\",\"statusDescription\":\"response preprocesing\",\"type\":\"Record\"},\"historyNotesDescription\":\"response preprocesing\",\"timestampLogHistory\":\"2017-07-11\",\"createdAt\":\"2017-07-11 03 : 50:26\"},{\"logHistoryNotesID\":0,\"smartDataStatus\":{\"smartDataStatusId\":10,\"statusName\":\"REVIEWED\",\"statusDescription\":\"Reviewed by Smart Data\",\"type\":\"Record\"},\"historyNotesDescription\":\"Reviewed by Smart Data\",\"timestampLogHistory\":\"2017-07-11\",\"createdAt\":\"2017-07-11 03 : 50:26\"}],\"globalClientSetting\":{\"ruleType\":\"FCRA\",\"numberOfReportableCases\":5,\"manualReviewRequired\":false,\"convictionState\":{\"stateA2Cd\":\"MD \",\"countryA2Cd\":\"US \",\"stateName\":\"Maryland\",\"qualifiedName\":\"Maryland, US \",\"jurisdictionType\":{\"jurisdictionTypeName\":\"State\"}},\"nonConvictionState\":{\"countryName\":\"United States\",\"countryA2Cd\":\"US \",\"qualifiedName\":\"US \",\"jurisdictionType\":{\"jurisdictionTypeName\":\"National\"}},\"isJuvenilleState\":false,\"juvenilleAgeLimit\":16,\"infoNeededRequired\":false},\"containsNotReviewedCases\":false,\"isKeywordSearchReviewed\":false,\"piiCaseThresoldScore\":1.5,\"additionalItems\":[\"Processed from SmartData AWS server\"],\"needsPIIReview\":false,\"keywordAsString\":[],\"slgMissing\":false,\"isCommonName\":false,\"verifyJuvenilleInfo\":false,\"overrideManualReview\":false,\"escapeKeywordSearch\":false,\"machineName\":\"4fdeded2a823\",\"smartDataOrderCreatedAt\":\"2017-07-11\",\"requestingSystem\":\"ag\",\"brokerName\":\"SQS\",\"dynamicDatasource\":{\"dataSourceId\":222,\"fulfillmentType\":{\"fulfillmentTypeId\":2,\"fulfillmentType\":\"MIP-CWI\",\"pdlProductCategoryJurisdictionDatasources\":[]},\"datasourceName\":\"CWI_MD_STATEWIDE\",\"scopeOfSearch\":25,\"dataFrequency\":3,\"dispositionTypes\":[{\"dispositionType\":\"CONVICTION\",\"description\":\"Conviction\",\"weightage\":0},{\"dispositionType\":\"NON_CONVICTION\",\"description\":\"Non Conviction\",\"weightage\":0},{\"dispositionType\":\"ACTIVE\",\"description\":\"ACTIVE\",\"weightage\":0},{\"dispositionType\":\"ALTERNATE_DISPOSITION\",\"description\":\"ALTERNATE DISPOSITION\",\"weightage\":0}],\"chargeLevels\":[{\"level\":\"MISDEMEANOR\",\"description\":\"Misdemeanor\"},{\"level\":\"FELONY\",\"description\":\"Felony\"}],\"datasourceStatus\":{\"DatasourceStatusId\":4,\"statusName\":\"NOTACTIVE\",\"statusDescription\":\"Datasource NotActive\",\"type\":\"Record\"},\"ignoreSample\":true,\"monitoringActive\":false,\"generateSample\":false,\"generateAlert\":false},\"isSampleRequest\":false,\"containsDisputeInfo\":false,\"raiseDatasourceWarning\":false,\"MRForQualityCheck\":false,\"salaryOverideFromSetting\":false,\"residentStateOverideFromSetting\":false,\"requestProcessTimeMillis\":0}";
		Map<String, String> crimdata = new HashMap<String, String>();
		crimdata = probationAndWarrantStatusData(s);
		System.out.println(crimdata);
		
	}

}

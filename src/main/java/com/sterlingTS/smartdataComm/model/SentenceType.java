package com.sterlingTS.smartdataComm.model;

import com.google.gson.annotations.Expose;

public class SentenceType {

	@Expose(serialize = false)
	private int sentenceTypeId;
	private String sentenceType;
	private String description;
	private String sentenceCategory;

	public int getSentenceTypeId() {
		return sentenceTypeId;
	}

	public void setSentenceTypeId(final int sentenceTypeId) {
		this.sentenceTypeId = sentenceTypeId;
	}

	public String getSentenceType() {
		return sentenceType;
	}

	public void setSentenceType(final String sentenceType) {
		this.sentenceType = sentenceType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getSentenceCategory() {
		return sentenceCategory;
	}

	public void setSentenceCategory(String sentenceCategory) {
		this.sentenceCategory = sentenceCategory;
	}
}

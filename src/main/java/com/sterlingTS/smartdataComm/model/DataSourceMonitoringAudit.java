package com.sterlingTS.smartdataComm.model;

public class DataSourceMonitoringAudit {

	private int dataSourceMonitoringAuditId;
	private DynamicDatasource dynamicDatasource;
	private ProductCategorySearchParam productCategorySearchParam;
	private String expectedValue;
	private String existingValue;
	private boolean compatible;
	private ProductCategoryMonitoringAudit productCategoryMonitoringAudit;

	public boolean isCompatible() {
		return compatible;
	}

	public void setCompatible(boolean compatible) {
		this.compatible = compatible;
	}

	public DynamicDatasource getDynamicDatasource() {
		return dynamicDatasource;
	}

	public void setDynamicDatasource(DynamicDatasource dynamicDatasource) {
		this.dynamicDatasource = dynamicDatasource;
	}

	public ProductCategorySearchParam getProductCategorySearchParam() {
		return productCategorySearchParam;
	}

	public void setProductCategorySearchParam(ProductCategorySearchParam productCategorySearchParam) {
		this.productCategorySearchParam = productCategorySearchParam;
	}

	public String getExpectedValue() {
		return expectedValue;
	}

	public void setExpectedValue(String expectedValue) {
		this.expectedValue = expectedValue;
	}

	public String getExistingValue() {
		return existingValue;
	}

	public void setExistingValue(String existingValue) {
		this.existingValue = existingValue;
	}

	public ProductCategoryMonitoringAudit getProductCategoryMonitoringAudit() {
		return productCategoryMonitoringAudit;
	}

	public void setProductCategoryMonitoringAudit(ProductCategoryMonitoringAudit productCategoryMonitoringAudit) {
		this.productCategoryMonitoringAudit = productCategoryMonitoringAudit;
	}

	public int getDataSourceMonitoringAuditId() {
		return dataSourceMonitoringAuditId;
	}

	public void setDataSourceMonitoringAuditId(int dataSourceMonitoringAuditId) {
		this.dataSourceMonitoringAuditId = dataSourceMonitoringAuditId;
	}
}

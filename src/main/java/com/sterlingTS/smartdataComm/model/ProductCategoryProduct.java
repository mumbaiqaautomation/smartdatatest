package com.sterlingTS.smartdataComm.model;

public class ProductCategoryProduct implements java.io.Serializable {

	private int productCategoryProductId;
	private Product product;
	private ProductCategory productCategory;
	public int getProductCategoryProductId() {
		return productCategoryProductId;
	}
	public void setProductCategoryProductId(int productCategoryProductId) {
		this.productCategoryProductId = productCategoryProductId;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public ProductCategory getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(ProductCategory productCategory) {
		this.productCategory = productCategory;
	}
}

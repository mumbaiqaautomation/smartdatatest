package com.sterlingTS.smartdataComm.model;

public class SentenceAbbreviation implements java.io.Serializable {

	private int sentenceAbbreviationId;
	private SentenceType sentenceType;
	private String abbreviation;
	private String description;
	public int getSentenceAbbreviationId() {
		return sentenceAbbreviationId;
	}
	public void setSentenceAbbreviationId(int sentenceAbbreviationId) {
		this.sentenceAbbreviationId = sentenceAbbreviationId;
	}
	public SentenceType getSentenceType() {
		return sentenceType;
	}
	public void setSentenceType(SentenceType sentenceType) {
		this.sentenceType = sentenceType;
	}
	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}

package com.sterlingTS.smartdataComm.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gson.annotations.Expose;
import com.sterlingTS.smartdataComm.model.enums.CaseStatusEnum;
import com.sterlingTS.smartdataComm.model.enums.GenderEnum;
import com.sterlingTS.smartdataComm.model.enums.KnownResultEnum;
import com.sterlingTS.smartdataComm.model.enums.RuleDecisionEnum;

public class CrimCase {

	private int crimCaseId;
	private String systemIdentifier;
	@Expose(serialize = false)
	private boolean isScoreComplete;
	@Expose(serialize = false)
	private boolean needsPIIReview;

	private String lastOnRecordName;

	private String firstOnRecordName;

	private String addressOnRecord;

	private String dlOnRecord;

	private String middleInitialOnRecord;

	private String actualDispositionDescription;

	private Date dobOnFile;

	private String governmentId;
	private Date dispositionDate;
	private Date arrestDate;
	@Expose(serialize = false)
	private Jurisdiction jurisdiction;

	private GenderEnum genderCode;
	@Expose(serialize = false)
	private int ageOnOffenseDate;
	private String caseType;
	private String caseNBR;
	private KnownResultEnum knownResult;
	private Boolean isReportedEarlier;
	@Expose(serialize = false)
	private DispositionDescription sanitizedDispositionDescription;
	private Set<CrimCharge> crimCharges = new HashSet<CrimCharge>();
	@Expose(serialize = false)
	private ScreeningResult screeningResult;
	@Expose(serialize = false)
	private List<ProductCategoryReportingParam> missingGlobalReportingParams;
	@Expose(serialize = false)
	private RuleDecisionEnum ruleDecision;
	private ChargeLevel chargeLevel;
	private DispositionType dispositionType;
	private JurisdictionDetail jurisdictionDetail;
	@Expose(serialize = false)
	private CaseStatusEnum caseStatus;
	@Expose(serialize = false)
	private BigDecimal piiWeightageScore = BigDecimal.ZERO;
	@Expose(serialize = false)
	private Set<String> mismatchedPiiParams = new HashSet<String>();
	private String cityOnRecord;
	private String stateOnRecord;
	private String postCodeOnRecord;
	private String birthYYYY;
	private String birthMM;
	private String birthDD;
	private List<String> additionalItems = new ArrayList<String>();
	private String requestAdditionalInfo;
	@Expose(serialize = false)
	private boolean slgMissing;
	@Expose(serialize = false)
	private boolean isPIIMisMatch;
	private String courtOnRecord;
	private String countyOnRecord;
	private String districtCode;
	private Date caseReleaseDate;
	private Date caseFileDate;
	private String arrestTypeClassification;
	private List<String> Offenses = new ArrayList<String>();
	private String subjectConfirmationType;
	private List<ValidatorCounty> validatorCounties = new ArrayList<ValidatorCounty>();
	private boolean isProhibitedState;
	private String arrestingAgency;
	private String agencyDescription;

	@Expose(serialize = false)
	private Set<PersonalIdentifier> personalIdentifiers = new HashSet<PersonalIdentifier>();

	public Set<CrimCharge> getCrimCharges() {
		return crimCharges;
	}

	public void setCrimCharges(final Set<CrimCharge> crimCharges) {
		this.crimCharges = crimCharges;
	}

	public int getCrimCaseId() {
		return crimCaseId;
	}

	public void setCrimCaseId(final int crimCaseId) {
		this.crimCaseId = crimCaseId;
	}

	public String getLastOnRecordName() {
		return lastOnRecordName;
	}

	public void setLastOnRecordName(final String lastOnRecordName) {
		this.lastOnRecordName = lastOnRecordName;
	}

	public String getFirstOnRecordName() {
		return firstOnRecordName;
	}

	public void setFirstOnRecordName(final String firstOnRecordName) {
		this.firstOnRecordName = firstOnRecordName;
	}

	public String getDlOnRecord() {
		return dlOnRecord;
	}

	public void setDlOnRecord(final String dlOnRecord) {
		this.dlOnRecord = dlOnRecord;
	}

	public String getMiddleInitialOnRecord() {
		return middleInitialOnRecord;
	}

	public void setMiddleInitialOnRecord(final String middleInitialOnRecord) {
		this.middleInitialOnRecord = middleInitialOnRecord;
	}

	public String getActualDispositionDescription() {
		return actualDispositionDescription;
	}

	public void setActualDispositionDescription(final String actualDispositionDescription) {
		this.actualDispositionDescription = actualDispositionDescription;
	}

	public Date getDobOnFile() {
		return dobOnFile;
	}

	public void setDobOnFile(final Date dobOnFile) {
		this.dobOnFile = dobOnFile;
	}

	public String getGovernmentId() {
		return governmentId;
	}

	public void setGovernmentId(final String governmentId) {
		this.governmentId = governmentId;
	}

	public Date getDispositionDate() {
		return dispositionDate;
	}

	public void setDispositionDate(final Date dispositionDate) {
		this.dispositionDate = dispositionDate;
	}

	public Date getArrestDate() {
		return arrestDate;
	}

	public void setArrestDate(final Date arrestDate) {
		this.arrestDate = arrestDate;
	}

	public Jurisdiction getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(final Jurisdiction jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public ScreeningResult getScreeningResult() {
		return screeningResult;
	}

	public void setScreeningResult(final ScreeningResult screeningResult) {
		this.screeningResult = screeningResult;
	}

	public GenderEnum getGenderCode() {
		return genderCode;
	}

	public void setGenderCode(final GenderEnum genderCode) {
		this.genderCode = genderCode;
	}

	public int getAgeOnOffenseDate() {
		return ageOnOffenseDate;
	}

	public void setAgeOnOffenseDate(final int ageOnOffenseDate) {
		this.ageOnOffenseDate = ageOnOffenseDate;
	}

	public String getCaseType() {
		return caseType;
	}

	public void setCaseType(final String caseType) {
		this.caseType = caseType;
	}

	public String getCaseNBR() {
		return caseNBR;
	}

	public void setCaseNBR(final String caseNBR) {
		this.caseNBR = caseNBR;
	}

	public KnownResultEnum getKnownResult() {
		return knownResult;
	}

	public void setKnownResult(final KnownResultEnum knownResult) {
		this.knownResult = knownResult;
	}

	public Boolean getIsReportedEarlier() {
		return isReportedEarlier;
	}

	public void setIsReportedEarlier(final Boolean isReportedEarlier) {
		this.isReportedEarlier = isReportedEarlier;
	}

	public DispositionDescription getSanitizedDispositionDescription() {
		return sanitizedDispositionDescription;
	}

	public void setSanitizedDispositionDescription(final DispositionDescription sanitizedDispositionDescription) {
		this.sanitizedDispositionDescription = sanitizedDispositionDescription;
	}

	public String getAddressOnRecord() {
		return addressOnRecord;
	}

	public void setAddressOnRecord(final String addressOnRecord) {
		this.addressOnRecord = addressOnRecord;
	}

	public boolean getIsScoreComplete() {
		return isScoreComplete;
	}

	public void setIsScoreComplete(final boolean isScoreComplete) {
		this.isScoreComplete = isScoreComplete;
	}

	public RuleDecisionEnum getRuleDecision() {
		return ruleDecision;
	}

	public void setRuleDecision(final RuleDecisionEnum ruleDecision) {
		this.ruleDecision = ruleDecision;
	}

	public ChargeLevel getChargeLevel() {
		return chargeLevel;
	}

	public void setChargeLevel(final ChargeLevel chargeLevel) {
		this.chargeLevel = chargeLevel;
	}

	public DispositionType getDispositionType() {
		return dispositionType;
	}

	public void setDispositionType(final DispositionType dispositionType) {
		this.dispositionType = dispositionType;
	}

	public boolean isNeedsPIIReview() {
		return needsPIIReview;
	}

	public void setNeedsPIIReview(final boolean needsPIIReview) {
		this.needsPIIReview = needsPIIReview;
	}

	public String getSystemIdentifier() {
		return systemIdentifier;
	}

	public void setSystemIdentifier(final String systemIdentifier) {
		this.systemIdentifier = systemIdentifier;
	}

	public JurisdictionDetail getJurisdictionDetail() {
		return jurisdictionDetail;
	}

	public void setJurisdictionDetail(final JurisdictionDetail jurisdictionDetail) {
		this.jurisdictionDetail = jurisdictionDetail;
	}

	public CaseStatusEnum getCaseStatus() {
		return caseStatus;
	}

	public void setCaseStatus(CaseStatusEnum caseStatus) {
		this.caseStatus = caseStatus;
	}

	public Set<String> getMismatchedPiiParams() {
		return mismatchedPiiParams;
	}

	public void setMismatchedPiiParams(final Set<String> mismatchedPiiParams) {
		this.mismatchedPiiParams = mismatchedPiiParams;
	}

	public BigDecimal getPiiWeightageScore() {
		return piiWeightageScore;
	}

	public void setPiiWeightageScore(BigDecimal piiWeightageScore) {
		this.piiWeightageScore = piiWeightageScore;
	}

	public String getCityOnRecord() {
		return cityOnRecord;
	}

	public void setCityOnRecord(String cityOnRecord) {
		this.cityOnRecord = cityOnRecord;
	}

	public String getStateOnRecord() {
		return stateOnRecord;
	}

	public void setStateOnRecord(String stateOnRecord) {
		this.stateOnRecord = stateOnRecord;
	}

	public String getPostCodeOnRecord() {
		return postCodeOnRecord;
	}

	public void setPostCodeOnRecord(String postCodeOnRecord) {
		this.postCodeOnRecord = postCodeOnRecord;
	}

	public String getBirthYYYY() {
		return birthYYYY;
	}

	public void setBirthYYYY(String birthYYYY) {
		this.birthYYYY = birthYYYY;
	}

	public String getBirthMM() {
		return birthMM;
	}

	public void setBirthMM(String birthMM) {
		this.birthMM = birthMM;
	}

	public String getBirthDD() {
		return birthDD;
	}

	public void setBirthDD(String birthDD) {
		this.birthDD = birthDD;
	}

	public String getRequestAdditionalInfo() {
		return requestAdditionalInfo;
	}

	public void setRequestAdditionalInfo(String requestAdditionalInfo) {
		this.requestAdditionalInfo = requestAdditionalInfo;
	}

	public List<String> getAdditionalItems() {
		return additionalItems;
	}

	public void setAdditionalItems(List<String> additionalItems) {
		this.additionalItems = additionalItems;
	}

	public List<ProductCategoryReportingParam> getMissingGlobalReportingParams() {
		return missingGlobalReportingParams;
	}

	public void setMissingGlobalReportingParams(List<ProductCategoryReportingParam> missingGlobalReportingParams) {
		this.missingGlobalReportingParams = missingGlobalReportingParams;
	}

	public boolean isSlgMissing() {
		return slgMissing;
	}

	public void setSlgMissing(boolean slgMissing) {
		this.slgMissing = slgMissing;
	}

	public boolean isPIIMisMatch() {
		return isPIIMisMatch;
	}

	public void setPIIMisMatch(boolean isPIIMisMatch) {
		this.isPIIMisMatch = isPIIMisMatch;
	}

	public Set<PersonalIdentifier> getPersonalIdentifiers() {
		return personalIdentifiers;
	}

	public void setPersonalIdentifiers(Set<PersonalIdentifier> personalIdentifiers) {
		this.personalIdentifiers = personalIdentifiers;
	}

	public String getCourtOnRecord() {
		return courtOnRecord;
	}

	public void setCourtOnRecord(String courtOnRecord) {
		this.courtOnRecord = courtOnRecord;
	}

	public String getCountyOnRecord() {
		return countyOnRecord;
	}

	public void setCountyOnRecord(String countyOnRecord) {
		this.countyOnRecord = countyOnRecord;
	}

	public String getDistrictCode() {
		return districtCode;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	public Date getCaseReleaseDate() {
		return caseReleaseDate;
	}

	public void setCaseReleaseDate(Date caseReleaseDate) {
		this.caseReleaseDate = caseReleaseDate;
	}

	public String getArrestTypeClassification() {
		return arrestTypeClassification;
	}

	public void setArrestTypeClassification(String arrestTypeClassification) {
		this.arrestTypeClassification = arrestTypeClassification;
	}

	public Date getCaseFileDate() {
		return caseFileDate;
	}

	public void setCaseFileDate(Date caseFileDate) {
		this.caseFileDate = caseFileDate;
	}

	public List<String> getOffenses() {
		return Offenses;
	}

	public void setOffenses(List<String> offenses) {
		Offenses = offenses;
	}

	public String getSubjectConfirmationType() {
		return subjectConfirmationType;
	}

	public void setSubjectConfirmationType(String subjectConfirmationType) {
		this.subjectConfirmationType = subjectConfirmationType;
	}

	public List<ValidatorCounty> getValidatorCounties() {
		return validatorCounties;
	}

	public void setValidatorCounties(List<ValidatorCounty> validatorCounties) {
		this.validatorCounties = validatorCounties;
	}
	
	public boolean isProhibitedState() {
		return isProhibitedState;
	}

	public void setProhibitedState(boolean isProhibitedState) {
		this.isProhibitedState = isProhibitedState;
	}

	public String getArrestingAgency() {
		return arrestingAgency;
	}

	public void setArrestingAgency(String arrestingAgency) {
		this.arrestingAgency = arrestingAgency;
	}

	public String getAgencyDescription() {
		return agencyDescription;
	}

	public void setAgencyDescription(String agencyDescription) {
		this.agencyDescription = agencyDescription;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((caseNBR == null) ? 0 : caseNBR.hashCode());
		result = prime * result + crimCaseId;
		result = prime * result + ((screeningResult == null) ? 0 : screeningResult.hashCode());
		result = prime * result + ((systemIdentifier == null) ? 0 : systemIdentifier.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CrimCase other = (CrimCase) obj;
		if (caseNBR == null) {
			if (other.caseNBR != null)
				return false;
		} else if (!caseNBR.equals(other.caseNBR))
			return false;
		if (crimCaseId != other.crimCaseId)
			return false;
		if (screeningResult == null) {
			if (other.screeningResult != null)
				return false;
		} else if (!screeningResult.equals(other.screeningResult))
			return false;
		if (systemIdentifier == null) {
			if (other.systemIdentifier != null)
				return false;
		} else if (!systemIdentifier.equals(other.systemIdentifier))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CrimCase [crimCaseId=" + crimCaseId + ", systemIdentifier=" + systemIdentifier + ", isScoreComplete="
				+ isScoreComplete + ", needsPIIReview=" + needsPIIReview + ", actualDispositionDescription="
				+ actualDispositionDescription + ", jurisdiction=" + jurisdiction + ", ageOnOffenseDate="
				+ ageOnOffenseDate + ", caseType=" + caseType + ", caseNBR=" + caseNBR + ", knownResult=" + knownResult
				+ ", isReportedEarlier=" + isReportedEarlier + ", sanitizedDispositionDescription="
				+ sanitizedDispositionDescription + ", crimCharges=" + crimCharges + ", ruleDecision=" + ruleDecision
				+ ", chargeLevel=" + chargeLevel + ", dispositionType=" + dispositionType + "]";
	}
}

package com.sterlingTS.smartdataComm.model;

import java.util.HashSet;
import java.util.Set;

public class ProductCategorySearchParam implements java.io.Serializable {

	private int productCategorySearchParamId;
	private ProductCategory productCategory;
	private GlobalSearchParam globalSearchParam;
	private Integer scopeOfSearch;
	private Integer dataFrequency;
	
	private Set<ChargeLevel> chargeLevels = new HashSet<ChargeLevel>();
	private Set<DispositionType> dispositionTypes;
	
	public Integer getScopeOfSearch() {
		return scopeOfSearch;
	}

	public void setScopeOfSearch(Integer scopeOfSearch) {
		this.scopeOfSearch = scopeOfSearch;
	}

	public Integer getDataFrequency() {
		return dataFrequency;
	}

	public void setDataFrequency(Integer dataFrequency) {
		this.dataFrequency = dataFrequency;
	}

	public int getProductCategorySearchParamId() {
		return productCategorySearchParamId;
	}

	public void setProductCategorySearchParamId(int productCategorySearchParamId) {
		this.productCategorySearchParamId = productCategorySearchParamId;
	}

	public ProductCategory getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(ProductCategory productCategory) {
		this.productCategory = productCategory;
	}

	public GlobalSearchParam getGlobalSearchParam() {
		return globalSearchParam;
	}

	public void setGlobalSearchParam(GlobalSearchParam globalSearchParam) {
		this.globalSearchParam = globalSearchParam;
	}

	public Set<DispositionType> getDispositionTypes() {
		return dispositionTypes;
	}

	public void setDispositionTypes(Set<DispositionType> dispositionTypes) {
		this.dispositionTypes = dispositionTypes;
	}

	public Set<ChargeLevel> getChargeLevels() {
		return chargeLevels;
	}
	
	public void setChargeLevels(Set<ChargeLevel> chargeLevels) {
		this.chargeLevels = chargeLevels;
	}
}

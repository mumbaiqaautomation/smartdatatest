package com.sterlingTS.smartdataComm.model;

import java.util.Date;

import com.google.gson.annotations.Expose;

public class LogHistoryNotes {

	private int logHistoryNotesID;
	private SmartDataStatus smartDataStatus;
	private String historyNotesDescription;
	private String createdAt;
	@Expose(serialize = false)
	private ScreeningResult screeningResult;
	
	/**
	 * @return the screeningResult
	 */
	public ScreeningResult getScreeningResult() {
		return screeningResult;
	}
	/**
	 * @param screeningResult the screeningResult to set
	 */
	public void setScreeningResult(ScreeningResult screeningResult) {
		this.screeningResult = screeningResult;
	}
	/**
	 * @return the logHistoryNotesID
	 */
	public int getLogHistoryNotesID() {
		return logHistoryNotesID;
	}
	/**
	 * @param logHistoryNotesID the logHistoryNotesID to set
	 */
	public void setLogHistoryNotesID(int logHistoryNotesID) {
		this.logHistoryNotesID = logHistoryNotesID;
	}
	
	/**
	 * @return the smartDataStatus
	 */
	public SmartDataStatus getSmartDataStatus() {
		return smartDataStatus;
	}
	/**
	 * @param smartDataStatus the smartDataStatus to set
	 */
	public void setSmartDataStatus(SmartDataStatus smartDataStatus) {
		this.smartDataStatus = smartDataStatus;
	}
	
	/**
	 * @return the historyNotesDescription
	 */
	public String getHistoryNotesDescription() {
		return historyNotesDescription;
	}
	/**
	 * @param historyNotesDescription the historyNotesDescription to set
	 */
	public void setHistoryNotesDescription(String historyNotesDescription) {
		this.historyNotesDescription = historyNotesDescription;
	}
	/**
	 * @return the createdAt
	 */
	public String getCreatedAt() {
		return createdAt;
	}
	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((createdAt == null) ? 0 : createdAt.hashCode());
		result = prime * result + ((historyNotesDescription == null) ? 0 : historyNotesDescription.hashCode());
		result = prime * result + logHistoryNotesID;
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LogHistoryNotes other = (LogHistoryNotes) obj;
		if (createdAt == null) {
			if (other.createdAt != null)
				return false;
		} else if (!createdAt.equals(other.createdAt))
			return false;
		if (historyNotesDescription == null) {
			if (other.historyNotesDescription != null)
				return false;
		} else if (!historyNotesDescription.equals(other.historyNotesDescription))
			return false;
		if (logHistoryNotesID != other.logHistoryNotesID)
			return false;
		return true;
	}
}

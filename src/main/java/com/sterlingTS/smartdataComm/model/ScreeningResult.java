package com.sterlingTS.smartdataComm.model;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gson.annotations.Expose;
import com.sterlingTS.smartdataComm.model.enums.GenderEnum;
import com.sterlingTS.smartdataComm.model.enums.KnownResultEnum;

public class ScreeningResult {

	private int screeningResultId;
	private Set<CrimCase> crimCases = new HashSet<CrimCase>();
	@Expose(serialize = false)
	private boolean resultScoreComplete;
	@Expose(serialize = false)
	private SmartDataStatus smartDataStatus;
	@Expose(serialize = false)
	private Boolean isHit;
	@Expose(serialize = false)
	private FulfillmentRequest fulfillmentRequest;
	private String screeningId;
	private String orderId;
	private Date orderDate;
	private ParentSystem parentSystem;
	private Product product;
	
	private String familyName;
	
	private String givenName;
	
	private String middleName;
	
	private Date subjectDob;
	
	private String governmentId;
	
	private String addressLine;
	
	private String postCode;
	
	private String cityName;
	
	private String licenseNumber;
	
	private String issuingAuthority;
	private SalaryRange salaryRange;
	
	private GenderEnum gender;
	
	private String subjectBirthState;
	
	private JurisdictionState subjectEmploymentState;
	
	private JurisdictionState subjectResidentState;
	private String screeningReferenceId;
	private Account account;
	private Profile profile;
	private KnownResultEnum knownResult;
	@Expose(serialize = false)
	private ProductCategory productCategory;
	@Expose(serialize = false)
	private JurisdictionType jurisdictionType;
	@Expose(serialize = false)
	private Jurisdiction jurisdiction;
	private JurisdictionDetail jurisdictionDetail;
	private Boolean isNewRequest;
	@Expose(serialize = false)
	private Jurisdiction ruleJurisdiction;
	@Expose(serialize = false)
	private Set<ProductCategoryReportingParam> missingGlobalReportingParams = new HashSet<ProductCategoryReportingParam>();
	private Set<Pii> piis = new HashSet<Pii>();
	@Expose(serialize = false)
	private Agent agent;
	@Expose(serialize = false)
	private Set<LogHistoryNotes> logHistoryNotes = new HashSet<LogHistoryNotes>();
	@Expose(serialize = false)
	private boolean isKeywordSearchReviewed;
	private List<String> additionalItems; 
	private SmartDataError smdError;
	private String requestURI;
	@Expose(serialize = false)
	private boolean isCommonName;
	@Expose(serialize = false)
	private boolean slgMissing;
	private String requestingSystem;
	private String brokerName;
	private Boolean containsDisputeInfo;
    private DynamicDatasource dynamicDatasource;
    private String piiCaseThresoldScore;
    	
	public String getPiiCaseThresoldScore() {
		return piiCaseThresoldScore;
	}

	public void setPiiCaseThresoldScore(String piiCaseThresoldScore) {
		this.piiCaseThresoldScore = piiCaseThresoldScore;
	}

	public Set<CrimCase> getCrimCases() {
		return crimCases;
	}

	public void setCrimCases(final Set<CrimCase> crimCases) {
		this.crimCases = crimCases;
	}

	public SmartDataStatus getSmartDataStatus() {
		return smartDataStatus;
	}

	public void setSmartDataStatus(final SmartDataStatus smartDataStatus) {
		this.smartDataStatus = smartDataStatus;
	}

	public Boolean getIsHit() {
		return isHit;
	}

	public void setIsHit(final Boolean isHit) {
		this.isHit = isHit;
	}

	public int getScreeningResultId() {
		return screeningResultId;
	}

	public void setScreeningResultId(final int screeningResultId) {
		this.screeningResultId = screeningResultId;
	}

	/**
	 * @return the fulfillmentRequest
	 */
	public FulfillmentRequest getFulfillmentRequest() {
		return fulfillmentRequest;
	}

	/**
	 * @param fulfillmentRequest
	 *            the fulfillmentRequest to set
	 */
	public void setFulfillmentRequest(final FulfillmentRequest fulfillmentRequest) {
		this.fulfillmentRequest = fulfillmentRequest;
	}

	/**
	 * @return the screeningId
	 */
	public String getScreeningId() {
		return screeningId;
	}

	/**
	 * @param screeningId
	 *            the screeningId to set
	 */
	public void setScreeningId(final String screeningId) {
		this.screeningId = screeningId;
	}

	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}

	/**
	 * @param orderId
	 *            the orderId to set
	 */
	public void setOrderId(final String orderId) {
		this.orderId = orderId;
	}

	/**
	 * @return the orderDate
	 */
	public Date getOrderDate() {
		return orderDate;
	}

	/**
	 * @param orderDate
	 *            the orderDate to set
	 */
	public void setOrderDate(final Date orderDate) {
		this.orderDate = orderDate;
	}

	/**
	 * @return the parentSystem
	 */
	public ParentSystem getParentSystem() {
		return parentSystem;
	}

	/**
	 * @param parentSystem
	 *            the parentSystem to set
	 */
	public void setParentSystem(final ParentSystem parentSystem) {
		this.parentSystem = parentSystem;
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}

	/**
	 * @param product
	 *            the product to set
	 */
	public void setProduct(final Product product) {
		this.product = product;
	}

	/**
	 * @return the familyName
	 */
	public String getFamilyName() {
		return familyName;
	}

	/**
	 * @param familyName
	 *            the familyName to set
	 */
	public void setFamilyName(final String familyName) {
		this.familyName = familyName;
	}

	/**
	 * @return the givenName
	 */
	public String getGivenName() {
		return givenName;
	}

	/**
	 * @param givenName
	 *            the givenName to set
	 */
	public void setGivenName(final String givenName) {
		this.givenName = givenName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName
	 *            the middleName to set
	 */
	public void setMiddleName(final String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the subjectDob
	 */
	public Date getSubjectDob() {
		return subjectDob;
	}

	/**
	 * @param subjectDob
	 *            the subjectDob to set
	 */
	public void setSubjectDob(final Date subjectDob) {
		this.subjectDob = subjectDob;
	}

	/**
	 * @return the governmentId
	 */
	public String getGovernmentId() {
		return governmentId;
	}

	/**
	 * @param governmentId
	 *            the governmentId to set
	 */
	public void setGovernmentId(final String governmentId) {
		this.governmentId = governmentId;
	}

	/**
	 * @return the addressLine
	 */
	public String getAddressLine() {
		return addressLine;
	}

	/**
	 * @param addressLine
	 *            the addressLine to set
	 */
	public void setAddressLine(final String addressLine) {
		this.addressLine = addressLine;
	}

	/**
	 * @return the postCode
	 */
	public String getPostCode() {
		return postCode;
	}

	/**
	 * @param postCode
	 *            the postCode to set
	 */
	public void setPostCode(final String postCode) {
		this.postCode = postCode;
	}

	/**
	 * @return the cityName
	 */
	public String getCityName() {
		return cityName;
	}

	/**
	 * @param cityName
	 *            the cityName to set
	 */
	public void setCityName(final String cityName) {
		this.cityName = cityName;
	}

	/**
	 * @return the licenseNumber
	 */
	public String getLicenseNumber() {
		return licenseNumber;
	}

	/**
	 * @param licenseNumber
	 *            the licenseNumber to set
	 */
	public void setLicenseNumber(final String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	/**
	 * @return the issuingAuthority
	 */
	public String getIssuingAuthority() {
		return issuingAuthority;
	}

	/**
	 * @param issuingAuthority
	 *            the issuingAuthority to set
	 */
	public void setIssuingAuthority(final String issuingAuthority) {
		this.issuingAuthority = issuingAuthority;
	}

	/**
	 * @return the salaryRange
	 */
	public SalaryRange getSalaryRange() {
		return salaryRange;
	}

	/**
	 * @param salaryRange
	 *            the salaryRange to set
	 */
	public void setSalaryRange(final SalaryRange salaryRange) {
		this.salaryRange = salaryRange;
	}

	/**
	 * @return the genderCode
	 */
	public GenderEnum getGender() {
		return gender;
	}

	/**
	 * @param genderCode
	 *            the genderCode to set
	 */
	public void setGender(final GenderEnum gender) {
		this.gender = gender;
	}

	/**
	 * @return the subjectBirthState
	 */
	public String getSubjectBirthState() {
		return subjectBirthState;
	}

	/**
	 * @param subjectBirthState
	 *            the subjectBirthState to set
	 */
	public void setSubjectBirthState(final String subjectBirthState) {
		this.subjectBirthState = subjectBirthState;
	}

	/**
	 * @return the subjectEmploymentState
	 */
	public JurisdictionState getSubjectEmploymentState() {
		return subjectEmploymentState;
	}

	/**
	 * @param subjectEmploymentState
	 *            the subjectEmploymentState to set
	 */
	public void setSubjectEmploymentState(final JurisdictionState subjectEmploymentState) {
		this.subjectEmploymentState = subjectEmploymentState;
	}

	/**
	 * @return the subjectResidentState
	 */
	public JurisdictionState getSubjectResidentState() {
		return subjectResidentState;
	}

	/**
	 * @param subjectResidentState
	 *            the subjectResidentState to set
	 */
	public void setSubjectResidentState(final JurisdictionState subjectResidentState) {
		this.subjectResidentState = subjectResidentState;
	}

	/**
	 * @return the screeningReferenceId
	 */
	public String getScreeningReferenceId() {
		return screeningReferenceId;
	}

	/**
	 * @param screeningReferenceId
	 *            the screeningReferenceId to set
	 */
	public void setScreeningReferenceId(final String screeningReferenceId) {
		this.screeningReferenceId = screeningReferenceId;
	}

	/**
	 * @return the account
	 */
	public Account getAccount() {
		return account;
	}

	/**
	 * @param account
	 *            the account to set
	 */
	public void setAccount(final Account account) {
		this.account = account;
	}

	/**
	 * @return the profile
	 */
	public Profile getProfile() {
		return profile;
	}

	/**
	 * @param profile
	 *            the profile to set
	 */
	public void setProfile(final Profile profile) {
		this.profile = profile;
	}

	/**
	 * @return the knownResult
	 */
	public KnownResultEnum getKnownResult() {
		return knownResult;
	}

	/**
	 * @param knownResult
	 *            the knownResult to set
	 */
	public void setKnownResult(final KnownResultEnum knownResult) {
		this.knownResult = knownResult;
	}

		public boolean isResultScoreComplete() {
		return resultScoreComplete;
	}

	public void setResultScoreComplete(final boolean resultScoreComplete) {
		this.resultScoreComplete = resultScoreComplete;
	}

	public ProductCategory getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(final ProductCategory productCategory) {
		this.productCategory = productCategory;
	}

	public JurisdictionType getJurisdictionType() {
		return jurisdictionType;
	}

	public void setJurisdictionType(final JurisdictionType jurisdictionType) {
		this.jurisdictionType = jurisdictionType;
	}

	public Jurisdiction getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(final Jurisdiction jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public JurisdictionDetail getJurisdictionDetail() {
		return jurisdictionDetail;
	}

	public void setJurisdictionDetail(final JurisdictionDetail jurisdictionDetail) {
		this.jurisdictionDetail = jurisdictionDetail;
	}

	public Boolean getIsNewRequest() {
		return isNewRequest;
	}

	public void setIsNewRequest(final Boolean isNewRequest) {
		this.isNewRequest = isNewRequest;
	}

	public Jurisdiction getRuleJurisdiction() {
		return ruleJurisdiction;
	}

	public void setRuleJurisdiction(Jurisdiction ruleJurisdiction) {
		this.ruleJurisdiction = ruleJurisdiction;
	}

	public Set<ProductCategoryReportingParam> getMissingGlobalReportingParams() {
		return missingGlobalReportingParams;
	}

	public void setMissingGlobalReportingParams(
			Set<ProductCategoryReportingParam> missingGlobalReportingParams) {
		this.missingGlobalReportingParams = missingGlobalReportingParams;
	}

	public Set<Pii> getPiis() {
		return piis;
	}

	public void setPiis(Set<Pii> piis) {
		this.piis = piis;
	}
	
	public Set<LogHistoryNotes> getLogHistoryNotes() {
		return logHistoryNotes;
	}

	public void setLogHistoryNotes(Set<LogHistoryNotes> logHistoryNotes) {
		this.logHistoryNotes = logHistoryNotes;
	}
	
	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public boolean isKeywordSearchReviewed() {
		return isKeywordSearchReviewed;
	}

	public void setKeywordSearchReviewed(boolean isKeywordSearchReviewed) {
		this.isKeywordSearchReviewed = isKeywordSearchReviewed;
	}
	
	public List<String> getAdditionalItems() {
		return additionalItems;
	}

	public void setAdditionalItems(List<String> additionalItems) {
		this.additionalItems = additionalItems;
	}

	public SmartDataError getSmdError() {
		return smdError;
	}

	public void setSmdError(SmartDataError smdError) {
		this.smdError = smdError;
	}
	
	public String getRequestURI() {
		return requestURI;
	}

	public void setRequestURI(String requestURI) {
		this.requestURI = requestURI;
	}

	public boolean isCommonName() {
		return isCommonName;
	}

	public void setCommonName(boolean isCommonName) {
		this.isCommonName = isCommonName;
	}

	public boolean isSlgMissing() {
		return slgMissing;
	}

	public void setSlgMissing(boolean slgMissing) {
		this.slgMissing = slgMissing;
	}
	
	public String getRequestingSystem() {
		return requestingSystem;
	}

	public void setRequestingSystem(String requestingSystem) {
		this.requestingSystem = requestingSystem;
	}

	@Override
	public String toString() {
		return "ScreeningResult [screeningResultId=" + screeningResultId
				+ ", crimCases=" + crimCases + ", resultScoreComplete="
				+ resultScoreComplete + ", smartDataStatus=" + smartDataStatus
				+ ", isHit=" + isHit + ", screeningId=" + screeningId
				+ ", orderId=" + orderId + ", orderDate=" + orderDate
				+ ", parentSystem=" + parentSystem + ", product=" + product
				+ ", subjectBirthState=" + subjectBirthState
				+ ", subjectEmploymentState=" + subjectEmploymentState
				+ ", subjectResidentState=" + subjectResidentState
				+ ", screeningReferenceId=" + screeningReferenceId + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((orderId == null) ? 0 : orderId.hashCode());
		result = prime * result
				+ ((screeningId == null) ? 0 : screeningId.hashCode());
		result = prime
				* result
				+ ((screeningReferenceId == null) ? 0 : screeningReferenceId
						.hashCode());
		result = prime * result + screeningResultId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScreeningResult other = (ScreeningResult) obj;
		if (orderId == null) {
			if (other.orderId != null)
				return false;
		} else if (!orderId.equals(other.orderId))
			return false;
		if (screeningId == null) {
			if (other.screeningId != null)
				return false;
		} else if (!screeningId.equals(other.screeningId))
			return false;
		if (screeningReferenceId == null) {
			if (other.screeningReferenceId != null)
				return false;
		} else if (!screeningReferenceId.equals(other.screeningReferenceId))
			return false;
		if (screeningResultId != other.screeningResultId)
			return false;
		return true;
	}

	public String getBrokerName() {
		return brokerName;
	}

	public void setBrokerName(String brokerName) {
		this.brokerName = brokerName;
	}

	public Boolean getContainsDisputeInfo() {
		return containsDisputeInfo;
	}

	public void setContainsDisputeInfo(Boolean containsDisputeInfo) {
		this.containsDisputeInfo = containsDisputeInfo;
	}

	public DynamicDatasource getDynamicDatasource() {
		return dynamicDatasource;
	}

	public void setDynamicDatasource(DynamicDatasource dynamicDatasource) {
		this.dynamicDatasource = dynamicDatasource;
	}

}

package com.sterlingTS.smartdataComm.model;

public class GlobalReportingParam implements java.io.Serializable {

	private int globalReportingParamId;
	private String param;
	private String description;
	private boolean isMandatory;
	private String logicalName;
	public int getGlobalReportingParamId() {
		return globalReportingParamId;
	}
	public void setGlobalReportingParamId(int globalReportingParamId) {
		this.globalReportingParamId = globalReportingParamId;
	}
	public String getParam() {
		return param;
	}
	public void setParam(String param) {
		this.param = param;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean getIsMandatory() {
		return isMandatory;
	}
	public void setIsMandatory(boolean isMandatory) {
		this.isMandatory = isMandatory;
	}
	public String getLogicalName() {
		return logicalName;
	}
	public void setLogicalName(String logicalName) {
		this.logicalName = logicalName;
	}

}

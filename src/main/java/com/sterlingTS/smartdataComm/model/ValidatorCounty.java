package com.sterlingTS.smartdataComm.model;

import com.sterlingTS.smartdataComm.model.enums.CountySearchLevel;

public class ValidatorCounty {

    private JurisdictionCounty jurisdictionCounty;
    private CountySearchLevel searchTypeEnum;
    private boolean isProhibitedCounty;

    public JurisdictionCounty getJurisdictionCounty() {
		return jurisdictionCounty;
	}

	public void setJurisdictionCounty(JurisdictionCounty jurisdictionCounty) {
		this.jurisdictionCounty = jurisdictionCounty;
	}

	public CountySearchLevel getSearchTypeEnum() {
           return searchTypeEnum;
    }

    public void setSearchTypeEnum(CountySearchLevel searchTypeEnum) {
           this.searchTypeEnum = searchTypeEnum;
    }

    public boolean isProhibitedCounty() {
           return isProhibitedCounty;
    }

    public void setProhibitedCounty(boolean isProhibitedCounty) {
           this.isProhibitedCounty = isProhibitedCounty;
    }
}

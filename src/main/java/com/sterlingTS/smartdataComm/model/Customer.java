package com.sterlingTS.smartdataComm.model;

import java.util.Set;

import com.google.gson.annotations.Expose;

public class Customer {

	@Expose(serialize = false)
	private int customerId;
	private String customerName;
	@Expose(serialize = false)
	private ParentSystem parentSystem;
	@Expose(serialize = false)
	private Pod pod;
	@Expose(serialize = false)
	private String customerDescription;
	@Expose(serialize = false)
	private Set<Account> accounts;

	/**
	 * @return the customerId
	 */
	public int getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId
	 *            the customerId to set
	 */
	public void setCustomerId(final int customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName
	 *            the customerName to set
	 */
	public void setCustomerName(final String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the parentSystem
	 */
	public ParentSystem getParentSystem() {
		return parentSystem;
	}

	/**
	 * @param parentSystem
	 *            the parentSystem to set
	 */
	public void setParentSystem(final ParentSystem parentSystem) {
		this.parentSystem = parentSystem;
	}

	public Pod getPod() {
		return pod;
	}

	public void setPod(Pod pod) {
		this.pod = pod;
	}

	public String getCustomerDescription() {
		return customerDescription;
	}

	public void setCustomerDescription(String customerDescription) {
		this.customerDescription = customerDescription;
	}

	public Set<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(Set<Account> accounts) {
		this.accounts = accounts;
	}

	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", customerName="
				+ customerName + ", parentSystem=" + parentSystem
				+ ", customerDescription=" + customerDescription + "]";
	}

}

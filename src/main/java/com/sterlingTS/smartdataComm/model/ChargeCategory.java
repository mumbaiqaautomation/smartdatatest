package com.sterlingTS.smartdataComm.model;

import com.google.gson.annotations.Expose;

public class ChargeCategory {
	
	@Expose(serialize = false)
	private int chargeCategoryId;
	private String category;
	private String description;

	/**
	 * @return the chargeCategoryId
	 */
	public int getChargeCategoryId() {
		return chargeCategoryId;
	}

	/**
	 * @param chargeCategoryId
	 *            the chargeCategoryId to set
	 */
	public void setChargeCategoryId(final int chargeCategoryId) {
		this.chargeCategoryId = chargeCategoryId;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(final String category) {
		this.category = category;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "ChargeCategory [chargeCategoryId=" + chargeCategoryId
				+ ", category=" + category + ", description=" + description
				+ "]";
	}
}

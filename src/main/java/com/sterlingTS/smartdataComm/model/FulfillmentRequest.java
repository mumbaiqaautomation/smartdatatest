package com.sterlingTS.smartdataComm.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class FulfillmentRequest {

	private int fulfillmentRequestId;
	private Date creationDate;
	private Agent agent;
	private SmartDataStatus smartDataStatus;
	private Set<ScreeningResult> screeningResults = new HashSet<ScreeningResult>(
			0);
	
	public int getFulfillmentRequestId() {
		return fulfillmentRequestId;
	}

	public void setFulfillmentRequestId(final int fulfillmentRequestId) {
		this.fulfillmentRequestId = fulfillmentRequestId;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(final Date creationDate) {
		this.creationDate = creationDate;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(final Agent agent) {
		this.agent = agent;
	}

	public SmartDataStatus getSmartDataStatus() {
		return smartDataStatus;
	}

	public void setSmartDataStatus(final SmartDataStatus smartDataStatus) {
		this.smartDataStatus = smartDataStatus;
	}

	public Set<ScreeningResult> getScreeningResults() {
		return screeningResults;
	}

	public void setScreeningResults(Set<ScreeningResult> screeningResults) {
		this.screeningResults = screeningResults;
	}

}

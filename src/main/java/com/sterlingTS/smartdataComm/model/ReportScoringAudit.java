package com.sterlingTS.smartdataComm.model;

import java.util.HashSet;
import java.util.Set;

public class ReportScoringAudit implements java.io.Serializable {

	private int reportScoringAuditId;
	private FulfillmentRequest fulfillmentRequest;
	private Double scoringPercentage;
	private Set reportScoringAuditParams = new HashSet(0);

	public ReportScoringAudit() {
	}

	public ReportScoringAudit(int reportScoringAuditId,
			FulfillmentRequest fulfillmentRequest) {
		this.reportScoringAuditId = reportScoringAuditId;
		this.fulfillmentRequest = fulfillmentRequest;
	}

	public ReportScoringAudit(int reportScoringAuditId,
			FulfillmentRequest fulfillmentRequest, Double scoringPercentage,
			Set reportScoringAuditParams) {
		this.reportScoringAuditId = reportScoringAuditId;
		this.fulfillmentRequest = fulfillmentRequest;
		this.scoringPercentage = scoringPercentage;
		this.reportScoringAuditParams = reportScoringAuditParams;
	}

	public int getReportScoringAuditId() {
		return this.reportScoringAuditId;
	}

	public void setReportScoringAuditId(int reportScoringAuditId) {
		this.reportScoringAuditId = reportScoringAuditId;
	}

	public FulfillmentRequest getFulfillmentRequest() {
		return this.fulfillmentRequest;
	}

	public void setFulfillmentRequest(FulfillmentRequest fulfillmentRequest) {
		this.fulfillmentRequest = fulfillmentRequest;
	}

	public Double getScoringPercentage() {
		return this.scoringPercentage;
	}

	public void setScoringPercentage(Double scoringPercentage) {
		this.scoringPercentage = scoringPercentage;
	}

	public Set getReportScoringAuditParams() {
		return this.reportScoringAuditParams;
	}

	public void setReportScoringAuditParams(Set reportScoringAuditParams) {
		this.reportScoringAuditParams = reportScoringAuditParams;
	}

}

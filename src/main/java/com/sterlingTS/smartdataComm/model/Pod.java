package com.sterlingTS.smartdataComm.model;

import com.google.gson.annotations.Expose;

public class Pod {

	@Expose(serialize = false)
	private int podId;
	private String podName;
	private String description;

	/**
	 * @return the podId
	 */
	public int getPodId() {
		return podId;
	}

	/**
	 * @param podId
	 *            the podId to set
	 */
	public void setPodId(final int podId) {
		this.podId = podId;
	}

	/**
	 * @return the podName
	 */
	public String getPodName() {
		return podName;
	}

	/**
	 * @param podName
	 *            the podName to set
	 */
	public void setPodName(final String podName) {
		this.podName = podName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(final String description) {
		this.description = description;
	}
}

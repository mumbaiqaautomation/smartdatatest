package com.sterlingTS.smartdataComm.model;

import java.util.HashSet;
import java.util.Set;

public class ProductCategoryJurisdictionDatasource implements
java.io.Serializable {

private int productCategoryJurisdictionDatasourceId;
private ProductCategoryJurisdiction productCategoryJurisdiction;
private FulfillmentType fulfillmentType;
private String datasourceName;
private String courtName;
private String openingTimings;
private String closingTimings;
private String otherInfo;
private boolean active;
private Set<ProductCategorySearchMonitoring> productCategorySearchMonitorings = new HashSet<ProductCategorySearchMonitoring>(0);
private Set<ProductCategoryReportingMonitoring> productCategoryReportingMonitorings= new HashSet<ProductCategoryReportingMonitoring>(0);

public int getProductCategoryJurisdictionDatasourceId() {
return productCategoryJurisdictionDatasourceId;
}
public void setProductCategoryJurisdictionDatasourceId(
	int productCategoryJurisdictionDatasourceId) {
this.productCategoryJurisdictionDatasourceId = productCategoryJurisdictionDatasourceId;
}
public ProductCategoryJurisdiction getProductCategoryJurisdiction() {
return productCategoryJurisdiction;
}
public void setProductCategoryJurisdiction(
	ProductCategoryJurisdiction productCategoryJurisdiction) {
this.productCategoryJurisdiction = productCategoryJurisdiction;
}
public FulfillmentType getFulfillmentType() {
return fulfillmentType;
}
public void setFulfillmentType(FulfillmentType fulfillmentType) {
this.fulfillmentType = fulfillmentType;
}
public String getDatasourceName() {
return datasourceName;
}
public void setDatasourceName(String datasourceName) {
this.datasourceName = datasourceName;
}
public String getCourtName() {
return courtName;
}
public void setCourtName(String courtName) {
this.courtName = courtName;
}
public String getOpeningTimings() {
return openingTimings;
}
public void setOpeningTimings(String openingTimings) {
this.openingTimings = openingTimings;
}
public String getClosingTimings() {
return closingTimings;
}
public void setClosingTimings(String closingTimings) {
this.closingTimings = closingTimings;
}
public String getOtherInfo() {
return otherInfo;
}
public void setOtherInfo(String otherInfo) {
this.otherInfo = otherInfo;
}
public boolean isActive() {
return active;
}
public void setActive(boolean active) {
this.active = active;
}
public Set<ProductCategorySearchMonitoring> getProductCategorySearchMonitorings() {
return productCategorySearchMonitorings;
}
public void setProductCategorySearchMonitorings(
	Set<ProductCategorySearchMonitoring> productCategorySearchMonitorings) {
this.productCategorySearchMonitorings = productCategorySearchMonitorings;
}
public Set<ProductCategoryReportingMonitoring> getProductCategoryReportingMonitorings() {
return productCategoryReportingMonitorings;
}
public void setProductCategoryReportingMonitorings(
	Set<ProductCategoryReportingMonitoring> productCategoryReportingMonitorings) {
this.productCategoryReportingMonitorings = productCategoryReportingMonitorings;
}

}

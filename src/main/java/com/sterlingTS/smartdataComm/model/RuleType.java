package com.sterlingTS.smartdataComm.model;

public class RuleType implements java.io.Serializable {

	private int ruleTypeId;
	private String ruleType;
	public int getRuleTypeId() {
		return ruleTypeId;
	}
	public void setRuleTypeId(int ruleTypeId) {
		this.ruleTypeId = ruleTypeId;
	}
	public String getRuleType() {
		return ruleType;
	}
	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}
}

package com.sterlingTS.smartdataComm.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class ProductCategoryReportingThreshold implements
java.io.Serializable {

private int productCategoryReportingThresholdId;
private Date timeStamp;
private ProductCategory productCategory;
private Date fromDate;
private Date toDate;
private int percentileChargesMetThreshold;
private Integer noOfChargesMetThreshold;
private Set<ProductCategoryReportingMonitoring> productCategoryReportingMonitorings = new HashSet<ProductCategoryReportingMonitoring>(0);
public int getProductCategoryReportingThresholdId() {
return productCategoryReportingThresholdId;
}
public void setProductCategoryReportingThresholdId(
	int productCategoryReportingThresholdId) {
this.productCategoryReportingThresholdId = productCategoryReportingThresholdId;
}
public Date getTimeStamp() {
return timeStamp;
}
public void setTimeStamp(Date timeStamp) {
this.timeStamp = timeStamp;
}
public ProductCategory getProductCategory() {
return productCategory;
}
public void setProductCategory(ProductCategory productCategory) {
this.productCategory = productCategory;
}
public Date getFromDate() {
return fromDate;
}
public void setFromDate(Date fromDate) {
this.fromDate = fromDate;
}
public Date getToDate() {
return toDate;
}
public void setToDate(Date toDate) {
this.toDate = toDate;
}
public int getPercentileChargesMetThreshold() {
return percentileChargesMetThreshold;
}
public void setPercentileChargesMetThreshold(int percentileChargesMetThreshold) {
this.percentileChargesMetThreshold = percentileChargesMetThreshold;
}
public Integer getNoOfChargesMetThreshold() {
return noOfChargesMetThreshold;
}
public void setNoOfChargesMetThreshold(Integer noOfChargesMetThreshold) {
this.noOfChargesMetThreshold = noOfChargesMetThreshold;
}
public Set<ProductCategoryReportingMonitoring> getProductCategoryReportingMonitorings() {
return productCategoryReportingMonitorings;
}
public void setProductCategoryReportingMonitorings(
	Set<ProductCategoryReportingMonitoring> productCategoryReportingMonitorings) {
this.productCategoryReportingMonitorings = productCategoryReportingMonitorings;
}
}

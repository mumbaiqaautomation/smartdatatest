package com.sterlingTS.smartdataComm.model;

public class RuleSetRuleMaster implements java.io.Serializable {

	private int ruleSetRuleMasterId;
	private RuleSet ruleSet;
	private RuleMaster ruleMaster;

	public RuleSetRuleMaster() {
	}

	public RuleSetRuleMaster(int ruleSetRuleMasterId) {
		this.ruleSetRuleMasterId = ruleSetRuleMasterId;
	}

	public RuleSetRuleMaster(int ruleSetRuleMasterId, RuleSet ruleSet,
			RuleMaster ruleMaster) {
		this.ruleSetRuleMasterId = ruleSetRuleMasterId;
		this.ruleSet = ruleSet;
		this.ruleMaster = ruleMaster;
	}

	public int getRuleSetRuleMasterId() {
		return this.ruleSetRuleMasterId;
	}

	public void setRuleSetRuleMasterId(int ruleSetRuleMasterId) {
		this.ruleSetRuleMasterId = ruleSetRuleMasterId;
	}

	public RuleSet getRuleSet() {
		return this.ruleSet;
	}

	public void setRuleSet(RuleSet ruleSet) {
		this.ruleSet = ruleSet;
	}

	public RuleMaster getRuleMaster() {
		return this.ruleMaster;
	}

	public void setRuleMaster(RuleMaster ruleMaster) {
		this.ruleMaster = ruleMaster;
	}
}

package com.sterlingTS.smartdataComm.model;

import java.util.HashSet;
import java.util.Set;

public class Role implements java.io.Serializable {

	private int roleId;
	private String roleName;
	private String description;
	private Set userRoles = new HashSet(0);
	private Set rolePrevileges = new HashSet(0);

	public Role() {
	}

	public Role(int roleId, String roleName) {
		this.roleId = roleId;
		this.roleName = roleName;
	}

	public Role(int roleId, String roleName, String description, Set userRoles,
			Set rolePrevileges) {
		this.roleId = roleId;
		this.roleName = roleName;
		this.description = description;
		this.userRoles = userRoles;
		this.rolePrevileges = rolePrevileges;
	}

	public int getRoleId() {
		return this.roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set getUserRoles() {
		return this.userRoles;
	}

	public void setUserRoles(Set userRoles) {
		this.userRoles = userRoles;
	}

	public Set getRolePrevileges() {
		return this.rolePrevileges;
	}

	public void setRolePrevileges(Set rolePrevileges) {
		this.rolePrevileges = rolePrevileges;
	}
}

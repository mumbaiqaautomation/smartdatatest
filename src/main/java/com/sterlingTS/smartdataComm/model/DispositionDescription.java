package com.sterlingTS.smartdataComm.model;

import com.google.gson.annotations.Expose;

public class DispositionDescription {

	@Expose(serialize = false)
	private int dispositionDescriptionId;
	private String description;

	/**
	 * @return the dispositionDescriptionId
	 */
	public int getDispositionDescriptionId() {
		return dispositionDescriptionId;
	}

	/**
	 * @param dispositionDescriptionId
	 *            the dispositionDescriptionId to set
	 */
	public void setDispositionDescriptionId(final int dispositionDescriptionId) {
		this.dispositionDescriptionId = dispositionDescriptionId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final DispositionDescription other = (DispositionDescription) obj;
		if (description == null) {
			if (other.description != null) {
				return false;
			}
		} else if (!description.equals(other.description)) {
			return false;
		}
		return true;
	}


	@Override
	public String toString() {
		return "DispositionDescription [dispositionDescriptionId="
				+ dispositionDescriptionId + ", description=" + description
				+ "]";
	}
}

package com.sterlingTS.smartdataComm.model;

import java.util.HashSet;
import java.util.Set;

public class StateConfigurationFact {

	/*
	 * TODO :- Confirm following
	 * If for State we have to apply 4 rules, then 4 facts will be loaded to carry that data
	 * So we have to inject 4 configuration facts and 1 charge in each call to Session. will that work
	 * 
	 */
	private String ruleName;
	private Integer  ageOfRecord;
	private Integer salary;
	private Integer  ageOfSentence;
	private Boolean  activeWarrant;
	private Integer  ageOfWarrant;
	private Boolean  probationActive;
	private String  caseType;
	private Integer  ageOnOffensedate;
	private Integer  currentAge;
	//Should we use the collection or arrays
	private Set<DispositionDescription>  dispositionDescriptions = new HashSet<DispositionDescription>();
	private Set<ChargeLevel> chargeLevels = new HashSet<ChargeLevel>();
	private Set<JurisdictionCounty> counties = new HashSet<JurisdictionCounty>();
	private Set<ChargeDescription> chargeDescriptions = new HashSet<ChargeDescription>();
	private Jurisdiction ruleJurisdiction;
	private DispositionType dispositionType;
	
	public StateConfigurationFact(){}
	
	public StateConfigurationFact(RuleConfiguration ruleConfiguration){
		this.ageOfRecord=ruleConfiguration.getAgeOfRecord();
		this.salary=ruleConfiguration.getSalaryLimit();
		this.ageOfSentence=ruleConfiguration.getAgeOfSentence();
		this.activeWarrant=ruleConfiguration.getActiveWarrant();
		this.ageOfWarrant=ruleConfiguration.getAgeOfWarrant();
		this.probationActive=ruleConfiguration.getProbationActive();
		this.caseType=ruleConfiguration.getCaseType();
		this.ageOnOffensedate=ruleConfiguration.getAgeOnOffenseDate();
		this.currentAge=ruleConfiguration.getCurrentAge();
		this.dispositionDescriptions = ruleConfiguration.getDispositionDescriptions();
		this.chargeDescriptions = ruleConfiguration.getChargeDescriptions();
		this.chargeLevels = ruleConfiguration.getChargeLevels();
		this.counties=ruleConfiguration.getCounties();
		this.ruleName=ruleConfiguration.getRuleMaster().getRuleName();
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public Integer getAgeOfRecord() {
		return ageOfRecord;
	}

	public void setAgeOfRecord(Integer ageOfRecord) {
		this.ageOfRecord = ageOfRecord;
	}

	public Integer getSalary() {
		return salary;
	}

	public void setSalary(Integer salary) {
		this.salary = salary;
	}

	public Integer getAgeOfSentence() {
		return ageOfSentence;
	}

	public void setAgeOfSentence(Integer ageOfSentence) {
		this.ageOfSentence = ageOfSentence;
	}

	public Boolean getActiveWarrant() {
		return activeWarrant;
	}

	public void setActiveWarrant(Boolean activeWarrant) {
		this.activeWarrant = activeWarrant;
	}

	public Integer getAgeOfWarrant() {
		return ageOfWarrant;
	}

	public void setAgeOfWarrant(Integer ageOfWarrant) {
		this.ageOfWarrant = ageOfWarrant;
	}

	public Boolean getProbationActive() {
		return probationActive;
	}

	public void setProbationActive(Boolean probationActive) {
		this.probationActive = probationActive;
	}

	public String getCaseType() {
		return caseType;
	}

	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}

	public Integer getAgeOnOffensedate() {
		return ageOnOffensedate;
	}

	public void setAgeOnOffensedate(Integer ageOnOffensedate) {
		this.ageOnOffensedate = ageOnOffensedate;
	}

	public Integer getCurrentAge() {
		return currentAge;
	}

	public void setCurrentAge(Integer currentAge) {
		this.currentAge = currentAge;
	}

	public Set<DispositionDescription> getDispositionDescriptions() {
		return dispositionDescriptions;
	}

	public void setDispositionDescriptions(
			Set<DispositionDescription> dispositionDescriptions) {
		this.dispositionDescriptions = dispositionDescriptions;
	}

	public Set<ChargeLevel> getChargeLevels() {
		return chargeLevels;
	}

	public void setChargeLevels(Set<ChargeLevel> chargeLevels) {
		this.chargeLevels = chargeLevels;
	}

	public Set<JurisdictionCounty> getCounties() {
		return counties;
	}

	public void setCounties(Set<JurisdictionCounty> counties) {
		this.counties = counties;
	}

	public Set<ChargeDescription> getChargeDescriptions() {
		return chargeDescriptions;
	}

	public void setChargeDescriptions(Set<ChargeDescription> chargeDescriptions) {
		this.chargeDescriptions = chargeDescriptions;
	}

	
	public Jurisdiction getRuleJurisdiction() {
		return ruleJurisdiction;
	}

	public void setRuleJurisdiction(Jurisdiction ruleJurisdiction) {
		this.ruleJurisdiction = ruleJurisdiction;
	}

	public DispositionType getDispositionType() {
		return dispositionType;
	}

	public void setDispositionType(DispositionType dispositionType) {
		this.dispositionType = dispositionType;
	}

	@Override
	public String toString() {
		return "StateConfigurationFact [ruleName=" + ruleName
				+ ", ageOfRecord=" + ageOfRecord + ", salary=" + salary
				+ ", ageOfSentence=" + ageOfSentence + ", activeWarrant="
				+ activeWarrant + ", ageOfWarrant=" + ageOfWarrant
				+ ", probationActive=" + probationActive + ", caseType="
				+ caseType + ", ageOnOffensedate=" + ageOnOffensedate
				+ ", currentAge=" + currentAge + ", dispositionDescriptions="
				+ dispositionDescriptions + ", chargeLevels=" + chargeLevels
				+ ", counties=" + counties + ", chargeDescriptions="
				+ chargeDescriptions + ", ruleJurisdiction=" + ruleJurisdiction
				+ ", dispositionType=" + dispositionType + "]";
	}
}

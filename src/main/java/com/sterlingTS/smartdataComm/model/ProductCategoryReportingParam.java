package com.sterlingTS.smartdataComm.model;

public class ProductCategoryReportingParam {

	private int productCategoryReportingParamId;
	private String param;
	private String description;
	private boolean isMandatory;
	private String logicalName;
	private ProductCategory productCategory;
	private GlobalReportingParam globalReportingParam;
	
	
	public void setProductCategoryReportingParamId(
			int productCategoryReportingParamId) {
		this.productCategoryReportingParamId = productCategoryReportingParamId;
	}
	public String getParam() {
		return param;
	}
	public void setParam(String param) {
		this.param = param;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isMandatory() {
		return isMandatory;
	}
	public void setMandatory(boolean isMandatory) {
		this.isMandatory = isMandatory;
	}
	public String getLogicalName() {
		return logicalName;
	}
	public void setLogicalName(String logicalName) {
		this.logicalName = logicalName;
	}
	public ProductCategory getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(ProductCategory productCategory) {
		this.productCategory = productCategory;
	}
	public int getProductCategoryReportingParamId() {
		return productCategoryReportingParamId;
	}
	public GlobalReportingParam getGlobalReportingParam() {
		return globalReportingParam;
	}
	public void setGlobalReportingParam(GlobalReportingParam globalReportingParam) {
		this.globalReportingParam = globalReportingParam;
	}
}

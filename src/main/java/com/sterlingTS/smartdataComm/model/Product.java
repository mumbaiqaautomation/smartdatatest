package com.sterlingTS.smartdataComm.model;

import java.util.HashSet;
import java.util.Set;

import com.google.gson.annotations.Expose;

public class Product {

	@Expose(serialize = false)
	private Integer productId;
	private Set<ProductCategory> productCategory = new HashSet<ProductCategory>();
	private String code;
	private String description;
	private ParentSystem parentSystem;

	/**
	 * @return the productId
	 */
	public Integer getProductId() {
		return productId;
	}

	/**
	 * @param productId
	 *            the productId to set
	 */
	public void setProductId(final Integer productId) {
		this.productId = productId;
	}
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(final String code) {
		this.code = code;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	public ParentSystem getParentSystem() {
		return parentSystem;
	}

	public void setParentSystem(final ParentSystem parentSystem) {
		this.parentSystem = parentSystem;
	}
	
	public Set<ProductCategory> getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(Set<ProductCategory> productCategory) {
		this.productCategory = productCategory;
	}

	@Override
	public String toString() {
		return "Product [productId=" + productId + ", code=" + code
				+ ", description=" + description + "]";
	}

}

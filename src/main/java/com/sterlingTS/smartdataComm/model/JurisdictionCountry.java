package com.sterlingTS.smartdataComm.model;

public class JurisdictionCountry extends Jurisdiction {
	private String countryName;
	private String countryA3Cd;
	private String countryA2Cd;
	private String countryIsoCd;
	private String qualifiedName;

	public String getCountryName() {
		return this.countryName;
	}

	public void setCountryName(final String countryName) {
		this.countryName = countryName;
	}

	public String getCountryA3Cd() {
		return this.countryA3Cd;
	}

	public void setCountryA3Cd(final String countryA3Cd) {
		this.countryA3Cd = countryA3Cd;
	}

	public String getCountryA2Cd() {
		return this.countryA2Cd;
	}

	public void setCountryA2Cd(final String countryA2Cd) {
		this.countryA2Cd = countryA2Cd;
	}

	public String getCountryIsoCd() {
		return this.countryIsoCd;
	}

	public void setCountryIsoCd(final String countryIsoCd) {
		this.countryIsoCd = countryIsoCd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((countryA2Cd == null) ? 0 : countryA2Cd.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final JurisdictionCountry other = (JurisdictionCountry) obj;
		if (countryA2Cd == null) {
			if (other.countryA2Cd != null) {
				return false;
			}
		} else if (!countryA2Cd.equals(other.countryA2Cd)) {
			return false;
		}
		return true;
	}

	public String getQualifiedName() {
		return qualifiedName;
	}

	public void setQualifiedName(final String qualifiedName) {
		this.qualifiedName = qualifiedName;
	}

	@Override
	public String getQName() {
		return qualifiedName;
	}
}

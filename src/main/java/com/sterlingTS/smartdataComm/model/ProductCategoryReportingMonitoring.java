package com.sterlingTS.smartdataComm.model;

import java.util.Date;

public class ProductCategoryReportingMonitoring implements
java.io.Serializable {

private int productCategoryReportingMonitoringId;
private ProductCategoryJurisdiction pdlProductCategoryJurisdiction;
private ProductCategoryReportingThreshold productCategoryReportingThreshold;
private ProductCategoryJurisdictionDatasource productCategoryJurisdictionDatasource;
private Date fromDate;
private Date toDate;
private int actualPercentileChargesMetThreshold;
private Date monitoringDate;
private boolean complaint;
public int getProductCategoryReportingMonitoringId() {
return productCategoryReportingMonitoringId;
}
public void setProductCategoryReportingMonitoringId(
	int productCategoryReportingMonitoringId) {
this.productCategoryReportingMonitoringId = productCategoryReportingMonitoringId;
}
public ProductCategoryJurisdiction getPdlProductCategoryJurisdiction() {
return pdlProductCategoryJurisdiction;
}
public void setPdlProductCategoryJurisdiction(
	ProductCategoryJurisdiction pdlProductCategoryJurisdiction) {
this.pdlProductCategoryJurisdiction = pdlProductCategoryJurisdiction;
}
public ProductCategoryReportingThreshold getProductCategoryReportingThreshold() {
return productCategoryReportingThreshold;
}
public void setProductCategoryReportingThreshold(
	ProductCategoryReportingThreshold productCategoryReportingThreshold) {
this.productCategoryReportingThreshold = productCategoryReportingThreshold;
}
public ProductCategoryJurisdictionDatasource getProductCategoryJurisdictionDatasource() {
return productCategoryJurisdictionDatasource;
}
public void setProductCategoryJurisdictionDatasource(
	ProductCategoryJurisdictionDatasource productCategoryJurisdictionDatasource) {
this.productCategoryJurisdictionDatasource = productCategoryJurisdictionDatasource;
}
public Date getFromDate() {
return fromDate;
}
public void setFromDate(Date fromDate) {
this.fromDate = fromDate;
}
public Date getToDate() {
return toDate;
}
public void setToDate(Date toDate) {
this.toDate = toDate;
}
public int getActualPercentileChargesMetThreshold() {
return actualPercentileChargesMetThreshold;
}
public void setActualPercentileChargesMetThreshold(
	int actualPercentileChargesMetThreshold) {
this.actualPercentileChargesMetThreshold = actualPercentileChargesMetThreshold;
}
public Date getMonitoringDate() {
return monitoringDate;
}
public void setMonitoringDate(Date monitoringDate) {
this.monitoringDate = monitoringDate;
}
public boolean isComplaint() {
return complaint;
}
public void setComplaint(boolean complaint) {
this.complaint = complaint;
}
}

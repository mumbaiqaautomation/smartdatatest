package com.sterlingTS.smartdataComm.model;

import com.google.gson.annotations.Expose;

public class JurisdictionType {

	@Expose(serialize = false)
	private int jurisdictionTypeId;
	private String jurisdictionTypeName;

	public JurisdictionType() {
	}

	public JurisdictionType(final String jurisdictionTypeName) {
		//this.jurisdictionTypeId = jurisdictionTypeId;
		this.jurisdictionTypeName = jurisdictionTypeName;
	}

	public int getJurisdictionTypeId() {
		return this.jurisdictionTypeId;
	}

	public void setJurisdictionTypeId(final int jurisdictionTypeId) {
		this.jurisdictionTypeId = jurisdictionTypeId;
	}

	public String getJurisdictionTypeName() {
		return this.jurisdictionTypeName;
	}

	public void setJurisdictionTypeName(final String jurisdictionTypeName) {
		this.jurisdictionTypeName = jurisdictionTypeName;
	}

	@Override
	public String toString() {
		return getJurisdictionTypeName();
	}
}

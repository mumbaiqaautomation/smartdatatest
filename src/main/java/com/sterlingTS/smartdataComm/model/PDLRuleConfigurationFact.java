package com.sterlingTS.smartdataComm.model;

import java.util.HashSet;
import java.util.Set;

public class PDLRuleConfigurationFact {

	private String ruleDescription;
	private String ruleName;
	private Integer scopeOfSearch;
	private Integer dataFrequency;
	private Set<ChargeLevel> chargeLevels = new HashSet<ChargeLevel>();
	private Set<DispositionType> dispositionTypes = new HashSet<DispositionType>();
	private GlobalSearchParam globalSearchParam;
	private ProductCategorySearchParam productCategorySearchParam;

	public PDLRuleConfigurationFact() {
	}

	public PDLRuleConfigurationFact(ProductCategorySearchParam productCategorySearchParam) {
		this.setChargeLevels(productCategorySearchParam.getChargeLevels());
		this.setDispositionTypes(productCategorySearchParam.getDispositionTypes());
		this.setRuleName(productCategorySearchParam.getGlobalSearchParam().getParam());
		this.setGlobalSearchParam(productCategorySearchParam.getGlobalSearchParam());
		this.setRuleDescription(productCategorySearchParam.getGlobalSearchParam().getDescription());
		this.setScopeOfSearch(productCategorySearchParam.getScopeOfSearch());
		this.setDataFrequency(productCategorySearchParam.getDataFrequency());
		this.setProductCategorySearchParam(productCategorySearchParam);
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public Set<ChargeLevel> getChargeLevels() {
		return chargeLevels;
	}

	public void setChargeLevels(Set<ChargeLevel> chargeLevels) {
		this.chargeLevels = chargeLevels;
	}

	@Override
	public String toString() {
		return "RuleConfigurationFact [ruleName=" + ruleName + ",scopeOfSearch=" + scopeOfSearch + ",dataFrequency="
				+ dataFrequency + "]";
	}

	public String getRuleDescription() {
		return ruleDescription;
	}

	public void setRuleDescription(String ruleDescription) {
		this.ruleDescription = ruleDescription;
	}

	public Set<DispositionType> getDispositionTypes() {
		return dispositionTypes;
	}

	public void setDispositionTypes(Set<DispositionType> dispositionTypes) {
		this.dispositionTypes = dispositionTypes;
	}

	public Integer getScopeOfSearch() {
		return scopeOfSearch;
	}

	public void setScopeOfSearch(Integer scopeOfSearch) {
		this.scopeOfSearch = scopeOfSearch;
	}

	public GlobalSearchParam getGlobalSearchParam() {
		return globalSearchParam;
	}

	public void setGlobalSearchParam(GlobalSearchParam globalSearchParam) {
		this.globalSearchParam = globalSearchParam;
	}

	public Integer getDataFrequency() {
		return dataFrequency;
	}

	public void setDataFrequency(Integer dataFrequency) {
		this.dataFrequency = dataFrequency;
	}

	public ProductCategorySearchParam getProductCategorySearchParam() {
		return productCategorySearchParam;
	}

	public void setProductCategorySearchParam(ProductCategorySearchParam productCategorySearchParam) {
		this.productCategorySearchParam = productCategorySearchParam;
	}

	public String unWrapChargeLevels(Set<ChargeLevel> chargeLevels) {
		StringBuilder chargeLevelString = new StringBuilder();
		int count = 1;

		if (!this.getChargeLevels().isEmpty()) {
			chargeLevelString.append("[");
			for (ChargeLevel chargeLevel : this.getChargeLevels()) {
				if (chargeLevel != null) {

					chargeLevelString.append(chargeLevel.getLevel());
					if (count != this.getChargeLevels().size()) {
						chargeLevelString.append(",");

					}

					count++;
				}
			}
			return chargeLevelString.append("]").toString();
		}
		return "";
	}

	public String unWrapDispositionTypes(Set<DispositionType> dispositionTypes) {
		StringBuilder dispositionTypesString = new StringBuilder();
		int count = 1;

		if (!this.getDispositionTypes().isEmpty()) {
			dispositionTypesString.append("[");
			for (DispositionType dispositionType : this.getDispositionTypes()) {
				if (dispositionType != null) {

					dispositionTypesString.append(dispositionType.getDispositionType());
					if (count != this.getDispositionTypes().size()) {
						dispositionTypesString.append(",");

					}

					count++;
				}
			}
			return dispositionTypesString.append("]").toString();
		}
		return "";
	}
}

package com.sterlingTS.smartdataComm.model;

public class DispositionMapping implements java.io.Serializable {

	private int dispositionMappingId;
	private DispositionAbbreviation dispositionAbbreviation;
	private DispositionType dispositionType;
	private Jurisdiction jurisdiction;

	
	public int getDispositionMappingId() {
		return this.dispositionMappingId;
	}

	public void setDispositionMappingId(int dispositionMappingId) {
		this.dispositionMappingId = dispositionMappingId;
	}

	public DispositionType getDispositionType() {
		return this.dispositionType;
	}

	public void setDispositionType(DispositionType dispositionType) {
		this.dispositionType = dispositionType;
	}

	public Jurisdiction getJurisdiction() {
		return this.jurisdiction;
	}

	public void setJurisdiction(Jurisdiction jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public DispositionAbbreviation getDispositionAbbreviation() {
		return dispositionAbbreviation;
	}

	public void setDispositionAbbreviation(
			DispositionAbbreviation dispositionAbbreviation) {
		this.dispositionAbbreviation = dispositionAbbreviation;
	}
}

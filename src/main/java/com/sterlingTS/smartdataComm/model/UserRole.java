package com.sterlingTS.smartdataComm.model;

public class UserRole implements java.io.Serializable {

	private int userRoleId;
	private User user;
	private Role role;

	public UserRole() {
	}

	public UserRole(int userRoleId, User user, Role role) {
		this.userRoleId = userRoleId;
		this.user = user;
		this.role = role;
	}

	public int getUserRoleId() {
		return this.userRoleId;
	}

	public void setUserRoleId(int userRoleId) {
		this.userRoleId = userRoleId;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
}

package com.sterlingTS.smartdataComm.model;

public class ProductCategorySearchMonitoringParam implements
java.io.Serializable {

private int productCategorySearchMonitoringParamId;
private ProductCategorySearchMonitoring productCategorySearchMonitoring;
private GlobalSearchParam globalSearchParam;
private Boolean complaint;
public int getProductCategorySearchMonitoringParamId() {
return productCategorySearchMonitoringParamId;
}
public void setProductCategorySearchMonitoringParamId(
	int productCategorySearchMonitoringParamId) {
this.productCategorySearchMonitoringParamId = productCategorySearchMonitoringParamId;
}
public ProductCategorySearchMonitoring getProductCategorySearchMonitoring() {
return productCategorySearchMonitoring;
}
public void setProductCategorySearchMonitoring(
	ProductCategorySearchMonitoring productCategorySearchMonitoring) {
this.productCategorySearchMonitoring = productCategorySearchMonitoring;
}
public GlobalSearchParam getGlobalSearchParam() {
return globalSearchParam;
}
public void setGlobalSearchParam(GlobalSearchParam globalSearchParam) {
this.globalSearchParam = globalSearchParam;
}
public Boolean getComplaint() {
return complaint;
}
public void setComplaint(Boolean complaint) {
this.complaint = complaint;
}
}

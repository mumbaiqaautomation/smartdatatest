package com.sterlingTS.smartdataComm.model;

import com.google.gson.annotations.Expose;

public class Account {
	@Expose(serialize = false)
	private int accountId;
	private Customer customer;
	@Expose(serialize = false)
	private Pod pod;
	private String accountName;

	/**
	 * @return the accountId
	 */
	public int getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId
	 *            the accountId to set
	 */
	public void setAccountId(final int accountId) {
		this.accountId = accountId;
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * @param customer
	 *            the customer to set
	 */
	public void setCustomer(final Customer customer) {
		this.customer = customer;
	}

	/**
	 * @return the pod
	 */
	public Pod getPod() {
		return pod;
	}

	/**
	 * @param pod
	 *            the pod to set
	 */
	public void setPod(final Pod pod) {
		this.pod = pod;
	}

	/**
	 * @return the accountName
	 */
	public String getAccountName() {
		return accountName;
	}

	/**
	 * @param accountName
	 *            the accountName to set
	 */
	public void setAccountName(final String accountName) {
		this.accountName = accountName;
	}

	@Override
	public String toString() {
		return "Account [accountId=" + accountId + ", customer=" + customer
				+ ", accountName=" + accountName + "]";
	}

}

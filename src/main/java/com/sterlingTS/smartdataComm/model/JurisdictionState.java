package com.sterlingTS.smartdataComm.model;

public class JurisdictionState extends Jurisdiction {
	private String stateA2Cd;
	private String countryA2Cd;
	private String stateName;
	private String fipsCode;
	private String qualifiedName;
	private String state;
	
	public String getStateA2Cd() {
		return this.stateA2Cd;
	}

	public void setStateA2Cd(final String stateA2Cd) {
		this.stateA2Cd = stateA2Cd;
	}

	public String getCountryA2Cd() {
		return this.countryA2Cd;
	}

	public void setCountryA2Cd(final String countryA2Cd) {
		this.countryA2Cd = countryA2Cd;
	}

	public String getStateName() {
		return this.stateName;
	}

	public void setStateName(final String stateName) {
		this.stateName = stateName;
	}

	public String getFipsCode() {
		return fipsCode;
	}

	public void setFipsCode(final String fipsCode) {
		this.fipsCode = fipsCode;
	}

	public String getQualifiedName() {
		return qualifiedName;
	}

	public void setQualifiedName(final String qualifiedName) {
		this.qualifiedName = qualifiedName;
	}
}

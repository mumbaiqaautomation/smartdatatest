package com.sterlingTS.smartdataComm.model;

public class ChargeMapping implements java.io.Serializable {
	private int chargeMappingId;
	private ChargeLevel chargeLevel;
	private ChargeCategory chargeCategory;
	private ChargeAbbreviation chargeAbbreviation;
	private Jurisdiction jurisdiction;
	private String statuteCode;


	public int getChargeMappingId() {
		return this.chargeMappingId;
	}

	public void setChargeMappingId(int chargeMappingId) {
		this.chargeMappingId = chargeMappingId;
	}

	public ChargeLevel getChargeLevel() {
		return this.chargeLevel;
	}

	public void setChargeLevel(ChargeLevel chargeLevel) {
		this.chargeLevel = chargeLevel;
	}

	public ChargeCategory getChargeCategory() {
		return this.chargeCategory;
	}

	public void setChargeCategory(ChargeCategory chargeCategory) {
		this.chargeCategory = chargeCategory;
	}

	
	public Jurisdiction getJurisdiction() {
		return this.jurisdiction;
	}

	public void setJurisdiction(Jurisdiction jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public String getStatuteCode() {
		return this.statuteCode;
	}

	public void setStatuteCode(String statuteCode) {
		this.statuteCode = statuteCode;
	}

	public ChargeAbbreviation getChargeAbbreviation() {
		return chargeAbbreviation;
	}

	public void setChargeAbbreviation(ChargeAbbreviation chargeAbbreviation) {
		this.chargeAbbreviation = chargeAbbreviation;
	}
}

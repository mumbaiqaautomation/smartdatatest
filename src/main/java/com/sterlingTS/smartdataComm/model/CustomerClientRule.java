package com.sterlingTS.smartdataComm.model;

import com.google.gson.annotations.Expose;

public class CustomerClientRule {

	@Expose(serialize = false)
	private String ruleName;
	@Expose(serialize = false)
	private String ruleDescription;
	
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	public String getRuleDescription() {
		return ruleDescription;
	}
	public void setRuleDescription(String ruleDescription) {
		this.ruleDescription = ruleDescription;
	}

}

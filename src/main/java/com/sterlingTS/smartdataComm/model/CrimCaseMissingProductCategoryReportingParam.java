package com.sterlingTS.smartdataComm.model;

public class CrimCaseMissingProductCategoryReportingParam implements
java.io.Serializable {

	private int crimCaseMissingProductCategoryReportingParamId;
	private CrimCase crimCase;
	private ProductCategoryReportingParam productCategoryReportingParam;



	public int getCrimCaseMissingProductCategoryReportingParamId() {
		return this.crimCaseMissingProductCategoryReportingParamId;
	}

	public void setCrimCaseMissingProductCategoryReportingParamId(
			int crimCaseMissingProductCategoryReportingParamId) {
		this.crimCaseMissingProductCategoryReportingParamId = crimCaseMissingProductCategoryReportingParamId;
	}

	public CrimCase getCrimCase() {
		return this.crimCase;
	}

	public void setCrimCase(CrimCase crimCase) {
		this.crimCase = crimCase;
	}

	public ProductCategoryReportingParam getProductCategoryReportingParam() {
		return productCategoryReportingParam;
	}

	public void setProductCategoryReportingParam(
			ProductCategoryReportingParam productCategoryReportingParam) {
		this.productCategoryReportingParam = productCategoryReportingParam;
	}

}

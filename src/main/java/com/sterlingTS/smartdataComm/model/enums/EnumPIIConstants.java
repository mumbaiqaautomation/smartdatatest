package com.sterlingTS.smartdataComm.model.enums;

public enum EnumPIIConstants {
	FIRST_LAST_NAME_MISMATCH("crimCases.lastOnRecordName"),
	MIDDLE_NAME_MISMATCH("crimCases.middleInitialOnRecord"),
	SSN_MISMATCH("crimCases.governmentId"),
	DL_MISMATCH("crimCases.dlOnRecord"),
	ADDRESS_MISMATCH("crimCases.addressOnRecord");
	
	private String name;
	
	EnumPIIConstants(String name){
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

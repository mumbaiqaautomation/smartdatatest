package com.sterlingTS.smartdataComm.model.enums;

public enum PIIMatchEnum {
	MATCH("Match"),
	NO_MATCH("No Match"),
	MISMATCH("MisMatch");
	
	
	PIIMatchEnum(String value){
		this.value = value;
	}
	
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}

package com.sterlingTS.smartdataComm.model.enums;

public enum SentenceSanitizationLevelEnum {
	//@formatter:off
		SPECIFIC_JURISDICTION_DESCRIPTION("SPECIFIC_JURISDICTION_DESCRIPTION"), 
		NATIONAL_JURISDICTION_DESCRIPTION("NATIONAL_JURISDICTION_DESCRIPTION"),
		NONE("NONE");
		
		private String name;
		
		SentenceSanitizationLevelEnum(String name){
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
		
		
		//@formatter:on
}

package com.sterlingTS.smartdataComm.model.enums;

import com.sterlingTS.smartdataComm.model.SmartDataStatus;

public enum SmartDataStatusEnum {
	//Record Status
	NEW(1, "NEW", "New ScreeningResult received for Processing","Record"), 
	INITIAL_RULES_APPLIED(2, "INITIAL_RULES_APPLIED","Pre sanitization Rules Applied","Record"),  
	SCORING_SUCCESS(3, "SCORING_SUCCESS", "Scoring and PII success","Record"),  
	INFO_NEEDED(4, "INFO_NEEDED", "Info needed due to insufficient scoring","Record"),  
	INFO_PROVIDED(5, "INFO_PROVIDED","Info Provided by Parent System","Record"),  
	DATA_SANITIZIED(6, "DATA_SANITIZIED", "Case/Charge sanitization done","Record"),  
	RULES_APPLIED(7, "RULES_APPLIED", "Rules Applied","Record"),  
	KNOWN_HIT_IDENTIFIED(8, "KNOWN_HIT_IDENTIFIED", "Known Hit Check","Record"),  
	COMPLETED_AS_CLEAR(9, "COMPLETED_AS_CLEAR", "Completed as clear","Record"),  
	REVIEWED(10, "REVIEWED", "Reviewed by Smart Data","Record"),  
	ERROR_IN_PROCESSING(11, "ERROR_IN_PROCESSING", "Any error resulted in Smart Data process failure","Record"),  
	NEEDS_MANUAL_REVIEW(12,"NEEDS_MANUAL_REVIEW", "SmartData identified that result needs extra Manual Review","Record");
	
	

	int statusId;
	String statusName;
	String description;
	String type;

	private SmartDataStatusEnum(final int statusId, final String statusName, final String description, final String type) {
		this.statusName = statusName;
		this.statusId = statusId;
		this.description = description;
		this.type=type;
	}

	public SmartDataStatus getSmartDataStatus(){
		SmartDataStatus smartDataStatus = new SmartDataStatus(statusId,statusName,type);
		return smartDataStatus;
	}
}

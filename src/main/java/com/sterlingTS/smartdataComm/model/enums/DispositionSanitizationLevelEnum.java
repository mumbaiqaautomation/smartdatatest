package com.sterlingTS.smartdataComm.model.enums;

public enum DispositionSanitizationLevelEnum {
	//@formatter:off
		SPECIFIC_JURISDICTION_DESCRIPTION("SPECIFIC_JURISDICTION_DESCRIPTION"), 
		NATIONAL_JURISDICTION_DESCRIPTION("NATIONAL_JURISDICTION_DESCRIPTION"),
		NONE("NONE");
		
		private String name;
		
		DispositionSanitizationLevelEnum(String name){
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
		
		
		//@formatter:on
}

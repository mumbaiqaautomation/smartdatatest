package com.sterlingTS.smartdataComm.model.enums;

import com.sterlingTS.smartdataComm.model.ChargeLevel;

public enum ChargeLevelEnum {
	FELONY( 1, "FELONY",      "Felony",MappedChargeLevelEnum.FELONY),
	  MISDEMEANOR( 2, "MISDEMEANOR", "Misdemeanor",MappedChargeLevelEnum.MISDEMEANOR),
	  VIOLATION( 3, "VIOLATION",   "Violation",MappedChargeLevelEnum.VIOLATION),
	  UNKNOWN  ( 4, "UNKNOWN",     "Unknown",MappedChargeLevelEnum.UNKNOWN),
	  DATAENTRY  ( 5, "DATAENTRY",     "DATA ENTRY",MappedChargeLevelEnum.DATAENTRY),
	  GROSS_MISDEMEANOR  ( 6, "GROSS_MISDEMEANOR",     "Gross Misdemeanor",MappedChargeLevelEnum.MISDEMEANOR),
	  SERIOUS_MISDEMEANOR  ( 7, "SERIOUS_MISDEMEANOR",     "Serious Misdemeanor",MappedChargeLevelEnum.MISDEMEANOR),
	  SIMPLE_MISDEMEANOR  ( 8, "SIMPLE_MISDEMEANOR",     "Simple Misdemeanor",MappedChargeLevelEnum.MISDEMEANOR),
	  AGRRAVATED_MISDEMEANOR  ( 9, "AGRRAVATED_MISDEMEANOR",     "Agrravated Misdemeanor",MappedChargeLevelEnum.MISDEMEANOR),
	  TRAFFIC  ( 10, "TRAFFIC",     "Traffic",MappedChargeLevelEnum.VIOLATION),
	  INFRACTION  ( 11, "INFRACTION",     "Infraction",MappedChargeLevelEnum.VIOLATION),
	  NOT_LISTED  ( 12, "NOT_LISTED",     "Not listed",MappedChargeLevelEnum.UNKNOWN),
	  SEALED  ( 13, "SEALED",     "Sealed",MappedChargeLevelEnum.UNKNOWN),
	  SHOW_CAUSE  ( 15, "SHOW_CAUSE",     "Show cause",MappedChargeLevelEnum.UNKNOWN),//Changed by DP for story AGIFN-1985
	  ORDINANCE_VIOLATION  ( 15, "ORDINANCE_VIOLATION","Ordinance violation",MappedChargeLevelEnum.VIOLATION),
	  MUNICIPAL_ORDINANCE  ( 16, "MUNICIPAL_ORDINANCE","Municipal Ordinance",MappedChargeLevelEnum.VIOLATION),
	  INDICTABLE  ( 17, "INDICTABLE",     "Indictable",MappedChargeLevelEnum.FELONY),
	  NON_INDICTABLE  ( 18, "NON_INDICTABLE","Non Indictable",MappedChargeLevelEnum.MISDEMEANOR),
	  DISORDERLY_PERSON  ( 19, "DISORDERLY_PERSON",     "Disorderly Person",MappedChargeLevelEnum.VIOLATION),//Changed by DP for story AGIFN-1985
	  FORFEITURE  ( 20, "FORFEITURE",     "Forfeiture",MappedChargeLevelEnum.VIOLATION),
	  CAPIAS  ( 21, "CAPIAS",     "Capias",MappedChargeLevelEnum.UNKNOWN),//Changed by DP for story AGIFN-1985
	  BRANCH  ( 22, "BRANCH",     "Branch",MappedChargeLevelEnum.UNKNOWN),
	  PETTY_OFFENSE  ( 23, "PETTY_OFFENSE",     "Petty Offense",MappedChargeLevelEnum.VIOLATION),
	  SUMMARY  ( 24, "SUMMARY",     "Summary",MappedChargeLevelEnum.VIOLATION),
	  UNDESIGNATED  ( 25, "UNDESIGNATED",     "Undesignated",MappedChargeLevelEnum.UNKNOWN);

	  private MappedChargeLevelEnum mappedChargeLevel;
	  private int chargeLevelId;
	  private String level;
	  private String chargeLevelDescription;
	  
	  private ChargeLevelEnum ( int chargeLevelId, String level, String chargeLevelDescription, MappedChargeLevelEnum mappedChargeLevel)
	  {
	    this.mappedChargeLevel=mappedChargeLevel;
	    this.chargeLevelId=chargeLevelId;
	    this.chargeLevelDescription=chargeLevelDescription;
	    this.level=level;
	  }

	 
	  public ChargeLevel getCrimChargeLevel ()
	  {
	    ChargeLevel crimChargeLevel = new ChargeLevel();
		//crimChargeLevel.setChargeLevelId ( chargeLevelId );
	    crimChargeLevel.setLevel ( level );
	    crimChargeLevel.setDescription ( chargeLevelDescription );

	    return crimChargeLevel;
	  }

}

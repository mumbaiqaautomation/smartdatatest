package com.sterlingTS.smartdataComm.model.enums;

public enum KnownResultEnum {
	NEW_RECORD(0,"New_Record"),KNOWN_CLEAR(1, "Known_Clear"), KNOWN_HIT(2, "Known_Hit");

	private int value;
	private String knownResult;

	private KnownResultEnum(final int value, final String knownResult) {
		this.value = value;
		this.knownResult = knownResult;
	}

	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(final int value) {
		this.value = value;
	}

	/**
	 * @return the knownResult
	 */
	public String getKnownResult() {
		return knownResult;
	}

	/**
	 * @param knownResult
	 *            the knownResult to set
	 */
	public void setKnownResult(final String knownResult) {
		this.knownResult = knownResult;
	}
}

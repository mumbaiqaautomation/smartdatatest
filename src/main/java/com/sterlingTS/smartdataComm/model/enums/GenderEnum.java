package com.sterlingTS.smartdataComm.model.enums;

public enum GenderEnum {
	NOT_KNOWN(0,"NOT_KNOWN"),
	MALE(1, "MALE"), 
	FEMALE(2, "FEMALE"),
	NOT_SPECIFIED(9,"NOT_SPECIFIED");

	private int value;
	private String gender;

	private GenderEnum(final int value, final String gender) {
		this.value = value;
		this.gender = gender;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
}

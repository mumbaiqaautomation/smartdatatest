package com.sterlingTS.smartdataComm.model.enums;

import java.util.ArrayList;
import java.util.Collection;

public enum MappedDispositionTypeEnum {
	CONVICTION("Conviction"),
	ARREST("Arrest"),
	UNKNOWN("Unknown"),
	NON_CONVICTION("Non Conviction");
	
	private String name;
	
	private MappedDispositionTypeEnum(String name){
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	private Collection<DispositionTypeEnum> chargeType = new ArrayList<DispositionTypeEnum>();
	
	public Collection<DispositionTypeEnum> getChargeType() {
		return chargeType;
	}
	
	public void addChargeType(DispositionTypeEnum enumCrimChargeType){
		chargeType.add(enumCrimChargeType);
	}
}

package com.sterlingTS.smartdataComm.model.enums;

public enum RuleEnum {
	CONVICTION_REPORT("Conviction_Report"),
	CONVICTION_SENTENCE("Conviction_Sentence"),
	CONVICTION_SENTENCE_SALARY("Conviction_Sentence_Salary"),
	OPEN_WARRANT("Open_Warrant"),
	WARRANT_AGEOFWARRANT("Warrant_AgeOfWarrant"),
	DEFFERED_DISPOSITION("DefferedDisposition"),
	PENDING_CASE("Pending"),
	ARREST_SALARY("Arrest_Salary"),
	ARREST_REPORT("Arrest_Report"),
	ARREST_DONOTREPORT("Arrest_DoNotReport"),
	DISPOSITION ("Disposition"),
	CHARGE_LEVEL ("ChargeLevel"),
	CHARGE_LEVEL_DESCRIPTION("ChargeLevel_ChargeDescription"),
	CHARGE_LEVEL_DISPOSITION("ChargeLevel_Disposition"),
	JUVENILLECASE_COUNTY("JuvenilleCase_County"),
	JUVENILLECASE_DONOTREPORT("JuvenilleCase_DoNotReport"),
	JUVENILLECASE_AGEOFRECORD("JuvenilleCase_AgeOfRecord"),
	JUVENILLECASE_AGEOFAPPLICANT("JuvenilleCase_AgeOfApplicant"),
	JUVENILLECASE_RECORDLEVEL("JuvenilleCase_RecordLevel");

	String ruleName;
	private RuleEnum(String ruleName){
		this.ruleName=ruleName;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return ruleName;
	}
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String rule) {
		this.ruleName = rule;
	}
}

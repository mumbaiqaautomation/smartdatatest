package com.sterlingTS.smartdataComm.model.enums;

import java.util.ArrayList;
import java.util.Collection;

import com.sterlingTS.smartdataComm.model.ChargeLevel;

public enum MappedChargeLevelEnum {
	FELONY("Felony"),
	MISDEMEANOR("Misdemeanor"),
	VIOLATION("Violation"),
	UNKNOWN("Unknown"),
	DATAENTRY("DataEntry");
	
	private String name;
	
	private MappedChargeLevelEnum(String name){
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private Collection<ChargeLevel> chargeLevel = new ArrayList<ChargeLevel>();

	public Collection<ChargeLevel> getChargeLevel() {
		return chargeLevel;
	}

	public void addChargeLevel(ChargeLevel enumCrimChargeLevel) {
		chargeLevel.add(enumCrimChargeLevel);
	}
}

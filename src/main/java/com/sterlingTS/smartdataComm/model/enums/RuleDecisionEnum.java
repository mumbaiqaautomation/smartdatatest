package com.sterlingTS.smartdataComm.model.enums;

public enum RuleDecisionEnum {
	REPORT("REPORT"), 
	DO_NOT_REPORT("DO_NOT_REPORT"), 
	ERROR("ERROR");

	private String decision;

	private RuleDecisionEnum(final String decision) {
		this.decision = decision;
	}

	public String getDecision() {
		return decision;
	}

	public void setDecision(final String decision) {
		this.decision = decision;
	}
}

package com.sterlingTS.smartdataComm.model.enums;

public enum ClientRuleStatusTypeEnum {
	CLIENT_RULE_AVAILABLE("CLIENT_RULE_AVAILABLE", "Client Rule Available"),
	CLIENT_RULE_UNAVAILABLE("CLIENT_RULE_UNAVAILABLE", "Client Rule Unavailable"),
	CLIENT_RULE_NOTSETUP("CLIENT_RULE_NOTSETUP", "Client Rule NotSetup"),
	CLIENT_RULE_ERROR("CLIENT_RULE_ERROR", "Client Rule Error ");
	

	private String name;
	private String description;
	
	private ClientRuleStatusTypeEnum(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}

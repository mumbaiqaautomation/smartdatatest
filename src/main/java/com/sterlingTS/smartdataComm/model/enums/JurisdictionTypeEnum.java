package com.sterlingTS.smartdataComm.model.enums;

import com.sterlingTS.smartdataComm.model.JurisdictionType;

public enum JurisdictionTypeEnum {
	NATIONAL(1,"National"),
	STATE(2,"State"),
	COUNTY(3,"County"),
	FEDERAL_DISTRICT(4,"Federal District"),
	LOCAL_DISTRICT(4,"Local District");
	
	private int id;
	private String description;
	
	private JurisdictionTypeEnum(int id, String description){
		this.id = id;
		this.description=description;
	}
	
	public JurisdictionType getJurisdictionType(){
		JurisdictionType jt = new JurisdictionType();
		//jt.setJurisdictionTypeId(id);
		jt.setJurisdictionTypeName(description);
		return jt;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}

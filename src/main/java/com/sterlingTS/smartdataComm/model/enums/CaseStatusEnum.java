package com.sterlingTS.smartdataComm.model.enums;

public enum CaseStatusEnum {
	PENDING_TRIAL("Pending Trial"),
	ACTIVE("Active Case"),
	CLOSED("Closed Case"),
	UNKNOWN("Unknown"),
	DATAENTRY("DATA ENTRY"),
	RELEASED("Released");
	
	private String statusName;
	
	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	CaseStatusEnum(String statusName)
	{
		this.statusName = statusName;
	}
	
	public static CaseStatusEnum findByStatusName(String statusName)
	{
		if(statusName.equalsIgnoreCase(PENDING_TRIAL.getStatusName()))
			return PENDING_TRIAL;
		else if(statusName.equalsIgnoreCase(ACTIVE.getStatusName()))
			return ACTIVE;
		else if(statusName.equalsIgnoreCase(CLOSED.getStatusName()))
			return CLOSED;
		else if(statusName.equalsIgnoreCase(UNKNOWN.getStatusName()))
			return UNKNOWN;
		else if(statusName.equalsIgnoreCase(DATAENTRY.getStatusName()))
			return DATAENTRY;
		else if(statusName.equalsIgnoreCase(RELEASED.getStatusName()))
			return RELEASED;
		else
			return null;
		
	}

}

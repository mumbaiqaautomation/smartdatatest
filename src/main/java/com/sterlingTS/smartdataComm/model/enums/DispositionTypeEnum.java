package com.sterlingTS.smartdataComm.model.enums;

import com.sterlingTS.smartdataComm.model.DispositionType;

public enum DispositionTypeEnum {
	ARREST( 1, "ARREST",     "Arrest" ,MappedDispositionTypeEnum.ARREST),
	 CONVICTION( 2, "CONVICTION", "Conviction",MappedDispositionTypeEnum.CONVICTION ),
	 UNKNOWN   ( 3, "UNKNOWN",    "Unknown",MappedDispositionTypeEnum.UNKNOWN ),
	 REMANDED   ( 4, "REMANDED",    "Remanded",MappedDispositionTypeEnum.UNKNOWN ),
	 DATAENTRY   ( 5, "DATAENTRY",    "DATA ENTRY",MappedDispositionTypeEnum.UNKNOWN ),
	 NON_CONVICTION   ( 6, "NON_CONVICTION",    "Non Conviction",MappedDispositionTypeEnum.NON_CONVICTION ),
	 NOT_DISPOSED   ( 7, "NOT_DISPOSED",    "Not Disposed",MappedDispositionTypeEnum.NON_CONVICTION );

	private int id;
	private String type;
	private String description;
	private MappedDispositionTypeEnum mappedChargeType;
	
	private DispositionTypeEnum ( int id, String type, String description,MappedDispositionTypeEnum mappedChargeType )
	  {
	   this.id=id;
	   this.type=type;
	   this.description=description;
	   this.mappedChargeType=mappedChargeType;
	  }
	
	  public DispositionType getDispositionType ()
	  {
	    DispositionType dispositionType = new DispositionType();
		dispositionType.setDescription(description);
		dispositionType.setDispositionType(type);
		//dispositionType.setDispositionTypeId(id);
		return dispositionType;
	  }
}

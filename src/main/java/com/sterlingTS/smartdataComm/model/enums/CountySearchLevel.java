package com.sterlingTS.smartdataComm.model.enums;

public enum CountySearchLevel {
	COUNTY_STATE, CITY_STATE, COURT_STATE, STATE_STATE, POSTALCODE_STATE, DISTRICTCODE_STATE, GLOBAL_COUNTY;
}

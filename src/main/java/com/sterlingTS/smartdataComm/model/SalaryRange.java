package com.sterlingTS.smartdataComm.model;

import com.google.gson.annotations.Expose;

public class SalaryRange {

	@Expose(serialize = false)
	private int salaryRangeId;
	private String salaryRange;
	private int salaryRangeHigh;
	private int salaryRangeLow;

	public int getSalaryRangeId() {
		return salaryRangeId;
	}

	public void setSalaryRangeId(final int salaryRangeId) {
		this.salaryRangeId = salaryRangeId;
	}

	public String getSalaryRange() {
		return salaryRange;
	}

	public void setSalaryRange(final String salaryRange) {
		this.salaryRange = salaryRange;
	}

	public int getSalaryRangeHigh() {
		return salaryRangeHigh;
	}

	public void setSalaryRangeHigh(final int salaryRangeHigh) {
		this.salaryRangeHigh = salaryRangeHigh;
	}

	public int getSalaryRangeLow() {
		return salaryRangeLow;
	}

	public void setSalaryRangeLow(final int salaryRangeLow) {
		this.salaryRangeLow = salaryRangeLow;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((salaryRange == null) ? 0 : salaryRange.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final SalaryRange other = (SalaryRange) obj;
		if (other.getSalaryRangeId() != this.getSalaryRangeId()) {
			return false;
		} else if (other.getSalaryRangeId() == this.getSalaryRangeId() && this.getSalaryRangeId() != 0) {
			return true;
		} else {
			return false;
		}
	}
}

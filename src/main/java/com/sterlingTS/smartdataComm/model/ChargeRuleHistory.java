package com.sterlingTS.smartdataComm.model;

import java.sql.Timestamp;

import com.google.gson.annotations.Expose;

public class ChargeRuleHistory {

	@Expose(serialize = false)
	private int chargeRuleHistoryId;
	private String ruleName;
	private String ruleType;
	private String ruleDescription;
	@Expose(serialize = false)
	private CrimCharge crimCharge;
	private Timestamp timestamp;

	public int getChargeRuleHistoryId() {
		return chargeRuleHistoryId;
	}

	public void setChargeRuleHistoryId(int chargeRuleHistoryId) {
		this.chargeRuleHistoryId = chargeRuleHistoryId;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public String getRuleType() {
		return ruleType;
	}

	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}

	public String getRuleDescription() {
		return ruleDescription;
	}

	public void setRuleDescription(String ruleDescription) {
		this.ruleDescription = ruleDescription;
	}

	public CrimCharge getCrimCharge() {
		return crimCharge;
	}

	public void setCrimCharge(CrimCharge crimCharge) {
		this.crimCharge = crimCharge;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}
}

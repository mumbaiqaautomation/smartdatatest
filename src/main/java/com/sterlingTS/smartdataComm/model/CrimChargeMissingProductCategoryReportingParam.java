package com.sterlingTS.smartdataComm.model;

public class CrimChargeMissingProductCategoryReportingParam implements
java.io.Serializable {

	private int crimChargeMissingProductCategoryReportingParamId;
	private CrimCharge crimCharge;
	private ProductCategoryReportingParam productCategoryReportingParam;

	
	public int getCrimChargeMissingProductCategoryReportingParamId() {
		return this.crimChargeMissingProductCategoryReportingParamId;
	}

	public void setCrimChargeMissingProductCategoryReportingParamId(
			int crimChargeMissingProductCategoryReportingParamId) {
		this.crimChargeMissingProductCategoryReportingParamId = crimChargeMissingProductCategoryReportingParamId;
	}

	public CrimCharge getCrimCharge() {
		return this.crimCharge;
	}

	public void setCrimCharge(CrimCharge crimCharge) {
		this.crimCharge = crimCharge;
	}

	public ProductCategoryReportingParam getProductCategoryReportingParam() {
		return productCategoryReportingParam;
	}

	public void setProductCategoryReportingParam(
			ProductCategoryReportingParam productCategoryReportingParam) {
		this.productCategoryReportingParam = productCategoryReportingParam;
	}
}

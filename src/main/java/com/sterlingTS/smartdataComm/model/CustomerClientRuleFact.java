package com.sterlingTS.smartdataComm.model;

import java.util.HashSet;
import java.util.Set;

import com.google.gson.annotations.Expose;
import com.sterlingTS.smartdataComm.model.enums.ClientRuleStatusTypeEnum;

public class CustomerClientRuleFact {

	private Account account;
    private Profile profile;
    private JurisdictionDetail jurisdictionDetail;
	private ParentSystem parentSystem;
	private Product product;

    
    @Expose(serialize = false)
    private Jurisdiction jurisdiction;
    @Expose(serialize = false)
	private ClientRuleStatusTypeEnum status;
    @Expose(serialize = false)
	private String errorDescription;
    @Expose(serialize = false)
    private Set<CustomerClientRule> customerClientRules =new HashSet<CustomerClientRule>();
   
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	public Profile getProfile() {
		return profile;
	}
	public void setProfile(Profile profile) {
		this.profile = profile;
	}
	public JurisdictionDetail getJurisdictionDetail() {
		return jurisdictionDetail;
	}
	public void setJurisdictionDetail(JurisdictionDetail jurisdictionDetail) {
		this.jurisdictionDetail = jurisdictionDetail;
	}
	public Jurisdiction getJurisdiction() {
		return jurisdiction;
	}
	public void setJurisdiction(Jurisdiction jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public Set<CustomerClientRule> getCustomerClientRules() {
		return customerClientRules;
	}
	public void setCustomerClientRules(Set<CustomerClientRule> customerClientRules) {
		this.customerClientRules = customerClientRules;
	}
	public ClientRuleStatusTypeEnum getStatus() {
		return status;
	}
	public void setStatus(ClientRuleStatusTypeEnum status) {
		this.status = status;
	}
	public String getErrorDescription() {
		return errorDescription;
	}
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	public ParentSystem getParentSystem() {
		return parentSystem;
	}
	public void setParentSystem(ParentSystem parentSystem) {
		this.parentSystem = parentSystem;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
  
}

package com.sterlingTS.smartdataComm.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class ProductCategoryMonitoringAudit {

	private int productCategoryMonitoringAuditId;
	private ProductCategory productCategory;
	private boolean compatible;
	private Date auditTime;
	private Set<DataSourceMonitoringAudit> dataSourceMonitoringAudits = new HashSet<DataSourceMonitoringAudit>();

	public int getProductCategoryMonitoringAuditId() {
		return productCategoryMonitoringAuditId;
	}

	public void setProductCategoryMonitoringAuditId(int productCategoryMonitoringAuditId) {
		this.productCategoryMonitoringAuditId = productCategoryMonitoringAuditId;
	}

	public ProductCategory getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(ProductCategory productCategory) {
		this.productCategory = productCategory;
	}

	public boolean isCompatible() {
		return compatible;
	}

	public void setCompatible(boolean compatible) {
		this.compatible = compatible;
	}

	public Date getAuditTime() {
		return auditTime;
	}

	public void setAuditTime(Date auditTime) {
		this.auditTime = auditTime;
	}

	public Set<DataSourceMonitoringAudit> getDataSourceMonitoringAudits() {
		return dataSourceMonitoringAudits;
	}

	public void setDataSourceMonitoringAudits(Set<DataSourceMonitoringAudit> dataSourceMonitoringAudits) {
		this.dataSourceMonitoringAudits = dataSourceMonitoringAudits;
	}
}

package com.sterlingTS.smartdataComm.model;

public class SmartDataStatus {

	private Integer smartDataStatusId;
	private String statusName;
	private String statusDescription;
	private String type;

	public SmartDataStatus(int smartDataStatusId, String statusName, String type){
		this.smartDataStatusId=smartDataStatusId;
		this.statusName=statusName;
		this.type=type;
	}
	
	/**
	 * @return the statusName
	 */
	public String getStatusName() {
		return statusName;
	}

	/**
	 * @param statusName
	 *            the statusName to set
	 */
	public void setStatusName(final String statusName) {
		this.statusName = statusName;
	}

	/**
	 * @return the statusDescription
	 */
	public String getStatusDescription() {
		return statusDescription;
	}

	/**
	 * @param statusDescription
	 *            the statusDescription to set
	 */
	public void setStatusDescription(final String statusDescription) {
		this.statusDescription = statusDescription;
	}

	/**
	 * @return the smartDataStatusId
	 */
	public Integer getSmartDataStatusId() {
		return smartDataStatusId;
	}

	/**
	 * @param smartDataStatusId
	 *            the smartDataStatusId to set
	 */
	public void setSmartDataStatusId(final Integer smartDataStatusId) {
		this.smartDataStatusId = smartDataStatusId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}

package com.sterlingTS.smartdataComm.model;

import java.math.BigDecimal;

import com.sterlingTS.smartdataComm.model.enums.PIIMatchEnum;

public class PersonalIdentifier {

	private String identifier;
	private BigDecimal weightage;
	private PIIMatchEnum piiMatch;
	
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public BigDecimal getWeightage() {
		return weightage;
	}
	public void setWeightage(BigDecimal weightage) {
		this.weightage = weightage;
	}
	public PIIMatchEnum getPiiMatch() {
		return piiMatch;
	}
	public void setPiiMatch(PIIMatchEnum piiMatch) {
		this.piiMatch = piiMatch;
	}
}

package com.sterlingTS.smartdataComm.model;

import java.util.HashSet;
import java.util.Set;

public class Previleges implements java.io.Serializable {

	private int previlegesId;
	private String previlegeName;
	private String description;
	private Set rolePrevileges = new HashSet(0);

	public Previleges() {
	}

	public Previleges(int previlegesId, String previlegeName) {
		this.previlegesId = previlegesId;
		this.previlegeName = previlegeName;
	}

	public Previleges(int previlegesId, String previlegeName,
			String description, Set rolePrevileges) {
		this.previlegesId = previlegesId;
		this.previlegeName = previlegeName;
		this.description = description;
		this.rolePrevileges = rolePrevileges;
	}

	public int getPrevilegesId() {
		return this.previlegesId;
	}

	public void setPrevilegesId(int previlegesId) {
		this.previlegesId = previlegesId;
	}

	public String getPrevilegeName() {
		return this.previlegeName;
	}

	public void setPrevilegeName(String previlegeName) {
		this.previlegeName = previlegeName;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set getRolePrevileges() {
		return this.rolePrevileges;
	}

	public void setRolePrevileges(Set rolePrevileges) {
		this.rolePrevileges = rolePrevileges;
	}

}

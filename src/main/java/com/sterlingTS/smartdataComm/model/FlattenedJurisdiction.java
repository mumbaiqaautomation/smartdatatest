package com.sterlingTS.smartdataComm.model;

public class FlattenedJurisdiction {

	private Integer jurisdictionId;
	private JurisdictionType jurisdictionType;
	private String jurisdictionTypeName;
	private String countyName;
	private String stateName;
	private String stateA2CD;
	private String country;
	private String qualifiedName;


	

	public Integer getJurisdictionId() {
		return jurisdictionId;
	}




	public void setJurisdictionId(Integer jurisdictionId) {
		this.jurisdictionId = jurisdictionId;
	}




	public JurisdictionType getJurisdictionType() {
		return jurisdictionType;
	}




	public void setJurisdictionType(JurisdictionType jurisdictionType) {
		this.jurisdictionType = jurisdictionType;
	}




	public String getJurisdictionTypeName() {
		return jurisdictionTypeName;
	}




	public void setJurisdictionTypeName(String jurisdictionTypeName) {
		this.jurisdictionTypeName = jurisdictionTypeName;
	}




	public String getCountyName() {
		return countyName;
	}




	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}




	public String getStateName() {
		return stateName;
	}




	public void setStateName(String stateName) {
		this.stateName = stateName;
	}




	public String getStateA2CD() {
		return stateA2CD;
	}




	public void setStateA2CD(String stateA2CD) {
		this.stateA2CD = stateA2CD;
	}




	public String getCountry() {
		return country;
	}




	public void setCountry(String country) {
		this.country = country;
	}




	public String getQualifiedName() {
		return qualifiedName;
	}




	public void setQualifiedName(String qualifiedName) {
		this.qualifiedName = qualifiedName;
	}




	@Override
	public String toString() {
		final String TAG = "> ";
		String retValue = "";

		retValue = "Jurisdiction ( " + super.toString() + " " + "id= <" + getJurisdictionId() + TAG + qualifiedName
				+ " )";
		return retValue;
	}
}

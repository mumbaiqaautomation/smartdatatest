package com.sterlingTS.smartdataComm.model;

public class ReportScoringAuditParam implements java.io.Serializable {

	private int reportScoringAuditParamId;
	private ReportScoringAudit reportScoringAudit;
	private GlobalReportingParam globalReportingParam;
	public int getReportScoringAuditParamId() {
		return reportScoringAuditParamId;
	}
	public void setReportScoringAuditParamId(int reportScoringAuditParamId) {
		this.reportScoringAuditParamId = reportScoringAuditParamId;
	}
	public ReportScoringAudit getReportScoringAudit() {
		return reportScoringAudit;
	}
	public void setReportScoringAudit(ReportScoringAudit reportScoringAudit) {
		this.reportScoringAudit = reportScoringAudit;
	}
	public GlobalReportingParam getGlobalReportingParam() {
		return globalReportingParam;
	}
	public void setGlobalReportingParam(GlobalReportingParam globalReportingParam) {
		this.globalReportingParam = globalReportingParam;
	}

}

package com.sterlingTS.smartdataComm.model;

public class JurisdictionCounty extends Jurisdiction {
	private String countyName;
	private String stateA2Cd;
	private String countryA2Cd;
	private String fipsCode;
	private String qualifiedName;
	private JurisdictionState state;

	public String getCountyName() {
		return this.countyName;
	}

	public void setCountyName(final String countyName) {
		this.countyName = countyName;
	}

	public String getStateA2Cd() {
		return this.stateA2Cd;
	}

	public void setStateA2Cd(final String stateA2Cd) {
		this.stateA2Cd = stateA2Cd;
	}

	public String getCountryA2Cd() {
		return this.countryA2Cd;
	}

	public void setCountryA2Cd(final String countryA2Cd) {
		this.countryA2Cd = countryA2Cd;
	}

	public String getQualifiedName() {
		return qualifiedName;
	}

	public void setQualifiedName(final String qualifiedName) {
		this.qualifiedName = qualifiedName;
	}

	public String getFipsCode() {
		return fipsCode;
	}

	public void setFipsCode(final String fipsCode) {
		this.fipsCode = fipsCode;
	}

	@Override
	public String getQName() {
		return qualifiedName;
	}

	public JurisdictionState getState() {
		return state;
	}

	public void setState(JurisdictionState state) {
		this.state = state;
	}
}

package com.sterlingTS.smartdataComm.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class User implements java.io.Serializable {

	private int userId;
	private String user;
	private Date creationDate;
	private Date modificationDate;
	private Set userRoles = new HashSet(0);

	public User() {
	}

	public User(int userId, String user, Date creationDate,
			Date modificationDate) {
		this.userId = userId;
		this.user = user;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	public User(int userId, String user, Date creationDate,
			Date modificationDate, Set userRoles) {
		this.userId = userId;
		this.user = user;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
		this.userRoles = userRoles;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUser() {
		return this.user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return this.modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	
	public Set getUserRoles() {
		return this.userRoles;
	}

	public void setUserRoles(Set userRoles) {
		this.userRoles = userRoles;
	}
}

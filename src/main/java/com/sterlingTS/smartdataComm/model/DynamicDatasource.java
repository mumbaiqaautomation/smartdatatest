package com.sterlingTS.smartdataComm.model;

import java.util.HashSet;
import java.util.Set;

import com.google.gson.annotations.Expose;

public class DynamicDatasource {

		@Expose(serialize = false)
        private int dataSourceId;
			 @Expose(serialize = false)
        private FulfillmentType fulfillmentType;
        private String datasourceName;
        @Expose(serialize = false)
        private Integer tat;
        @Expose(serialize = false)
        private Integer scopeOfSearch;
        @Expose(serialize = false)
        private Integer dataFrequency;
        @Expose(serialize = false)
        private Set<Jurisdiction> supportedJurisdictions = new HashSet<Jurisdiction>();
        @Expose(serialize = false)
        private Set<DispositionType> dispositionTypes = new HashSet<DispositionType>();
        @Expose(serialize = false)
        private Set<ChargeLevel> chargeLevels = new HashSet<ChargeLevel>();
        @Expose(serialize = false)
        private Set<ProductCategory> productCategories = new HashSet<ProductCategory>();
        @Expose(serialize = false)
        private Set<DataSourceMonitoringAudit> dataSourceMonitoringAudits = new HashSet<DataSourceMonitoringAudit>();
        @Expose(serialize = false)
        private boolean scopeOfSearchCompatible = false;
        @Expose(serialize = false)
        private boolean frequencyCompatible = false;
        @Expose(serialize = false)
        private boolean chargeLevelCompatible = false;
        @Expose(serialize = false)
        private boolean dispositionTypeCompatible = false;
        @Expose(serialize = false)
        private String expectedValue;
        @Expose(serialize = false)
        private String existingValue;
        @Expose(serialize = false)
        private DatasourceStatus datasourceStatus;
        @Expose(serialize = false)
        private boolean ignoreSample = false;
        @Expose(serialize = false)
        private boolean monitoringActive = false;
        @Expose(serialize = false)
        private boolean generateSample = false;
        @Expose(serialize = false)
        private boolean generateAlert = false;

        public DatasourceStatus getDatasourceStatus() {
                        return datasourceStatus;
        }

        public void setDatasourceStatus(final DatasourceStatus datasourceStatus) {
                        this.datasourceStatus = datasourceStatus;
        }

        public boolean isScopeOfSearchCompatible() {
                        return scopeOfSearchCompatible;
        }

        public void setScopeOfSearchCompatible(boolean scopeOfSearchCompatible) {
                        this.scopeOfSearchCompatible = scopeOfSearchCompatible;
        }

        public boolean isFrequencyCompatible() {
                        return frequencyCompatible;
        }

        public void setFrequencyCompatible(boolean frequencyCompatible) {
                        this.frequencyCompatible = frequencyCompatible;
        }

        public boolean isChargeLevelCompatible() {
                        return chargeLevelCompatible;
        }

        public void setChargeLevelCompatible(boolean chargeLevelCompatible) {
                        this.chargeLevelCompatible = chargeLevelCompatible;
        }

        public boolean isDispositionTypeCompatible() {
                        return dispositionTypeCompatible;
        }

        public void setDispositionTypeCompatible(boolean dispositionTypeCompatible) {
                        this.dispositionTypeCompatible = dispositionTypeCompatible;
        }

        public Set<ProductCategory> getProductCategories() {
                        return productCategories;
        }

        public void setProductCategories(Set<ProductCategory> productCategories) {
                        this.productCategories = productCategories;
        }

        public int getDataSourceId() {
                        return dataSourceId;
        }

        public void setDataSourceId(int dataSourceId) {
                        this.dataSourceId = dataSourceId;
        }

        public FulfillmentType getFulfillmentType() {
                        return fulfillmentType;
        }

        public void setFulfillmentType(FulfillmentType fulfillmentType) {
                        this.fulfillmentType = fulfillmentType;
        }

        public String getDatasourceName() {
                        return datasourceName;
        }

        public void setDatasourceName(String datasourceName) {
                        this.datasourceName = datasourceName;
        }

        public Integer getTat() {
                        return tat;
        }

        public void setTat(Integer tat) {
                        this.tat = tat;
        }

        public Integer getScopeOfSearch() {
                        return scopeOfSearch;
        }

        public void setScopeOfSearch(Integer scopeOfSearch) {
                        this.scopeOfSearch = scopeOfSearch;
        }

        public Integer getDataFrequency() {
                        return dataFrequency;
        }

        public void setDataFrequency(Integer dataFrequency) {
                        this.dataFrequency = dataFrequency;
        }

        public Set<Jurisdiction> getSupportedJurisdictions() {
                        return supportedJurisdictions;
        }

        public void setSupportedJurisdictions(Set<Jurisdiction> supportedJurisdictions) {
                        this.supportedJurisdictions = supportedJurisdictions;
        }

        public Set<DispositionType> getDispositionTypes() {
                        return dispositionTypes;
        }

        public void setDispositionTypes(Set<DispositionType> dispositionTypes) {
                        this.dispositionTypes = dispositionTypes;
        }

        public Set<ChargeLevel> getChargeLevels() {
                        return chargeLevels;
        }

        public void setChargeLevels(Set<ChargeLevel> chargeLevels) {
                        this.chargeLevels = chargeLevels;
        }

        public String getExpectedValue() {
                        return expectedValue;
        }

        public void setExpectedValue(String expectedValue) {
                        this.expectedValue = expectedValue;
        }

        public String getExistingValue() {
                        return existingValue;
        }

        public void setExistingValue(String existingValue) {
                        this.existingValue = existingValue;
        }

        public Set<DataSourceMonitoringAudit> getDataSourceMonitoringAudits() {
                        return dataSourceMonitoringAudits;
        }

        public void setDataSourceMonitoringAudits(Set<DataSourceMonitoringAudit> dataSourceMonitoringAudits) {
                        this.dataSourceMonitoringAudits = dataSourceMonitoringAudits;
        }

        @Override
        public String toString() {
                        return "DynamicDatasource [dataSourceId=" + dataSourceId + ", scopeOfSearch=" + scopeOfSearch
                                                        + ", dataFrequency=" + dataFrequency + ", DatasourceStatus=" + datasourceStatus + "]";
        }

        public void addDataSourceMonitoringAudit(PDLRuleConfigurationFact pdlRuleConfigurationFact, boolean compatible) {
                        DataSourceMonitoringAudit dataSourceMonitoringAudit = new DataSourceMonitoringAudit();
                        dataSourceMonitoringAudit
                                                        .setProductCategorySearchParam(pdlRuleConfigurationFact.getProductCategorySearchParam());
                        dataSourceMonitoringAudit.setDynamicDatasource(this);
                        dataSourceMonitoringAudit.setCompatible(compatible);
                        dataSourceMonitoringAudit.setExistingValue(this.getExistingValue());
                        dataSourceMonitoringAudit.setExpectedValue(this.getExpectedValue());
                        this.getDataSourceMonitoringAudits().add(dataSourceMonitoringAudit);
        }

        public Boolean checkDispositionTypesCompatibility(Set<DispositionType> dispositionTypes) {
                        if (this.getDispositionTypes().isEmpty() && dispositionTypes.isEmpty()) {
                                        return true;
                        }
                        if (!this.getDispositionTypes().isEmpty() && !dispositionTypes.isEmpty()) {
                                        if (this.getDispositionTypes().containsAll(dispositionTypes)) {
                                                        return true;
                                        }
                        }

                        return false;
        }

        public Boolean checkChargeLevelsCompatibility(Set<ChargeLevel> chargeLevels) {
                        if (this.getChargeLevels().isEmpty() && chargeLevels.isEmpty()) {
                                        return true;
                        }
                        if (!this.getChargeLevels().isEmpty() && !chargeLevels.isEmpty()) {

                                        if (this.getChargeLevels().containsAll(chargeLevels)) {
                                                        return true;
                                        }
                        }
                        return false;
        }

        public String unWrapChargeLevels(Set<ChargeLevel> chargeLevels) {
                        StringBuilder chargeLevelString = new StringBuilder();
                        int count = 1;

                        if (!this.getChargeLevels().isEmpty()) {
                                        chargeLevelString.append("[");
                                        for (ChargeLevel chargeLevel : this.getChargeLevels()) {
                                                        if (chargeLevel != null) {

                                                                        chargeLevelString.append(chargeLevel.getLevel());
                                                                        if (count != this.getChargeLevels().size()) {
                                                                                        chargeLevelString.append(",");

                                                                        }

                                                                        count++;
                                                        }
                                        }
                                        return chargeLevelString.append("]").toString();
                        }
                        return "";
        }

        public String unWrapDispositionTypes(Set<DispositionType> dispositionTypes) {
                        StringBuilder dispositionTypesString = new StringBuilder();
                        int count = 1;

                        if (!this.getDispositionTypes().isEmpty()) {
                                        dispositionTypesString.append("[");
                                        for (DispositionType dispositionType : this.getDispositionTypes()) {
                                                        if (dispositionType != null) {

                                                                        dispositionTypesString.append(dispositionType.getDispositionType());
                                                                        if (count != this.getDispositionTypes().size()) {
                                                                                        dispositionTypesString.append(",");

                                                                        }

                                                                        count++;
                                                        }
                                        }
                                        return dispositionTypesString.append("]").toString();
                        }
                        return "";
        }

        @Override
        public int hashCode() {
                        final int prime = 31;
                        int result = 1;
                        result = prime * result + ((dataFrequency == null) ? 0 : dataFrequency.hashCode());
                        result = prime * result + dataSourceId;
                        result = prime * result + ((datasourceName == null) ? 0 : datasourceName.hashCode());
                        result = prime * result + ((datasourceStatus == null) ? 0 : datasourceStatus.hashCode());
                        result = prime * result + ((scopeOfSearch == null) ? 0 : scopeOfSearch.hashCode());
                        result = prime * result + ((tat == null) ? 0 : tat.hashCode());
                        return result;
        }

        @Override
        public boolean equals(Object obj) {
                        if (this == obj)
                                        return true;
                        if (obj == null)
                                        return false;
                        if (getClass() != obj.getClass())
                                        return false;
                        DynamicDatasource other = (DynamicDatasource) obj;
                        if (dataFrequency == null) {
                                        if (other.dataFrequency != null)
                                                        return false;
                        } else if (!dataFrequency.equals(other.dataFrequency))
                                        return false;
                        if (dataSourceId != other.dataSourceId)
                                        return false;
                        if (datasourceName == null) {
                                        if (other.datasourceName != null)
                                                        return false;
                        } else if (!datasourceName.equals(other.datasourceName))
                                        return false;
                        if (datasourceStatus == null) {
                                        if (other.datasourceStatus != null)
                                                        return false;
                        } else if (!datasourceStatus.equals(other.datasourceStatus))
                                        return false;
                        if (scopeOfSearch == null) {
                                        if (other.scopeOfSearch != null)
                                                        return false;
                        } else if (!scopeOfSearch.equals(other.scopeOfSearch))
                                        return false;
                        if (tat == null) {
                                        if (other.tat != null)
                                                        return false;
                        } else if (!tat.equals(other.tat))
                                        return false;
                        return true;
        }

        public boolean isIgnoreSample() {
                        return ignoreSample;
        }

        public void setIgnoreSample(boolean ignoreSample) {
                        this.ignoreSample = ignoreSample;
        }

        public boolean isMonitoringActive() {
                        return monitoringActive;
        }

        public void setMonitoringActive(boolean monitoringActive) {
                        this.monitoringActive = monitoringActive;
        }

        public boolean isGenerateSample() {
                        return generateSample;
        }

        public void setGenerateSample(boolean generateSample) {
                        this.generateSample = generateSample;
        }

        public boolean isGenerateAlert() {
                        return generateAlert;
        }

        public void setGenerateAlert(boolean generateAlert) {
                        this.generateAlert = generateAlert;
        }

}

package com.sterlingTS.smartdataComm.model;

public class DatasourceStatus {

	private Integer DatasourceStatusId;
	private String statusName;
	private String statusDescription;
	private String type;

	public DatasourceStatus() {

	}

	public DatasourceStatus(Integer DatasourceStatusId, String statusName, String type, String statusDescription) {
		this.DatasourceStatusId = DatasourceStatusId;
		this.statusName = statusName;
		this.type = type;
		this.statusDescription = statusDescription;
	}

	/**
	 * @return the statusName
	 */
	public String getStatusName() {
		return statusName;
	}

	/**
	 * @param statusName
	 *            the statusName to set
	 */
	public void setStatusName(final String statusName) {
		this.statusName = statusName;
	}

	/**
	 * @return the statusDescription
	 */
	public String getStatusDescription() {
		return statusDescription;
	}

	/**
	 * @param statusDescription
	 *            the statusDescription to set
	 */
	public void setStatusDescription(final String statusDescription) {
		this.statusDescription = statusDescription;
	}

	/**
	 * @return the smartDataStatusId
	 */
	public Integer getDatasourceStatusId() {
		return DatasourceStatusId;
	}

	/**
	 * @param smartDataStatusId
	 *            the smartDataStatusId to set
	 */
	public void setDatasourceStatusId(final Integer DatasourceStatusId) {
		this.DatasourceStatusId = DatasourceStatusId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((statusName == null) ? 0 : statusName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DatasourceStatus other = (DatasourceStatus) obj;
		if (statusName == null) {
			if (other.statusName != null)
				return false;
		} else if (!statusName.equals(other.statusName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DatasourceStatus [statusName=" + statusName + ", type=" + type + "]";
	}
}

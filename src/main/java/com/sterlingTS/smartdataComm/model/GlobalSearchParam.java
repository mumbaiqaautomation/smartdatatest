package com.sterlingTS.smartdataComm.model;

public class GlobalSearchParam implements java.io.Serializable {

	private int globalSearchParameterId;
	private String param;
	private String description;
	private boolean isConfigurable;
	private String operator;
	
	
	public int getGlobalSearchParameterId() {
		return this.globalSearchParameterId;
	}

	public void setGlobalSearchParameterId(int globalSearchParameterId) {
		this.globalSearchParameterId = globalSearchParameterId;
	}

	public String getParam() {
		return this.param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean getIsConfigurable() {
		return this.isConfigurable;
	}

	public void setIsConfigurable(boolean isConfigurable) {
		this.isConfigurable = isConfigurable;
	}

	public String getOperator() {
		return this.operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}


}

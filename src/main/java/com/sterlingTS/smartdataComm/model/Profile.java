package com.sterlingTS.smartdataComm.model;

import com.google.gson.annotations.Expose;

public class Profile {

	@Expose(serialize = false)
	private int profileId;
	@Expose(serialize = false)
	private Account account;
	private String profileName;
	@Expose(serialize = false)
	private String description;

	/**
	 * @return the profileId
	 */
	public int getProfileId() {
		return profileId;
	}

	/**
	 * @param profileId
	 *            the profileId to set
	 */
	public void setProfileId(final int profileId) {
		this.profileId = profileId;
	}

	/**
	 * @return the account
	 */
	public Account getAccount() {
		return account;
	}

	/**
	 * @param account
	 *            the account to set
	 */
	public void setAccount(final Account account) {
		this.account = account;
	}

	/**
	 * @return the profileName
	 */
	public String getProfileName() {
		return profileName;
	}

	/**
	 * @param profileName
	 *            the profileName to set
	 */
	public void setProfileName(final String profileName) {
		this.profileName = profileName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(final String description) {
		this.description = description;
	}
}

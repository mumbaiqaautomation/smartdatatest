package com.sterlingTS.smartdataComm.model;

public class RolePrevilege implements java.io.Serializable {

	private int rolePrevilegeId;
	private Role role;
	private Previleges previleges;

	public RolePrevilege() {
	}

	public RolePrevilege(int rolePrevilegeId, Role role, Previleges previleges) {
		this.rolePrevilegeId = rolePrevilegeId;
		this.role = role;
		this.previleges = previleges;
	}

	public int getRolePrevilegeId() {
		return this.rolePrevilegeId;
	}

	public void setRolePrevilegeId(int rolePrevilegeId) {
		this.rolePrevilegeId = rolePrevilegeId;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Previleges getPrevileges() {
		return this.previleges;
	}

	public void setPrevileges(Previleges previleges) {
		this.previleges = previleges;
	}
}

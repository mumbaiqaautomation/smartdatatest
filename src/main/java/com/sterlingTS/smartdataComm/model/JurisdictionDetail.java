package com.sterlingTS.smartdataComm.model;

public class JurisdictionDetail {
	private String countyName;
	private String stateA2Cd;
	private String countryA2Cd;
	private JurisdictionType type;
	private String federalDistrictName;
	private String districtName;
	public String getCountyName() {
		return countyName;
	}
	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}
	public String getStateA2Cd() {
		return stateA2Cd;
	}
	public void setStateA2Cd(String stateA2Cd) {
		this.stateA2Cd = stateA2Cd;
	}
	public String getCountryA2Cd() {
		return countryA2Cd;
	}
	public void setCountryA2Cd(String countryA2Cd) {
		this.countryA2Cd = countryA2Cd;
	}
	public JurisdictionType getType() {
		return type;
	}
	public void setType(JurisdictionType type) {
		this.type = type;
	}
	public String getFederalDistrictName() {
		return federalDistrictName;
	}
	public void setFederalDistrictName(String federalDistrictName) {
		this.federalDistrictName = federalDistrictName;
	}
	public String getDistrictName() {
		return districtName;
	}
	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}
}

package com.sterlingTS.smartdataComm.model;

public class SentenceMapping implements java.io.Serializable {

	private String sentenceMappingId;
	private SentenceType sentenceType;
	private Jurisdiction jurisdiction;
	public String getSentenceMappingId() {
		return sentenceMappingId;
	}
	public void setSentenceMappingId(String sentenceMappingId) {
		this.sentenceMappingId = sentenceMappingId;
	}
	public SentenceType getSentenceType() {
		return sentenceType;
	}
	public void setSentenceType(SentenceType sentenceType) {
		this.sentenceType = sentenceType;
	}
	public Jurisdiction getJurisdiction() {
		return jurisdiction;
	}
	public void setJurisdiction(Jurisdiction jurisdiction) {
		this.jurisdiction = jurisdiction;
	}
}

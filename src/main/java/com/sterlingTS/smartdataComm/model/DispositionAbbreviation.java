package com.sterlingTS.smartdataComm.model;

import java.util.HashSet;
import java.util.Set;

public class DispositionAbbreviation implements java.io.Serializable {

	private int dispositionAbbreviationId;
	private DispositionDescription dispositionDescription;
	private String abbreviation;
	private Set<DispositionMapping> dispositionMappings = new HashSet<DispositionMapping>(0);

	

	public int getDispositionAbbreviationId() {
		return this.dispositionAbbreviationId;
	}

	public void setDispositionAbbreviationId(int dispositionAbbreviationId) {
		this.dispositionAbbreviationId = dispositionAbbreviationId;
	}

	public String getAbbreviation() {
		return this.abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public DispositionDescription getDispositionDescription() {
		return dispositionDescription;
	}

	public void setDispositionDescription(
			DispositionDescription dispositionDescription) {
		this.dispositionDescription = dispositionDescription;
	}

	public Set<DispositionMapping> getDispositionMappings() {
		return dispositionMappings;
	}

	public void setDispositionMappings(Set<DispositionMapping> dispositionMappings) {
		this.dispositionMappings = dispositionMappings;
	}

}

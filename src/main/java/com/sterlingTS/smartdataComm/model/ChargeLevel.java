package com.sterlingTS.smartdataComm.model;

import com.google.gson.annotations.Expose;

public class ChargeLevel {

	@Expose(serialize = false)
	private int chargeLevelId;
	private String level;
	private String description;

	/**
	 * @return the chargeLevelId
	 */
	public int getChargeLevelId() {
		return chargeLevelId;
	}

	/**
	 * @param chargeLevelId
	 *            the chargeLevelId to set
	 */
	public void setChargeLevelId(final int chargeLevelId) {
		this.chargeLevelId = chargeLevelId;
	}

	/**
	 * @return the level
	 */
	public String getLevel() {
		return level;
	}

	/**
	 * @param level
	 *            the level to set
	 */
	public void setLevel(final String level) {
		this.level = level;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}


	public void setDescription(final String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((level == null) ? 0 : level.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ChargeLevel other = (ChargeLevel) obj;
		if (level == null) {
			if (other.level != null) {
				return false;
			}
		} else if (!level.equals(other.level)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ChargeLevel [chargeLevelId=" + chargeLevelId + ", level="
				+ level + ", description=" + description + "]";
	}
}

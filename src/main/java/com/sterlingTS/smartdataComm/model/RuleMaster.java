package com.sterlingTS.smartdataComm.model;

import java.util.Date;

public class RuleMaster implements java.io.Serializable {

	private int ruleTypeId;
	private RuleType ruleType;
	private String ruleName;
	private String description;
	private String template;
	private Date creationDate;
	private Date modificationDate;
	private int ruleMasterId;
	
	public int getRuleTypeId() {
		return ruleTypeId;
	}
	public void setRuleTypeId(int ruleTypeId) {
		this.ruleTypeId = ruleTypeId;
	}
	public RuleType getRuleType() {
		return ruleType;
	}
	public void setRuleType(RuleType ruleType) {
		this.ruleType = ruleType;
	}
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getModificationDate() {
		return modificationDate;
	}
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}
	public int getRuleMasterId() {
		return ruleMasterId;
	}
	public void setRuleMasterId(int ruleMasterId) {
		this.ruleMasterId = ruleMasterId;
	}	
}

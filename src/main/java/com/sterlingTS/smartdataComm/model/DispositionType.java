package com.sterlingTS.smartdataComm.model;

import com.google.gson.annotations.Expose;

public class DispositionType {

	@Expose(serialize = false)
	private int dispositionTypeId;
	private String dispositionType;
	private String description;

	/**
	 * @return the dispositionTypeId
	 */
	public int getDispositionTypeId() {
		return dispositionTypeId;
	}

	/**
	 * @param dispositionTypeId
	 *            the dispositionTypeId to set
	 */
	public void setDispositionTypeId(final int dispositionTypeId) {
		this.dispositionTypeId = dispositionTypeId;
	}

	/**
	 * @return the disposition
	 */
	public String getDispositionType() {
		return dispositionType;
	}

	/**
	 * @param disposition
	 *            the disposition to set
	 */
	public void setDispositionType(final String dispositionType) {
		this.dispositionType = dispositionType;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dispositionType == null) ? 0 : dispositionType.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final DispositionType other = (DispositionType) obj;
		if (dispositionType == null) {
			if (other.dispositionType != null) {
				return false;
			}
		} else if (!dispositionType.equals(other.dispositionType)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "DispositionType [dispositionTypeId=" + dispositionTypeId
				+ ", dispositionType=" + dispositionType + ", description="
				+ description + "]";
	}
}

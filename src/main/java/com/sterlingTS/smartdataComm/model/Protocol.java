package com.sterlingTS.smartdataComm.model;

public class Protocol {

	private int protocolId;
	private String protocolType;

	public int getProtocolId() {
		return protocolId;
	}

	public void setProtocolId(final int protocolId) {
		this.protocolId = protocolId;
	}

	public String getProtocolType() {
		return protocolType;
	}

	public void setProtocolType(final String protocolType) {
		this.protocolType = protocolType;
	}
}

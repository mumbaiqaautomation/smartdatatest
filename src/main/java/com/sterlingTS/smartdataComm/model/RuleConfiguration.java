package com.sterlingTS.smartdataComm.model;

import java.util.HashSet;
import java.util.Set;

public class RuleConfiguration implements java.io.Serializable {

	private int ruleConfigurationId;
	private RuleSet ruleSet;
	private RuleMaster ruleMaster;
	private Integer ageOfRecord;
	private Integer salaryLimit;
	private Integer ageOfSentence;
	private Boolean activeWarrant;
	private Integer ageOfWarrant;
	private Boolean probationActive;
	private String caseType;
	private Integer ageOnOffenseDate;
	private Integer currentAge;
	private DispositionType dispositionType;
	
	
	private Set<DispositionDescription> dispositionDescriptions = new HashSet<DispositionDescription>();
	private Set<ChargeLevel> chargeLevels = new HashSet<ChargeLevel>();
	private Set<JurisdictionCounty> counties = new HashSet<JurisdictionCounty>();
	private Set<ChargeDescription> chargeDescriptions = new HashSet<ChargeDescription>();
	
	
	
	public int getRuleConfigurationId() {
		return ruleConfigurationId;
	}
	public void setRuleConfigurationId(int ruleConfigurationId) {
		this.ruleConfigurationId = ruleConfigurationId;
	}
	public RuleSet getRuleSet() {
		return ruleSet;
	}
	public void setRuleSet(RuleSet ruleSet) {
		this.ruleSet = ruleSet;
	}
	public RuleMaster getRuleMaster() {
		return ruleMaster;
	}
	public void setRuleMaster(RuleMaster ruleMaster) {
		this.ruleMaster = ruleMaster;
	}
	public Integer getAgeOfRecord() {
		return ageOfRecord;
	}
	public void setAgeOfRecord(Integer ageOfRecord) {
		this.ageOfRecord = ageOfRecord;
	}
	public Integer getSalaryLimit() {
		return salaryLimit;
	}
	public void setSalaryLimit(Integer salaryLimit) {
		this.salaryLimit = salaryLimit;
	}
	public Integer getAgeOfSentence() {
		return ageOfSentence;
	}
	public void setAgeOfSentence(Integer ageOfSentence) {
		this.ageOfSentence = ageOfSentence;
	}
	public Boolean getActiveWarrant() {
		return activeWarrant;
	}
	public void setActiveWarrant(Boolean activeWarrant) {
		this.activeWarrant = activeWarrant;
	}
	public Integer getAgeOfWarrant() {
		return ageOfWarrant;
	}
	public void setAgeOfWarrant(Integer ageOfWarrant) {
		this.ageOfWarrant = ageOfWarrant;
	}
	public Boolean getProbationActive() {
		return probationActive;
	}
	public void setProbationActive(Boolean probationActive) {
		this.probationActive = probationActive;
	}
	public String getCaseType() {
		return caseType;
	}
	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}
	public Integer getAgeOnOffenseDate() {
		return ageOnOffenseDate;
	}
	public void setAgeOnOffenseDate(Integer ageOnOffenseDate) {
		this.ageOnOffenseDate = ageOnOffenseDate;
	}
	public Integer getCurrentAge() {
		return currentAge;
	}
	public void setCurrentAge(Integer currentAge) {
		this.currentAge = currentAge;
	}
	public DispositionType getDispositionType() {
		return dispositionType;
	}
	public void setDispositionType(DispositionType dispositionType) {
		this.dispositionType = dispositionType;
	}
	
	public Set<DispositionDescription> getDispositionDescriptions() {
		return dispositionDescriptions;
	}
	public void setDispositionDescriptions(
			Set<DispositionDescription> dispositionDescriptions) {
		this.dispositionDescriptions = dispositionDescriptions;
	}
	public Set<ChargeLevel> getChargeLevels() {
		return chargeLevels;
	}
	public void setChargeLevels(Set<ChargeLevel> chargeLevels) {
		this.chargeLevels = chargeLevels;
	}
	public Set<JurisdictionCounty> getCounties() {
		return counties;
	}
	public void setCounties(Set<JurisdictionCounty> counties) {
		this.counties = counties;
	}
	public Set<ChargeDescription> getChargeDescriptions() {
		return chargeDescriptions;
	}
	public void setChargeDescriptions(Set<ChargeDescription> chargeDescriptions) {
		this.chargeDescriptions = chargeDescriptions;
	}
}

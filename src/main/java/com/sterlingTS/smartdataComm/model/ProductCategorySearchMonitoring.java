package com.sterlingTS.smartdataComm.model;

import java.util.HashSet;
import java.util.Set;

public class ProductCategorySearchMonitoring implements java.io.Serializable {

	private int productCategorySearchMonitoringId;
	private ProductCategoryJurisdictionDatasource productCategoryJurisdictionDatasource;
	private Boolean complaint;
	private Boolean deviationAcceptable;
	private String deviationComment;
	private Set<ProductCategorySearchMonitoringParam> productCategorySearchMonitoringParams = new HashSet<ProductCategorySearchMonitoringParam>(0);
	public int getProductCategorySearchMonitoringId() {
		return productCategorySearchMonitoringId;
	}
	public void setProductCategorySearchMonitoringId(
			int productCategorySearchMonitoringId) {
		this.productCategorySearchMonitoringId = productCategorySearchMonitoringId;
	}
	public ProductCategoryJurisdictionDatasource getProductCategoryJurisdictionDatasource() {
		return productCategoryJurisdictionDatasource;
	}
	public void setProductCategoryJurisdictionDatasource(
			ProductCategoryJurisdictionDatasource productCategoryJurisdictionDatasource) {
		this.productCategoryJurisdictionDatasource = productCategoryJurisdictionDatasource;
	}
	public Boolean getComplaint() {
		return complaint;
	}
	public void setComplaint(Boolean complaint) {
		this.complaint = complaint;
	}
	public Boolean getDeviationAcceptable() {
		return deviationAcceptable;
	}
	public void setDeviationAcceptable(Boolean deviationAcceptable) {
		this.deviationAcceptable = deviationAcceptable;
	}
	public String getDeviationComment() {
		return deviationComment;
	}
	public void setDeviationComment(String deviationComment) {
		this.deviationComment = deviationComment;
	}
	public Set<ProductCategorySearchMonitoringParam> getProductCategorySearchMonitoringParams() {
		return productCategorySearchMonitoringParams;
	}
	public void setProductCategorySearchMonitoringParams(
			Set<ProductCategorySearchMonitoringParam> productCategorySearchMonitoringParams) {
		this.productCategorySearchMonitoringParams = productCategorySearchMonitoringParams;
	}

}

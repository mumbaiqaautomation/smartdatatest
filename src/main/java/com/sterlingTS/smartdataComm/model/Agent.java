package com.sterlingTS.smartdataComm.model;

public class Agent {
	private int agentId;
	private String agentName;
	private String defaultResponseAddress;
	private String defaultInfoNeededAddress;
	private Protocol protocol;

	/**
	 * @return the agentId
	 */
	public int getAgentId() {
		return agentId;
	}

	/**
	 * @param agentId
	 *            the agentId to set
	 */
	public void setAgentId(final int agentId) {
		this.agentId = agentId;
	}

	/**
	 * @return the agentName
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * @param agentName
	 *            the agentName to set
	 */
	public void setAgentName(final String agentName) {
		this.agentName = agentName;
	}

	/**
	 * @return the defaultResponseAddress
	 */
	public String getDefaultResponseAddress() {
		return defaultResponseAddress;
	}

	/**
	 * @param defaultResponseAddress
	 *            the defaultResponseAddress to set
	 */
	public void setDefaultResponseAddress(final String defaultResponseAddress) {
		this.defaultResponseAddress = defaultResponseAddress;
	}

	/**
	 * @return the defaultInfoNeededAddress
	 */
	public String getDefaultInfoNeededAddress() {
		return defaultInfoNeededAddress;
	}

	/**
	 * @param defaultInfoNeededAddress
	 *            the defaultInfoNeededAddress to set
	 */
	public void setDefaultInfoNeededAddress(final String defaultInfoNeededAddress) {
		this.defaultInfoNeededAddress = defaultInfoNeededAddress;
	}

	/**
	 * @return the protocol
	 */
	public Protocol getProtocol() {
		return protocol;
	}

	/**
	 * @param protocol
	 *            the protocol to set
	 */
	public void setProtocol(final Protocol protocol) {
		this.protocol = protocol;
	}

	@Override
	public String toString() {
		return "Agent [agentId=" + agentId + ", agentName=" + agentName
				+ ", defaultResponseAddress=" + defaultResponseAddress
				+ ", defaultInfoNeededAddress=" + defaultInfoNeededAddress
				+ ", protocol=" + protocol + "]";
	}
}

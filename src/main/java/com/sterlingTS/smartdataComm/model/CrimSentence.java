package com.sterlingTS.smartdataComm.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.google.gson.annotations.Expose;
import com.sterlingTS.smartdataComm.model.enums.SentenceSanitizationLevelEnum;

public class CrimSentence {

	private int crimSentenceId;
	private String systemIdentifier;
	private String sentenceInfo;
	private SentenceType actualSentenceType;
	private BigDecimal fine;
	private String paymentType;
	private String sentenceOtherInfo;
	private String sentenceLengthOther;
	private Integer sentenceLengthHours;
	private Integer sentenceLengthDays;
	private Integer sentenceLengthWeeks;
	private Integer sentenceLengthMonths;
	private Integer sentenceLengthYears;
	private Boolean consecutive;
	private Boolean suspended;
	private Integer suspendedLengthHours;
	private Integer suspendedLengthDays;
	private Integer suspendedLengthWeeks;
	private Integer suspendedLengthMonths;
	private Integer suspendedLengthYears;
	private BigDecimal suspendedFine;
	@Expose(serialize = false)
	private SentenceType sanitizedSenteceType;
	@Expose(serialize = false)
	private CrimCharge crimCharge;
	@Expose(serialize = false)
	private Set<ProductCategoryReportingParam> missingGlobalReportingParams = new HashSet<ProductCategoryReportingParam>();
	@Expose(serialize = false)
	private Boolean isScoreComplete = true;
	@Expose(serialize = false)
	private SentenceSanitizationLevelEnum sentenceSanitizationLevel;
	private ArrayList<String> additionalItems;
	@Expose(serialize = false)
	private boolean slgMissing;



	public BigDecimal getFine() {
		return fine;
	}

	public void setFine(final BigDecimal fine) {
		this.fine = fine;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(final String paymentType) {
		this.paymentType = paymentType;
	}

	public String getSentenceOtherInfo() {
		return sentenceOtherInfo;
	}

	public void setSentenceOtherInfo(final String sentenceOtherInfo) {
		this.sentenceOtherInfo = sentenceOtherInfo;
	}

	public int getCrimSentenceId() {
		return crimSentenceId;
	}

	public void setCrimSentenceId(final int crimSentenceId) {
		this.crimSentenceId = crimSentenceId;
	}

	public String getSentenceLengthOther() {
		return sentenceLengthOther;
	}

	public void setSentenceLengthOther(final String sentenceLengthOther) {
		this.sentenceLengthOther = sentenceLengthOther;
	}

	public Integer getSentenceLengthHours() {
		return sentenceLengthHours;
	}

	public void setSentenceLengthHours(final Integer sentenceLengthHours) {
		this.sentenceLengthHours = sentenceLengthHours;
	}

	public Integer getSentenceLengthDays() {
		return sentenceLengthDays;
	}

	public void setSentenceLengthDays(final Integer sentenceLengthDays) {
		this.sentenceLengthDays = sentenceLengthDays;
	}

	public Integer getSentenceLengthWeeks() {
		return sentenceLengthWeeks;
	}

	public void setSentenceLengthWeeks(final Integer sentenceLengthWeeks) {
		this.sentenceLengthWeeks = sentenceLengthWeeks;
	}

	public Integer getSentenceLengthMonths() {
		return sentenceLengthMonths;
	}

	public void setSentenceLengthMonths(final Integer sentenceLengthMonths) {
		this.sentenceLengthMonths = sentenceLengthMonths;
	}

	public Integer getSentenceLengthYears() {
		return sentenceLengthYears;
	}

	public void setSentenceLengthYears(final Integer sentenceLengthYears) {
		this.sentenceLengthYears = sentenceLengthYears;
	}

	public Boolean getConsecutive() {
		return consecutive;
	}

	public void setConsecutive(final Boolean consecutive) {
		this.consecutive = consecutive;
	}

	public Boolean getSuspended() {
		return suspended;
	}

	public void setSuspended(final Boolean suspended) {
		this.suspended = suspended;
	}

	public Integer getSuspendedLengthHours() {
		return suspendedLengthHours;
	}

	public void setSuspendedLengthHours(final Integer suspendedLengthHours) {
		this.suspendedLengthHours = suspendedLengthHours;
	}

	public Integer getSuspendedLengthDays() {
		return suspendedLengthDays;
	}

	public void setSuspendedLengthDays(final Integer suspendedLengthDays) {
		this.suspendedLengthDays = suspendedLengthDays;
	}

	public Integer getSuspendedLengthWeeks() {
		return suspendedLengthWeeks;
	}

	public void setSuspendedLengthWeeks(final Integer suspendedLengthWeeks) {
		this.suspendedLengthWeeks = suspendedLengthWeeks;
	}

	public Integer getSuspendedLengthMonths() {
		return suspendedLengthMonths;
	}

	public void setSuspendedLengthMonths(final Integer suspendedLengthMonths) {
		this.suspendedLengthMonths = suspendedLengthMonths;
	}

	public Integer getSuspendedLengthYears() {
		return suspendedLengthYears;
	}

	public void setSuspendedLengthYears(final Integer suspendedLengthYears) {
		this.suspendedLengthYears = suspendedLengthYears;
	}

	public BigDecimal getSuspendedFine() {
		return suspendedFine;
	}

	public void setSuspendedFine(final BigDecimal suspendedFine) {
		this.suspendedFine = suspendedFine;
	}

	public SentenceType getSanitizedSenteceType() {
		return sanitizedSenteceType;
	}

	public void setSanitizedSenteceType(final SentenceType sanitizedSenteceType) {
		this.sanitizedSenteceType = sanitizedSenteceType;
	}

	public CrimCharge getCrimCharge() {
		return crimCharge;
	}

	public void setCrimCharge(final CrimCharge crimCharge) {
		this.crimCharge = crimCharge;
	}

	@Override
	public String toString() {
		return "CrimSentence [crimSentenceId=" + crimSentenceId
				+ ", actualSentenceType=" + actualSentenceType
				+ ", sanitizedSenteceType=" + sanitizedSenteceType + "]";
	}

	public String getSentenceInfo() {
		return sentenceInfo;
	}

	public void setSentenceInfo(String sentenceInfo) {
		this.sentenceInfo = sentenceInfo;
	}

	public String getSystemIdentifier() {
		return systemIdentifier;
	}

	public void setSystemIdentifier(String systemIdentifier) {
		this.systemIdentifier = systemIdentifier;
	}

	public SentenceType getActualSentenceType() {
		return actualSentenceType;
	}

	public void setActualSentenceType(SentenceType actualSentenceType) {
		this.actualSentenceType = actualSentenceType;
	}

	public Set<ProductCategoryReportingParam> getMissingGlobalReportingParams() {
		return missingGlobalReportingParams;
	}

	public void setMissingGlobalReportingParams(
			Set<ProductCategoryReportingParam> missingGlobalReportingParams) {
		this.missingGlobalReportingParams = missingGlobalReportingParams;
	}

	public Boolean getIsScoreComplete() {
		return isScoreComplete;
	}

	public void setIsScoreComplete(Boolean isScoreComplete) {
		this.isScoreComplete = isScoreComplete;
	}

	public SentenceSanitizationLevelEnum getSentenceSanitizationLevel() {
		return sentenceSanitizationLevel;
	}

	public void setSentenceSanitizationLevel(
			SentenceSanitizationLevelEnum sentenceSanitizationLevel) {
		this.sentenceSanitizationLevel = sentenceSanitizationLevel;
	}

	public ArrayList<String> getAdditionalItems() {
		return additionalItems;
	}

	public void setAdditionalItems(ArrayList<String> additionalItems) {
		this.additionalItems = additionalItems;
	}

	public boolean isSlgMissing() {
		return slgMissing;
	}

	public void setSlgMissing(boolean slgMissing) {
		this.slgMissing = slgMissing;
	}

	@Override
	public int hashCode() {
		return systemIdentifier.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CrimSentence other = (CrimSentence) obj;
		if (crimCharge == null) {
			if (other.crimCharge != null)
				return false;
		} else if (!crimCharge.equals(other.crimCharge))
			return false;
		if (!systemIdentifier.equals(other.systemIdentifier))
			return false;
		return true;
	}

}

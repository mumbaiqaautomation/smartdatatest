package com.sterlingTS.smartdataComm.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gson.annotations.Expose;
import com.sterlingTS.smartdataComm.model.enums.ChargeSanitizationLevelEnum;
import com.sterlingTS.smartdataComm.model.enums.DispositionSanitizationLevelEnum;
import com.sterlingTS.smartdataComm.model.enums.RuleDecisionEnum;

public class CrimCharge {

	private int crimChargeId;
	private String systemIdentifier;
	@Expose(serialize = false)
	private Boolean isScoreComplete;
	private String actualChargeDescription;
	private ChargeLevel actualChargeLevel;
	private DispositionType actualDispositionType;
	private Date offenseDate;
	private Date dispositionDate;
	private Date arrestDate;
	private Date convictionDate;
	private String statuteCode;
	@Expose(serialize = false)
	private ChargeCategory chargeCategory;
	@Expose(serialize = false)
	private Boolean isActiveCase;
	@Expose(serialize = false)
	private Boolean isDispositionDeffered;
	@Expose(serialize = false)
	private int ageOfRecord;
	private Boolean isProbationActive=false;
	@Expose(serialize = false)
	private int ageOfSentence;
	private int ageofSentenceProbation;
	
	private int ageOfWarrant;

	public int getAgeOfWarrant() {
		return ageOfWarrant;
	}

	public void setAgeOfWarrant(int ageOfWarrant) {
		this.ageOfWarrant = ageOfWarrant;
	}

	private SalaryRange salaryRange;
	@Expose(serialize = false)
	private ChargeLevel sanitizedChargeLevel;
	@Expose(serialize = false)
	private DispositionType sanitizedDispositionType;
	@Expose(serialize = false)
	private ChargeDescription saniztizedChargeDescription;
	@Expose(serialize = false)
	private CrimCase crimCase;
	@Expose(serialize = false)
	private List<ProductCategoryReportingParam> missingGlobalReportingParams;
	@Expose(serialize = false)
	private RuleDecisionEnum ruleDecision;
	private String actualDispositionDescription;
	@Expose(serialize = false)
	private DispositionDescription sanitizedDispositionDescription;
	private Boolean isReportedEarlier;
	private Set<CrimSentence> crimSentences = new HashSet<CrimSentence>();
	@Expose(serialize = false)
	private ChargeSanitizationLevelEnum chargeSanitizationLevel;
	@Expose(serialize = false)
	private DispositionSanitizationLevelEnum dispositionSanitizationLevel;
	private Date fileDate;
	private String warrantDate;
	private Date jailDate;
	private ArrayList<String> additionalItems;
	@Expose(serialize = false)
	private Set<ChargeRuleHistory> chargeRuleHistory = new HashSet<ChargeRuleHistory>();
	@Expose(serialize = false)
	private boolean slgMissing;
	private Date nextCourtDate;
	private boolean sentenceEmptyForConvictionCharge;
	private boolean isInScope;

   
	public Boolean getIsReportedEarlier() {
		return isReportedEarlier;
	}

	public void setIsReportedEarlier(final Boolean isReportedEarlier) {
		this.isReportedEarlier = isReportedEarlier;
	}

	public int getCrimChargeId() {
		return crimChargeId;
	}

	public void setCrimChargeId(final int crimChargeId) {
		this.crimChargeId = crimChargeId;
	}

	public String getActualChargeDescription() {
		return actualChargeDescription;
	}

	public void setActualChargeDescription(final String actualChargeDescription) {
		this.actualChargeDescription = actualChargeDescription;
	}

	public ChargeLevel getActualChargeLevel() {
		return actualChargeLevel;
	}

	public void setActualChargeLevel(final ChargeLevel actualChargeLevel) {
		this.actualChargeLevel = actualChargeLevel;
	}

	public DispositionType getActualDispositionType() {
		return actualDispositionType;
	}

	public void setActualDispositionType(final DispositionType actualDispositionType) {
		this.actualDispositionType = actualDispositionType;
	}

	public Date getOffenseDate() {
		return offenseDate;
	}

	public void setOffenseDate(final Date offenseDate) {
		this.offenseDate = offenseDate;
	}

	public Date getDispositionDate() {
		return dispositionDate;
	}

	public void setDispositionDate(final Date dispositionDate) {
		this.dispositionDate = dispositionDate;
	}

	public Date getArrestDate() {
		return arrestDate;
	}

	public void setArrestDate(final Date arrestDate) {
		this.arrestDate = arrestDate;
	}

	public Date getConvictionDate() {
		return convictionDate;
	}

	public void setConvictionDate(final Date convictionDate) {
		this.convictionDate = convictionDate;
	}

	public String getStatuteCode() {
		return statuteCode;
	}

	public void setStatuteCode(final String statuteCode) {
		this.statuteCode = statuteCode;
	}

	public ChargeCategory getChargeCategory() {
		return chargeCategory;
	}

	public void setChargeCategory(final ChargeCategory chargeCategory) {
		this.chargeCategory = chargeCategory;
	}

	public Boolean getIsActiveCase() {
		return isActiveCase;
	}

	public void setIsActiveCase(final Boolean isActiveCase) {
		this.isActiveCase = isActiveCase;
	}

	public Boolean getIsDispositionDeffered() {
		return isDispositionDeffered;
	}

	public void setIsDispositionDeffered(final Boolean isDispositionDeffered) {
		this.isDispositionDeffered = isDispositionDeffered;
	}

	public int getAgeOfRecord() {
		return ageOfRecord;
	}

	public void setAgeOfRecord(final int ageOfRecord) {
		this.ageOfRecord = ageOfRecord;
	}

	public int getAgeofSentenceProbation() {
		return ageofSentenceProbation;
	}

	public void setAgeofSentenceProbation(int ageofSentenceProbation) {
		this.ageofSentenceProbation = ageofSentenceProbation;
	}

	public Boolean getIsProbationActive() {
		return isProbationActive;
	}

	public void setIsProbationActive(Boolean isProbationActive) {
		this.isProbationActive = isProbationActive;
	}

	public float getAgeOfSentence() {
		return ageOfSentence;
	}

	public void setAgeOfSentence(final int ageOfSentence) {
		this.ageOfSentence = ageOfSentence;
	}

	public SalaryRange getSalaryRange() {
		return salaryRange;
	}

	public void setSalaryRange(final SalaryRange salaryRange) {
		this.salaryRange = salaryRange;
	}

	public ChargeLevel getSanitizedChargeLevel() {
		return sanitizedChargeLevel;
	}

	public void setSanitizedChargeLevel(final ChargeLevel sanitizedChargeLevel) {
		this.sanitizedChargeLevel = sanitizedChargeLevel;
	}

	public DispositionType getSanitizedDispositionType() {
		return sanitizedDispositionType;
	}

	public void setSanitizedDispositionType(final DispositionType sanitizedDispositionType) {
		this.sanitizedDispositionType = sanitizedDispositionType;
	}

	public ChargeDescription getSaniztizedChargeDescription() {
		return saniztizedChargeDescription;
	}

	public void setSaniztizedChargeDescription(final ChargeDescription saniztizedChargeDescription) {
		this.saniztizedChargeDescription = saniztizedChargeDescription;
	}

	public CrimCase getCrimCase() {
		return crimCase;
	}

	public void setCrimCase(final CrimCase crimCase) {
		this.crimCase = crimCase;
	}

	public Boolean getIsScoreComplete() {
		return isScoreComplete;
	}

	public void setIsScoreComplete(final Boolean isScoreComplete) {
		this.isScoreComplete = isScoreComplete;
	}

	public RuleDecisionEnum getRuleDecision() {
		return ruleDecision;
	}

	public void setRuleDecision(final RuleDecisionEnum ruleDecision) {
		this.ruleDecision = ruleDecision;
	}

	public String getActualDispositionDescription() {
		return actualDispositionDescription;
	}

	public void setActualDispositionDescription(final String actualDispositionDescription) {
		this.actualDispositionDescription = actualDispositionDescription;
	}

	public DispositionDescription getSanitizedDispositionDescription() {
		return sanitizedDispositionDescription;
	}

	public void setSanitizedDispositionDescription(final DispositionDescription sanitizedDispositionDescription) {
		this.sanitizedDispositionDescription = sanitizedDispositionDescription;
	}

	public List<ProductCategoryReportingParam> getMissingGlobalReportingParams() {
		return missingGlobalReportingParams;
	}

	public void setMissingGlobalReportingParams(final List<ProductCategoryReportingParam> missingGlobalReportingParams) {
		this.missingGlobalReportingParams = missingGlobalReportingParams;
	}

	public Set<CrimSentence> getCrimSentences() {
		return crimSentences;
	}

	public void setCrimSentences(final Set<CrimSentence> crimSentences) {
		this.crimSentences = crimSentences;
	}

	public String getSystemIdentifier() {
		return systemIdentifier;
	}

	public void setSystemIdentifier(final String systemIdentifier) {
		this.systemIdentifier = systemIdentifier;
	}

	public ChargeSanitizationLevelEnum getChargeSanitizationLevel() {
		return chargeSanitizationLevel;
	}

	public void setChargeSanitizationLevel(
			ChargeSanitizationLevelEnum chargeSanitizationLevel) {
		this.chargeSanitizationLevel = chargeSanitizationLevel;
	}

	public DispositionSanitizationLevelEnum getDispositionSanitizationLevel() {
		return dispositionSanitizationLevel;
	}

	public void setDispositionSanitizationLevel(
			DispositionSanitizationLevelEnum dispositionSanitizationLevel) {
		this.dispositionSanitizationLevel = dispositionSanitizationLevel;
	}

	public Date getFileDate() {
		return fileDate;
	}

	public void setFileDate(Date fileDate) {
		this.fileDate = fileDate;
	}

	public String getWarrantDate() {
		return warrantDate;
	}

	public void setWarrantDate(String warrantDate) {
		this.warrantDate = warrantDate;
	}

	public Date getJailDate() {
		return jailDate;
	}

	public void setJailDate(Date jailDate) {
		this.jailDate = jailDate;
	}

	public ArrayList<String> getAdditionalItems() {
		return additionalItems;
	}

	public void setAdditionalItems(ArrayList<String> additionalItems) {
		this.additionalItems = additionalItems;
	}
	
	public Set<ChargeRuleHistory> getChargeRuleHistory() {
         return chargeRuleHistory;
    }

    public void setChargeRuleHistory(Set<ChargeRuleHistory> chargeRuleHistory) {
         this.chargeRuleHistory = chargeRuleHistory;
    }
	

	public boolean isSlgMissing() {
		return slgMissing;
	}

	public void setSlgMissing(boolean slgMissing) {
		this.slgMissing = slgMissing;
	}

	
	public boolean isInScope() {
		return isInScope;
	}

	public void setInScope(boolean isInScope) {
		this.isInScope = isInScope;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((crimCase == null) ? 0 : crimCase.hashCode());
		result = prime * result + crimChargeId;
		result = prime
				* result
				+ ((systemIdentifier == null) ? 0 : systemIdentifier.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CrimCharge other = (CrimCharge) obj;
		if (crimCase == null) {
			if (other.crimCase != null)
				return false;
		} else if (!crimCase.equals(other.crimCase))
			return false;
		if (crimChargeId != other.crimChargeId)
			return false;
		if (systemIdentifier == null) {
			if (other.systemIdentifier != null)
				return false;
		} else if (!systemIdentifier.equals(other.systemIdentifier))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CrimCharge [crimChargeId=" + crimChargeId
				+ ", systemIdentifier=" + systemIdentifier
				+ ", isScoreComplete=" + isScoreComplete
				+ ", actualChargeDescription=" + actualChargeDescription
				+ ", actualChargeLevel=" + actualChargeLevel
				+ ", actualDispositionType=" + actualDispositionType
				+ ", statuteCode=" + statuteCode + ", chargeCategory="
				+ chargeCategory + ", isActiveCase=" + isActiveCase
				+ ", isDispositionDeffered=" + isDispositionDeffered
				+ ", ageOfRecord=" + ageOfRecord + ", isProbabtionActive="
				+ isProbationActive + ", ageOfSentence=" + ageOfSentence
				+ ", salaryRange=" + salaryRange + ", sanitizedChargeLevel="
				+ sanitizedChargeLevel + ", sanitizedDispositionType="
				+ sanitizedDispositionType + ", saniztizedChargeDescription="
				+ saniztizedChargeDescription
				+ ", missingGlobalReportingParams="
				+ missingGlobalReportingParams + ", ruleDecision="
				+ ruleDecision + ", actualDispositionDescription="
				+ actualDispositionDescription
				+ ", sanitizedDispositionDescription="
				+ sanitizedDispositionDescription + ", isReportedEarlier="
				+ isReportedEarlier + ", crimSentences=" + crimSentences + "]";
	}

	public Date getNextCourtDate() {
		return nextCourtDate;
	}

	public void setNextCourtDate(Date nextCourtDate) {
		this.nextCourtDate = nextCourtDate;
	}

	public boolean isSentenceEmptyForConvictionCharge() {
		return sentenceEmptyForConvictionCharge;
	}

	public void setSentenceEmptyForConvictionCharge(
			boolean sentenceEmptyForConvictionCharge) {
		this.sentenceEmptyForConvictionCharge = sentenceEmptyForConvictionCharge;
	}
}

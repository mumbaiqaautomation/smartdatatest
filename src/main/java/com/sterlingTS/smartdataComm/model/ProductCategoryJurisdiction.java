package com.sterlingTS.smartdataComm.model;

import java.util.HashSet;
import java.util.Set;

public class ProductCategoryJurisdiction implements java.io.Serializable {

	private int productCategoryJurisdictionId;
	private ProductCategory productCategory;
	private Jurisdiction jurisdiction;
	private Set<ProductCategoryReportingMonitoring> productCategoryReportingMonitorings = new HashSet<ProductCategoryReportingMonitoring>(0);
	private Set<ProductCategoryJurisdictionDatasource> productCategoryJurisdictionDatasources = new HashSet<ProductCategoryJurisdictionDatasource>(0);
	public int getProductCategoryJurisdictionId() {
		return productCategoryJurisdictionId;
	}
	public void setProductCategoryJurisdictionId(int productCategoryJurisdictionId) {
		this.productCategoryJurisdictionId = productCategoryJurisdictionId;
	}
	public ProductCategory getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(ProductCategory productCategory) {
		this.productCategory = productCategory;
	}
	public Jurisdiction getJurisdiction() {
		return jurisdiction;
	}
	public void setJurisdiction(Jurisdiction jurisdiction) {
		this.jurisdiction = jurisdiction;
	}
	public Set<ProductCategoryReportingMonitoring> getProductCategoryReportingMonitorings() {
		return productCategoryReportingMonitorings;
	}
	public void setProductCategoryReportingMonitorings(
			Set<ProductCategoryReportingMonitoring> productCategoryReportingMonitorings) {
		this.productCategoryReportingMonitorings = productCategoryReportingMonitorings;
	}
	public Set<ProductCategoryJurisdictionDatasource> getProductCategoryJurisdictionDatasources() {
		return productCategoryJurisdictionDatasources;
	}
	public void setProductCategoryJurisdictionDatasources(
			Set<ProductCategoryJurisdictionDatasource> productCategoryJurisdictionDatasources) {
		this.productCategoryJurisdictionDatasources = productCategoryJurisdictionDatasources;
	}
}

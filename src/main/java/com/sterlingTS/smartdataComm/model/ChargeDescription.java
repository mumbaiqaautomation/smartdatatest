package com.sterlingTS.smartdataComm.model;

import com.google.gson.annotations.Expose;

public class ChargeDescription {

	@Expose(serialize = false)
	private int chargeDescriptionId;
	private String description;

	/**
	 * @return the chargeDescriptionId
	 */
	public int getChargeDescriptionId() {
		return chargeDescriptionId;
	}

	/**
	 * @param chargeDescriptionId
	 *            the chargeDescriptionId to set
	 */
	public void setChargeDescriptionId(final int chargeDescriptionId) {
		this.chargeDescriptionId = chargeDescriptionId;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ChargeDescription other = (ChargeDescription) obj;
		if (description == null) {
			if (other.description != null) {
				return false;
			}
		} else if (!description.equals(other.description)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ChargeDescription [chargeDescriptionId=" + chargeDescriptionId
				+ ", description=" + description + "]";
	}
}

package com.sterlingTS.smartdataComm.model;

import java.util.HashSet;
import java.util.Set;

public class SmartDataComponent implements java.io.Serializable {

	private int smartDataComponentId;
	private String component;
	private String description;
	private Set reportProcessingNoteses = new HashSet(0);

	public SmartDataComponent() {
	}

	public SmartDataComponent(int smartDataComponentId) {
		this.smartDataComponentId = smartDataComponentId;
	}

	public SmartDataComponent(int smartDataComponentId, String component,
			String description, Set reportProcessingNoteses) {
		this.smartDataComponentId = smartDataComponentId;
		this.component = component;
		this.description = description;
		this.reportProcessingNoteses = reportProcessingNoteses;
	}

	public int getSmartDataComponentId() {
		return this.smartDataComponentId;
	}

	public void setSmartDataComponentId(int smartDataComponentId) {
		this.smartDataComponentId = smartDataComponentId;
	}

	public String getComponent() {
		return this.component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set getReportProcessingNoteses() {
		return this.reportProcessingNoteses;
	}

	public void setReportProcessingNoteses(Set reportProcessingNoteses) {
		this.reportProcessingNoteses = reportProcessingNoteses;
	}
}

package com.sterlingTS.smartdataComm.model;

import java.util.HashSet;
import java.util.Set;

public class RuleSet implements java.io.Serializable {

	private int ruleSetId;
	private String name;
	private String description;
	private Set<RuleConfiguration> ruleConfigurations = new HashSet<RuleConfiguration>(0);
	private Set<RuleSetRuleMaster> ruleSetRuleMasters = new HashSet<RuleSetRuleMaster>(0);
	private Set<Jurisdiction> jurisdictions = new HashSet<Jurisdiction>(0);
	public int getRuleSetId() {
		return ruleSetId;
	}
	public void setRuleSetId(int ruleSetId) {
		this.ruleSetId = ruleSetId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Set<RuleConfiguration> getRuleConfigurations() {
		return ruleConfigurations;
	}
	public void setRuleConfigurations(Set<RuleConfiguration> ruleConfigurations) {
		this.ruleConfigurations = ruleConfigurations;
	}
	public Set<RuleSetRuleMaster> getRuleSetRuleMasters() {
		return ruleSetRuleMasters;
	}
	public void setRuleSetRuleMasters(Set<RuleSetRuleMaster> ruleSetRuleMasters) {
		this.ruleSetRuleMasters = ruleSetRuleMasters;
	}
	public Set<Jurisdiction> getJurisdictions() {
		return jurisdictions;
	}
	public void setJurisdictions(Set<Jurisdiction> jurisdictions) {
		this.jurisdictions = jurisdictions;
	}
}

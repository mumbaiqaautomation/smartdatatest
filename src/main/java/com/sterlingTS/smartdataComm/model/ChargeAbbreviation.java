package com.sterlingTS.smartdataComm.model;

import java.util.HashSet;
import java.util.Set;

public class ChargeAbbreviation  implements java.io.Serializable {

	private int chargeAbbreviationId;
	private ChargeDescription chargeDescription;
	private String abbreviation;
	private Set<ChargeMapping> chargeMappings = new HashSet<ChargeMapping>(0);

	public ChargeAbbreviation() {
	}

	
	public int getChargeAbbreviationId() {
		return this.chargeAbbreviationId;
	}

	public void setChargeAbbreviationId(int chargeAbbreviationId) {
		this.chargeAbbreviationId = chargeAbbreviationId;
	}

	public ChargeDescription getChargeDescription() {
		return this.chargeDescription;
	}

	public void setChargeDescription(
			ChargeDescription ChargeDescription) {
		this.chargeDescription = chargeDescription;
	}

	public String getAbbreviation() {
		return this.abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}


	public Set<ChargeMapping> getChargeMappings() {
		return chargeMappings;
	}


	public void setChargeMappings(Set<ChargeMapping> chargeMappings) {
		this.chargeMappings = chargeMappings;
	}
}

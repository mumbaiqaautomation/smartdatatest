package com.sterlingTS.smartdataComm.model;

import com.google.gson.annotations.Expose;

public class Jurisdiction {

	@Expose(serialize = false)
	private int jurisdictionId;
	private JurisdictionType jurisdictionType;
	@Expose(serialize = false)
	private FlattenedJurisdiction flattenedJurisdiction;

	public int getJurisdictionId() {
		return jurisdictionId;
	}

	public void setJurisdictionId(final int jurisdictionId) {
		this.jurisdictionId = jurisdictionId;
	}

	public JurisdictionType getJurisdictionType() {
		return jurisdictionType;
	}

	public void setJurisdictionType(final JurisdictionType jurisdictionType) {
		this.jurisdictionType = jurisdictionType;
	}

	public String getQName() {
		return null;
	}

	public FlattenedJurisdiction getFlattenedJurisdiction() {
		return flattenedJurisdiction;
	}

	public void setFlattenedJurisdiction(final FlattenedJurisdiction flattenedJurisdiction) {
		this.flattenedJurisdiction = flattenedJurisdiction;
	}

	@Override
	public String toString() {
		final String qName = getQName();
		if (qName == null) {
			return getClass().getSimpleName() + "(" + getJurisdictionId() + ":" + getJurisdictionType() + ")";
		}
		return getClass().getSimpleName() + "(" + getJurisdictionId() + ":" + getJurisdictionType() + qName + ")";
	}
}

package com.sterlingTS.smartdataComm.model;

import java.util.Date;

public class ReportProcessingNotes implements java.io.Serializable {

	private int reportProcessingNotesId;
	private FulfillmentRequest fulfillmentRequest;
	private SmartDataComponent smartDataComponent;
	private String note;
	private Date creationDate;

	public ReportProcessingNotes() {
	}

	public ReportProcessingNotes(int reportProcessingNotesId,
			FulfillmentRequest fulfillmentRequest,
			SmartDataComponent smartDataComponent) {
		this.reportProcessingNotesId = reportProcessingNotesId;
		this.fulfillmentRequest = fulfillmentRequest;
		this.smartDataComponent = smartDataComponent;
	}

	public ReportProcessingNotes(int reportProcessingNotesId,
			FulfillmentRequest fulfillmentRequest,
			SmartDataComponent smartDataComponent, String note,
			Date creationDate) {
		this.reportProcessingNotesId = reportProcessingNotesId;
		this.fulfillmentRequest = fulfillmentRequest;
		this.smartDataComponent = smartDataComponent;
		this.note = note;
		this.creationDate = creationDate;
	}

	public int getReportProcessingNotesId() {
		return this.reportProcessingNotesId;
	}

	public void setReportProcessingNotesId(int reportProcessingNotesId) {
		this.reportProcessingNotesId = reportProcessingNotesId;
	}

	public FulfillmentRequest getFulfillmentRequest() {
		return this.fulfillmentRequest;
	}

	public void setFulfillmentRequest(FulfillmentRequest fulfillmentRequest) {
		this.fulfillmentRequest = fulfillmentRequest;
	}

	public SmartDataComponent getSmartDataComponent() {
		return this.smartDataComponent;
	}

	public void setSmartDataComponent(SmartDataComponent smartDataComponent) {
		this.smartDataComponent = smartDataComponent;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
}

package com.sterlingTS.smartdataComm.model;

import java.util.HashSet;
import java.util.Set;

public class FulfillmentType implements java.io.Serializable {

	private int fulfillmentTypeId;
	private String fulfillmentType;
	private Set pdlProductCategoryJurisdictionDatasources = new HashSet(0);

	public FulfillmentType() {
	}

	public FulfillmentType(int fulfillmentTypeId, String fulfillmentType) {
		this.fulfillmentTypeId = fulfillmentTypeId;
		this.fulfillmentType = fulfillmentType;
	}

	public FulfillmentType(int fulfillmentTypeId, String fulfillmentType,
			Set pdlProductCategoryJurisdictionDatasources) {
		this.fulfillmentTypeId = fulfillmentTypeId;
		this.fulfillmentType = fulfillmentType;
		this.pdlProductCategoryJurisdictionDatasources = pdlProductCategoryJurisdictionDatasources;
	}

	public int getFulfillmentTypeId() {
		return this.fulfillmentTypeId;
	}

	public void setFulfillmentTypeId(int fulfillmentTypeId) {
		this.fulfillmentTypeId = fulfillmentTypeId;
	}

	public String getFulfillmentType() {
		return this.fulfillmentType;
	}

	public void setFulfillmentType(String fulfillmentType) {
		this.fulfillmentType = fulfillmentType;
	}

	public Set getPdlProductCategoryJurisdictionDatasources() {
		return this.pdlProductCategoryJurisdictionDatasources;
	}

	public void setPdlProductCategoryJurisdictionDatasources(
			Set pdlProductCategoryJurisdictionDatasources) {
		this.pdlProductCategoryJurisdictionDatasources = pdlProductCategoryJurisdictionDatasources;
	}

}

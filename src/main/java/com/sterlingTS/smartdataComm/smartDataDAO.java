package com.sterlingTS.smartdataComm;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.sterlingTS.utils.commonUtils.database.DataBaseManager;
import com.sterlingTS.utils.commonVariables.Globals;

public class smartDataDAO {
	
	protected static Logger log = Logger.getLogger(smartDataDAO.class);
	
	public static DataBaseManager db = null;
	
	public static Map<String, String> smartdataChargeInfo = new HashMap<String, String>();
	
	public static String getSMARTDATADBConnection()
	{
		try{
		String dbURL = Globals.getEnvPropertyValue("SmartData_dbUrl");
		String dbUserName = Globals.getEnvPropertyValue("SmartData_dbUser");
		String dbPassword = Globals.getEnvPropertyValue("SmartData_dbPwd");
		
		log.info("DB URL is :"+"\t"+dbURL);
		
		db = new DataBaseManager(dbURL,dbUserName,dbPassword);
		}catch(Exception ie)
		{
			log.info("DB Connection not created properly"+ie.toString());
			return Globals.KEYWORD_FAIL;
		}
		
		return Globals.KEYWORD_PASS;
	}
	
	
	public static void smartdataDBClose()
	{
		try{
		db.closeConnection();
		log.info("DB connection Closed Properly");
		}catch(Exception ie){
			log.info("connection not closed Properly "+ie.toString());
		}
	}
	
	public static Map<String,String> getSmartDataChargeData()
	{
		String stateName = Globals.testSuiteXLS.getCellData_fromTestData("JurisState");
		String[] chargeDescriptionArray = Globals.testSuiteXLS.getCellData_fromTestData("Charge").split("#AddCharges#");
		String[] dispositionDescriptionArray = Globals.testSuiteXLS.getCellData_fromTestData("Disposition").split("#AddDisposition#");
		//		String dbquery = "select top 1 fulfillment.Status,fulfillment.ResultStatus, fulfillment.Error_Code FROM Screening,fulfillment where Screening.screeningid = fulfillment.screeningid and RequestOrderNumber = '"+prismOrderId+"' order by fulfillment.Fulfillment_Data_ID desc";
		
		try{
			if(!smartdataChargeInfo.isEmpty())
			{
				smartdataChargeInfo.clear();
			}
			
			String dbGetSateIdQuery = "select Jurisdiction_ID from vw_flattened_jurisdiction where state = '"+stateName+"' and Type = 'state'";
			Object[][] stateIDArray = db.executeSelectQuery(dbGetSateIdQuery);
			String stateId = null;
			if(stateIDArray.length>=1)
			{
				stateId = stateIDArray[0][0].toString();
			}
			
			for(int i=0;i<chargeDescriptionArray.length;i++)
			{
				String dbGetChargeInfoQuery = "select cm.Charge_Category_id,cm.Charge_Level_id,cm.Jurisdiction_ID,cm.Statute_Code from SLG_Charge_Mapping cm,SLG_Charge_Description cd where cm.Charge_Description_Id = cd.Charge_Description_Id and cd.Description='"+chargeDescriptionArray[i]+"' and Jurisdiction_ID = '"+stateId+"'";
				
				Object[][] smartdataChargeResult = db.executeSelectQuery(dbGetChargeInfoQuery);
				
				String chargeCategoryId = null;
				String chargeLevelId = null;
				String jurisdictionId = null;
				String statuteCode = null;
				
				if(smartdataChargeResult.length == 0)
				{
					dbGetChargeInfoQuery = "select cm.Charge_Category_id,cm.Charge_Level_id,cm.Jurisdiction_ID,cm.Statute_Code from SLG_Charge_Mapping cm,SLG_Charge_Description cd where cm.Charge_Description_Id = cd.Charge_Description_Id and cd.Description='"+chargeDescriptionArray[i]+"' and Jurisdiction_ID = '1'";
					
					smartdataChargeResult = db.executeSelectQuery(dbGetChargeInfoQuery);
					
					if(smartdataChargeResult.length>0)
					{
						log.info("SQL Query got executed successfully ...");
						log.debug("Results are :  "+"|"+smartdataChargeResult[0][0]+"|"+smartdataChargeResult[0][1]+"|"+smartdataChargeResult[0][2]+"|"+smartdataChargeResult[0][3]);
						log.info("Results are :  "+"|"+smartdataChargeResult[0][0]+"|"+smartdataChargeResult[0][1]+"|"+smartdataChargeResult[0][2]+"|"+smartdataChargeResult[0][3]);
						
						chargeCategoryId = smartdataChargeResult[0][0].toString();
						chargeLevelId = smartdataChargeResult[0][1].toString();
						jurisdictionId = smartdataChargeResult[0][2].toString();
						statuteCode = smartdataChargeResult[0][3].toString();
						
						String dbGetChargeCategoryQuery = "select Description from charge_Category where Charge_Category_id ='"+chargeCategoryId+"'";
						
						Object[][] smartdataChargeCategory = db.executeSelectQuery(dbGetChargeCategoryQuery);
						
						if(smartdataChargeCategory.length>0)
						{
							smartdataChargeInfo.put("chargeCategory_1_"+(i+1), smartdataChargeCategory[0][0].toString());
						}
						
						String dbGetChargeLevelQuery = "select Description from charge_level where Charge_Level_id='"+chargeLevelId+"'";
						
						Object[][] smartdataChargeLevel = db.executeSelectQuery(dbGetChargeLevelQuery);
						
						if(smartdataChargeLevel.length>0)
						{
							smartdataChargeInfo.put("sanitizedChargeLevel_1_"+(i+1), smartdataChargeLevel[0][0].toString());
						}
					}
					
				}if(smartdataChargeResult.length>0)
				{
					log.info("SQL Query got executed successfully ...");
					log.debug("Results are :  "+"|"+smartdataChargeResult[0][0]+"|"+smartdataChargeResult[0][1]+"|"+smartdataChargeResult[0][2]+"|"+smartdataChargeResult[0][3]);
					log.info("Results are :  "+"|"+smartdataChargeResult[0][0]+"|"+smartdataChargeResult[0][1]+"|"+smartdataChargeResult[0][2]+"|"+smartdataChargeResult[0][3]);
					
					chargeCategoryId = smartdataChargeResult[0][0].toString();
					chargeLevelId = smartdataChargeResult[0][1].toString();
					jurisdictionId = smartdataChargeResult[0][2].toString();
					statuteCode = smartdataChargeResult[0][3].toString();
					
					String dbGetChargeCategoryQuery = "select Description from charge_Category where Charge_Category_id ='"+chargeCategoryId+"'";
					
					Object[][] smartdataChargeCategory = db.executeSelectQuery(dbGetChargeCategoryQuery);
					
					if(smartdataChargeCategory.length>0)
					{
						smartdataChargeInfo.put("chargeCategory_1_"+(i+1), smartdataChargeCategory[0][0].toString());
					}
					
					String dbGetChargeLevelQuery = "select Description from charge_level where Charge_Level_id='"+chargeLevelId+"'";
					
					Object[][] smartdataChargeLevel = db.executeSelectQuery(dbGetChargeLevelQuery);
					
					if(smartdataChargeLevel.length>0)
					{
						smartdataChargeInfo.put("sanitizedChargeLevel_1_"+(i+1), smartdataChargeLevel[0][0].toString());
					}
				}
				
				String dbDispositionIDQuery = "select dm.Disposition_Type_id from SLG_Disposition_Mapping dm, SLG_Disposition_Description dd where dd.Disposition_Description_id = dm.Disposition_Description_id and dm.Jurisdiction_ID='"+stateId+"' and dd.Description = '"+dispositionDescriptionArray[i]+"'";
				
				Object[][] smartdataDispositionId = db.executeSelectQuery(dbDispositionIDQuery);
				
				if(smartdataDispositionId.length == 0)
				{
					dbDispositionIDQuery = "select dm.Disposition_Type_id from SLG_Disposition_Mapping dm, SLG_Disposition_Description dd where dd.Disposition_Description_id = dm.Disposition_Description_id and dm.Jurisdiction_ID='"+stateId+"' and dd.Description = '"+dispositionDescriptionArray[i]+"'";
					
					smartdataDispositionId = db.executeSelectQuery(dbDispositionIDQuery);
					
					if(smartdataDispositionId.length > 0)
					{
						String dispositionId = smartdataDispositionId[0][0].toString();
						
						if(dispositionId !=null && !dispositionId.isEmpty())
						{
							String dbDispositionTypeQuery = "select Description from Disposition_Type where Disposition_Type_id='"+dispositionId+"'";
							
							Object[][] smartdataDispositionType = db.executeSelectQuery(dbDispositionTypeQuery);
							
							if(smartdataDispositionType.length>0)
							{
								smartdataChargeInfo.put("sanitizedDispositionType_1_"+(i+1), smartdataDispositionType[0][0].toString());
							}
						}
					}
				}else if(smartdataDispositionId.length > 0)
				{
					String dispositionId = smartdataDispositionId[0][0].toString();
					
					if(dispositionId !=null && !dispositionId.isEmpty())
					{
						String dbDispositionTypeQuery = "select Description from Disposition_Type where Disposition_Type_id='"+dispositionId+"'";
						
						Object[][] smartdataDispositionType = db.executeSelectQuery(dbDispositionTypeQuery);
						
						if(smartdataDispositionType.length>0)
						{
							smartdataChargeInfo.put("sanitizedDispositionType_1_"+(i+1), smartdataDispositionType[0][0].toString());
						}
					}
				}
			}
			
		}catch(Exception ie)
		{
			ie.printStackTrace();
			db.closeConnection();
			log.error("SQL Query not successfully Executed ...");
			return null;
		}
		
		
		return smartdataChargeInfo;
	}
	

}
